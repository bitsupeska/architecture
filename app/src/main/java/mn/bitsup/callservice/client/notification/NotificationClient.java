package mn.bitsup.callservice.client.notification;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.notification.data.NotificationData;
import mn.bitsup.callservice.client.notification.data.NotificationDataImp;
import mn.bitsup.callservice.client.notification.dto.Notification;
import mn.bitsup.callservice.client.notification.listener.NotificationListener;
import mn.bitsup.callservice.client.notification.listener.NotificationsListener;

public class NotificationClient {
    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private NotificationData notificationData;

    public NotificationClient(URI baseUri) {
        this((DBSDataProvider) null, baseUri, new DefaultNotificationParser());
    }

    public NotificationClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultNotificationParser());
    }

    public NotificationClient(DBSDataProvider dbsDataProvider, URI baseUri, NotificationParser notificationParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.notificationData = new NotificationDataImp(notificationParser);
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getUnreadCount(Map<String, String> params, final StatusListener statusListener) {
        try {
            URI countURI = new URI(this.baseUri + "/be-internal-notification-service/v1.0/unreadCount");
            this.notificationData.getUnreadCount(countURI, this.dbsDataProvider, params, new StatusListener() {
                @Override
                public void onSuccess(StatusResponse var1) {
                    statusListener.onSuccess(var1);
                }

                @Override
                public void onError(Response var2) {
                    statusListener.onError(var2);
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public void notifications(Map<String, String> params, final NotificationsListener notificationsListener) {
        try {
            URI notificationURI = new URI(this.baseUri + "/be-internal-notification-service/v1.0/allNotifications");
            this.notificationData.getNotification(notificationURI, this.dbsDataProvider, params, new NotificationsListener() {

                @Override
                public void onSuccess(List<Notification> var1) {
                    notificationsListener.onSuccess(var1);
                }

                @Override
                public void onError(Response var1) {
                    notificationsListener.onError(var1);

                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    public void notificationDetail(Map<String, String> params, final NotificationListener notificationListener) {
        try {
            URI notificationURI = new URI(this.baseUri + "/be-internal-notification-service/v1.0/getNotificationDetail");
            this.notificationData.getNotificationDetail(notificationURI, this.dbsDataProvider, params, new NotificationListener() {

                @Override
                public void onSuccess(Notification var1) {
                    notificationListener.onSuccess(var1);
                }

                @Override
                public void onError(Response var1) {
                    notificationListener.onError(var1);

                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void notificationArchive(Map<String, String> params, final StatusListener statusListener) {
        this.notificationData.archiveNotification(this.baseUri, this.dbsDataProvider, params, new StatusListener() {

            @Override
            public void onSuccess(StatusResponse var1) {
                statusListener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                statusListener.onError(var1);

            }
        });
    }
}
