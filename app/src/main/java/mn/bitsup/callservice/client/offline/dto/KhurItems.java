package mn.bitsup.callservice.client.offline.dto;

public class KhurItems {

    private int code;
    private String status;
    private String message;

     String addressApartmentName;
     String addressDetail;
     String addressRegionName;
     String addressStreetName;
     String aimagCityCode;
     String aimagCityName;
     String bagKhorooCode;
     String bagKhorooName;
     String birthDate;
     String birthdateastext;
     String birthPlace;
     String civilId;
     String firstName;
     int age;
     String gender;
     String lastName;
     String nationality;
     String passportAddress;
     String passportExpireDate;
     String passportIssueDate;
     String regNum;
     String soumDistrictCode;
     String soumDistrictName;
     String surName;
     int isPension;
     String downloadDate;
     int avgSalary;
     int siPayment;
     int hasSiPayment;
     int conSiPayMon;
     String spouse;
     int nonSalary;
     String compRegNum;
     int voluntarySoInPayment;


    public KhurItems() {
    }

    public String getAddressApartmentName() {
        return addressApartmentName;
    }

    public void setAddressApartmentName(String addressApartmentName) {
        this.addressApartmentName = addressApartmentName;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getAddressRegionName() {
        return addressRegionName;
    }

    public void setAddressRegionName(String addressRegionName) {
        this.addressRegionName = addressRegionName;
    }

    public String getAimagCityCode() {
        return aimagCityCode;
    }

    public void setAimagCityCode(String aimagCityCode) {
        this.aimagCityCode = aimagCityCode;
    }

    public String getAimagCityName() {
        return aimagCityName;
    }

    public void setAimagCityName(String aimagCityName) {
        this.aimagCityName = aimagCityName;
    }

    public String getBagKhorooCode() {
        return bagKhorooCode;
    }

    public void setBagKhorooCode(String bagKhorooCode) {
        this.bagKhorooCode = bagKhorooCode;
    }

    public String getBagKhorooName() {
        return bagKhorooName;
    }

    public void setBagKhorooName(String bagKhorooName) {
        this.bagKhorooName = bagKhorooName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthdateastext() {
        return birthdateastext;
    }

    public void setBirthdateastext(String birthdateastext) {
        this.birthdateastext = birthdateastext;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getCivilId() {
        return civilId;
    }

    public void setCivilId(String civilId) {
        this.civilId = civilId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPassportAddress() {
        return passportAddress;
    }

    public void setPassportAddress(String passportAddress) {
        this.passportAddress = passportAddress;
    }

    public String getPassportExpireDate() {
        return passportExpireDate;
    }

    public void setPassportExpireDate(String passportExpireDate) {
        this.passportExpireDate = passportExpireDate;
    }

    public String getPassportIssueDate() {
        return passportIssueDate;
    }

    public void setPassportIssueDate(String passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }


    public String getSoumDistrictCode() {
        return soumDistrictCode;
    }

    public void setSoumDistrictCode(String soumDistrictCode) {
        this.soumDistrictCode = soumDistrictCode;
    }

    public String getSoumDistrictName() {
        return soumDistrictName;
    }

    public void setSoumDistrictName(String soumDistrictName) {
        this.soumDistrictName = soumDistrictName;
    }

    public int getIsPension() {
        return isPension;
    }

    public void setIsPension(int isPension) {
        this.isPension = isPension;
    }

    public String getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(String downloadDate) {
        this.downloadDate = downloadDate;
    }

    public int getAvgSalary() {
        return avgSalary;
    }

    public void setAvgSalary(int avgSalary) {
        this.avgSalary = avgSalary;
    }

    public int getSiPayment() {
        return siPayment;
    }

    public void setSiPayment(int siPayment) {
        this.siPayment = siPayment;
    }

    public int getHasSiPayment() {
        return hasSiPayment;
    }

    public void setHasSiPayment(int hasSiPayment) {
        this.hasSiPayment = hasSiPayment;
    }

    public int getConSiPayMon() {
        return conSiPayMon;
    }

    public void setConSiPayMon(int conSiPayMon) {
        this.conSiPayMon = conSiPayMon;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public int getNonSalary() {
        return nonSalary;
    }

    public void setNonSalary(int nonSalary) {
        this.nonSalary = nonSalary;
    }

    public String getCompRegNum() {
        return compRegNum;
    }

    public void setCompRegNum(String compRegNum) {
        this.compRegNum = compRegNum;
    }

    public int getVoluntarySoInPayment() {
        return voluntarySoInPayment;
    }

    public void setVoluntarySoInPayment(int voluntarySoInPayment) {
        this.voluntarySoInPayment = voluntarySoInPayment;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getAddressStreetName() {
        return addressStreetName;
    }

    public void setAddressStreetName(String addressStreetName) {
        this.addressStreetName = addressStreetName;
    }

    @Override
    public String toString() {
        return "KhurItems{" +
                "code=" + code +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", addressApartmentName='" + addressApartmentName + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", addressRegionName='" + addressRegionName + '\'' +
                ", addressStreetName='" + addressStreetName + '\'' +
                ", aimagCityCode='" + aimagCityCode + '\'' +
                ", aimagCityName='" + aimagCityName + '\'' +
                ", bagKhorooCode='" + bagKhorooCode + '\'' +
                ", bagKhorooName='" + bagKhorooName + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", birthdateastext='" + birthdateastext + '\'' +
                ", birthPlace='" + birthPlace + '\'' +
                ", civilId='" + civilId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", lastName='" + lastName + '\'' +
                ", nationality='" + nationality + '\'' +
                ", passportAddress='" + passportAddress + '\'' +
                ", passportExpireDate='" + passportExpireDate + '\'' +
                ", passportIssueDate='" + passportIssueDate + '\'' +
                ", regNum='" + regNum + '\'' +
                ", soumDistrictCode='" + soumDistrictCode + '\'' +
                ", soumDistrictName='" + soumDistrictName + '\'' +
                ", surName='" + surName + '\'' +
                ", isPension=" + isPension +
                ", downloadDate='" + downloadDate + '\'' +
                ", avgSalary=" + avgSalary +
                ", siPayment=" + siPayment +
                ", hasSiPayment=" + hasSiPayment +
                ", conSiPayMon=" + conSiPayMon +
                ", spouse='" + spouse + '\'' +
                ", nonSalary=" + nonSalary +
                ", compRegNum='" + compRegNum + '\'' +
                ", voluntarySoInPayment=" + voluntarySoInPayment +
                '}';
    }
}
