package mn.bitsup.callservice.client.profile.listener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.Questions;

public interface QuestionListener {
    void onSuccess(List<Questions> var1);

    void onError(Response var1);
}
