package mn.bitsup.callservice.client.loan.dto;

public class CoreFee {
    private String amt_ind;
    private String amt_pcnt;
    private String fixed_amt;

    public String getAmt_ind() {
        return amt_ind;
    }

    public void setAmt_ind(String amt_ind) {
        this.amt_ind = amt_ind;
    }

    public String getAmt_pcnt() {
        return amt_pcnt;
    }

    public void setAmt_pcnt(String amt_pcnt) {
        this.amt_pcnt = amt_pcnt;
    }

    public String getFixed_amt() {
        return fixed_amt;
    }

    public void setFixed_amt(String fixed_amt) {
        this.fixed_amt = fixed_amt;
    }
}
