package mn.bitsup.callservice.client.notification.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.notification.dto.Notification;

public interface NotificationListener {
    void onSuccess(Notification var1);

    void onError(Response var1);
}

