package mn.bitsup.callservice.client.card.parser;

import android.util.Log;
import androidx.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import mn.bitsup.callservice.client.card.dto.Card;
import mn.bitsup.callservice.client.card.dto.CardDetail;
import mn.bitsup.callservice.client.card.dto.CardItem;
import mn.bitsup.callservice.client.card.dto.CardReasonItem;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import org.json.JSONException;
import org.json.JSONObject;

public class DefaultCardParser extends DefaultCardManagementDataParser implements CardParser {
    private final Gson a;
    public DefaultCardParser(@NonNull Gson var1) {
        super(var1);
        this.a = a();
    }

    public DefaultCardParser() {
        super();
        this.a = new Gson();
    }
    @Override
    public List<Card> a(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("data").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
        Type listType = (new TypeToken<List<CardItem>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
        }catch (Exception ex){
            Log.e("DefaultCardParserError", "catch: "+ex.getMessage() );
        }
        return null;
    }
    @Override
    public List<CardReasonItem> parseCardReasonItem(String json){
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("data").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type statusType = (new TypeToken<List<CardReasonItem>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, statusType);
    }

    @Override
    public StatusResponse statusParse(String json) {
        Type listType = (new TypeToken<StatusResponse>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(json, listType);
    }

    @Override
    public CardDetail cardDetailParse(String json) {
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                json = obj.getJSONObject("data").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type cardDetailType = (new TypeToken<CardDetail>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return gson.fromJson(json, cardDetailType);
    }
}
