package mn.bitsup.callservice.client.offline.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.Block;

public interface BlockListener {
    void onSuccess(Block var1);
    void onError(Response var1);
}
