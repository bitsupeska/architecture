package mn.bitsup.callservice.client.otp.data;

import android.util.Log;
import androidx.annotation.NonNull;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.otp.OTPListener;

public class OTPDataImpl implements OTPData {
    private static final String TAG = "OTPDataImpl";


    @Override
    public void getOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final OTPListener otpListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/otpSend"), new OTPDataImpl.GetOTPDBSListener(otpListener));
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            otpListener.onError(errorResponse);
        }
    }

    @Override
    public void checkOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final OTPListener otpListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/otpCheck"), new OTPDataImpl.GetOTPDBSListener(otpListener));
        } catch ( URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            otpListener.onError(errorResponse);
        }
    }

    @Override
    public void getEmailOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final OTPListener otpListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/otpEmailSend"), new OTPDataImpl.GetOTPDBSListener(otpListener));
        } catch ( URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            otpListener.onError(errorResponse);
        }
    }

    @Override
    public void checkEmailOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final OTPListener otpListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/otpEmailCheck"), new OTPDataImpl.GetOTPDBSListener(otpListener));
        } catch ( URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            otpListener.onError(errorResponse);
        }
    }

    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        Log.e("asd", "get access token: "+storageComponent.getItem(AuthClient.ACCESS_TOKEN));

        headers.put(AuthClient.AUTH_HEADER, "Bearer "+ storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }

    private class GetOTPDBSListener implements DBSDataProviderListener {
        final OTPListener otpListener;
        GetOTPDBSListener(OTPListener otpListener) {
            this.otpListener = otpListener;
        }
        public void onSuccess(Response response) {
            int responseCode = response.getResponseCode();
            if (responseCode >= 200 && responseCode < 400) {
                StatusResponse otp = parseOTP(response.getStringResponse());
                if (otp == null) {
                    Response errorResponsex = new Response();
                    errorResponsex.setErrorMessage("CANNOT PARSE RESPONSE BODY");
                    errorResponsex.setResponseCode(500);
                    this.otpListener.onError(errorResponsex);
                }else{
                    this.otpListener.onSuccess(otp);
                }

            } else {
                Response errorResponse = new Response();
                errorResponse.setErrorMessage(response.getStringResponse());
                errorResponse.setResponseCode(responseCode);
                this.otpListener.onError(errorResponse);
            }
        }
        public void onError(Response errorResponse) {
            this.otpListener.onError(errorResponse);
        }
    }

    public StatusResponse parseOTP(String json) {
        Type itemsMapType = new TypeToken<StatusResponse>() {}.getType();
        StatusResponse otp = new Gson().fromJson(json, itemsMapType);
        return  otp;
    }
}
