package mn.bitsup.callservice.client.info.listener;

import java.util.List;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.Location;

public interface LocationListener {
    void onSuccess(List<Location> var1);
    void onError(Response var1);
}
