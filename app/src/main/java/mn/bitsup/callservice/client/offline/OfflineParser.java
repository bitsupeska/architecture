package mn.bitsup.callservice.client.offline;


import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.CheckUsernameItems;
import mn.bitsup.callservice.client.offline.dto.ResetInfo;

public interface OfflineParser {
    String parseConvertToken(String var1);
    StatusResponse parseStatus(String var1);
    CheckUsernameItems parseCheckUserName(String var1);
    Block parseCheckBlock(String var1);
    BlockIncrement parseBlockIncrement(String var1);
    ResetInfo parseResetInfo(String var1);
}
