package mn.bitsup.callservice.client.info.listener;

import java.util.List;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.DeliveryState;

public interface DeliveryStateListener {
    void onSuccess(List<DeliveryState> var1);
    void onError(Response var1);
}
