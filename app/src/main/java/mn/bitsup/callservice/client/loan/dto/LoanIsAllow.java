package mn.bitsup.callservice.client.loan.dto;

public class LoanIsAllow {
    String code;
    String status;
    String message;
    boolean loanAllowed;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isLoanAllowed() {
        return loanAllowed;
    }

    public void setLoanAllowed(boolean loanAllowed) {
        this.loanAllowed = loanAllowed;
    }
}
