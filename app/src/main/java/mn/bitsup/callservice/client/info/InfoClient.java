package mn.bitsup.callservice.client.info;

import android.util.Log;

import java.net.URI;
import java.util.List;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.info.data.InfoData;
import mn.bitsup.callservice.client.info.data.InfoDataImpl;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.dto.CurrencyItem;
import mn.bitsup.callservice.client.info.dto.DeliveryDate;
import mn.bitsup.callservice.client.info.dto.DeliveryState;
import mn.bitsup.callservice.client.info.dto.Item;
import mn.bitsup.callservice.client.info.listener.BackgroundImageListener;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.info.listener.CurrencyListener;
import mn.bitsup.callservice.client.info.listener.DeliveryDateListener;
import mn.bitsup.callservice.client.info.listener.DeliveryStateListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;

public class InfoClient implements DBSClient {

    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private InfoData infoData;
    private List<BankItem> banksCached;
    private List<CurrencyItem> currencyItemCached;


    public InfoClient(URI baseUri) {
        this((DBSDataProvider)null, baseUri, new DefaultInfoParser());
    }

    public InfoClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultInfoParser());
    }

    public InfoClient(DBSDataProvider dbsDataProvider, URI baseUri, InfoParser infoParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.infoData = new InfoDataImpl(infoParser);
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getCityData(LocationListener listener) {
        this.infoData.getCityData(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getDistrictData(LocationListener listener, String code) {
        this.infoData.getDistrictData(this.baseUri, this.dbsDataProvider, listener, code);
    }

    public void getBank(final BankListener bankListener, Map<String, String> params) {
//        if (this.banksCached != null) {
//            Log.e("banksCached", "size: "+banksCached.size());
//
//            bankListener.onSuccess(this.banksCached);
//        } else {
            this.infoData.getBank(this.baseUri,  this.dbsDataProvider, params, new BankListener() {
                public void onSuccess(List<BankItem> bankItems) {
//                    if (InfoClient.this.banksCached == null) {
//                        InfoClient.this.banksCached = bankItems;
//                    } else {
//                        InfoClient.this.banksCached.addAll(bankItems);
//                    }
                    bankListener.onSuccess(bankItems);
                }
                public void onError(Response errorResponse) {
                    Log.e("InfoClient", "getBanks error: "+errorResponse.getStringResponse());
                    bankListener.onError(errorResponse);
                }
            });
//        }
    }

    public void getCurrency(final CurrencyListener currencyListener) {
        if ( this.currencyItemCached != null) {
            currencyListener.onSuccess(this.currencyItemCached);
        } else {
            this.infoData.getCurrency(this.baseUri, this.dbsDataProvider, new CurrencyListener() {
                public void onSuccess(List<CurrencyItem> currencies) {
                    if (InfoClient.this.currencyItemCached == null) {
                        InfoClient.this.currencyItemCached = currencies;
                    } else {
                        InfoClient.this.currencyItemCached.addAll(currencies);
                    }
                    currencyListener.onSuccess(currencies);
                }

                public void onError(Response errorResponse) {
                    Log.e("InfoClient", "getCurrency errorResponse: "+errorResponse.getStringResponse());
                    currencyListener.onError(errorResponse);

                }
            });
        }
    }

    public void getStateDelivery( DeliveryStateListener deliveryStateListener) {
            this.infoData.getStateDelivery(this.baseUri, this.dbsDataProvider, new DeliveryStateListener() {
                public void onSuccess(List<DeliveryState> deliveryStates) {
                    deliveryStateListener.onSuccess(deliveryStates);
                }

                public void onError(Response errorResponse) {
                    Log.e("InfoClient", "getStates errorResponse: "+errorResponse.getStringResponse());
                    deliveryStateListener.onError(errorResponse);

                }
            });
    }

    public void getDistrictDelivery( DeliveryStateListener deliveryStateListener,String code) {
        this.infoData.getDistrictDelivery(this.baseUri, this.dbsDataProvider, new DeliveryStateListener() {
            public void onSuccess(List<DeliveryState> deliveryStates) {
                deliveryStateListener.onSuccess(deliveryStates);
            }

            public void onError(Response errorResponse) {
                Log.e("InfoClient", "getStates errorResponse: "+errorResponse.getStringResponse());
                deliveryStateListener.onError(errorResponse);

            }
        },code);
    }

    public void getDeliveryDate(DeliveryDateListener deliveryDateListener, String dayCount) {
        this.infoData.getDeliveryDate(this.baseUri, this.dbsDataProvider, new DeliveryDateListener() {
            public void onSuccess(List<DeliveryDate> deliveryDateList) {
                deliveryDateListener.onSuccess(deliveryDateList);
            }

            public void onError(Response errorResponse) {
                Log.e("InfoClient", "getStates errorResponse: "+errorResponse.getStringResponse());
                deliveryDateListener.onError(errorResponse);

            }
        },dayCount);
    }

    public void getCheckCitizenInfo(Map<String, String> params,  final CitizenInfoListener listener) {
        this.infoData.getCheckCitizenInfo(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getBackgroundImage(BackgroundImageListener listener) {
        this.infoData.getBackgroundImage(this.baseUri, this.dbsDataProvider, new BackgroundImageListener() {
            public void onSuccess(Item item) {
                listener.onSuccess(item);
            }

            public void onError(Response errorResponse) {
                Log.e("InfoClient", "getStates errorResponse: "+errorResponse.getStringResponse());
                listener.onError(errorResponse);

            }
        });
    }
}
