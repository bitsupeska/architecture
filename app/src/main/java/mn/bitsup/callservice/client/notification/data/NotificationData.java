package mn.bitsup.callservice.client.notification.data;

import java.net.URI;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.notification.listener.NotificationListener;
import mn.bitsup.callservice.client.notification.listener.NotificationsListener;

public interface NotificationData {
    void getUnreadCount(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener statusListener);
    void getNotification(URI baseUri, DBSDataProvider dataProvider,Map<String, String> queryParams, NotificationsListener notificationsListener);
    void getNotificationDetail(URI baseUri, DBSDataProvider dataProvider,Map<String, String> queryParams, NotificationListener notificationListener);
    void archiveNotification(URI baseUri, DBSDataProvider dataProvider,Map<String, String> queryParams, StatusListener statusListener);
}
