package mn.bitsup.callservice.client.dataprovider.listener;

public interface PasscodeAuthClient {
    void setPasscode(char[] var1, PasscodeAuthListener var2);

    void authenticate(char[] var1, PasscodeAuthListener var2);

    boolean isEnrolled();
}
