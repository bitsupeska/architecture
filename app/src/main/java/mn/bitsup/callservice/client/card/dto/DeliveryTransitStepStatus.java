package mn.bitsup.callservice.client.card.dto;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public enum DeliveryTransitStepStatus {
    SUCCESS("SUCCESS"),
    FAILED("FAILED"),
    PENDING("PENDING");

    @NonNull
    private String statusName;

    private DeliveryTransitStepStatus(@NonNull String var3) {
        this.statusName = var3;
    }

    @Nullable
    public static DeliveryTransitStepStatus from(@Nullable String var0) {
        if (var0 == null) {
            return null;
        } else {
            byte var1 = -1;
            int var2;
            if ((var2 = var0.hashCode()) != -1149187101) {
                if (var2 != 35394935) {
                    if (var2 == 2066319421 && var0.equals("FAILED")) {
                        var1 = 1;
                    }
                } else if (var0.equals("PENDING")) {
                    var1 = 2;
                }
            } else if (var0.equals("SUCCESS")) {
                var1 = 0;
            }

            if (var1 != 0) {
                if (var1 != 1) {
                    return var1 != 2 ? null : PENDING;
                } else {
                    return FAILED;
                }
            } else {
                return SUCCESS;
            }
        }
    }

    @NonNull
    public String getStatusName() {
        return this.statusName;
    }
}

