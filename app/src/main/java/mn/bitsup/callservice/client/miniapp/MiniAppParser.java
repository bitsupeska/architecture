package mn.bitsup.callservice.client.miniapp;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;

public interface MiniAppParser {

    StatusResponse parseStatus(String var1);
    List parseApps(String var1);

}
