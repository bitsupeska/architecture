package mn.bitsup.callservice.client.call.data;

import java.net.URI;
import java.util.Map;

import mn.bitsup.callservice.client.call.listener.CallHistoryListener;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;

public interface CallHistoryData {

    void getHistories(URI var1, DBSDataProvider var2, Map<String, String> var3,
                       CallHistoryListener var4);
}
