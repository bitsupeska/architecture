package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanAccount;

public interface LoanAccountListener {
    void onSuccess(LoanAccount var1);
    void onError(Response var1);
}
