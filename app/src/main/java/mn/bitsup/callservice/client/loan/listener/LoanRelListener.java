package mn.bitsup.callservice.client.loan.listener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;

public interface LoanRelListener {
    void onSuccess(List<TemporaryItems> var1);
    void onError(Response var1);
}
