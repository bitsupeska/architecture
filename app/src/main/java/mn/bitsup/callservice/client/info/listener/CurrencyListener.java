package mn.bitsup.callservice.client.info.listener;

import java.util.List;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.CurrencyItem;

public interface CurrencyListener {
    void onSuccess(List<CurrencyItem> var1);
    void onError(Response var1);
}
