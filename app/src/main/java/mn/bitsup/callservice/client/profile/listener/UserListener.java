package mn.bitsup.callservice.client.profile.listener;

        import mn.bitsup.callservice.client.dataprovider.response.Response;
        import mn.bitsup.callservice.client.profile.dto.User;

public interface UserListener {
    void onSuccess(User var1);

    void onError(Response var1);
}
