package mn.bitsup.callservice.client.loan.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoanOffer {
    int code;
    String status;
    String message;
    Boolean isOfferExpired;
    Boolean notOffer;
    Boolean hasPendingLoan;

    @SerializedName("data")
    @Expose
    private LoanOfferData data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getOfferExpired() {
        return isOfferExpired;
    }

    public void setOfferExpired(Boolean offerExpired) {
        isOfferExpired = offerExpired;
    }

    public Boolean getNotOffer() {
        return notOffer;
    }

    public void setNotOffer(Boolean notOffer) {
        this.notOffer = notOffer;
    }

    public LoanOfferData getData() {
        return data;
    }

    public void setData(LoanOfferData data) {
        this.data = data;
    }

    public Boolean getHasPendingLoan() {
        return hasPendingLoan;
    }

    public void setHasPendingLoan(Boolean hasPendingLoan) {
        this.hasPendingLoan = hasPendingLoan;
    }

    @Override
    public String toString() {
        return "LoanOffer{" +
            "code=" + code +
            ", status='" + status + '\'' +
            ", message='" + message + '\'' +
            ", isOfferExpired=" + isOfferExpired +
            ", notOffer=" + notOffer +
            ", hasPendingLoan=" + hasPendingLoan +
            ", data=" + data +
            '}';
    }
}
