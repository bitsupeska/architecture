package mn.bitsup.callservice.client.miniapp;
import java.net.URI;
import java.util.HashMap;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.miniapp.data.MiniAppData;
import mn.bitsup.callservice.client.miniapp.data.MiniAppDataImpl;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;


public class MiniAppClient implements DBSClient {

    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private MiniAppData miniAppData;

    public MiniAppClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultMiniAppParser());
    }

    public MiniAppClient(DBSDataProvider dbsDataProvider, URI baseUri, MiniAppParser miniAppParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.miniAppData = new MiniAppDataImpl(miniAppParser) {
        };
    }

    @Override
    public void setBaseURI(URI uri) {
        this.baseUri = uri;

    }

    @Override
    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    @Override
    public URI getBaseURI() {
        return this.baseUri;
    }

    @Override
    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getAllAppList(final MiniAppListener listener, String param) {
        this.miniAppData.getAllAppList(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void getMyAppList(final MiniAppListener listener,  String param) {
        this.miniAppData.getMyAppList(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void installApp(final StatusListener listener,  HashMap<String, String> param) {
        this.miniAppData.installApp(this.baseUri, this.dbsDataProvider, param, listener);
    }
}
