package mn.bitsup.callservice.client.profile.dto;

import java.util.List;

public class User {

    private String code;
    private String status;
    private String message;
    private String uid;
    private String cif;
    private String macAddress;
    private String dmsUser;
    private String regNum;
    private String familyName;
    private String firstName;
    private String lastName;
    private String didSign;
    private String phoneNo;
    private String handPhone;
    private String gender;
    private String birthDt;
    private String familyNum;
    private String docExpDt;
    private String docIssueDt;
    private String mobileDuration;
    private String isPep;
    private String chooseDanOrKhur;
    private String businessOperation;
    private String companyName;
    private String incomeSource;
    private String incomeSourceDesc;
    private String employmentStatus;
    private String premiumStatus;
    private String didVerifyPhone;
    private String occupation;
    private List<RelatedContacts> relatedContacts;
    private List<mn.bitsup.callservice.client.profile.dto.Addresses> addresses;
    private List<mn.bitsup.callservice.client.profile.dto.Emails> emails;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getDmsUser() {
        return dmsUser;
    }

    public void setDmsUser(String dmsUser) {
        this.dmsUser = dmsUser;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDidSign() {
        return didSign;
    }

    public void setDidSign(String didSign) {
        this.didSign = didSign;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getHandPhone() {
        return handPhone;
    }

    public void setHandPhone(String handPhone) {
        this.handPhone = handPhone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDt() {
        return birthDt;
    }

    public void setBirthDt(String birthDt) {
        this.birthDt = birthDt;
    }

    public String getFamilyNum() {
        return familyNum;
    }

    public void setFamilyNum(String familyNum) {
        this.familyNum = familyNum;
    }

    public String getDocExpDt() {
        return docExpDt;
    }

    public void setDocExpDt(String docExpDt) {
        this.docExpDt = docExpDt;
    }

    public String getDocIssueDt() {
        return docIssueDt;
    }

    public void setDocIssueDt(String docIssueDt) {
        this.docIssueDt = docIssueDt;
    }

    public String getMobileDuration() {
        return mobileDuration;
    }

    public void setMobileDuration(String mobileDuration) {
        this.mobileDuration = mobileDuration;
    }

    public String getIsPep() {
        return isPep;
    }

    public void setIsPep(String isPep) {
        this.isPep = isPep;
    }

    public String getChooseDanOrKhur() {
        return chooseDanOrKhur;
    }

    public void setChooseDanOrKhur(String chooseDanOrKhur) {
        this.chooseDanOrKhur = chooseDanOrKhur;
    }

    public String getBusinessOperation() {
        return businessOperation;
    }

    public void setBusinessOperation(String businessOperation) {
        this.businessOperation = businessOperation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIncomeSource() {
        return incomeSource;
    }

    public void setIncomeSource(String incomeSource) {
        this.incomeSource = incomeSource;
    }

    public String getIncomeSourceDesc() {
        return incomeSourceDesc;
    }

    public void setIncomeSourceDesc(String incomeSourceDesc) {
        this.incomeSourceDesc = incomeSourceDesc;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getPremiumStatus() {
        return premiumStatus;
    }

    public void setPremiumStatus(String premiumStatus) {
        this.premiumStatus = premiumStatus;
    }

    public String getDidVerifyPhone() {
        return didVerifyPhone;
    }

    public void setDidVerifyPhone(String didVerifyPhone) {
        this.didVerifyPhone = didVerifyPhone;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    public List<Emails> getEmails() {
        return emails;
    }

    public void setEmails(List<Emails> emails) {
        this.emails = emails;
    }

    public List<RelatedContacts> getRelatedContacts() {
        return relatedContacts;
    }

    public void setRelatedContacts(List<RelatedContacts> relatedContacts) {
        this.relatedContacts = relatedContacts;
    }
}