package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import java.util.Objects;
import mn.bitsup.callservice.view.widget.card.dto.LockStatus;

public class Card extends Additions {
    public static final Creator<Card> CREATOR = new Creator<Card>() {
        public Card createFromParcel(Parcel var1) {
            return new Card(var1);
        }

        public Card[] newArray(int var1) {
            return new Card[var1];
        }
    };
    @SerializedName("id")
    private String id;
    @SerializedName("brand")
    private String brand;
    @SerializedName("type")
    private String type;
    @SerializedName("subType")
    private String subType;
    @SerializedName("name")
    private String name;
    @SerializedName("status")
    private String status;
    @SerializedName("replacement")
    private Replacement replacement;
    @SerializedName("delivery")
    private Delivery delivery;
    @SerializedName("lockStatus")
    private String lockStatus;
    @SerializedName("holder")
    private CardHolder cardHolder;
    @SerializedName("expiryDate")
    private ExpiryDate expiryDate;
    @SerializedName("currency")
    private String currency;
    @SerializedName("maskedNumber")
    private String maskedNumber;
    @SerializedName("limits")
    private List<Limit> limits;

    @Deprecated
    public Card(@NonNull String var1, @NonNull String var2, @NonNull String var3, @Nullable String var4,@Nullable String var7,  @Nullable String var10, @NonNull String var11, @NonNull String var12) {
        this(var1, var2, var3, var11, var12);
        this.currency = var10;
        this.subType = var4;
        this.lockStatus = var7;
    }
//
    @Deprecated
    public Card(@NonNull String var1, @NonNull String var2, @NonNull String var3, @Nullable String var4, @Nullable Replacement var5, @Nullable Delivery var6, @Nullable String var7, @Nullable CardHolder var8, @Nullable ExpiryDate var9, @Nullable String var10, @NonNull String var11, @NonNull String var12) {
        this(var1, var2, var3, var11, var12);
        this.delivery = var6;
        this.replacement = var5;
        this.cardHolder = var8;
        this.expiryDate = var9;
        this.currency = var10;
        this.subType = var4;
        this.lockStatus = var7;
    }

    public Card(@NonNull String var1, @NonNull String var2, @NonNull String var3, @NonNull String var4, @NonNull String var5) {
        this.id = var1;
        this.brand = var2;
        this.type = var3;
        this.status = var5;
        this.maskedNumber = var4;
    }
//
//    @Deprecated
//    public Card(String var1, String var2, String var3, String var4, String var5, CardHolder var6, ExpiryDate var7, String var8, String var9) {
//        this.id = var1;
//        this.brand = var2;
//        this.type = var3;
//        this.status = var4;
//        this.replacement = new Replacement(var5);
//        this.cardHolder = var6;
//        this.expiryDate = var7;
//        this.currency = var8;
//        this.maskedNumber = var9;
//    }

    @Deprecated
    public Card(String var1, String var2, String var3, String var4, String var5, CardHolder var6, ExpiryDate var7, String var8, String var9) {
        this.id = var1;
        this.brand = var2;
        this.type = var3;
        this.status = var4;
        this.currency = var8;
        this.maskedNumber = var9;
    }

    protected Card(Parcel var1) {
        super(var1);
        this.id = var1.readString();
        this.brand = var1.readString();
        this.type = var1.readString();
        this.subType = var1.readString();
        this.name = var1.readString();
        this.status = var1.readString();
        this.replacement = (Replacement)var1.readParcelable(Replacement.class.getClassLoader());
        this.delivery = (Delivery)var1.readParcelable(Delivery.class.getClassLoader());
        this.lockStatus = var1.readString();
        this.cardHolder = (CardHolder)var1.readParcelable(CardHolder.class.getClassLoader());
        this.expiryDate = (ExpiryDate)var1.readParcelable(ExpiryDate.class.getClassLoader());
        this.currency = var1.readString();
        this.maskedNumber = var1.readString();
        this.limits = var1.createTypedArrayList(Limit.CREATOR);
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeString(this.id);
        var1.writeString(this.brand);
        var1.writeString(this.type);
        var1.writeString(this.subType);
        var1.writeString(this.name);
        var1.writeString(this.status);
        var1.writeParcelable(this.replacement, var2);
        var1.writeParcelable(this.delivery, var2);
        var1.writeString(this.lockStatus);
        var1.writeParcelable(this.cardHolder, var2);
        var1.writeParcelable(this.expiryDate, var2);
        var1.writeString(this.currency);
        var1.writeString(this.maskedNumber);
        var1.writeTypedList(this.limits);
    }

    public int describeContents() {
        return 0;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String var1) {
        this.id = var1;
    }

    public String getBrand() {
        return this.brand;
    }

    public void setBrand(String var1) {
        this.brand = var1;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String var1) {
        this.type = var1;
    }

    public String getSubType() {
        return this.subType;
    }

    public void setSubType(String var1) {
        this.subType = var1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String var1) {
        this.status = var1;
    }

    public Replacement getReplacement() {
        return this.replacement;
    }

    public void setReplacement(Replacement var1) {
        this.replacement = var1;
    }

    public Delivery getDelivery() {
        return this.delivery;
    }

    public void setDelivery(Delivery var1) {
        this.delivery = var1;
    }

    public LockStatus getLockStatus() {
        return this.lockStatus.equals(LockStatus.LOCKED.getLockStatusAsStr()) ? LockStatus.LOCKED : LockStatus.UNLOCKED;
    }

    public void setLockStatus(LockStatus var1) {
        LockStatus var2;
        if (var1 == (var2 = LockStatus.LOCKED)) {
            this.lockStatus = var2.getLockStatusAsStr();
        } else if (var1 == (var1 = LockStatus.UNLOCKED)) {
            this.lockStatus = var1.getLockStatusAsStr();
        }

    }

    public CardHolder getCardHolder() {
        return this.cardHolder;
    }

    public void setCardHolder(CardHolder var1) {
        this.cardHolder = var1;
    }

    public ExpiryDate getExpiryDate() {
        return this.expiryDate;
    }

    public void setExpiryDate(ExpiryDate var1) {
        this.expiryDate = var1;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String var1) {
        this.currency = var1;
    }

    public String getMaskedNumber() {
        return this.maskedNumber;
    }

    public void setMaskedNumber(String var1) {
        this.maskedNumber = var1;
    }

    public List<Limit> getLimits() {
        return this.limits;
    }

    public void setLimits(List<Limit> var1) {
        this.limits = var1;
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 != null && Card.class == var1.getClass()) {
            if (!super.equals(var1)) {
                return false;
            } else {
                Card var2 = (Card)var1;
                return Objects.equals(this.getId(), var2.getId()) && Objects.equals(this.getBrand(), var2.getBrand()) && Objects.equals(this.getType(), var2.getType()) && Objects.equals(this.getSubType(), var2.getSubType()) && Objects.equals(this.getName(), var2.getName()) && Objects.equals(this.getStatus(), var2.getStatus()) && Objects.equals(this.getReplacement(), var2.getReplacement()) && Objects.equals(this.getDelivery(), var2.getDelivery()) && Objects.equals(this.lockStatus, var2.lockStatus) && Objects.equals(this.getCardHolder(), var2.getCardHolder()) && Objects.equals(this.getExpiryDate(), var2.getExpiryDate()) && Objects.equals(this.getCurrency(), var2.getCurrency()) && Objects.equals(this.getMaskedNumber(), var2.getMaskedNumber()) && Objects.equals(this.getLimits(), var2.getLimits());
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        Object[] var1;
        Object[] var10000 = var1 = new Object[14];
        var1[0] = super.hashCode();
        var1[1] = this.getId();
        var1[2] = this.getBrand();
        var1[3] = this.getType();
        var1[4] = this.getSubType();
        var1[5] = this.getName();
        var1[6] = this.getStatus();
        var1[7] = this.getReplacement();
        var1[8] = this.lockStatus;
        var1[9] = this.getCardHolder();
        var1[10] = this.getExpiryDate();
        var1[11] = this.getCurrency();
        var1[12] = this.getMaskedNumber();
        var10000[13] = this.getLimits();
        return Objects.hash(var10000);
    }
}
