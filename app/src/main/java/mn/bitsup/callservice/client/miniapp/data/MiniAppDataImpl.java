package mn.bitsup.callservice.client.miniapp.data;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.miniapp.MiniAppParser;
import mn.bitsup.callservice.client.miniapp.dto.MiniApp;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;

public class MiniAppDataImpl implements MiniAppData {

    private MiniAppParser miniAppParser;

    protected MiniAppDataImpl(MiniAppParser miniAppParser) {
        this.miniAppParser = miniAppParser;
    }

    @Override
    public void getAllAppList(URI baseUri, DBSDataProvider dataProvider, String param, MiniAppListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-appstore-service/v1.0/all?uid=" + param), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<MiniApp> apps = miniAppParser.parseApps(response.getStringResponse());
                        if (apps == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(apps);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        }
        catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getMyAppList(URI baseUri, DBSDataProvider dataProvider, String param, MiniAppListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-appstore-service/v1.0/userApps?uid=" + param), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<MiniApp> apps = miniAppParser.parseApps(response.getStringResponse());
                        if (apps == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(apps);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        }
        catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void installApp(URI baseUri, DBSDataProvider dataProvider, Map<String, String> param, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, param,"/be-internal-appstore-service/v1.0/install"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = miniAppParser.parseStatus(response.getStringResponse());
                        if (statusResponse == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        }
        catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    private Request buildRequest(URI baseUri, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(
                AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
}
