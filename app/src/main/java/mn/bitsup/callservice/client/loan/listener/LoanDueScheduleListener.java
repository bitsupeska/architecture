package mn.bitsup.callservice.client.loan.listener;

import java.util.List;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;

public interface LoanDueScheduleListener {
    void onSuccess(List<LoanDueScheduleData> var1);
    void onError(Response var1);
}
