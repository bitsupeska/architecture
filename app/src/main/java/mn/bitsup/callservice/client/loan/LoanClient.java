package mn.bitsup.callservice.client.loan;

import java.net.URI;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.loan.data.LoanData;
import mn.bitsup.callservice.client.loan.data.LoanDataImpl;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.client.loan.listener.LoanCreateInvoiceListener;
import mn.bitsup.callservice.client.loan.listener.LoanIsAllowedListener;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelCreateListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelListener;
import mn.bitsup.callservice.client.loan.listener.LoanSalaryListener;
import mn.bitsup.callservice.client.loan.listener.SysDateListener;

public class LoanClient implements DBSClient {

    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private LoanData loanData;

    public LoanClient(URI baseUri) {
        this((DBSDataProvider) null, baseUri, new DefaultLoanParser());
    }

    public LoanClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultLoanParser());
    }

    public LoanClient(DBSDataProvider dbsDataProvider, URI baseUri, LoanParser loanParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.loanData = new LoanDataImpl(loanParser);
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getLastOffer(Map<String, String> params, LoanOfferListener listener) {
        this.loanData.getLastOffer(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getRelStatLookup(LoanRelListener loanRelListener) {
        this.loanData.getRelStatLookup(this.baseUri, this.dbsDataProvider, loanRelListener);
    }

    public void addRelStatLookup(Map<String, String> params, LoanRelCreateListener loanRelCreateListener) {
        this.loanData.createRelated(this.baseUri, this.dbsDataProvider, params, loanRelCreateListener);
    }
    public void getSalary(Map<String, String> params, LoanSalaryListener listener) {
        this.loanData.getSalary(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getOffer(Map<String, String> params, LoanOfferListener listener) {
        this.loanData.getOffer(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getLoanAccount(Map<String, String> params, LoanAccountListener listener) {
        this.loanData.getLoanAccount(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void createLoan(Loan params, StatusListener listener) {
        this.loanData.createLoan(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getLoanDueSchedule(Map<String, String> params, LoanDueScheduleListener listener) {
        this.loanData.getDueSchedule(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getLoanFindAmortSchedule(String param, LoanDueScheduleListener listener) {
        this.loanData.getLoanFindAmortSchedule(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void createLoanInvoice(Map<String, String> params, LoanCreateInvoiceListener listener) {
        this.loanData.createInvoice(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void getSysDate(SysDateListener listener) {
        this.loanData.getSysDate(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getLoanAllowed(LoanIsAllowedListener listener,Map<String, String> params) {
        this.loanData.getLoanAllowed(this.baseUri, this.dbsDataProvider, params, listener);
    }

}
