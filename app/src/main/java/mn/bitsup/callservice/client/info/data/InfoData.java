package mn.bitsup.callservice.client.info.data;

import java.net.URI;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.info.listener.BackgroundImageListener;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.info.listener.CurrencyListener;
import mn.bitsup.callservice.client.info.listener.DeliveryDateListener;
import mn.bitsup.callservice.client.info.listener.DeliveryStateListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;

public interface InfoData {
    void getCityData(URI baseUri, DBSDataProvider dataProvider,
        final LocationListener locationListener);
    void getDistrictData(URI baseUri, DBSDataProvider dataProvider,
        final LocationListener locationListener, String stateCode);

    void getBank(URI var1, DBSDataProvider var2, Map<String, String> var3, BankListener var4);
    void getCurrency(URI var1, DBSDataProvider var2, CurrencyListener var4);
    void getStateDelivery(URI var1, DBSDataProvider var2, DeliveryStateListener var4);
    void getDistrictDelivery(URI var1, DBSDataProvider var2, DeliveryStateListener var4,
        String cityCode);
    void getDeliveryDate(URI var1, DBSDataProvider var2, DeliveryDateListener var4, String cityCode);

    void getCheckCitizenInfo(URI var1, DBSDataProvider var2, Map<String, String> var3,
        CitizenInfoListener var4);

    void getBackgroundImage(URI var1, DBSDataProvider var2, BackgroundImageListener var4);

}
