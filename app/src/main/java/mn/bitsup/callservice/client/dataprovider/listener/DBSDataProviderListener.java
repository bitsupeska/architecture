package mn.bitsup.callservice.client.dataprovider.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface DBSDataProviderListener {
    void onSuccess(Response var1);

    void onError(Response var1);
}
