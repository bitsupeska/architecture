package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.math.BigDecimal;
import java.util.Objects;

public class Limit extends Additions {
    public static final Creator<Limit> CREATOR = new Creator<Limit>() {
        public Limit createFromParcel(Parcel var1) {
            return new Limit(var1);
        }

        public Limit[] newArray(int var1) {
            return new Limit[var1];
        }
    };
    @SerializedName("id")
    @NonNull
    private String id;
    @SerializedName("channel")
    @Nullable
    private String channel;
    @SerializedName("frequency")
    @Nullable
    private String frequency;
    @SerializedName("amount")
    @NonNull
    private BigDecimal amount;
    @SerializedName("maxAmount")
    @NonNull
    private BigDecimal maxAmount;
    @SerializedName("minAmount")
    @Nullable
    private BigDecimal minAmount;

    public Limit(@NonNull String var1, @NonNull BigDecimal var2, @NonNull BigDecimal var3) {
        this.id = var1;
        this.amount = var2;
        this.maxAmount = var3;
    }

    protected Limit(Parcel var1) {
        super(var1);
        this.id = (String) Objects.requireNonNull(var1.readString());
        this.channel = var1.readString();
        this.frequency = var1.readString();
        this.amount = (BigDecimal)Objects.requireNonNull(var1.readSerializable());
        this.maxAmount = (BigDecimal)Objects.requireNonNull(var1.readSerializable());
        this.minAmount = (BigDecimal)var1.readSerializable();
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeString(this.id);
        var1.writeString(this.channel);
        var1.writeString(this.frequency);
        var1.writeSerializable(this.amount);
        var1.writeSerializable(this.maxAmount);
        var1.writeSerializable(this.minAmount);
    }

    @NonNull
    public String getId() {
        return this.id;
    }

    public void setId(@NonNull String var1) {
        this.id = var1;
    }

    @Nullable
    public String getChannel() {
        return this.channel;
    }

    public void setChannel(@Nullable String var1) {
        this.channel = var1;
    }

    @Nullable
    public String getFrequency() {
        return this.frequency;
    }

    public void setFrequency(@Nullable String var1) {
        this.frequency = var1;
    }

    @NonNull
    public BigDecimal getAmount() {
        return this.amount;
    }

    public void setAmount(@NonNull BigDecimal var1) {
        this.amount = var1;
    }

    @NonNull
    public BigDecimal getMaxAmount() {
        return this.maxAmount;
    }

    public void setMaxAmount(@NonNull BigDecimal var1) {
        this.maxAmount = var1;
    }

    @Nullable
    public BigDecimal getMinAmount() {
        return this.minAmount;
    }

    public void setMinAmount(@Nullable BigDecimal var1) {
        this.minAmount = var1;
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 != null && Limit.class == var1.getClass()) {
            if (!super.equals(var1)) {
                return false;
            } else {
                Limit var2 = (Limit)var1;
                return this.getId().equals(var2.getId()) && Objects.equals(this.getChannel(), var2.getChannel()) && Objects.equals(this.getFrequency(), var2.getFrequency()) && this.getAmount().equals(var2.getAmount()) && this.getMaxAmount().equals(var2.getMaxAmount()) && Objects.equals(this.getMinAmount(), var2.getMinAmount());
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        Object[] var1;
        Object[] var10000 = var1 = new Object[7];
        var1[0] = super.hashCode();
        var1[1] = this.getId();
        var1[2] = this.getChannel();
        var1[3] = this.getFrequency();
        var1[4] = this.getAmount();
        var1[5] = this.getMaxAmount();
        var10000[6] = this.getMinAmount();
        return Objects.hash(var10000);
    }
}
