package mn.bitsup.callservice.client.temporary.dto;

public class TemporaryItems {

    String value;
    String localeText;
    String localetext;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLocaleText() {
        return localeText;
    }

    public void setLocaleText(String localeText) {
        this.localeText = localeText;
    }

    public String getLocaletext() {
        return localetext;
    }

    public void setLocaletext(String localetext) {
        this.localetext = localetext;
    }
}
