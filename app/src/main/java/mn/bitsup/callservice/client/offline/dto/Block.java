package mn.bitsup.callservice.client.offline.dto;

import java.util.List;

public class Block {

    private String code;
    private String status;
    private String message;
    private List<BlockData> data;

    public Block() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BlockData> getData() {
        return data;
    }

    public void setData(List<BlockData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Block{" +
            "code='" + code + '\'' +
            ", status='" + status + '\'' +
            ", message='" + message + '\'' +
            ", data=" + data +
            '}';
    }
}
