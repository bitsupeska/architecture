package mn.bitsup.callservice.client.call.parser;

import java.util.List;

import mn.bitsup.callservice.client.call.dto.CallHistory;

public interface CallHistoryParser {
    List<CallHistory> parseHistories(String json);
}
