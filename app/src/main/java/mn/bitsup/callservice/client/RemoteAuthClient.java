package mn.bitsup.callservice.client;

import static mn.bitsup.callservice.view.activity.BaseActivity.context;
import static mn.bitsup.callservice.view.activity.EnrollmentActivity.INTENT_ANOTHER_DEVICE_LOGOUT;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.apptakk.http_request.HttpRequest;
import com.apptakk.http_request.HttpRequestTask;
import com.google.gson.Gson;

import java.util.HashMap;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthClient;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.listener.PasswordAuthListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.dataprovider.util.ResponseCodes;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.util.crypto.CryptoUtil;
import mn.bitsup.callservice.util.crypto.LoginCredentialsStorage;

import java.net.HttpURLConnection;
import java.util.Map;

import mn.bitsup.callservice.view.activity.EnrollmentActivity;

import org.json.JSONException;
import org.json.JSONObject;

public class RemoteAuthClient implements PasscodeAuthClient {
    public static final String KEY_PASSCODE_HASH = "KEY_PASSCODE_HASH";
    private static final String ERR_NOT_ENROLLED = "User is not enrolled";
    private static final String ERR_SERVER = "SERVER ERROR";
    private String ENDPOINT_ROOT;
    private final LoginCredentialsStorage credentialsStorage;
    private StorageComponent storageComponent;
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json";

    public RemoteAuthClient(final Context context) {
        storageComponent = new StorageComponent();
        credentialsStorage = new LoginCredentialsStorage(context);
        ENDPOINT_ROOT = MainApplication.getServerURL() + "/auth/re{\n" +
                "        storageComponent = new StorageComponent();\n" +
                "        credentialsStorage = new LoginCredentialsStorage(context);\n" +
                "        ENDPOINT_ROOT = MainApplication.getServerURL() + \"/auth/refresh\";\n" +
                "    }fresh";
    }

    public void authenticate(final char[] userId,
                             final char[] password,
                             final Map<String, String> headers,
                             final Map<String, String> bodyParameters,
                             final PasswordAuthListener passwordAuthListener,
                             final String... authTokenNames) {
    }

    public void authenticate(final char[] chars, final PasscodeAuthListener passcodeAuthListener) {
        final String providedPinHash = CryptoUtil.hashOfCharArray(chars);
        final String storedPinHash = storageComponent.getItem(KEY_PASSCODE_HASH);
        final boolean pinCodeMatches = providedPinHash != null && providedPinHash.equals(storedPinHash);

        if (!isEnrolled()) {
            Log.e("refreshed", "isEnrolled false");
            final Response notEnrolledResponse = new Response();
            notEnrolledResponse.setResponse(ERR_NOT_ENROLLED.getBytes());
            notEnrolledResponse.setResponseCode(HttpURLConnection.HTTP_UNAUTHORIZED);
            passcodeAuthListener.onError(notEnrolledResponse);
        } else if (pinCodeMatches) {
            performAutomatedLogin(passcodeAuthListener);
        } else {
            Log.e("refreshed", "error");
            final Response serverErrorResponse = new Response();
            serverErrorResponse.setResponse(ERR_SERVER.getBytes());
            serverErrorResponse.setResponseCode(ResponseCodes.ERROR.getCode());
            passcodeAuthListener.onError(serverErrorResponse);
        }
    }

    public void endSession() {
        storageComponent.setItem(KEY_PASSCODE_HASH, null);
        credentialsStorage.clearEncryptedCredentials();
    }

    @Override
    public void setPasscode(char[] chars, PasscodeAuthListener passcodeAuthListener) {
        storageComponent.setItem(KEY_PASSCODE_HASH, CryptoUtil.hashOfCharArray(chars));
        passcodeAuthListener.onSuccess();
    }


    @Override
    public boolean isEnrolled() {
        return storageComponent.getItem(KEY_PASSCODE_HASH) != null && credentialsStorage.hasEncryptedCredentials();
    }

    private void refreshData(JSONObject obj) throws JSONException {
        if (UserInteractionTimers.isRunning)
            UserInteractionTimers.restart();
        else {
            UserInteractionTimers.startTimer();
        }
        UserInteractionTimers.isLogged = true;
        context.stopService(new Intent(context, RefreshService.class));
        context.startService(new Intent(context, RefreshService.class));
        RefreshService.isRunning = true;

        StorageComponent storageComponent = new StorageComponent();
        storageComponent.setItem(AuthClient.ACCESS_TOKEN, obj.getString(AuthClient.ACCESS_TOKEN));
        storageComponent.setItem(AuthClient.EXPIRES_IN, obj.getString(AuthClient.EXPIRES_IN));
        storageComponent.setItem(AuthClient.TOKEN_TYPE, obj.getString(AuthClient.TOKEN_TYPE));
        Log.e("refreshed", "saved");
    }

    public String getUid() {
        return storageComponent.getItem("uid");
    }

    public void setUser(User user) {
        if (user != null) {
            Gson gson = new Gson();
            String jsonString = gson.toJson(user);
            storageComponent.setItem("user", jsonString);
        }
    }

    public User getUser() {
        Gson gson = new Gson();
        User user = gson.fromJson(storageComponent.getItem("user"), User.class);
        return user;
    }

    public String getExpiresIn() {
        return storageComponent.getItem(AuthClient.EXPIRES_IN);
    }

    public void performAutomatedLogin(final PasscodeAuthListener passcodeAuthListener) {
        new HttpRequestTask(
                new HttpRequest(ENDPOINT_ROOT, HttpRequest.GET, null, null, getRefreshHeaders()),
                response -> {
                    Log.e("performAutomatedLogin", "token: " + response.body);
                    if (response.code == 200) {
                        try {
                            refreshData(new JSONObject(response.body));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        passcodeAuthListener.onSuccess();
                    } else if (response.code == 401) {
                        ClientsManager.authClient.endSession();
                        Intent intent = new Intent(context, EnrollmentActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(INTENT_ANOTHER_DEVICE_LOGOUT, true);
                        context.startActivity(intent);
                    } else {
                        passcodeAuthListener.onError(new Response("Алдаа гарлаа", 405));
                    }
                }).execute();
    }

    @NonNull
    private Map<String, String> getRefreshHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(AuthClient.REFRESH_TOKEN));
        headers.put(CONTENT_TYPE, CONTENT_TYPE_VALUE);
        Log.e("refresh", "headers: " + storageComponent.getItem(AuthClient.REFRESH_TOKEN));
        return headers;
    }
}
