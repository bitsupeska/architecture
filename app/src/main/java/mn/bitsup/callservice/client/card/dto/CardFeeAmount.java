package mn.bitsup.callservice.client.card.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class CardFeeAmount extends Additions implements Parcelable {

    private String payment;
    private String account;
    private String name;
    private String amount;
    private String currency;
    private String currencyAmount;

    public static final Parcelable.Creator<CardFeeAmount> CREATOR = new Parcelable.Creator<CardFeeAmount>() {
        public CardFeeAmount createFromParcel(Parcel source) {
            return new CardFeeAmount(source);
        }

        public CardFeeAmount[] newArray(int size) {
            return new CardFeeAmount[size];
        }
    };

    private  CardFeeAmount(Parcel in) {
        this.payment = in.readString();
        this.account = in.readString();
        this.name = in.readString();
        this.amount = in.readString();
        this.currency = in.readString();
        this.currencyAmount = in.readString();
    }

    public void writeToParcel(Parcel dest) {
        dest.writeString(this.payment);
        dest.writeString(this.account);
        dest.writeString(this.name);
        dest.writeString(this.amount);
        dest.writeString(this.currency);
        dest.writeString(this.currencyAmount);
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyAmount() {
        return currencyAmount;
    }

    public void setCurrencyAmount(String currencyAmount) {
        this.currencyAmount = currencyAmount;
    }

    public static Creator<CardFeeAmount> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "CardFeeAmount{" +
                "payment='" + payment + '\'' +
                ", feeaccount='" + account + '\'' +
                ", feename='" + name + '\'' +
                ", feeamount='" + amount + '\'' +
                ", feecurrency='" + currency + '\'' +
                ", feecurrencyamount='" + currencyAmount + '\'' +
                '}';
    }
}