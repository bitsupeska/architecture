package mn.bitsup.callservice.client.card.listener;


import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface CardPinChangeListener {
    void onSuccess(StatusListener var1);
    void onError(Response var1);
}
