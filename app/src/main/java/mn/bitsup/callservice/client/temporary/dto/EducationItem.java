package mn.bitsup.callservice.client.temporary.dto;

public class EducationItem {
    String value;
    String localetext;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLocaletext() {
        return localetext;
    }

    public void setLocaletext(String localetext) {
        this.localetext = localetext;
    }
}
