package mn.bitsup.callservice.client.profile.data;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.ProfileParser;
import mn.bitsup.callservice.client.profile.dto.Questions;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.client.profile.listener.QuestionListener;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public class ProfileDataImpl implements ProfileData {

    private ProfileParser profileParser;

    protected ProfileDataImpl(ProfileParser profileParser) {
        this.profileParser = profileParser;
    }

    @Override
    public void getUser(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, UserListener userListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-ldap-service/v1.0/getUserInfo"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        User user = ProfileDataImpl.this.profileParser.parseUser(response.getStringResponse());
                        if (user == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            userListener.onError(errorResponsex);
                            return;
                        }
                        userListener.onSuccess(user);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        userListener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    Log.e("getUser", "error: " + errorResponse.getResponseCode());
                    userListener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            userListener.onError(errorResponse);
        }
    }

    @Override
    public void setEmail(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkcore-service/v1.0/updateEmail"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        if (statusResponse == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void setNumber(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkcore-service/v1.0/updatePhone"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        if (statusResponse == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void setAddress(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkcore-service/v1.0/updateAddress"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        if (statusResponse == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void matchPassword(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/ldap-service/v1/matchpassword"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        if (statusResponse == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void setPassword(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-ldap-service/v1.0/changePassword"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        if (statusResponse == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getAnswerQuestion(URI baseUri, DBSDataProvider dataProvider, QuestionListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-basic-service/v1.0/getAnswerQuestion"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<Questions> questions = profileParser.parseQuestions(response.getStringResponse());
                        if (questions == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(questions);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getAccountList(URI baseUri, DBSDataProvider dataProvider, String param, AccountListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-basic-service/v1.0/getAccountList?cif=" + param), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<Account> accounts = profileParser.parseAccounts(response.getStringResponse());
                        if (accounts == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(accounts);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void addAccount(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-basic-service/v1.0/addAccount"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void deleteAccount(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-basic-service/v1.0/deleteAccount"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = profileParser.parseStatus(response.getStringResponse());
                        listener.onSuccess(statusResponse);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    private Request buildRequest(URI baseUri, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(
                AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
}
