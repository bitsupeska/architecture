package mn.bitsup.callservice.client.temporary.dto;

public class TemporaryCif {

    String birthDate;
    String didSign;
    String docExpDate;
    String docIssueDate;
    String familyName;
    String firstName;
    String lastName;
    String gender;
    String handPhone;
    String handPhoneStatus;
    String regNum;


    String apartmentHome; //town
    String localityHome; //  city
    String localityHomeName; //  city
    String doorNumberHome; // Building level
    String districtHome; //premise
    String townHome; //state
    String sectionHome; //Locality
    String townHomeName; //state

    String userName;
    String email;
    String emailStatus;

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDidSign() {
        return didSign;
    }

    public void setDidSign(String didSign) {
        this.didSign = didSign;
    }

    public String getDocExpDate() {
        return docExpDate;
    }

    public void setDocExpDate(String docExpDate) {
        this.docExpDate = docExpDate;
    }

    public String getDocIssueDate() {
        return docIssueDate;
    }

    public void setDocIssueDate(String docIssueDate) {
        this.docIssueDate = docIssueDate;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHandPhone() {
        return handPhone;
    }

    public void setHandPhone(String handPhone) {
        this.handPhone = handPhone;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getApartmentHome() {
        return apartmentHome;
    }

    public void setApartmentHome(String apartmentHome) {
        this.apartmentHome = apartmentHome;
    }

    public String getLocalityHome() {
        return localityHome;
    }

    public void setLocalityHome(String localityHome) {
        this.localityHome = localityHome;
    }

    public String getDoorNumberHome() {
        return doorNumberHome;
    }

    public void setDoorNumberHome(String doorNumberHome) {
        this.doorNumberHome = doorNumberHome;
    }

    public String getDistrictHome() {
        return districtHome;
    }

    public void setDistrictHome(String districtHome) {
        this.districtHome = districtHome;
    }

    public String getTownHome() {
        return townHome;
    }

    public void setTownHome(String townHome) {
        this.townHome = townHome;
    }

    public String getSectionHome() {
        return sectionHome;
    }

    public void setSectionHome(String sectionHome) {
        this.sectionHome = sectionHome;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHandPhoneStatus() {
        return handPhoneStatus;
    }

    public void setHandPhoneStatus(String handPhoneStatus) {
        this.handPhoneStatus = handPhoneStatus;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

    public String getLocalityHomeName() {
        return localityHomeName;
    }

    public void setLocalityHomeName(String localityHomeName) {
        this.localityHomeName = localityHomeName;
    }

    public String getTownHomeName() {
        return townHomeName;
    }

    public void setTownHomeName(String townHomeName) {
        this.townHomeName = townHomeName;
    }

    @Override
    public String toString() {
        return "TemporaryCif{" +
            "birthDate='" + birthDate + '\'' +
            ", didSign='" + didSign + '\'' +
            ", docExpDate='" + docExpDate + '\'' +
            ", docIssueDate='" + docIssueDate + '\'' +
            ", familyName='" + familyName + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", gender='" + gender + '\'' +
            ", handPhone='" + handPhone + '\'' +
            ", handPhoneStatus='" + handPhoneStatus + '\'' +
            ", regNum='" + regNum + '\'' +
            ", apartmentHome='" + apartmentHome + '\'' +
            ", localityHome='" + localityHome + '\'' +
            ", localityHomeName='" + localityHomeName + '\'' +
            ", doorNumberHome='" + doorNumberHome + '\'' +
            ", districtHome='" + districtHome + '\'' +
            ", townHome='" + townHome + '\'' +
            ", sectionHome='" + sectionHome + '\'' +
            ", townHomeName='" + townHomeName + '\'' +
            ", userName='" + userName + '\'' +
            ", email='" + email + '\'' +
            ", emailStatus='" + emailStatus + '\'' +
            '}';
    }
}
