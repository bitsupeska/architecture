package mn.bitsup.callservice.client.dataprovider.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface PasscodeAuthListener {
    void onSuccess();

    void onError(Response var1);
}
