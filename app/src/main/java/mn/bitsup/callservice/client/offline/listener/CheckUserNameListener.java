package mn.bitsup.callservice.client.offline.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.CheckUsernameItems;

public interface CheckUserNameListener {
    void onSuccess(CheckUsernameItems var1);
    void onError(Response var1);
}
