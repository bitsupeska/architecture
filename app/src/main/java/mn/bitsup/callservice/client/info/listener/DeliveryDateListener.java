package mn.bitsup.callservice.client.info.listener;

import java.util.List;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.DeliveryDate;

public interface DeliveryDateListener {
    void onSuccess(List<DeliveryDate> var1);
    void onError(Response var1);
}
