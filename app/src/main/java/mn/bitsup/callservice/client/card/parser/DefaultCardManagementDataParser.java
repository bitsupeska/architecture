package mn.bitsup.callservice.client.card.parser;

import androidx.annotation.NonNull;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import mn.bitsup.callservice.client.card.dto.Card;

public class DefaultCardManagementDataParser implements CardManagementDataParser {
    @NonNull
    private final Gson a;

    public DefaultCardManagementDataParser(@NonNull Gson var1) {
        this.a = var1;
    }

    public DefaultCardManagementDataParser() {
        this.a = new Gson();
    }

    public List<Card> a(String var1) {
        DefaultCardManagementDataParser var10000 = this;
        Type var2 = (new TypeToken<List<Card>>() {
        }).getType();
        return (List)var10000.a.fromJson(var1, var2);
    }

    public Card b(String var1) {
        DefaultCardManagementDataParser var10000 = this;
        Type var2 = (new TypeToken<Card>() {
        }).getType();
        return (Card)var10000.a.fromJson(var1, var2);
    }

    @NonNull
    protected Gson a() {
        return this.a;
    }
}
