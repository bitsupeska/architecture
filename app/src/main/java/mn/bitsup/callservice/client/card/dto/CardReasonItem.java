package mn.bitsup.callservice.client.card.dto;

public class CardReasonItem {
    private int reason;
    private String name;

    public CardReasonItem(int reason, String name) {
        this.reason = reason;
        this.name = name;
    }

    public int getReason() {
        return reason;
    }

    public void setReason(int reason) {
        this.reason = reason;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
