package mn.bitsup.callservice.client.miniapp.data;

import java.net.URI;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;

public interface MiniAppData {
    void getAllAppList(URI var1, DBSDataProvider var2, String var3, MiniAppListener var4);

    void getMyAppList(URI var1, DBSDataProvider var2, String var3, MiniAppListener var4);

    void installApp(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);
}
