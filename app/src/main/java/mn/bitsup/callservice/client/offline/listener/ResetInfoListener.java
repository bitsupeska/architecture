package mn.bitsup.callservice.client.offline.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;

import mn.bitsup.callservice.client.offline.dto.ResetInfo;

public interface ResetInfoListener {
    void onSuccess(ResetInfo var1);
    void onError(Response var1);
}
