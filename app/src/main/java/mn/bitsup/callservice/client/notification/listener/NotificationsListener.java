package mn.bitsup.callservice.client.notification.listener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.notification.dto.Notification;

public interface NotificationsListener {
    void onSuccess(List<Notification> var1);

    void onError(Response var1);
}

