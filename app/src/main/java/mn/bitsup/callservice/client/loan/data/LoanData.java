package mn.bitsup.callservice.client.loan.data;


import java.net.URI;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.client.loan.listener.LoanCreateInvoiceListener;
import mn.bitsup.callservice.client.loan.listener.LoanIsAllowedListener;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelCreateListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelListener;
import mn.bitsup.callservice.client.loan.listener.LoanSalaryListener;
import mn.bitsup.callservice.client.loan.listener.SysDateListener;

public interface LoanData {
    void getLastOffer(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanOfferListener var4);

    void getRelStatLookup(URI var1, DBSDataProvider var2, LoanRelListener loanRelListener);

    void createRelated(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanRelCreateListener var4);

    void getSalary(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanSalaryListener var4);

    void getOffer(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanOfferListener var4);

    void getLoanAccount(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanAccountListener var4);

    void createLoan(URI var1, DBSDataProvider var2, Loan var3, StatusListener var4);

    void getDueSchedule(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanDueScheduleListener var4);

    void createInvoice(URI var1, DBSDataProvider var2, Map<String, String> var3, LoanCreateInvoiceListener var4);

    void getLoanFindAmortSchedule(URI var1, DBSDataProvider var2, String var3, LoanDueScheduleListener var4);

    void getSysDate(URI var1, DBSDataProvider var2, SysDateListener var4);

    void getLoanAllowed(URI baseUri, DBSDataProvider dbsDataProvider, Map<String, String> params, LoanIsAllowedListener listener);
}
