package mn.bitsup.callservice.client.dataprovider.util;

import java.net.URI;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;

public interface DBSClient {
    void setBaseURI(URI var1);

    void setDataProvider(DBSDataProvider var1);

    URI getBaseURI();

    DBSDataProvider getDBSDataProvider();
}
