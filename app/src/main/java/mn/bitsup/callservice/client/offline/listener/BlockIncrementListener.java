package mn.bitsup.callservice.client.offline.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;

public interface BlockIncrementListener {
    void onSuccess(BlockIncrement var1);
    void onError(Response var1);
}
