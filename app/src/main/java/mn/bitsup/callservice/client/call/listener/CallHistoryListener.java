package mn.bitsup.callservice.client.call.listener;

import java.util.List;

import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface CallHistoryListener {
    void onSuccess(List<CallHistory> historyList);
    void onError(Response var1);
}
