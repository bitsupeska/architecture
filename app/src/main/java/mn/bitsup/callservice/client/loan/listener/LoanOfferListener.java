package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;

public interface LoanOfferListener {
    void onSuccess(LoanOffer var1);
    void onError(Response var1);
}
