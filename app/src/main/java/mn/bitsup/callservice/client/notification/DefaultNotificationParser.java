package mn.bitsup.callservice.client.notification;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.notification.dto.Notification;

public class DefaultNotificationParser implements NotificationParser {
    @Override
    public StatusResponse parseStatus(String json) {
        return new Gson().fromJson(json, StatusResponse.class);
    }

    @Override
    public List<Notification> parseNotifications(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("items").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Type listType = (new TypeToken<List<Notification>>() {
            }).getType();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            return (List) gson.fromJson(jsonArray, listType);
        } catch (Exception ex) {
            Log.e("DefaultCardParserError", "catch: " + ex.getMessage());
        }
        return null;
    }

    @Override
    public Notification parseNotification(String json) {
        String jsonObject = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonObject = obj.getJSONObject("data").toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new Gson().fromJson(jsonObject, Notification.class);
    }

}
