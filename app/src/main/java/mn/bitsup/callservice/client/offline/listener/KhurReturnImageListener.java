package mn.bitsup.callservice.client.offline.listener;

public interface KhurReturnImageListener {
    void returnImage(String var1);
    void onErrorHandling();
}
