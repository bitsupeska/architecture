package mn.bitsup.callservice.client.dataprovider.util;

import android.annotation.SuppressLint;
import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.CustomBase64;
import mn.bitsup.callservice.util.DeviceAddressUtil;

public class OfflineCrypto {

    private static String charset = "UTF-8";

    public static String encrypt(String content, String key, String salt)
        throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        byte[] contentBytes = content.getBytes(charset);
        byte[] keyBytes = key.getBytes(charset);
        byte[] encryptedBytes = aesEncryptBytes(contentBytes, keyBytes, salt);
        return CustomBase64.encode(encryptedBytes);
    }

    public static String decrypt(String content, String key, String salt)
        throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        byte[] encryptedBytes = CustomBase64.decode(content);
        byte[] keyBytes = key.getBytes(charset);
        byte[] decryptedBytes = aesDecryptBytes(encryptedBytes, keyBytes, salt);
        return new String(decryptedBytes, charset);
    }

    public static byte[] aesEncryptBytes(byte[] contentBytes, byte[] keyBytes, String salt)
        throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        return cipherOperation(contentBytes, keyBytes, salt, Cipher.ENCRYPT_MODE);
    }

    public static byte[] aesDecryptBytes(byte[] contentBytes, byte[] keyBytes, String salt)
        throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        return cipherOperation(contentBytes, keyBytes, salt, Cipher.DECRYPT_MODE);
    }

    private static byte[] cipherOperation(byte[] contentBytes, byte[] keyBytes, String salt,
        int mode)
        throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
        SecretKeySpec secretKey = new SecretKeySpec(keyBytes, "AES");
        byte[] initParam = salt.getBytes(charset);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initParam);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(mode, secretKey, ivParameterSpec);
        return cipher.doFinal(contentBytes);
    }

    @SuppressLint("HardwareIds")
    public static String tokenEncrypt(Context context) {

        String params = "{\"macaddress\":\"" + DeviceAddressUtil.getMACAddress(context,"wlan0")
            + "\", \"ipaddress\":\"" + DeviceAddressUtil.getIPAddress(true)
            + "\", \"os\":\"android\"}";

        String data = "";
        try {
            data = encrypt(params, context.getResources().getString(R.string.encrypt_name),
                context.getResources().getString(
                    R.string.encrypt_value));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    public static String tokenDecrypt(String data, Context context) {
        String ddata = "";
        try {
            ddata = decrypt(data, context.getResources().getString(R.string.encrypt_name),
                context.getResources().getString(
                    R.string.encrypt_value));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ddata;
    }
}
