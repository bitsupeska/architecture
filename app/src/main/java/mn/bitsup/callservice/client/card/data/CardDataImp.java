package mn.bitsup.callservice.client.card.data;

import androidx.annotation.NonNull;
import com.google.gson.Gson;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.card.dto.Card;
import mn.bitsup.callservice.client.card.dto.CardDetail;
import mn.bitsup.callservice.client.card.dto.CardReasonItem;
import mn.bitsup.callservice.client.card.dto.CardReissueItem;
import mn.bitsup.callservice.client.card.listener.CardDetailListener;
import mn.bitsup.callservice.client.card.listener.CardReasonListener;
import mn.bitsup.callservice.client.card.listener.CardsListener;
import mn.bitsup.callservice.client.card.parser.DefaultCardParser;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.util.CustomParams;
import mn.bitsup.callservice.view.widget.deliver.Deliver;

public class CardDataImp implements CardData {
    private String TAG = CardDataImp.class.getName();
    private DefaultCardParser cardManagementDataParser;
    public CardDataImp(DefaultCardParser cardManagementDataParser) {
        this.cardManagementDataParser = cardManagementDataParser;
    }

    @Override
    public void getCardList(@NonNull URI baseUri, @NonNull DBSDataProvider dataProvider, @NonNull CustomParams params, @NonNull final CardsListener cardsListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, paramToMap(params), "/v2/cards"), new DBSDataProviderListener() {
                public void onSuccess(Response var1) {
                    List<Card> cards = CardDataImp.this.cardManagementDataParser
                        .a(var1.getStringResponse());
                    cardsListener.onSuccess(cards);
                }

                public void onError(Response var1) {
                    cardsListener.onError(var1);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void getCardReason(
        URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, CardReasonListener cardReasonListener) {
        try{
            baseUri = new URI(baseUri.toString().replace("cards-presentation-service/client-api", "cards-reasonlist-service/v2"));
            dataProvider.execute(
                    this.buildGetRequest(baseUri, "/reasonlist/", dataProvider ),
                    new DBSDataProviderListener() {
                        @Override
                        public void onSuccess(Response response) {
                            int responseCode = response.getResponseCode();
                            if (responseCode >= 200 && responseCode < 400) {
                                List<CardReasonItem> cardReasonItems = cardManagementDataParser.parseCardReasonItem(response.getStringResponse());

                                if (cardReasonItems != null) {
                                    cardReasonListener.onSuccess(cardReasonItems);
                                } else {
                                    Response errorResponsex = new Response();
                                    errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                                    errorResponsex.setResponseCode(500);
                                    cardReasonListener.onError(errorResponsex);
                                }

                            } else {
                                Response errorResponse = new Response();
                                errorResponse.setErrorMessage(response.getStringResponse());
                                errorResponse.setResponseCode(responseCode);
                                cardReasonListener.onError(errorResponse);
                            }
                        }
                        @Override
                        public void onError(Response response) {
                            cardReasonListener.onError(response);
                        }
                    });
        }catch (Exception e ){
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(e.getMessage());
            errorResponse.setResponseCode(500);
            cardReasonListener.onError(errorResponse);
        }
    }

    @Override
    public void reissue(URI baseUri, DBSDataProvider dbsDataProvider, Deliver deliver, StatusListener listener) {
        CardReissueItem cardReissueItem = new CardReissueItem();
        cardReissueItem.setGuid(deliver.getCardPayload().getCard().getGuid());
        cardReissueItem.setCif(deliver.getCardPayload().getCard().getCif());
        cardReissueItem.setAccountNo(deliver.getCardPayload().getCard().getMainAccount());
        cardReissueItem.setProductCode(deliver.getCardPayload().getCard().getProductCode());
        cardReissueItem.setPlasticType(deliver.getCardPayload().getCard().getPlasticType());
        cardReissueItem.setNameOnCard(deliver.getCardPayload().getCard().getNameOfCard());
        cardReissueItem.setCardType(
            Objects.requireNonNull(deliver.getCardPayload().getCard().getAdditions()).get("CARD_TYPE"));
        cardReissueItem.setReason(deliver.getCardPayload().getReasonCode());
        cardReissueItem.setDeliveryAddress(deliver.getAddress());
        cardReissueItem.setDeliveryPhone(deliver.getPhoneNumber());
        cardReissueItem.setDeliveryMode(deliver.getMode());
        double balance;
        if (!Objects.equals(deliver.getCardPayload().getCard().getAdditions().get("CARD_TYPE"), "V")) {
            balance= Double.parseDouble(deliver.getCardPayload().getCard().getFeeAmount().get(0).getAmount());
        } else {
            balance = 0;
        }
        cardReissueItem.setFeeAmount((int) balance);
        try {
            baseUri = new URI(baseUri.toString().replace("cards-presentation-service/client-api", "cards-reissue-service/v2/reissuecard"));
            dbsDataProvider.execute(this.buildReissue(baseUri, cardReissueItem), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = CardDataImp.this.cardManagementDataParser.statusParse(response.getStringResponse());
                        if (statusResponse != null) {
                            listener.onSuccess(statusResponse);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    private Request buildRequestPost(
        URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }
    private Request buildGetRequest(
        URI baseUri,  String param, DBSDataProvider dataProvider) throws URISyntaxException, UnsupportedEncodingException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }
    private Request buildRequestPatch(
        URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("PATCH");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }
    @Override
    public void changeStatus(
        URI baseUri, DBSDataProvider dbsDataProvider, Map<String, String> queryParams, final StatusListener listener) {
        try {
            baseUri = new URI(baseUri.toString().replace("cards-presentation-service/client-api", "cards-statuschange-service/v2/statuschange"));
            dbsDataProvider.execute(this.buildRequestPost(baseUri, dbsDataProvider, queryParams,""), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = CardDataImp.this.cardManagementDataParser.statusParse(response.getStringResponse());
                        if (statusResponse != null) {
                            listener.onSuccess(statusResponse);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void cardActive(
        URI baseUri, DBSDataProvider dbsDataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            baseUri = new URI(baseUri.toString().replace("cards-presentation-service/client-api", "cards-active-service/v2/activecard"));
            dbsDataProvider.execute(this.buildRequestPost(baseUri, dbsDataProvider, queryParams,""), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = CardDataImp.this.cardManagementDataParser.statusParse(response.getStringResponse());
                        if (statusResponse != null) {
                            listener.onSuccess(statusResponse);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void cardDetail(
        URI baseUri, DBSDataProvider dbsDataProvider, Map<String, String> queryParams, CardDetailListener listener) {
        try {
            baseUri = new URI(baseUri.toString().replace("cards-presentation-service/client-api", "cards-carddetail-service/v2/carddetail"));
            dbsDataProvider.execute(this.buildRequestPost(baseUri, dbsDataProvider, queryParams,""), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        CardDetail cardDetail = CardDataImp.this.cardManagementDataParser.cardDetailParse(response.getStringResponse());
                        if (cardDetail != null) {;
                            listener.onSuccess(cardDetail);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void changePin(
        URI baseUri, DBSDataProvider dbsDataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
            baseUri = new URI(baseUri.toString().replace("cards-presentation-service/client-api", "tranzware-card-info/v1/pinset"));
            dbsDataProvider.execute(this.buildRequestPost(baseUri, dbsDataProvider, queryParams,""), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse statusResponse = CardDataImp.this.cardManagementDataParser.statusParse(response.getStringResponse());
                        if (statusResponse != null) {
                            listener.onSuccess(statusResponse);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer "+ storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
    private Map<String, String> paramToMap(@NonNull CustomParams var0) {
        HashMap var1;
        var1 = new HashMap();
        Iterator var2 = var0.keySet().iterator();
        while(var2.hasNext()) {
            String var10001 = (String)var2.next();
            var1.put(var10001, var0.get(var10001));
        }
        return var1;
    }
    private Request buildReissue(URI baseUri, CardReissueItem reissueItem){
        Request request = new Request();
        request.setRequestMethod("POST");
        request.setUri(baseUri);
        request.setBody((new Gson()).toJson(reissueItem));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }
}
