package mn.bitsup.callservice.client.offline;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.CheckUsernameItems;
import mn.bitsup.callservice.client.offline.dto.ResetInfo;
import org.json.JSONException;
import org.json.JSONObject;

public class DefaultOfflineParser implements OfflineParser {

    public DefaultOfflineParser() {
    }

    @Override
    public String parseConvertToken(String json) {
        String data = null;
        try {
            data = new JSONObject(json).getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public StatusResponse parseStatus(String json) {
        Gson gson = new Gson();
        return  gson.fromJson(json, StatusResponse.class);
    }

    @Override
    public CheckUsernameItems parseCheckUserName(String json) {
        Gson gson = new Gson();
        return  gson.fromJson(json, CheckUsernameItems.class);
    }



    @Override
    public Block parseCheckBlock(String json) {
        Gson gson = new Gson();
        return  gson.fromJson(json, Block.class);
    }

    @Override
    public BlockIncrement parseBlockIncrement(String json) {
        Gson gson = new Gson();
        return  gson.fromJson(json, BlockIncrement.class);
    }
    @Override
    public ResetInfo parseResetInfo(String json) {
        Type itemsMapType = new TypeToken<ResetInfo>() {}.getType();
        return new Gson().fromJson(json, itemsMapType);
    }
}
