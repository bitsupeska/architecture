package mn.bitsup.callservice.client.profile.dto;

import java.util.List;

public class Questions {

    private String categName;
    private List<QuestionItem> data;

    public String getCategName() {
        return categName;
    }

    public void setCategName(String categName) {
        this.categName = categName;
    }

    public List<QuestionItem> getData() {
        return data;
    }

    public void setData(List<QuestionItem> data) {
        this.data = data;
    }
}
