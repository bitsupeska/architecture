package mn.bitsup.callservice.client.profile.listener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public interface AccountListener {
    void onSuccess(List<Account> var1);

    void onError(Response var1);
}
