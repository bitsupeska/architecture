package mn.bitsup.callservice.client.profile;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.profile.dto.User;

public interface ProfileParser {

    User parseUser(String var1);

    StatusResponse parseStatus(String var1);

    List parseQuestions(String var1);

    List parseAccounts(String var1);
}
