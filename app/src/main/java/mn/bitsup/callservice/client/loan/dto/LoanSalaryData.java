package mn.bitsup.callservice.client.loan.dto;


public class LoanSalaryData {
    Boolean isDownload;
    int downloadBeginYear;
    int downloadEndYear;
    int BeginYear;
    int BeginMonth;
    int EndYear;
    int EndMonth;
    String voluntary_so_in_payment;
    double avg_salary;
    double si_salary;
    int con_si_pay_mon;
    int si_pay_mon_last12;
    String comp_type;
    int mcs_group_class;
    String has_si_payment;
    String gender;

    public Boolean getDownload() {
        return isDownload;
    }

    public void setDownload(Boolean download) {
        isDownload = download;
    }

    public int getDownloadBeginYear() {
        return downloadBeginYear;
    }

    public void setDownloadBeginYear(int downloadBeginYear) {
        this.downloadBeginYear = downloadBeginYear;
    }

    public int getDownloadEndYear() {
        return downloadEndYear;
    }

    public void setDownloadEndYear(int downloadEndYear) {
        this.downloadEndYear = downloadEndYear;
    }

    public int getBeginYear() {
        return BeginYear;
    }

    public void setBeginYear(int beginYear) {
        BeginYear = beginYear;
    }

    public int getBeginMonth() {
        return BeginMonth;
    }

    public void setBeginMonth(int beginMonth) {
        BeginMonth = beginMonth;
    }

    public int getEndYear() {
        return EndYear;
    }

    public void setEndYear(int endYear) {
        EndYear = endYear;
    }

    public int getEndMonth() {
        return EndMonth;
    }

    public void setEndMonth(int endMonth) {
        EndMonth = endMonth;
    }

    public String getVoluntary_so_in_payment() {
        return voluntary_so_in_payment;
    }

    public void setVoluntary_so_in_payment(String voluntary_so_in_payment) {
        this.voluntary_so_in_payment = voluntary_so_in_payment;
    }

    public double getAvg_salary() {
        return avg_salary;
    }

    public void setAvg_salary(double avg_salary) {
        this.avg_salary = avg_salary;
    }

    public double getSi_salary() {
        return si_salary;
    }

    public void setSi_salary(double si_salary) {
        this.si_salary = si_salary;
    }

    public int getCon_si_pay_mon() {
        return con_si_pay_mon;
    }

    public void setCon_si_pay_mon(int con_si_pay_mon) {
        this.con_si_pay_mon = con_si_pay_mon;
    }

    public int getSi_pay_mon_last12() {
        return si_pay_mon_last12;
    }

    public void setSi_pay_mon_last12(int si_pay_mon_last12) {
        this.si_pay_mon_last12 = si_pay_mon_last12;
    }

    public String getComp_type() {
        return comp_type;
    }

    public void setComp_type(String comp_type) {
        this.comp_type = comp_type;
    }

    public int getMcs_group_class() {
        return mcs_group_class;
    }

    public void setMcs_group_class(int mcs_group_class) {
        this.mcs_group_class = mcs_group_class;
    }

    public String getHas_si_payment() {
        return has_si_payment;
    }

    public void setHas_si_payment(String has_si_payment) {
        this.has_si_payment = has_si_payment;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
