package mn.bitsup.callservice.client.loan.data;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.loan.LoanParser;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.dto.LoanAccount;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.client.loan.dto.LoanCreateInvoice;
import mn.bitsup.callservice.client.loan.dto.LoanIsAllow;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.dto.LoanRelated;
import mn.bitsup.callservice.client.loan.dto.SysDate;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.client.loan.listener.LoanCreateInvoiceListener;
import mn.bitsup.callservice.client.loan.listener.LoanIsAllowedListener;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelCreateListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelListener;
import mn.bitsup.callservice.client.loan.listener.SysDateListener;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.client.loan.dto.LoanSalary;
import mn.bitsup.callservice.client.loan.listener.LoanSalaryListener;

public class LoanDataImpl implements LoanData {

    private LoanParser loanParser;

    public LoanDataImpl(LoanParser loanParser) {
        this.loanParser = loanParser;
    }


    @Override
    public void getLastOffer(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                             LoanOfferListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-css-service/v1.0/getLastOffer"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanOffer data = loanParser.parseLoanOffer(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getRelStatLookup(URI baseUri, DBSDataProvider dataProvider, LoanRelListener loanRelListener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-basic-service/v1.0/relStatLookup"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        List<TemporaryItems> data = loanParser.parseRels(response.getStringResponse());
                        if (data != null) {
                            loanRelListener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            loanRelListener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        loanRelListener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    loanRelListener.onError(response);
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createRelated(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, LoanRelCreateListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-finacle-loan-service/v1.0/related"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanRelated data = loanParser.parseCreateRelation(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getOffer(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                         LoanOfferListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-css-service/v1.0/getOffer"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanOffer data = loanParser.parseLoanOffer(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getLoanAccount(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                               LoanAccountListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-finacle-loan-service/v1.0/cifLoanAccountInfo"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanAccount data = loanParser.parseLoanAccount(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void createLoan(URI baseUri, DBSDataProvider dataProvider, Loan queryParams,
                           StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestCreate(baseUri, dataProvider, queryParams, "/be-internal-corporate-gateway-service/v1.0/disburseLoan"), new DBSDataProviderListener() {  //Bank GW
                //            dataProvider.execute(this.buildRequestCreate(baseUri, dataProvider, queryParams,  "/be-internal-finacle-loan-service/v1.0/loanFinal"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        StatusResponse data = loanParser.parseResponse(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getLoanFindAmortSchedule(URI baseUri, DBSDataProvider dataProvider, String accountNo, LoanDueScheduleListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-finacle-loan-service/v1.0/findAmortSchedule/" + accountNo), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        List<LoanDueScheduleData> data = loanParser.parseDoScheduleData(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getSysDate(URI baseUri, DBSDataProvider dataProvider, SysDateListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-basic-service/v1.0/sysDate"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        SysDate data = loanParser.parseSysDate(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getLoanAllowed(URI baseUri, DBSDataProvider dataProvider, Map<String, String> params, LoanIsAllowedListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, params, "/be-internal-finacle-loan-service/v1.0/isLoanAllowed"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanIsAllow data = loanParser.parseLoanIsAllow(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getDueSchedule(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                               LoanDueScheduleListener listener) {

        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-finacle-loan-service/v1.0/doSchedule"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        List<LoanDueScheduleData> data = loanParser.parseDoScheduleData(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    public void createInvoice(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, LoanCreateInvoiceListener loanCreateInvoiceListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-qpay-service/v1.0/createInvoice"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanCreateInvoice data = loanParser.parseCreateInvoice(response.getStringResponse());
                        if (data != null) {
                            loanCreateInvoiceListener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            loanCreateInvoiceListener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        loanCreateInvoiceListener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    loanCreateInvoiceListener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            loanCreateInvoiceListener.onError(errorResponse);
        }
    }

    @Override
    public void getSalary(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                          LoanSalaryListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-xyp-service/v1.0/salary"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 500) {
                        LoanSalary data = loanParser.parseLoanSalary(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    private Request buildRequest(URI baseUri, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestCreate(URI baseUri, DBSDataProvider dataProvider, Loan queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }

}
