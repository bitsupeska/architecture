package mn.bitsup.callservice.client.offline.dto;

public class BlockIncrement {

    private String status;
    private BlockIncrementData data;

    public BlockIncrement() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BlockIncrementData getData() {
        return data;
    }

    public void setData(BlockIncrementData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BlockIncrement{" +
            "status='" + status + '\'' +
            ", data=" + data +
            '}';
    }
}
