package mn.bitsup.callservice.client.card.dto;

import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CardItem extends Card {
    private String guid;
    private String pan;
    private String cvv;
    private String cif;
    private String mainAccount;
    private String nameOfCard;
    private String expirationDate;
    private String expiredYear;
    private String plasticType;
    private String signStat;
    private String crdStat;
    private String ecStatus;
    private String pinLen;
    private String productCode;
    private String shipDay;
    @SerializedName("feeAmount")
    @Expose
    private List<CardFeeAmount> feeAmount;

    @SerializedName("cardLimit")
    @Expose
    private List<CardLimit> cardLimit;

    public CardItem(@NonNull String s, @NonNull String s1, @NonNull String s2, @Nullable String s3, @Nullable Replacement replacement, @Nullable Delivery delivery, @Nullable String s4, @Nullable CardHolder cardHolder, @Nullable ExpiryDate expiryDate, @Nullable String s5, @NonNull String s6, @NonNull String s7) {
        super(s, s1, s2, s3, replacement, delivery, s4, cardHolder, expiryDate, s5, s6, s7);
    }

    public CardItem(@NonNull String s, @NonNull String s1, @NonNull String s2, @NonNull String s3, @NonNull String s4) {
        super(s, s1, s2, s3, s4);
    }

    public CardItem(String s, String s1, String s2, String s3, String s4, CardHolder cardHolder, ExpiryDate expiryDate, String s5, String s6) {
        super(s, s1, s2, s3, s4, cardHolder, expiryDate, s5, s6);
    }

    public CardItem(Parcel parcel) {
        super(parcel);
    }
    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getNameOfCard() {
        return nameOfCard;
    }

    public void setNameOfCard(String nameOfCard) {
        this.nameOfCard = nameOfCard;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getExpiredYear() {
        return expiredYear;
    }

    public void setExpiredYear(String expiredYear) {
        this.expiredYear = expiredYear;
    }

    public String getPlasticType() {
        return plasticType;
    }

    public void setPlasticType(String plasticType) {
        this.plasticType = plasticType;
    }

    public String getSignStat() {
        return signStat;
    }

    public void setSignStat(String signStat) {
        this.signStat = signStat;
    }

    public String getCrdStat() {
        return crdStat;
    }

    public void setCrdStat(String crdStat) {
        this.crdStat = crdStat;
    }

    public String getEcStatus() {
        return ecStatus;
    }

    public void setEcStatus(String ecStatus) {
        this.ecStatus = ecStatus;
    }


    public String getPinLen() {
        return pinLen;
    }

    public void setPinLen(String pinLen) {
        this.pinLen = pinLen;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getShipDay() {
        return shipDay;
    }

    public void setShipDay(String shipDay) {
        this.shipDay = shipDay;
    }

    public List<CardFeeAmount> getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(List<CardFeeAmount> feeAmount) {
        this.feeAmount = feeAmount;
    }

    public List<CardLimit> getCardLimit() {
        return cardLimit;
    }

    public void setCardLimit(List<CardLimit> cardLimit) {
        this.cardLimit = cardLimit;
    }
}
