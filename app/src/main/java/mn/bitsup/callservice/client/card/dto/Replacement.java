package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.util.Objects;

public class Replacement extends Additions {
    public static final Creator<Replacement> CREATOR = new Creator<Replacement>() {
        public Replacement createFromParcel(Parcel var1) {
            return new Replacement(var1);
        }

        public Replacement[] newArray(int var1) {
            return new Replacement[var1];
        }
    };
    @SerializedName("status")
    @NonNull
    private String status;
    @SerializedName("reason")
    @Nullable
    private String reason;
    @SerializedName("replacedById")
    @Nullable
    private String replacedById;
    @SerializedName("replacingId")
    @Nullable
    private String replacingId;

    @Deprecated
    public Replacement(@NonNull String var1, @Nullable String var2, @Nullable String var3, @Nullable String var4) {
        this.status = var1;
        this.reason = var2;
        this.replacedById = var3;
        this.replacingId = var4;
    }

    public Replacement(@NonNull String var1) {
        this.status = var1;
    }

    protected Replacement(Parcel var1) {
        super(var1);
        this.status = (String) Objects.requireNonNull(var1.readString());
        this.reason = var1.readString();
        this.replacedById = var1.readString();
        this.replacingId = var1.readString();
    }

    @NonNull
    public String getStatus() {
        return this.status;
    }

    public void setStatus(@NonNull String var1) {
        this.status = var1;
    }

    @Nullable
    public String getReason() {
        return this.reason;
    }

    public void setReason(@Nullable String var1) {
        this.reason = var1;
    }

    @Nullable
    public String getReplacedById() {
        return this.replacedById;
    }

    public void setReplacedById(@Nullable String var1) {
        this.replacedById = var1;
    }

    @Nullable
    public String getReplacingId() {
        return this.replacingId;
    }

    public void setReplacingId(@Nullable String var1) {
        this.replacingId = var1;
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeString(this.status);
        var1.writeString(this.reason);
        var1.writeString(this.replacedById);
        var1.writeString(this.replacingId);
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 != null && Replacement.class == var1.getClass()) {
            if (!super.equals(var1)) {
                return false;
            } else {
                Replacement var2 = (Replacement)var1;
                return this.getStatus().equals(var2.getStatus()) && Objects.equals(this.getReason(), var2.getReason()) && Objects.equals(this.getReplacedById(), var2.getReplacedById()) && Objects.equals(this.getReplacingId(), var2.getReplacingId());
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        Object[] var1;
        Object[] var10000 = var1 = new Object[5];
        var1[0] = super.hashCode();
        var1[1] = this.getStatus();
        var1[2] = this.getReason();
        var1[3] = this.getReplacedById();
        var10000[4] = this.getReplacingId();
        return Objects.hash(var10000);
    }
}
