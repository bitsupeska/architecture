package mn.bitsup.callservice.client.profile.dto;


public class Addresses {

    String townId;
    String localityId;
    String town;
    String locality;
    String section;
    String street;
    String district;
    String apartment;
    String doorNumber;
    String addressType;


    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTownId() {
        return townId;
    }

    public void setTownId(String townId) {
        this.townId = townId;
    }

    public String getLocalityId() {
        return localityId;
    }

    public void setLocalityId(String localityId) {
        this.localityId = localityId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Override
    public String toString() {
        return "Addresses{" +
                "townId='" + townId + '\'' +
                ", localityId='" + localityId + '\'' +
                ", town='" + town + '\'' +
                ", locality='" + locality + '\'' +
                ", section='" + section + '\'' +
                ", street='" + street + '\'' +
                ", district='" + district + '\'' +
                ", apartment='" + apartment + '\'' +
                ", doorNumber='" + doorNumber + '\'' +
                ", addressType='" + addressType + '\'' +
                '}';
    }
}
