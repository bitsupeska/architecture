package mn.bitsup.callservice.client.offline.dto;


public class BlockData {
    String id;
    int blocktype;
    String blocktypename;
    String blockinfo;
    int blockchannel;
    String blockchannelname;
    String blockmsg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getBlocktype() {
        return blocktype;
    }

    public void setBlocktype(int blocktype) {
        this.blocktype = blocktype;
    }

    public String getBlocktypename() {
        return blocktypename;
    }

    public void setBlocktypename(String blocktypename) {
        this.blocktypename = blocktypename;
    }

    public String getBlockinfo() {
        return blockinfo;
    }

    public void setBlockinfo(String blockinfo) {
        this.blockinfo = blockinfo;
    }

    public int getBlockchannel() {
        return blockchannel;
    }

    public void setBlockchannel(int blockchannel) {
        this.blockchannel = blockchannel;
    }

    public String getBlockchannelname() {
        return blockchannelname;
    }

    public void setBlockchannelname(String blockchannelname) {
        this.blockchannelname = blockchannelname;
    }

    public String getBlockmsg() {
        return blockmsg;
    }

    public void setBlockmsg(String blockmsg) {
        this.blockmsg = blockmsg;
    }
}
