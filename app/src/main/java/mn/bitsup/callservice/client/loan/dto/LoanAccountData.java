package mn.bitsup.callservice.client.loan.dto;

import java.util.List;
import mn.bitsup.callservice.view.widget.loan.view.dto.Charge;

public class LoanAccountData {

    String accountNo;
    String loanAmt;
    String currencyCode;
    String principleAmt;
    String thisMonthPayment;
    String nextMonthPayment;
    String nextPaymentDay;
    String intRate;
    String totalPaidPrincAmt;
    String totalIntCltd;
    String payOffAmt;
    String bankName;
    String intAmount;
    String feeAmount;
    String fineAmount;
    String otherBankAccountNo;
    String otherBankAccountName;
    String loanStatus;
    String loanPeriod;
    List<Charge> charges;

    public String getNextMonthPayment() {
        return nextMonthPayment;
    }

    public void setNextMonthPayment(String nextMonthPayment) {
        this.nextMonthPayment = nextMonthPayment;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(String loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPrincipleAmt() {
        return principleAmt;
    }

    public void setPrincipleAmt(String principleAmt) {
        this.principleAmt = principleAmt;
    }

    public String getThisMonthPayment() {
        return thisMonthPayment;
    }

    public void setThisMonthPayment(String thisMonthPayment) {
        this.thisMonthPayment = thisMonthPayment;
    }

    public String getIntRate() {
        return intRate;
    }

    public void setIntRate(String intRate) {
        this.intRate = intRate;
    }

    public String getTotalPaidPrincAmt() {
        return totalPaidPrincAmt;
    }

    public void setTotalPaidPrincAmt(String totalPaidPrincAmt) {
        this.totalPaidPrincAmt = totalPaidPrincAmt;
    }

    public String getTotalIntCltd() {
        return totalIntCltd;
    }

    public void setTotalIntCltd(String totalIntCltd) {
        this.totalIntCltd = totalIntCltd;
    }

    public String getPayOffAmt() {
        return payOffAmt;
    }

    public void setPayOffAmt(String payOffAmt) {
        this.payOffAmt = payOffAmt;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getIntAmount() {
        return intAmount;
    }

    public void setIntAmount(String intAmount) {
        this.intAmount = intAmount;
    }

    public String getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getFineAmount() {
        return fineAmount;
    }

    public void setFineAmount(String fineAmount) {
        this.fineAmount = fineAmount;
    }

    public String getOtherBankAccountNo() {
        return otherBankAccountNo;
    }

    public void setOtherBankAccountNo(String otherBankAccountNo) {
        this.otherBankAccountNo = otherBankAccountNo;
    }

    public String getOtherBankAccountName() {
        return otherBankAccountName;
    }

    public void setOtherBankAccountName(String otherBankAccountName) {
        this.otherBankAccountName = otherBankAccountName;
    }

    public String getLoanStatus() {
        return loanStatus;
    }

    public void setLoanStatus(String loanStatus) {
        this.loanStatus = loanStatus;
    }

    public String getLoanPeriod() {
        return loanPeriod;
    }

    public void setLoanPeriod(String loanPeriod) {
        this.loanPeriod = loanPeriod;
    }

    public String getNextPaymentDay() {
        return nextPaymentDay;
    }

    public void setNextPaymentDay(String nextPaymentDay) {
        this.nextPaymentDay = nextPaymentDay;
    }

    public List<Charge> getCharges() {
        return charges;
    }

    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }

    @Override
    public String toString() {
        return "LoanAccountData{" +
            "accountNo='" + accountNo + '\'' +
            ", loanAmt='" + loanAmt + '\'' +
            ", currencyCode='" + currencyCode + '\'' +
            ", principleAmt='" + principleAmt + '\'' +
            ", thisMonthPayment='" + thisMonthPayment + '\'' +
            ", nextMonthPayment='" + nextMonthPayment + '\'' +
            ", nextPaymentDay='" + nextPaymentDay + '\'' +
            ", intRate='" + intRate + '\'' +
            ", totalPaidPrincAmt='" + totalPaidPrincAmt + '\'' +
            ", totalIntCltd='" + totalIntCltd + '\'' +
            ", payOffAmt='" + payOffAmt + '\'' +
            ", bankName='" + bankName + '\'' +
            ", intAmount='" + intAmount + '\'' +
            ", feeAmount='" + feeAmount + '\'' +
            ", fineAmount='" + fineAmount + '\'' +
            ", otherBankAccountNo='" + otherBankAccountNo + '\'' +
            ", otherBankAccountName='" + otherBankAccountName + '\'' +
            ", loanStatus='" + loanStatus + '\'' +
            ", loanPeriod='" + loanPeriod + '\'' +
            ", charges=" + charges +
            '}';
    }
}
