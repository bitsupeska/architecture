package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanFindAmortSchedule;

public interface LoanFindAmortScheduleListener {
    void onSuccess(LoanFindAmortSchedule var1);
    void onError(Response var1);
}
