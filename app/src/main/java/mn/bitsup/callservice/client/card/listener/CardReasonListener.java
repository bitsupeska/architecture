package mn.bitsup.callservice.client.card.listener;


import java.util.List;
import mn.bitsup.callservice.client.card.dto.CardReasonItem;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface CardReasonListener {
    void onSuccess(List<CardReasonItem> var1);
    void onError(Response var1);
}
