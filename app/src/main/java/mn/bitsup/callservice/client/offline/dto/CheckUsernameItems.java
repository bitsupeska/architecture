package mn.bitsup.callservice.client.offline.dto;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Arrays;
import mn.bitsup.callservice.client.dataprovider.data.Additions;

public class CheckUsernameItems extends Additions implements Parcelable {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("reCommendations")
    @Expose
    private String[] reCommendations;

    public static final Creator<CheckUsernameItems> CREATOR = new Creator<CheckUsernameItems>() {
        public CheckUsernameItems createFromParcel(Parcel source) {
            return new CheckUsernameItems(source);
        }

        public CheckUsernameItems[] newArray(int size) {
            return new CheckUsernameItems[size];
        }
    };

    public CheckUsernameItems() {
    }

    private CheckUsernameItems(Parcel in) {
        this.code = in.readString();
        this.status = in.readString();
        this.message = in.readString();
        this.reCommendations = in.createStringArray();
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.code);
        dest.writeString(this.status);
        dest.writeString(this.message);
        dest.writeStringArray(this.reCommendations);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String[] getRecommendations() {
        return reCommendations;
    }

    public void setRecommendations(String[] reCommendations) {
        this.reCommendations = reCommendations;
    }

    @Override
    public String toString() {
        return "CheckUsernameItems{" +
            "code='" + code + '\'' +
            ", status='" + status + '\'' +
            ", message='" + message + '\'' +
            ", recommendations=" + Arrays.toString(reCommendations) +
            '}';
    }
}
