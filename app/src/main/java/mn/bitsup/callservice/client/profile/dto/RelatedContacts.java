package mn.bitsup.callservice.client.profile.dto;

public class RelatedContacts {
    String lastname;
    String firstname;
    String handphone;
    String regnum;
    String guardtype;

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getRegnum() {
        return regnum;
    }

    public void setRegnum(String regnum) {
        this.regnum = regnum;
    }

    public String getGuardtype() {
        return guardtype;
    }

    public void setGuardtype(String guardtype) {
        this.guardtype = guardtype;
    }
}
