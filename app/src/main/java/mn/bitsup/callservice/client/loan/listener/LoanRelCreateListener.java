package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanRelated;

public interface LoanRelCreateListener {
    void onSuccess(LoanRelated var1);
    void onError(Response var1);
}
