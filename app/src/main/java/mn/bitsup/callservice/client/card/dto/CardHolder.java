package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class CardHolder extends Additions implements Parcelable {
    public static final Creator<CardHolder> CREATOR = new Creator<CardHolder>() {
        public CardHolder createFromParcel(Parcel var1) {
            return new CardHolder(var1);
        }

        public CardHolder[] newArray(int var1) {
            return new CardHolder[var1];
        }
    };
    @SerializedName("name")
    private String name;

    public CardHolder() {
    }

    public CardHolder(String var1) {
        this.name = var1;
    }

    protected CardHolder(Parcel var1) {
        super(var1);
        this.name = var1.readString();
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeString(this.name);
    }

    public int describeContents() {
        return 0;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }
}
