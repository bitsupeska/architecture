package mn.bitsup.callservice.client.notification;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.notification.dto.Notification;

public interface NotificationParser {
    StatusResponse parseStatus(String var1);
    List<Notification> parseNotifications(String json);
    Notification parseNotification(String json);
}
