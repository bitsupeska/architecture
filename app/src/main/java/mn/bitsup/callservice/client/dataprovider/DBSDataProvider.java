package mn.bitsup.callservice.client.dataprovider;

import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;

public interface DBSDataProvider {

    void execute(Request var1, DBSDataProviderListener var2);

    default boolean cancel(Request request) {
        return false;
    }
}
