package mn.bitsup.callservice.client.miniapp;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.miniapp.dto.MiniApp;

public class DefaultMiniAppParser implements MiniAppParser  {
    @Override
    public List parseApps(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("items").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<MiniApp>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }

    public StatusResponse parseStatus(String json) {
        return new Gson().fromJson(json, StatusResponse.class);
    }

}
