package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanSalary;

public interface LoanSalaryListener {
    void onSuccess(LoanSalary var1);
    void onError(Response var1);
}
