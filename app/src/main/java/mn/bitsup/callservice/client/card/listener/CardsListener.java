package mn.bitsup.callservice.client.card.listener;


import java.util.List;
import mn.bitsup.callservice.client.card.dto.Card;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface CardsListener {
    void onSuccess(List<Card> var1);

    void onError(Response var1);
}
