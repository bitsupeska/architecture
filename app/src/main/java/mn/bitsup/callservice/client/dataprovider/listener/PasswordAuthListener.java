package mn.bitsup.callservice.client.dataprovider.listener;


import java.util.List;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface PasswordAuthListener {
    void onAuthSuccess(Map<String, List<String>> var1);

    void onAuthError(Response var1);
}
