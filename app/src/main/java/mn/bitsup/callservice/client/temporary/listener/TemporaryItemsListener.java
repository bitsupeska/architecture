package mn.bitsup.callservice.client.temporary.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import java.util.List;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;

public interface TemporaryItemsListener {
    void onSuccess(List<TemporaryItems> var1);
    void onError(Response var1);
}
