package mn.bitsup.callservice.client.info.dto;

import android.os.Parcelable;
import mn.bitsup.callservice.client.dataprovider.data.Additions;

public class BankItem extends Additions implements Parcelable {

    private Integer id;
    private String bankname;
    private String qpay;
    private String transfer;
    private String profilesettings;
    private String accountno;
    private String accountname;
    private String qpayname;
    private String ioslink;
    private String androidlink;

    public BankItem() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getQpay() {
        return qpay;
    }

    public void setQpay(String qpay) {
        this.qpay = qpay;
    }

    public String getTransfer() {
        return transfer;
    }

    public void setTransfer(String transfer) {
        this.transfer = transfer;
    }

    public String getProfilesettings() {
        return profilesettings;
    }

    public void setProfilesettings(String profilesettings) {
        this.profilesettings = profilesettings;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getQpayname() {
        return qpayname;
    }

    public void setQpayname(String qpayname) {
        this.qpayname = qpayname;
    }

    public String getIoslink() {
        return ioslink;
    }

    public void setIoslink(String ioslink) {
        this.ioslink = ioslink;
    }

    public String getAndroidlink() {
        return androidlink;
    }

    public void setAndroidlink(String androidlink) {
        this.androidlink = androidlink;
    }
}
