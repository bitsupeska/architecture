package mn.bitsup.callservice.client;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.activity.EnrollmentActivity;

import static android.content.Context.NOTIFICATION_SERVICE;
import static mn.bitsup.callservice.view.activity.BaseActivity.context;
import static mn.bitsup.callservice.view.activity.EnrollmentActivity.INTENT_EXTRA_FORCED_LOGOUT;

public class UserInteractionTimers {
    private static long expiredTime = 300 * 1000;
    private static long timer = expiredTime / 1000;
    private static CountDownTimer countDownTimer;
    public static boolean isRunning = false;
    public static boolean isLogged = false;
    public static boolean isNotFinish = false;

    public static void startTimer() {
        isRunning = true;
        countDownTimer = new CountDownTimer(expiredTime, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                timer = millisUntilFinished / 1000;
                Log.e("Timer", "user integration onTick: " + timer);
                if (timer == 0) {
                    logOff();
                }
            }

            @Override
            public void onFinish() {
                isRunning = false;
                isLogged = false;
                Log.e("TIMER", "onFinish: " + timer);
                if (timer > 1) {
                    isNotFinish = true;
                    logOff();
                }
                RefreshService.countDownTimer.cancel();
                this.cancel();
            }
        }.start();
    }

    public static void logOff() {
        isLogged = false;
        createNotification();
        context.stopService(new Intent(context, RefreshService.class));
        Intent intent = new Intent(context, EnrollmentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(INTENT_EXTRA_FORCED_LOGOUT, true);
        context.startActivity(intent);
    }

    public static void endTimer() {
        isRunning = false;
        expiredTime = 0;
        timer = 0;
        if(countDownTimer!=null)
        countDownTimer.onFinish();
    }

    public static void restart() {
        isRunning = true;
        expiredTime = 300 * 1000;
        if(countDownTimer!=null){
            countDownTimer.cancel();
            countDownTimer.start();
        }
    }

    private static void createNotification() {
        Intent intent = new Intent(context, EnrollmentActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = "Default";
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.app_icon)
                .setSound(alarmSound)
                .setContentTitle(context.getResources().getString(R.string.shared_alert_generic_alert_error_title))
                .setContentText(context.getResources().getString(R.string.shared_alert_session_expired_message))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
            assert manager != null;
            manager.createNotificationChannel(channel);
        }
        assert manager != null;
        manager.notify(0, builder.build());
    }
}