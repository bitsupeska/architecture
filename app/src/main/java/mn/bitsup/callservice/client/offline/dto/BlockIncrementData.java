package mn.bitsup.callservice.client.offline.dto;

public class BlockIncrementData {
    String blockName;
    String blockValue;
    int failedCount;
    int blockLimit;
    String blockChannel;

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }

    public String getBlockValue() {
        return blockValue;
    }

    public void setBlockValue(String blockValue) {
        this.blockValue = blockValue;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public int getBlockLimit() {
        return blockLimit;
    }

    public void setBlockLimit(int blockLimit) {
        this.blockLimit = blockLimit;
    }

    public String getBlockChannel() {
        return blockChannel;
    }

    public void setBlockChannel(String blockChannel) {
        this.blockChannel = blockChannel;
    }

    @Override
    public String toString() {
        return "BlockIncrementData{" +
            "block_name='" + blockName + '\'' +
            ", block_value='" + blockValue + '\'' +
            ", failed_count=" + failedCount +
            ", block_limit=" + blockLimit +
            ", block_channel='" + blockChannel + '\'' +
            '}';
    }
}
