package mn.bitsup.callservice.client.card.data;

import androidx.annotation.NonNull;
import java.net.URI;
import java.util.Map;
import mn.bitsup.callservice.client.card.listener.CardDetailListener;
import mn.bitsup.callservice.client.card.listener.CardReasonListener;
import mn.bitsup.callservice.client.card.listener.CardsListener;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.util.CustomParams;
import mn.bitsup.callservice.view.widget.deliver.Deliver;

public interface CardData {

    void getCardList(@NonNull URI var1, @NonNull DBSDataProvider var2, @NonNull CustomParams var3,
        @NonNull CardsListener var4);

    void getCardReason(URI var1, DBSDataProvider var2, Map<String, String> var3,
        CardReasonListener var4);

    void reissue(URI var1, DBSDataProvider var2, Deliver deliver, StatusListener var4);

    void changeStatus(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void cardActive(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void cardDetail(URI var1, DBSDataProvider var2, Map<String, String> var3,
        CardDetailListener var4);

    void changePin(URI baseUri, DBSDataProvider dbsDataProvider, Map<String, String> queryParams,
        final StatusListener listener);

}
