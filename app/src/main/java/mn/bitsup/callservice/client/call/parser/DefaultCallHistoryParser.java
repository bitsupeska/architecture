package mn.bitsup.callservice.client.call.parser;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.client.notification.dto.Notification;

public class DefaultCallHistoryParser implements CallHistoryParser{
    private String TAG = DefaultCallHistoryParser.class.getName();
    @Override
    public List<CallHistory> parseHistories(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            Log.e(TAG, "parseHistories: "+obj.get("success") );
            if (obj.get("success").equals(true)) {
                jsonArray = obj.getJSONArray("data").toString();
                Log.e(TAG, "parseHistories: "+jsonArray );
            }
        } catch (JSONException e) {
            Log.e(TAG, "parseHistories: "+e.getMessage() );
            e.printStackTrace();
        }
        try {
            Type listType = (new TypeToken<List<CallHistory>>() {
            }).getType();
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.create();
            return (List) gson.fromJson(jsonArray, listType);
        } catch (Exception ex) {
            Log.e("DefaultCardParserError", "catch: " + ex.getMessage());
        }
        return null;
    }
}
