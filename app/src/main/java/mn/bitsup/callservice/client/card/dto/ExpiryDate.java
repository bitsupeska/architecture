package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class ExpiryDate extends Additions implements Parcelable {
    public static final Creator<ExpiryDate> CREATOR = new Creator<ExpiryDate>() {
        public ExpiryDate createFromParcel(Parcel var1) {
            return new ExpiryDate(var1);
        }

        public ExpiryDate[] newArray(int var1) {
            return new ExpiryDate[var1];
        }
    };
    @SerializedName("year")
    private String year;
    @SerializedName("month")
    private String month;

    public ExpiryDate() {
    }

    protected ExpiryDate(Parcel var1) {
        super(var1);
        this.year = var1.readString();
        this.month = var1.readString();
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeString(this.year);
        var1.writeString(this.month);
    }

    public int describeContents() {
        return 0;
    }

    public String getYear() {
        return this.year;
    }

    public void setYear(String var1) {
        this.year = var1;
    }

    public String getMonth() {
        return this.month;
    }

    public void setMonth(String var1) {
        this.month = var1;
    }
}

