package mn.bitsup.callservice.client.info.data;


import android.util.Log;

import androidx.annotation.NonNull;
import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.info.InfoParser;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;
import mn.bitsup.callservice.client.info.dto.CurrencyItem;
import mn.bitsup.callservice.client.info.dto.DeliveryDate;
import mn.bitsup.callservice.client.info.dto.DeliveryState;
import mn.bitsup.callservice.client.info.dto.Item;
import mn.bitsup.callservice.client.info.dto.Location;
import mn.bitsup.callservice.client.info.listener.BackgroundImageListener;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.info.listener.CurrencyListener;
import mn.bitsup.callservice.client.info.listener.DeliveryDateListener;
import mn.bitsup.callservice.client.info.listener.DeliveryStateListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;

public class InfoDataImpl implements InfoData{

    private static final String TAG = InfoDataImpl.class.getSimpleName();
    private InfoParser infoParser;

    public InfoDataImpl(InfoParser infoParser) {
        this.infoParser = infoParser;
    }

    @Override
    public void getCityData(URI baseUri, DBSDataProvider dataProvider, LocationListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-checkcore-service/v1.0/getStates"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<Location> data = infoParser.parseStateList(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }

    }

    @Override
    public void getDistrictData(URI baseUri, DBSDataProvider dataProvider, LocationListener listener,
                                String stateCode) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-checkcore-service/v1.0/city/"+stateCode), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<Location> data = infoParser.parseCityList(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }


    @Override
    public void getBank(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,  final BankListener bankListener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-basic-service/v1.0/getBankList"), new DBSDataProviderListener(){
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<BankItem> bankItems = InfoDataImpl.this.infoParser.parseBankList(response.getStringResponse());
                        if (bankItems == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE RESPONSE BODY");
                            errorResponsex.setResponseCode(500);
                            bankListener.onError(errorResponsex);
                            return;
                        }
                        bankListener.onSuccess(bankItems);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        bankListener.onError(errorResponse);
                    }

                }
                public void onError(Response errorResponse) {
                    bankListener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            bankListener.onError(errorResponse);
        }
    }



    @Override
    public void getCurrency(URI baseUri, DBSDataProvider dataProvider, CurrencyListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/finacle-currencyname-service/v1/currency"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<CurrencyItem> currencies = InfoDataImpl.this.infoParser.parseCurrencyList(response.getStringResponse());
                        if (currencies == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE RESPONSE BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }

                        listener.onSuccess(currencies);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getStateDelivery(URI baseUri, DBSDataProvider dataProvider, DeliveryStateListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/portal-getstate-service/v1/getstate" ), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<DeliveryState> currencies = InfoDataImpl.this.infoParser.parseDeliveryList(response.getStringResponse());
                        if (currencies == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE RESPONSE BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }

                        listener.onSuccess(currencies);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getDistrictDelivery(URI baseUri, DBSDataProvider dataProvider, DeliveryStateListener listener,String cityCode) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/portal-getstate-service/v1/getcity/"+cityCode), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<DeliveryState> currencies = InfoDataImpl.this.infoParser.parseDeliveryList(response.getStringResponse());
                        if (currencies == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE RESPONSE BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }

                        listener.onSuccess(currencies);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getDeliveryDate(URI baseUri, DBSDataProvider dataProvider, DeliveryDateListener listener, String dayCount) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/portal-getstate-service/v1/checkdelivery/"+dayCount), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<DeliveryDate> dates = InfoDataImpl.this.infoParser.parseDeliveryDateList(response.getStringResponse());
                        if (dates == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE RESPONSE BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }

                        listener.onSuccess(dates);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getCheckCitizenInfo(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,final CitizenInfoListener listener) {
        try {
            dataProvider.execute(
                    this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-xyp-service/v1.0/getCheckCitizenInfo"),
                    new DBSDataProviderListener() {
                        @Override
                        public void onSuccess(Response response) {
                            int responseCode = response.getResponseCode();
                            if (responseCode >= 200 && responseCode < 400) {
                                CitizenInfo data = infoParser.parseCitizenInfo(response.getStringResponse());
                                if (data != null) {
                                    listener.onSuccess(data);
                                } else {
                                    Response errorResponsex = new Response();
                                    errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                                    errorResponsex.setResponseCode(500);
                                    listener.onError(errorResponsex);
                                }
                            } else {
                                Response errorResponse = new Response();
                                errorResponse.setErrorMessage(response.getErrorMessage());
                                errorResponse.setResponseCode(responseCode);
                                listener.onError(errorResponse);
                            }
                        }

                        @Override
                        public void onError(Response response) {
                            listener.onError(response);
                        }
                    });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);

        }
    }

    @Override
    public void getBackgroundImage(URI baseUri, DBSDataProvider dataProvider, BackgroundImageListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-finacle-onboard-service/v1.0/carouselLayout"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    Log.e("carousel", "onSuccess: " + response.getErrorMessage() );
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        Item item = infoParser.parseBackgroundItem(response.getStringResponse());
                        if (item != null) {
                                listener.onSuccess(item);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                        Log.e("background", "onError: " + errorResponse );
                    }

                }
                @Override
                public void onError(Response errorResponse) {
                    Log.e("background", "onError: " + errorResponse );
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
            Log.e("background", "catch error: " + errorResponse );
        }
    }

    private Request buildRequest(URI baseUri,String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider,  Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer "+ storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
}
