package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.SysDate;

public interface SysDateListener {
    void onSuccess(SysDate var1);
    void onError(Response var1);
}
