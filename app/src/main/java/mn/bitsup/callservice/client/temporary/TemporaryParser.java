package mn.bitsup.callservice.client.temporary;


import java.util.List;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.temporary.dto.EducationItem;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;

public interface TemporaryParser {
    KhurItems parseKhurData(String var1);
    List<TemporaryItems> parseTemporaryItems(String var1);
    StatusResponse parseStatus(String var1);
    List<TemporaryItems> parseEmployeeStatus(String var1);
    List<TemporaryItems> parseOccupationList(String var1);
    List<EducationItem> parseEduType(String var1);
}
