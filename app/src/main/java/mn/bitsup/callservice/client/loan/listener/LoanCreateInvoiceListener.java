package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanCreateInvoice;

public interface LoanCreateInvoiceListener {
    void onSuccess(LoanCreateInvoice var1);
    void onError(Response var1);
}
