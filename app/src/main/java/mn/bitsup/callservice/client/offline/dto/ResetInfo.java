package mn.bitsup.callservice.client.offline.dto;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import mn.bitsup.callservice.client.profile.dto.User;

public class ResetInfo {
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<User> data;

    public static final Parcelable.Creator<ResetInfo> CREATOR = new Parcelable.Creator<ResetInfo>() {
        public ResetInfo createFromParcel(Parcel source) {
            return new ResetInfo(source);
        }

        public ResetInfo[] newArray(int size) {
            return new ResetInfo[size];
        }
    };

    public ResetInfo() {
    }

    private ResetInfo(Parcel in) {
        this.code = in.readString();
        this.status = in.readString();
        this.message = in.readString();
        this.data = in.readParcelable(User.class.getClassLoader());
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public int describeContents() {
        return 0;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<User> getData() {
        return data;
    }

    public void setData(List<User> data) {
        this.data = data;
    }
}