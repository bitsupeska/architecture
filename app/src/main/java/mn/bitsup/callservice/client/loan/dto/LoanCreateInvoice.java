package mn.bitsup.callservice.client.loan.dto;

import java.util.List;

public class LoanCreateInvoice {
    private String payment_id;
    private String qPay_QRcode;
    private String qPay_QRimage;
    private String qPay_url;
    private String qPay_shortUrl;
    private List<CreateInvoiceBankItem> qPay_deeplink;

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getqPay_QRcode() {
        return qPay_QRcode;
    }

    public void setqPay_QRcode(String qPay_QRcode) {
        this.qPay_QRcode = qPay_QRcode;
    }

    public String getqPay_QRimage() {
        return qPay_QRimage;
    }

    public void setqPay_QRimage(String qPay_QRimage) {
        this.qPay_QRimage = qPay_QRimage;
    }

    public String getqPay_url() {
        return qPay_url;
    }

    public void setqPay_url(String qPay_url) {
        this.qPay_url = qPay_url;
    }

    public String getqPay_shortUrl() {
        return qPay_shortUrl;
    }

    public void setqPay_shortUrl(String qPay_shortUrl) {
        this.qPay_shortUrl = qPay_shortUrl;
    }

    public List<CreateInvoiceBankItem> getqPay_deeplink() {
        return qPay_deeplink;
    }

    public void setqPay_deeplink(List<CreateInvoiceBankItem> qPay_deeplink) {
        this.qPay_deeplink = qPay_deeplink;
    }
}
