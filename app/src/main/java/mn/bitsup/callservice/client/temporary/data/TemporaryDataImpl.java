package mn.bitsup.callservice.client.temporary.data;

import android.util.Log;

import androidx.annotation.NonNull;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;

import com.google.gson.Gson;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.temporary.TemporaryParser;
import mn.bitsup.callservice.client.temporary.dto.EducationItem;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.dto.TemporaryCif;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.client.temporary.listener.EducationListener;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;

public class TemporaryDataImpl implements TemporaryData {

    private TemporaryParser temporaryParser;

    public TemporaryDataImpl(TemporaryParser temporaryParser) {
        this.temporaryParser = temporaryParser;
    }

    @Override
    public void getKhurData(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final KhurDataListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, queryParams, "/be-internal-xyp-service/v1.0/onboard"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        KhurItems data = temporaryParser.parseKhurData(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getEmpstatlookup(URI baseUri, DBSDataProvider dataProvider,
                                 TemporaryItemsListener listener) {

        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-checkcore-service/v1.0/getEmployeeStatus"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<TemporaryItems> data = temporaryParser.parseEmployeeStatus(response.getStringResponse());
                        if (data != null) {

                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getOccstatlookup(URI baseUri, DBSDataProvider dataProvider,
                                 TemporaryItemsListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-checkcore-service/v1.0/getOccupationList"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<TemporaryItems> data = temporaryParser.parseOccupationList(response.getStringResponse());
                        if (data != null && data.size() != 0) {
                            Log.e("getOccstatlookup", "success: data size: " + data.size());
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getWorkstatlookup(URI baseUri, DBSDataProvider dataProvider,
                                  TemporaryItemsListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/finacle-workstat-service/v1/workstatlookup"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<TemporaryItems> data = temporaryParser.parseTemporaryItems(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getRelstatlookup(URI baseUri, DBSDataProvider dataProvider, TemporaryItemsListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/finacle-relstat-service/v1/relstatlookup"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<TemporaryItems> data = temporaryParser.parseTemporaryItems(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void checkEmail(URI baseUri, DBSDataProvider dataProvider, StatusListener listener, String param) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-checkcore-service/v1.0/emailCheck?email=" + param), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse data = temporaryParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void createCif(URI baseUri, DBSDataProvider dataProvider, StatusListener listener, TemporaryCif queryParams) {
        try {
            dataProvider.execute(this.buildRequestCreate(baseUri, (new Gson()).toJson(queryParams), "/be-internal-finacle-onboard-service/v1.0/createCif"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse data = temporaryParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void updateCif(URI baseUri, DBSDataProvider dataProvider, StatusListener listener, Temporary queryParams) {
        try {
            dataProvider.execute(this.buildRequestCreate(baseUri, (new Gson()).toJson(queryParams), "/finacle-retcustmod-service/v1/updatecif"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse data = temporaryParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void createCA(URI baseUri, DBSDataProvider dataProvider, StatusListener listener, Temporary queryParams) {
        try {
            dataProvider.execute(this.buildRequestCreate(baseUri, (new Gson()).toJson(queryParams), "/be-internal-finacle-onboard-service/v1.0/tempoCreateCA"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse data = temporaryParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getEduType(URI baseUri, DBSDataProvider dataProvider, EducationListener listener) {
        try {
            dataProvider.execute(this.buildRequest(baseUri, "/be-internal-basic-service/v1.0/getEduType"), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<EducationItem> accounts = temporaryParser.parseEduType(response.getStringResponse());
                        if (accounts == null) {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD BODY");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                            return;
                        }
                        listener.onSuccess(accounts);
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getStringResponse());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    listener.onError(errorResponse);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getXyrData(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, KhurDataListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, queryParams, "/be-internal-basic-service/v1.0/getXyp"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        KhurItems data = temporaryParser.parseKhurData(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    private Request buildRequest(URI baseUri, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestPost(URI baseUri, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestCreate(URI baseUri, String queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody(queryParams);
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }


}
