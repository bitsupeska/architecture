package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;
import androidx.annotation.NonNull;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import java.util.Objects;

public class Delivery extends Additions {
    public static final Creator<Delivery> CREATOR = new Creator<Delivery>() {
        public Delivery createFromParcel(Parcel var1) {
            return new Delivery(var1);
        }

        public Delivery[] newArray(int var1) {
            return new Delivery[var1];
        }
    };
    @SerializedName("transitSteps")
    @NonNull
    private List<DeliveryTransitStep> transitSteps;

    public Delivery(@NonNull List<DeliveryTransitStep> var1) {
        this.transitSteps = var1;
    }

    protected Delivery(Parcel var1) {
        super(var1);
        this.transitSteps = (List) Objects.requireNonNull(var1.createTypedArrayList(DeliveryTransitStep.CREATOR));
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeTypedList(this.transitSteps);
    }

    public int describeContents() {
        return 0;
    }

    @NonNull
    public List<DeliveryTransitStep> getTransitSteps() {
        return this.transitSteps;
    }
}