package mn.bitsup.callservice.client.notification.data;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.notification.NotificationParser;
import mn.bitsup.callservice.client.notification.dto.Notification;
import mn.bitsup.callservice.client.notification.listener.NotificationListener;
import mn.bitsup.callservice.client.notification.listener.NotificationsListener;

public class NotificationDataImp implements NotificationData {
    private static final String TAG = NotificationData.class.getSimpleName();
    private NotificationParser notificationParser;

    public NotificationDataImp(NotificationParser notificationParser) {
        this.notificationParser = notificationParser;
    }

    @Override
    public void getUnreadCount(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener statusListener) {
        try {
            dataProvider.execute(this.buildRequest("GET", baseUri, dataProvider, queryParams), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse cardReasonList = notificationParser.parseStatus(response.getStringResponse());
                        if (cardReasonList != null) {
                            statusListener.onSuccess(cardReasonList);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("cardReasonList CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            statusListener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        statusListener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    statusListener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            statusListener.onError(errorResponse);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getNotification(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, NotificationsListener notificationsListener) {
        try {
            dataProvider.execute(this.buildRequest("GET", baseUri, dataProvider, queryParams), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<Notification> notifications = notificationParser.parseNotifications(response.getStringResponse());
                        if (notifications != null) {
                            notificationsListener.onSuccess(notifications);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("cardReasonList CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            notificationsListener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        notificationsListener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    notificationsListener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            notificationsListener.onError(errorResponse);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getNotificationDetail(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, NotificationListener notificationListener) {
        try {
            dataProvider.execute(this.buildRequest("GET", baseUri, dataProvider, queryParams), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        Notification notifications = notificationParser.parseNotification(response.getStringResponse());
                        if (notifications != null) {
                            notificationListener.onSuccess(notifications);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("cardReasonList CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            notificationListener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        notificationListener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {
                    notificationListener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            notificationListener.onError(errorResponse);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void archiveNotification(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener statusListener) {
        try {
            dataProvider.execute(
                    this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-notification-service/v1.0/archiveNotification"),
                    new DBSDataProviderListener() {
                        @Override
                        public void onSuccess(Response response) {
                            int responseCode = response.getResponseCode();
                            if (responseCode >= 200 && responseCode < 400) {
                                StatusResponse data = notificationParser.parseStatus(response.getStringResponse());
                                if (data != null) {
                                    statusListener.onSuccess(data);
                                } else {
                                    Response errorResponsex = new Response();
                                    errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                                    errorResponsex.setResponseCode(500);
                                    statusListener.onError(errorResponsex);
                                }
                            } else {
                                Response errorResponse = new Response();
                                errorResponse.setErrorMessage(response.getErrorMessage());
                                errorResponse.setResponseCode(responseCode);
                                statusListener.onError(errorResponse);
                            }
                        }

                        @Override
                        public void onError(Response response) {
                            statusListener.onError(response);
                        }
                    });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);

        }
    }

    private Request buildRequest(String methodName, URI baseUri, DBSDataProvider dataProvider, Map<String, String> params) throws URISyntaxException, UnsupportedEncodingException {
        Request request = new Request();
        request.setRequestMethod(methodName);
        request.setBody((new Gson()).toJson(params));
        request.setUri(new URI(baseUri + (params != null ? this.buildQuery(params) : "")));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    protected String buildQuery(Map<String, String> queryParams) throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("?");
        Iterator var3 = queryParams.entrySet().iterator();

        while (var3.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) var3.next();
            stringBuilder.append(URLEncoder.encode((String) entry.getKey(), "UTF-8")).append("=").append(URLEncoder.encode((String) entry.getValue(), "UTF-8")).append("&");
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }


    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer "+ storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
}
