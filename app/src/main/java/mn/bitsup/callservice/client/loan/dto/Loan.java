package mn.bitsup.callservice.client.loan.dto;


public class Loan {

    String amountCurrency;
    String beginDate;
    String beginDay;
    String cifId;
    String firstName;
    String lastDayOfMonth;
    String lastName;
    String loanAmount;
    String loanMonthCount;
    String mainAccount;
    String pkSeq;
    String prodCode;
    String rate;
    String rePaymentMonthCount;
    String rePaymentScheduledAmount;
    String rePaymentType;
    String regNum;
    String relatedFirstName;
    String relatedHandPhone;
    String relatedLastName;
    String relatedRegNum;
    String relatedWhoIs;
    String type;
    String dmsUser;
    String toAccount;
    String toAccName;
    String uid;
    String toBank;
    String offerLimit;

    public String getDmsUser() {
        return dmsUser;
    }

    public void setDmsUser(String dmsUser) {
        this.dmsUser = dmsUser;
    }

    public String getAmountCurrency() {
        return amountCurrency;
    }

    public void setAmountCurrency(String amountCurrency) {
        this.amountCurrency = amountCurrency;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getBeginDay() {
        return beginDay;
    }

    public void setBeginDay(String beginDay) {
        this.beginDay = beginDay;
    }

    public String getCifId() {
        return cifId;
    }

    public void setCifId(String cifId) {
        this.cifId = cifId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastDayOfMonth() {
        return lastDayOfMonth;
    }

    public void setLastDayOfMonth(String lastDayOfMonth) {
        this.lastDayOfMonth = lastDayOfMonth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(String loanAmount) {
        this.loanAmount = loanAmount;
    }

    public String getLoanMonthCount() {
        return loanMonthCount;
    }

    public void setLoanMonthCount(String loanMonthCount) {
        this.loanMonthCount = loanMonthCount;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getPkSeq() {
        return pkSeq;
    }

    public void setPkSeq(String pkSeq) {
        this.pkSeq = pkSeq;
    }

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRePaymentMonthCount() {
        return rePaymentMonthCount;
    }

    public void setRePaymentMonthCount(String rePaymentMonthCount) {
        this.rePaymentMonthCount = rePaymentMonthCount;
    }

    public String getRePaymentScheduledAmount() {
        return rePaymentScheduledAmount;
    }

    public void setRePaymentScheduledAmount(String rePaymentScheduledAmount) {
        this.rePaymentScheduledAmount = rePaymentScheduledAmount;
    }

    public String getRePaymentType() {
        return rePaymentType;
    }

    public void setRePaymentType(String rePaymentType) {
        this.rePaymentType = rePaymentType;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getRelatedFirstName() {
        return relatedFirstName;
    }

    public void setRelatedFirstName(String relatedFirstName) {
        this.relatedFirstName = relatedFirstName;
    }

    public String getRelatedHandPhone() {
        return relatedHandPhone;
    }

    public void setRelatedHandPhone(String relatedHandPhone) {
        this.relatedHandPhone = relatedHandPhone;
    }

    public String getRelatedLastName() {
        return relatedLastName;
    }

    public void setRelatedLastName(String relatedLastName) {
        this.relatedLastName = relatedLastName;
    }

    public String getRelatedRegNum() {
        return relatedRegNum;
    }

    public void setRelatedRegNum(String relatedRegNum) {
        this.relatedRegNum = relatedRegNum;
    }

    public String getRelatedWhoIs() {
        return relatedWhoIs;
    }

    public void setRelatedWhoIs(String relatedWhoIs) {
        this.relatedWhoIs = relatedWhoIs;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getToAccName() {
        return toAccName;
    }

    public void setToAccName(String toAccName) {
        this.toAccName = toAccName;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getToBank() {
        return toBank;
    }

    public void setToBank(String toBank) {
        this.toBank = toBank;
    }

    public String getOfferLimit() {
        return offerLimit;
    }

    public void setOfferLimit(String offerLimit) {
        this.offerLimit = offerLimit;
    }
}
