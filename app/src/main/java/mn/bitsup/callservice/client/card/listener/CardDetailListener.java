package mn.bitsup.callservice.client.card.listener;


import mn.bitsup.callservice.client.card.dto.CardDetail;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface CardDetailListener {
    void onSuccess(CardDetail var1);
    void onError(Response var1);
}
