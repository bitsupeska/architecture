package mn.bitsup.callservice.client.dataprovider.data;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface StatusListener {
    void onSuccess(StatusResponse var1);
    void onError(Response var2);
}
