package mn.bitsup.callservice.client.loan.dto;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoanSalary {
    int code;
    String status;
    String message;

    @SerializedName("data")
    @Expose
    private LoanSalaryData data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoanSalaryData getData() {
        return data;
    }

    public void setData(LoanSalaryData data) {
        this.data = data;
    }
}
