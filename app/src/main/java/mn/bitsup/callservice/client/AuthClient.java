package mn.bitsup.callservice.client;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.apptakk.http_request.HttpRequest;
import com.apptakk.http_request.HttpRequestTask;

import mn.bitsup.callservice.MainApplication;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.listener.PasswordAuthListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.util.DeviceAddressUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class AuthClient extends RemoteAuthClient {

    private static String LOGIN_ROOT = "";
    private static String ENDPOINT_LOGIN = "/auth/login";
    static final String REFRESH_TOKEN = "refresh_token";
    static final String EXPIRES_IN = "expires_in";
    static final String TOKEN_TYPE = "token_type";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String AUTH_HEADER = "Authorization";
    private static final String UID = "uid";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String CONTENT_TYPE_VALUE = "application/json";
    private StorageComponent storageComponent;
    private Context context;

    public AuthClient(Context context) {
        super(context);
        this.context = context;
        this.storageComponent = new StorageComponent();
        String ENDPOINT_ROOT = MainApplication.getServerURL();
        LOGIN_ROOT = ENDPOINT_ROOT + ENDPOINT_LOGIN;
    }

    @Override
    public void authenticate(final char[] userId,
                             final char[] password,
                             final Map<String, String> headers,
                             final Map<String, String> bodyParameters,
                             final PasswordAuthListener passwordAuthListener,
                             final String... authTokenNames) {
        login(userId, password, headers, bodyParameters, passwordAuthListener);
    }


    private void login(final char[] userId,
                       final char[] password,
                       Map<String, String> headers,
                       Map<String, String> bodyParameters,
                       final PasswordAuthListener passwordAuthListener) {

        Map<String, String> loginHeaders = getLoginHeaders();
        String BODY_PARAMETERS = "{\"username\":\"%s\",\"password\":\"%s\",\"macAddress\":\"%s\",\"ipAddress\":\"%s\",\"firebaseToken\":\"%s\"}";
        String firebaseToken = onTokenFirebase();

        Log.e("AuthClient", "Firebase token: " + firebaseToken);

        String macaddress = DeviceAddressUtil.getMACAddress(context,"wlan0");
        String ipaddress = DeviceAddressUtil.getIPAddress(true);
        String body = String.format(BODY_PARAMETERS, new String(userId).toLowerCase(), new String(password), macaddress, ipaddress, firebaseToken);

        try {
            new HttpRequestTask(
                    new HttpRequest(LOGIN_ROOT, HttpRequest.POST, body),
                    response -> {
                        if (response.code == 200) {
                            Log.e("login", "response: " + response.body);
                            try {
                                storeUserData(new String(userId), new JSONObject(response.body));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            passwordAuthListener.onAuthSuccess(null);
                        } else {
                            passwordAuthListener
                                    .onAuthError(new Response(response.body, response.code));
                        }
                    }).execute();
        } catch (Exception e) {
            passwordAuthListener.onAuthError(new Response("Алдаа гарлаа", 405));
        }
    }

    @NonNull
    private Map<String, String> getLoginHeaders() {
        Map<String, String> loginHeaders = new HashMap<>();
        loginHeaders.put(CONTENT_TYPE, CONTENT_TYPE_VALUE);

        return loginHeaders;
    }

    private void storeUserData(String uid, JSONObject obj) throws JSONException {
        storageComponent.setItem(UID, uid);
        storageComponent.setItem(REFRESH_TOKEN, obj.getString(REFRESH_TOKEN));
        storageComponent.setItem(ACCESS_TOKEN, obj.getString(ACCESS_TOKEN));
        storageComponent.setItem(EXPIRES_IN, obj.getString(EXPIRES_IN));
        storageComponent.setItem(TOKEN_TYPE, obj.getString(TOKEN_TYPE));
    }


    @Override
    public void performAutomatedLogin(final PasscodeAuthListener passcodeAuthListener) {
        super.performAutomatedLogin(passcodeAuthListener);
    }

    @Override
    public boolean isEnrolled() {
        return storageComponent.getItem(KEY_PASSCODE_HASH) != null;
    }

    @Override
    public void endSession() {
        super.endSession();
        storageComponent.removeItem(REFRESH_TOKEN);
        storageComponent.removeItem(ACCESS_TOKEN);
        storageComponent.removeItem(EXPIRES_IN);
        storageComponent.removeItem(UID);
        storageComponent.removeItem(TOKEN_TYPE);
        storageComponent.removeItem("user");
        storageComponent.removeItem("passcode");
    }

    private String onTokenFirebase() {

        String firebaseToken = null;
//        FirebaseInstanceId.getInstance().getInstanceId()
//                .addOnCompleteListener(task -> {
//                    if (!task.isSuccessful()) {
//                        return;
//                    }
//                    String token = task.getResult().getToken();
//                });
//        firebaseToken = FirebaseInstanceId.getInstance().getToken();
        return firebaseToken;
    }

}
