package mn.bitsup.callservice.client.loan;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.loan.dto.LoanAccount;
import mn.bitsup.callservice.client.loan.dto.LoanIsAllow;
import mn.bitsup.callservice.client.loan.dto.SysDate;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.client.loan.dto.LoanFindAmortSchedule;

import mn.bitsup.callservice.client.loan.dto.LoanCreateInvoice;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.dto.LoanRelated;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.client.loan.dto.LoanSalary;

public class DefaultLoanParser implements LoanParser {

    public DefaultLoanParser() {
    }

    @Override
    public StatusResponse parseResponse(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, StatusResponse.class);
    }

    @Override
    public LoanOffer parseLoanOffer(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LoanOffer.class);
    }

    @Override
    public LoanAccount parseLoanAccount(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LoanAccount.class);
    }

    @Override
    public List<LoanDueScheduleData> parseDoScheduleData(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type itemsMapType = new TypeToken<List<LoanDueScheduleData>>() {
        }.getType();
        return new Gson().fromJson(jsonArray, itemsMapType);
    }

    @Override
    public LoanFindAmortSchedule parseFindAmortSchedule(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LoanFindAmortSchedule.class);
    }

    @Override
    public LoanCreateInvoice parseCreateInvoice(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONObject("data").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type itemsMapType = new TypeToken<LoanCreateInvoice>() {
        }.getType();
        return new Gson().fromJson(jsonArray, itemsMapType);
    }

    @Override
    public SysDate parseSysDate(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, SysDate.class);
    }

    @Override
    public LoanIsAllow parseLoanIsAllow(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LoanIsAllow.class);
    }

    @Override
    public List<TemporaryItems> parseRels(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type itemsMapType = new TypeToken<List<TemporaryItems>>() {
        }.getType();
        return new Gson().fromJson(jsonArray, itemsMapType);
    }

    @Override
    public LoanRelated parseCreateRelation(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LoanRelated.class);
    }

    @Override
    public LoanSalary parseLoanSalary(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, LoanSalary.class);
    }
}
