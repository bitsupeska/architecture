package mn.bitsup.callservice.client.card.parser;

import java.util.List;
import mn.bitsup.callservice.client.card.dto.CardDetail;
import mn.bitsup.callservice.client.card.dto.CardReasonItem;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;

public interface CardParser {
    List<CardReasonItem> parseCardReasonItem(String json);
    StatusResponse statusParse(String var1);
    CardDetail cardDetailParse(String var1);
}
