package mn.bitsup.callservice.client.call;

import android.telecom.Call;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.call.data.CallHistoryData;
import mn.bitsup.callservice.client.call.data.CallHistoryDataImp;
import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.client.call.listener.CallHistoryListener;
import mn.bitsup.callservice.client.call.parser.CallHistoryParser;
import mn.bitsup.callservice.client.call.parser.DefaultCallHistoryParser;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public class CallClient {
    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private CallHistoryData callHistoryData;

    public CallClient(URI baseUri) {
        this((DBSDataProvider) null, baseUri, new DefaultCallHistoryParser());
    }

    public CallClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultCallHistoryParser());
    }

    public CallClient(DBSDataProvider dbsDataProvider, URI baseUri, CallHistoryParser callHistoryParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.callHistoryData = new CallHistoryDataImp(callHistoryParser);
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getHistories(Map<String, String> params, final CallHistoryListener callHistoryListener) {
//        List<CallHistory> datas = new ArrayList<>();
//        for(int i=0;i<30;i++){
//            CallHistory data = new CallHistory();
//            data.setId(String.valueOf(i+1));
//            data.setConnectionDate("connected - " + 1);
//            datas.add(data);
//        }
//        callHistoryListener.onSuccess(datas);
        try {
            URI historiesUri = new URI(this.baseUri + "/books");
            this.callHistoryData.getHistories(historiesUri, this.dbsDataProvider, params, new CallHistoryListener() {

                @Override
                public void onSuccess(List<CallHistory> var1) {
                    callHistoryListener.onSuccess(var1);
                }

                @Override
                public void onError(Response var1) {
                    callHistoryListener.onError(var1);
                }
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }
}
