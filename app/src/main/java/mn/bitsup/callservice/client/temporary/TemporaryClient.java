package mn.bitsup.callservice.client.temporary;

import java.net.URI;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.temporary.data.TemporaryData;
import mn.bitsup.callservice.client.temporary.data.TemporaryDataImpl;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.dto.TemporaryCif;
import mn.bitsup.callservice.client.temporary.listener.EducationListener;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;

public class TemporaryClient implements DBSClient {

    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private TemporaryData temporaryData;

    public TemporaryClient(URI baseUri) {
        this((DBSDataProvider)null, baseUri, new DefaultTemporaryParser());
    }

    public TemporaryClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultTemporaryParser());
    }

    public TemporaryClient(DBSDataProvider dbsDataProvider, URI baseUri, TemporaryParser temporaryParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.temporaryData = new TemporaryDataImpl(temporaryParser);
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }


    public void getKhurData(Map<String, String> params, final KhurDataListener listener) {
        this.temporaryData.getKhurData(this.baseUri, this.dbsDataProvider, params, listener);
    }


    public void getEmpstatlookup(TemporaryItemsListener listener){
        this.temporaryData.getEmpstatlookup(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getOccstatlookup(TemporaryItemsListener listener){
        this.temporaryData.getOccstatlookup(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getWorkstatlookup(TemporaryItemsListener listener){
        this.temporaryData.getWorkstatlookup(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getRelstatlookup(TemporaryItemsListener listener){
        this.temporaryData.getRelstatlookup(this.baseUri, this.dbsDataProvider, listener);
    }

    public void checkEmail(StatusListener listener, String param){
        this.temporaryData.checkEmail(this.baseUri, this.dbsDataProvider, listener, param);
    }

    public void createCif(StatusListener listener, TemporaryCif param){
        this.temporaryData.createCif(this.baseUri, this.dbsDataProvider, listener, param);
    }

    public void updateCif(StatusListener listener, Temporary param){
        this.temporaryData.updateCif(this.baseUri, this.dbsDataProvider, listener, param);
    }

    public void createCA(StatusListener listener, Temporary param){
        this.temporaryData.createCA(this.baseUri, this.dbsDataProvider, listener, param);
    }

    public void getEduType(EducationListener listener){
        this.temporaryData.getEduType(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getXyrData(Map<String, String> params, final KhurDataListener listener) {
        this.temporaryData.getXyrData(this.baseUri, this.dbsDataProvider, params, listener);
    }
}
