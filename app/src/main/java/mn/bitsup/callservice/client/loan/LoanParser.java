package mn.bitsup.callservice.client.loan;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.loan.dto.LoanAccount;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.client.loan.dto.LoanFindAmortSchedule;
import mn.bitsup.callservice.client.loan.dto.LoanCreateInvoice;
import mn.bitsup.callservice.client.loan.dto.LoanIsAllow;
import mn.bitsup.callservice.client.loan.dto.LoanSalary;

import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.dto.LoanRelated;
import mn.bitsup.callservice.client.loan.dto.SysDate;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;

public interface LoanParser {
    StatusResponse parseResponse(String var1);
    LoanOffer parseLoanOffer(String var1);
    List<TemporaryItems> parseRels(String var1);
    LoanRelated parseCreateRelation(String var1);
    LoanSalary parseLoanSalary(String var1);
    LoanAccount parseLoanAccount(String var1);
    List<LoanDueScheduleData> parseDoScheduleData(String var1);
    LoanFindAmortSchedule parseFindAmortSchedule(String var1);
    LoanCreateInvoice parseCreateInvoice(String var1);
    SysDate parseSysDate(String var1);
    LoanIsAllow parseLoanIsAllow(String var1);
}
