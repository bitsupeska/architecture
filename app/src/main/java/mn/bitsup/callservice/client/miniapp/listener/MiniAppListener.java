package mn.bitsup.callservice.client.miniapp.listener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.miniapp.dto.MiniApp;

public interface MiniAppListener {
    void onSuccess(List<MiniApp> var1);

    void onError(Response var1);
}
