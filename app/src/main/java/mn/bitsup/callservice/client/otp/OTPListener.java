package mn.bitsup.callservice.client.otp;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;

public interface OTPListener {

    void onSuccess(StatusResponse var1);
    void onError(Response var1);
}
