package mn.bitsup.callservice.client.otp;

import java.net.URI;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.otp.data.OTPData;
import mn.bitsup.callservice.client.otp.data.OTPDataImpl;

public class OTPClient implements DBSClient {
    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private OTPData otpData;
    private StatusResponse otp;

    public OTPClient(URI baseUri) {
        this((DBSDataProvider)null, baseUri);
    }

    public OTPClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this.baseUri=baseUri;
        this.dbsDataProvider=dbsDataProvider;
        this.otpData = new OTPDataImpl();
    }

    @Override
    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    @Override
    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    @Override
    public URI getBaseURI() {
        return this.baseUri;
    }

    @Override
    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getOTP(Map<String, String> params, final OTPListener otpListener) {
        this.otpData.getOTP(this.baseUri, this.dbsDataProvider, params, new OTPListener() {
            public void onSuccess(StatusResponse otp) {
                otpListener.onSuccess(otp);
            }
            public void onError(Response errorResponse) {
                otpListener.onError(errorResponse);
            }
        });
    }

    public void checkOTP(Map<String, String> params, final OTPListener otpListener) {
        this.otpData.checkOTP(this.baseUri, this.dbsDataProvider, params, new OTPListener() {
            public void onSuccess(StatusResponse otp) {
                otpListener.onSuccess(otp);
            }
            public void onError(Response errorResponse) {
                otpListener.onError(errorResponse);
            }
        });
    }

    public void getEmailOTP(Map<String, String> params, final OTPListener otpListener) {
        this.otpData.getEmailOTP(this.baseUri, this.dbsDataProvider, params, new OTPListener() {
            public void onSuccess(StatusResponse otp) {
                otpListener.onSuccess(otp);
            }
            public void onError(Response errorResponse) {
                otpListener.onError(errorResponse);
            }
        });
    }

    public void checkEmailOTP(Map<String, String> params, final OTPListener otpListener) {
        this.otpData.checkEmailOTP(this.baseUri, this.dbsDataProvider, params, new OTPListener() {
            public void onSuccess(StatusResponse otp) {
                otpListener.onSuccess(otp);
            }
            public void onError(Response errorResponse) {

                otpListener.onError(errorResponse);
            }
        });
    }

}
