package mn.bitsup.callservice.client.loan.dto;

import java.util.Arrays;

public class LoanOfferData {

    private int prodMaxLimit;
    private int prodMinLimit;
    private int prodMaxDur;
    private double monPayAmtMbank;
    private double monPayAmtMbankPrevious;
    private double totalDisbursedAmountMbank;
    private double remainingLimit;
    private LoanReasonItem[] rejectReasons;
    private int pk_seq;
    private String pk_product_cd;
    private float final_limit;
    private double final_rate;
    private double dti_amount;
    private double dti;
    private String decision_yn;
    private String last_chng_dtm;
    private String max_tenor;
    private String max_available_payment;
    private String multiplier;
    private double max_limit;
    private Boolean IsXyp;
    private Boolean IsPCR;
    private Boolean isDownload;
    private Boolean IsLoanCount;
    private Boolean IsCRDP;
    private Boolean IsOffer;
    private int downloadEndYear;
    private int downloadBeginYear;
    private CoreFee coreFee;

    public LoanOfferData(int prodMaxLimit, int prodMinLimit, int prodMaxDur, double monPayAmtMbank,
                         double monPayAmtMbankPrevious, double totalDisbursedAmountMbank, double remainingLimit,
                         LoanReasonItem[] rejectReasons, int pk_seq, String pk_product_cd, float final_limit,
                         double final_rate,
                         double dti_amount, double dti, String decision_yn, String last_chng_dtm,
                         String max_tenor, String max_available_payment, String multiplier, double max_limit) {
        this.prodMaxLimit = prodMaxLimit;
        this.prodMinLimit = prodMinLimit;
        this.prodMaxDur = prodMaxDur;
        this.monPayAmtMbank = monPayAmtMbank;
        this.monPayAmtMbankPrevious = monPayAmtMbankPrevious;
        this.totalDisbursedAmountMbank = totalDisbursedAmountMbank;
        this.remainingLimit = remainingLimit;
        this.rejectReasons = rejectReasons;
        this.pk_seq = pk_seq;
        this.pk_product_cd = pk_product_cd;
        this.final_limit = final_limit;
        this.final_rate = final_rate;
        this.dti_amount = dti_amount;
        this.dti = dti;
        this.decision_yn = decision_yn;
        this.last_chng_dtm = last_chng_dtm;
        this.max_tenor = max_tenor;
        this.max_available_payment = max_available_payment;
        this.multiplier = multiplier;
        this.max_limit = max_limit;
    }

    public int getProdMaxLimit() {
        return prodMaxLimit;
    }

    public void setProdMaxLimit(int prodMaxLimit) {
        this.prodMaxLimit = prodMaxLimit;
    }

    public int getProdMinLimit() {
        return prodMinLimit;
    }

    public void setProdMinLimit(int prodMinLimit) {
        this.prodMinLimit = prodMinLimit;
    }

    public int getProdMaxDur() {
        return prodMaxDur;
    }

    public void setProdMaxDur(int prodMaxDur) {
        this.prodMaxDur = prodMaxDur;
    }

    public double getMonPayAmtMbank() {
        return monPayAmtMbank;
    }

    public void setMonPayAmtMbank(double monPayAmtMbank) {
        this.monPayAmtMbank = monPayAmtMbank;
    }

    public double getMonPayAmtMbankPrevious() {
        return monPayAmtMbankPrevious;
    }

    public void setMonPayAmtMbankPrevious(double monPayAmtMbankPrevious) {
        this.monPayAmtMbankPrevious = monPayAmtMbankPrevious;
    }

    public double getTotalDisbursedAmountMbank() {
        return totalDisbursedAmountMbank;
    }

    public void setTotalDisbursedAmountMbank(double totalDisbursedAmountMbank) {
        this.totalDisbursedAmountMbank = totalDisbursedAmountMbank;
    }

    public double getRemainingLimit() {
        return remainingLimit;
    }

    public void setRemainingLimit(double remainingLimit) {
        this.remainingLimit = remainingLimit;
    }

    public LoanReasonItem[] getRejectReasons() {
        return rejectReasons;
    }

    public void setRejectReasons(LoanReasonItem[] rejectReasons) {
        this.rejectReasons = rejectReasons;
    }

    public int getPk_seq() {
        return pk_seq;
    }

    public void setPk_seq(int pk_seq) {
        this.pk_seq = pk_seq;
    }

    public String getPk_product_cd() {
        return pk_product_cd;
    }

    public void setPk_product_cd(String pk_product_cd) {
        this.pk_product_cd = pk_product_cd;
    }

    public float getFinal_limit() {
        return final_limit;
    }

    public void setFinal_limit(float final_limit) {
        this.final_limit = final_limit;
    }

    public double getFinal_rate() {
        return final_rate;
    }

    public void setFinal_rate(double final_rate) {
        this.final_rate = final_rate;
    }

    public double getDti_amount() {
        return dti_amount;
    }

    public void setDti_amount(double dti_amount) {
        this.dti_amount = dti_amount;
    }

    public double getDti() {
        return dti;
    }

    public void setDti(double dti) {
        this.dti = dti;
    }

    public String getDecision_yn() {
        return decision_yn;
    }

    public void setDecision_yn(String decision_yn) {
        this.decision_yn = decision_yn;
    }

    public String getLast_chng_dtm() {
        return last_chng_dtm;
    }

    public void setLast_chng_dtm(String last_chng_dtm) {
        this.last_chng_dtm = last_chng_dtm;
    }

    public String getMax_tenor() {
        return max_tenor;
    }

    public void setMax_tenor(String max_tenor) {
        this.max_tenor = max_tenor;
    }

    public String getMax_available_payment() {
        return max_available_payment;
    }

    public void setMax_available_payment(String max_available_payment) {
        this.max_available_payment = max_available_payment;
    }

    public String getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(String multiplier) {
        this.multiplier = multiplier;
    }

    public double getMax_limit() {
        return max_limit;
    }

    public void setMax_limit(double max_limit) {
        this.max_limit = max_limit;
    }

    public Boolean getXyp() {
        return IsXyp;
    }

    public void setXyp(Boolean xyp) {
        IsXyp = xyp;
    }

    public Boolean getPCR() {
        return IsPCR;
    }

    public void setPCR(Boolean PCR) {
        IsPCR = PCR;
    }

    public Boolean getDownload() {
        return isDownload;
    }

    public void setDownload(Boolean download) {
        isDownload = download;
    }

    public Boolean getLoanCount() {
        return IsLoanCount;
    }

    public void setLoanCount(Boolean loanCount) {
        IsLoanCount = loanCount;
    }

    public Boolean getCRDP() {
        return IsCRDP;
    }

    public void setCRDP(Boolean CRDP) {
        IsCRDP = CRDP;
    }

    public Boolean getOffer() {
        return IsOffer;
    }

    public void setOffer(Boolean offer) {
        IsOffer = offer;
    }

    public int getDownloadEndYear() {
        return downloadEndYear;
    }

    public void setDownloadEndYear(int downloadEndYear) {
        this.downloadEndYear = downloadEndYear;
    }

    public int getDownloadBeginYear() {
        return downloadBeginYear;
    }

    public void setDownloadBeginYear(int downloadBeginYear) {
        this.downloadBeginYear = downloadBeginYear;
    }

    public CoreFee getCoreFee() {
        return coreFee;
    }

    public void setCoreFee(CoreFee coreFee) {
        this.coreFee = coreFee;
    }

    @Override
    public String toString() {
        return "LoanOfferData{" +
                "prodMaxLimit=" + prodMaxLimit +
                ", prodMinLimit=" + prodMinLimit +
                ", prodMaxDur=" + prodMaxDur +
                ", monPayAmtMbank=" + monPayAmtMbank +
                ", monPayAmtMbankPrevious=" + monPayAmtMbankPrevious +
                ", totalDisbursedAmountMbank=" + totalDisbursedAmountMbank +
                ", remainingLimit=" + remainingLimit +
                ", rejectReasons=" + Arrays.toString(rejectReasons) +
                ", pk_seq=" + pk_seq +
                ", pk_product_cd='" + pk_product_cd + '\'' +
                ", final_limit=" + final_limit +
                ", final_rate=" + final_rate +
                ", dti_amount=" + dti_amount +
                ", dti=" + dti +
                ", decision_yn='" + decision_yn + '\'' +
                ", last_chng_dtm='" + last_chng_dtm + '\'' +
                ", max_tenor='" + max_tenor + '\'' +
                ", max_available_payment='" + max_available_payment + '\'' +
                ", multiplier='" + multiplier + '\'' +
                ", max_limit=" + max_limit +
                ", IsXyp=" + IsXyp +
                ", IsPCR=" + IsPCR +
                ", isDownload=" + isDownload +
                ", IsLoanCount=" + IsLoanCount +
                ", IsCRDP=" + IsCRDP +
                ", IsOffer=" + IsOffer +
                ", downloadEndYear=" + downloadEndYear +
                ", downloadBeginYear=" + downloadBeginYear +
                ", coreFee=" + coreFee +
                '}';
    }
}
