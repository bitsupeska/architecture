package mn.bitsup.callservice.client.info.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;

public interface CitizenInfoListener {
    void onSuccess(CitizenInfo var1);
    void onError(Response var1);
}
