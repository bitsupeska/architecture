package mn.bitsup.callservice.client.loan.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

public class LoanAccount {
    int code;
    String status;
    String message;

    @SerializedName("items")
    @Expose
    private List<LoanAccountData> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LoanAccountData> getData() {
        return data;
    }

    public void setData(List<LoanAccountData> data) {
        this.data = data;
    }
}
