package mn.bitsup.callservice.client.offline;

import android.util.Log;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.offline.data.OfflineData;
import mn.bitsup.callservice.client.offline.data.OfflineDataImpl;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.offline.dto.ResetInfo;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.CheckUserNameListener;
import mn.bitsup.callservice.client.offline.listener.ResetInfoListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;

public class OfflineClient implements DBSClient {

    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private OfflineData offlineData;

    public OfflineClient(URI baseUri) {
        this((DBSDataProvider) null, baseUri, new DefaultOfflineParser());
    }

    public OfflineClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultOfflineParser());
    }

    private OfflineClient(DBSDataProvider dbsDataProvider, URI baseUri, OfflineParser offlineParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.offlineData = new OfflineDataImpl(offlineParser);
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void convertToken(final TokenListener listener, HashMap<String, String> param) {
        this.offlineData.getConvertToken(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void checkRegister(Map<String, String> params, final StatusListener listener) {
        this.offlineData.checkRegister(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void checkBaml(Map<String, String> params, final StatusListener listener) {
        this.offlineData.checkBaml(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void requestPCR(Map<String, String> params, final StatusListener listener) {
        this.offlineData.requestPCR(this.baseUri, this.dbsDataProvider, params, listener);
    }


    public void checkUsername(Map<String, String> params, final CheckUserNameListener listener) {
        this.offlineData.checkUsername(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void createUser(EasyOnboardItem params, final StatusListener listener) {
        this.offlineData.createUser(this.baseUri, this.dbsDataProvider, params, listener);
    }

    public void checkBlock(BlockListener listener, HashMap<String, String> param) {
        Log.e("checkBlock", "1");
        this.offlineData.checkBlock(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void checkBlockIncrement(BlockIncrementListener listener, HashMap<String, String> param) {
        this.offlineData.blockIncrement(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void setBlock(StatusListener listener, HashMap<String, String> param) {
        this.offlineData.setBlock(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void getResetInfo(final ResetInfoListener listener, HashMap<String, String> param) {
        this.offlineData.getResetInfo(this.baseUri, this.dbsDataProvider, param, new ResetInfoListener() {
            public void onSuccess(ResetInfo resetInfo) {
                listener.onSuccess(resetInfo);
            }

            public void onError(Response errorResponse) {
                listener.onError(errorResponse);
            }
        });
    }
    public void resetPassword(final StatusListener listener, HashMap<String, String> param) {
        this.offlineData.getResetPassword(this.baseUri, this.dbsDataProvider, param, new StatusListener() {
            public void onSuccess(StatusResponse response) {
                listener.onSuccess(response);
            }
            public void onError(Response errorResponse) {
                listener.onError(errorResponse);
            }
        });
    }
}
