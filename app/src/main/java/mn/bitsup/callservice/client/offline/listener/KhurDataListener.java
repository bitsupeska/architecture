package mn.bitsup.callservice.client.offline.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.KhurItems;

public interface KhurDataListener {
    void onSuccess(KhurItems var1);
    void onError(Response var1);
}
