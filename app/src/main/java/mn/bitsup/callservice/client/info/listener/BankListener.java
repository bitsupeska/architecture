package mn.bitsup.callservice.client.info.listener;

import java.util.List;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;

public interface BankListener {
    void onSuccess(List<BankItem> var1);
    void onError(Response var1);
}
