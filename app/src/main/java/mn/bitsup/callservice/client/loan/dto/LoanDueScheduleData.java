package mn.bitsup.callservice.client.loan.dto;

public class LoanDueScheduleData {


    private String flowDate;
    private String instlAmt;
    private String intAmt;
    private String princAmt;
    private String princOutStanding;
    private String serialNum;


    public LoanDueScheduleData(){

    }

    public LoanDueScheduleData(String flowDate, String instlAmt, String intAmt,
        String princAmt, String princOutStanding, String serialNum) {
        this.flowDate = flowDate;
        this.instlAmt = instlAmt;
        this.intAmt = intAmt;
        this.princAmt = princAmt;
        this.princOutStanding = princOutStanding;
        this.serialNum = serialNum;
    }

    public String getFlowDate() {
        return flowDate;
    }

    public void setFlowDate(String flowDate) {
        this.flowDate = flowDate;
    }

    public String getInstlAmt() {
        return instlAmt;
    }

    public void setInstlAmt(String instlAmt) {
        this.instlAmt = instlAmt;
    }

    public String getIntAmt() {
        return intAmt;
    }

    public void setIntAmt(String intAmt) {
        this.intAmt = intAmt;
    }

    public String getPrincAmt() {
        return princAmt;
    }

    public void setPrincAmt(String princAmt) {
        this.princAmt = princAmt;
    }

    public String getPrincOutStanding() {
        return princOutStanding;
    }

    public void setPrincOutStanding(String princOutStanding) {
        this.princOutStanding = princOutStanding;
    }

    public String getSerialNum() {
        return serialNum;
    }

    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    @Override
    public String toString() {
        return "LoanDueScheduleData{" +
            "flowDate='" + flowDate + '\'' +
            ", instlAmt='" + instlAmt + '\'' +
            ", intAmt='" + intAmt + '\'' +
            ", princAmt='" + princAmt + '\'' +
            ", princOutStanding='" + princOutStanding + '\'' +
            ", serialNum='" + serialNum + '\'' +
            '}';
    }
}
