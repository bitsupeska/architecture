package mn.bitsup.callservice.client.profile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.profile.dto.Questions;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

import org.json.JSONException;
import org.json.JSONObject;

public class DefaultProfileParser implements ProfileParser {
    @Override
    public User parseUser(String json) {
        User user = null;
        String jsonObj;
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESSFUL")) {
                jsonObj = obj.getJSONObject("items").toString();
                Type itemsMapType = new TypeToken<User>() {
                }.getType();
                user = new Gson().fromJson(jsonObj, itemsMapType);

                user.setCode(obj.getString("code"));
                user.setStatus(obj.getString("status"));
                user.setMessage(obj.getString("message"));

            } else {
                user = new User();
                user.setCode(obj.getString("code"));
                user.setStatus(obj.getString("status"));
                user.setMessage(obj.getString("message"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    @Override
    public StatusResponse parseStatus(String json) {
        return new Gson().fromJson(json, StatusResponse.class);
    }

    @Override
    public List<Questions> parseQuestions(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("items").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<Questions>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }

    @Override
    public List parseAccounts(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("items").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<Account>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }
}
