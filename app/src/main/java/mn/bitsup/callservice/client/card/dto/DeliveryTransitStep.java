package mn.bitsup.callservice.client.card.dto;


import android.os.Parcel;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import java.util.Objects;

public class DeliveryTransitStep extends Additions {
    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String STATUS_FAILED = "FAILED";
    public static final String STATUS_PENDING = "PENDING";
    public static final String KEY_NAME = "name";
    public static final String KEY_STATUS = "status";
    public static final String KEY_DATE_TIME = "stepDateTime";
    public static final Creator<DeliveryTransitStep> CREATOR = new Creator<DeliveryTransitStep>() {
        public DeliveryTransitStep createFromParcel(Parcel var1) {
            return new DeliveryTransitStep(var1);
        }

        public DeliveryTransitStep[] newArray(int var1) {
            return new DeliveryTransitStep[var1];
        }
    };
    @SerializedName("name")
    @NonNull
    private String name;
    @SerializedName("status")
    @NonNull
    private DeliveryTransitStepStatus status;
    @SerializedName("stepDateTime")
    @Nullable
    private Date stepDateTime;

    public DeliveryTransitStep(@NonNull String var1, @NonNull DeliveryTransitStepStatus var2, @Nullable Date var3) {
        this.name = var1;
        this.status = var2;
        this.stepDateTime = var3;
    }

    protected DeliveryTransitStep(Parcel var1) {
        super(var1);
        this.name = (String) Objects.requireNonNull(var1.readString());
        this.status = DeliveryTransitStepStatus.valueOf(var1.readString());
        this.stepDateTime = (Date)var1.readSerializable();
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public DeliveryTransitStepStatus getStatus() {
        return this.status;
    }

    @Nullable
    public Date getStepDateTime() {
        return this.stepDateTime;
    }

    public void writeToParcel(Parcel var1, int var2) {
        super.writeToParcel(var1, var2);
        var1.writeString(this.name);
        var1.writeString(this.status.name());
        var1.writeSerializable(this.stepDateTime);
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 != null && DeliveryTransitStep.class == var1.getClass()) {
            if (!super.equals(var1)) {
                return false;
            } else {
                DeliveryTransitStep var2 = (DeliveryTransitStep)var1;
                return this.getName().equals(var2.getName()) && this.getStatus() == var2.getStatus() && Objects.equals(this.getStepDateTime(), var2.getStepDateTime());
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        Object[] var1;
        Object[] var10000 = var1 = new Object[4];
        var1[0] = super.hashCode();
        var1[1] = this.getName();
        var1[2] = this.getStatus();
        var10000[3] = this.getStepDateTime();
        return Objects.hash(var10000);
    }
}
