package mn.bitsup.callservice.client.temporary.listener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.temporary.dto.EducationItem;

public interface EducationListener {
    void onSuccess(List<EducationItem> var1);
    void onError(Response var1);
}
