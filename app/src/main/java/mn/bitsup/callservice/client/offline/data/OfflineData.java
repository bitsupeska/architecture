package mn.bitsup.callservice.client.offline.data;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.CheckUserNameListener;
import mn.bitsup.callservice.client.offline.listener.ResetInfoListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;

public interface OfflineData {
    void getConvertToken(URI var1, DBSDataProvider var2, Map<String, String> var3,
        TokenListener var4);
    void checkRegister(URI var1, DBSDataProvider var2, Map<String, String> var3,
        StatusListener var4);
    void checkBaml(URI var1, DBSDataProvider var2, Map<String, String> var3,
        StatusListener var4);
    void requestPCR(URI var1, DBSDataProvider var2, Map<String, String> var3,
        StatusListener var4);
    void checkUsername(URI var1, DBSDataProvider var2, Map<String, String> var3,
        CheckUserNameListener var4);

    void createUser(URI var1, DBSDataProvider var2, EasyOnboardItem var3, StatusListener var4);
    void checkBlock(URI var1, DBSDataProvider var2, Map<String, String> var3, BlockListener var4);
    void blockIncrement(URI var1, DBSDataProvider var2, Map<String, String> var3,
        BlockIncrementListener var4);
    void setBlock(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);
    void getResetInfo(URI var1, DBSDataProvider var2, Map<String, String> var3,
        ResetInfoListener var4);
    void getResetPassword(URI baseUri, DBSDataProvider dbsDataProvider,
        HashMap<String, String> param, StatusListener statusListener);
}
