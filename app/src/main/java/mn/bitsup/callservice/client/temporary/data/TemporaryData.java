package mn.bitsup.callservice.client.temporary.data;

import java.net.URI;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.dto.TemporaryCif;
import mn.bitsup.callservice.client.temporary.listener.EducationListener;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;

public interface TemporaryData {
    void getKhurData(URI var1, DBSDataProvider var2, Map<String, String> var3, KhurDataListener var4);
    void getEmpstatlookup(URI var1, DBSDataProvider var2, TemporaryItemsListener var4);
    void getOccstatlookup(URI var1, DBSDataProvider var2, TemporaryItemsListener var4);
    void getWorkstatlookup(URI var1, DBSDataProvider var2, TemporaryItemsListener var4);
    void getRelstatlookup(URI var1, DBSDataProvider var2, TemporaryItemsListener var4);
    void checkEmail(URI var1, DBSDataProvider var2, StatusListener var3, String var4);
    void createCif(URI var1, DBSDataProvider var2, StatusListener var3, TemporaryCif var4);
    void updateCif(URI var1, DBSDataProvider var2, StatusListener var3, Temporary var4);
    void createCA(URI var1, DBSDataProvider var2, StatusListener var3, Temporary var4);
    void getEduType(URI var1, DBSDataProvider var2, EducationListener var3);
    void getXyrData(URI var1, DBSDataProvider var2, Map<String, String> var3, KhurDataListener var4);
}
