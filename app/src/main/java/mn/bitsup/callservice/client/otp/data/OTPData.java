package mn.bitsup.callservice.client.otp.data;

import java.net.URI;
import java.util.Map;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.otp.OTPListener;

public interface OTPData {

    void getOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
        final OTPListener otpListener);
    void checkOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
        final OTPListener otpListener);
    void getEmailOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
        final OTPListener otpListener);
    void checkEmailOTP(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
        final OTPListener otpListener);

}
