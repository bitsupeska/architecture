package mn.bitsup.callservice.client.info.listener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.Item;

public interface BackgroundImageListener {
    void onSuccess(Item var1);
    void onError(Response var1);
}
