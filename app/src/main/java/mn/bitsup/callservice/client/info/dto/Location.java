package mn.bitsup.callservice.client.info.dto;

public class Location {
    private String locationText;
    private String locationValue;

    public Location() {
    }

    public String getLocationText() {
        return locationText;
    }

    public void setLocationText(String locationText) {
        this.locationText = locationText;
    }

    public String getLocationValue() {
        return locationValue;
    }

    public void setLocationValue(String locationValue) {
        this.locationValue = locationValue;
    }
}
