package mn.bitsup.callservice.client.offline.data;

import android.util.Log;
import androidx.annotation.NonNull;
import com.google.gson.Gson;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.offline.OfflineParser;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.CheckUsernameItems;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.offline.dto.ResetInfo;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.CheckUserNameListener;
import mn.bitsup.callservice.client.offline.listener.ResetInfoListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;

public class OfflineDataImpl implements OfflineData {

    private OfflineParser offlineParser;

    public OfflineDataImpl(OfflineParser offlineParser) {
        this.offlineParser = offlineParser;
    }

    @Override
    public void getConvertToken(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final TokenListener listener) {
        try {
            dataProvider.execute(this.buildRequestPostNotToken(baseUri, dataProvider, queryParams, "/auth/anonymous"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    Log.e("asd", "getConvertToken: "+response.toString());
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        String data = offlineParser.parseConvertToken(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }
                @Override
                public void onError(Response response) {

                    Log.e("asd", "getConvertTokenError: "+response.toString());

                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Log.e("asd", "getConvertToken: "+var7.toString() );
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void checkRegister(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, StatusListener listener) {
        try {
//            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkcore-service/v1.0/deDup"), new DBSDataProviderListener() {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-onboard-service/v1.0/check"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 400) {
                        StatusResponse data = offlineParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {

                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void checkBaml(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
        StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkcore-service/v1.0/baml"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 400) {
                        StatusResponse data = offlineParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {

                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void requestPCR(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
        StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-pcr-service/v1.0/pcr/request"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 400) {
                        StatusResponse data = offlineParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {

                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void checkBlock(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, BlockListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkblock-service/v1.0/checkBlock"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    Log.e("checkBlock", "2");
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        Block data = offlineParser.parseCheckBlock(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void blockIncrement(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                               BlockIncrementListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkblock-service/v1.0/block/increment"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        BlockIncrement data = offlineParser.parseBlockIncrement(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void setBlock(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams,
                         StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-checkblock-service/v1.0/block"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse data = offlineParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void checkUsername(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, CheckUserNameListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-ldap-service/v1.0/checkUserName"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode <= 400) {
                        CheckUsernameItems data = offlineParser.parseCheckUserName(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }


    @Override
    public void createUser(URI baseUri, DBSDataProvider dataProvider, EasyOnboardItem queryParams, final StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestCreate(baseUri, dataProvider, queryParams, "/be-internal-ldap-service/v1.0/createUser"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse data = offlineParser.parseStatus(response.getStringResponse());
                        if (data != null) {
                            listener.onSuccess(data);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT DOWNLOAD FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (URISyntaxException var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getResetInfo(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, final ResetInfoListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-ldap-service/v1.0/resetInfo"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        ResetInfo resetInfo = offlineParser.parseResetInfo(response.getStringResponse());
                        if (resetInfo != null) {
                            listener.onSuccess(resetInfo);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    @Override
    public void getResetPassword(URI baseUri, DBSDataProvider dataProvider, HashMap<String, String> queryParams, final StatusListener listener) {
        try {
            dataProvider.execute(this.buildRequestPost(baseUri, dataProvider, queryParams, "/be-internal-ldap-service/v1.0/resetPassword"), new DBSDataProviderListener() {
                @Override
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        StatusResponse user = offlineParser.parseStatus(response.getStringResponse());
                        if (user != null) {
                            listener.onSuccess(user);
                        } else {
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception var7) {
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getMessage());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        }
    }

    private Request buildRequest(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("GET");
        request.setUri(new URI(baseUri + param));
        request.getHeaders().put("Content-Type", "application/json");
        return request;
    }

    private Request buildRequestPostNotToken(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        request.setHeaders(headers);
        return request;
    }

    private Request buildRequestPost(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    private Request buildRequestCreate(URI baseUri, DBSDataProvider dataProvider, EasyOnboardItem queryParams, String param) throws URISyntaxException {
        Request request = new Request();
        request.setRequestMethod("POST");
        String url = baseUri.toString() + param;
        request.setUri(new URI(url));
        request.setBody((new Gson()).toJson(queryParams));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        Log.e("getAuthHeaders", "Token: " + storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
}
