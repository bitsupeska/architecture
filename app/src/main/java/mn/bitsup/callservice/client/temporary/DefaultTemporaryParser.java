package mn.bitsup.callservice.client.temporary;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.temporary.dto.EducationItem;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;

import org.json.JSONException;
import org.json.JSONObject;

public class DefaultTemporaryParser implements TemporaryParser {

    public DefaultTemporaryParser() {
    }

    @Override
    public KhurItems parseKhurData(String json) {
        KhurItems khurItems = null;
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                khurItems = new Gson().fromJson(obj.getJSONObject("data").toString(), KhurItems.class);
            } else {
                khurItems = new KhurItems();
            }

            khurItems.setCode(obj.getInt("code"));
            khurItems.setMessage(obj.getString("message"));
            khurItems.setStatus(obj.getString("status"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return khurItems;
    }

    @Override
    public List<TemporaryItems> parseTemporaryItems(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS"))
                jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<TemporaryItems>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }

    @Override
    public StatusResponse parseStatus(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, StatusResponse.class);
    }

    @Override
    public List<TemporaryItems> parseEmployeeStatus(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS"))
                jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<TemporaryItems>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }

    @Override
    public List<TemporaryItems> parseOccupationList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS"))
                jsonArray = obj.getJSONObject("items").getJSONArray("occupations").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<TemporaryItems>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }

    @Override
    public List<EducationItem> parseEduType(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            if (obj.get("status").equals("SUCCESS")) {
                jsonArray = obj.getJSONArray("items").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type listType = (new TypeToken<List<EducationItem>>() {
        }).getType();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        return (List) gson.fromJson(jsonArray, listType);
    }
}
