package mn.bitsup.callservice.client.info.dto;

import android.os.Parcel;
import android.os.Parcelable;
import mn.bitsup.callservice.client.dataprovider.data.Additions;

public class CurrencyItem extends Additions implements Parcelable {

    String ccy;
    String ccyname;
    String ccrate;
    String dsc;

    public static final Creator<CurrencyItem> CREATOR = new Creator<CurrencyItem>() {
        public CurrencyItem createFromParcel(Parcel source) {
            return new CurrencyItem(source);
        }
        public CurrencyItem[] newArray(int size) {
            return new CurrencyItem[size];
        }
    };

    public CurrencyItem(String ccy, String ccyname) {
        this.ccy = ccy;
        this.ccyname = ccyname;
        this.ccrate = ccrate;
        this.dsc = dsc;
    }

    public CurrencyItem(Parcel in) {
        this.ccy = in.readString();
        this.ccyname = in.readString();
        this.ccrate = in.readString();
        this.dsc = in.readString();
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ccy);
    }

    @Override
    public String toString() {
        return "CurrencyItem{" +
            "ccy='" + ccy + '\'' +
            ", ccyname='" + ccyname + '\'' +
            ", ccrate='" + ccrate + '\'' +
            ", dsc='" + dsc + '\'' +
            '}';
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getCcyname() {
        return ccyname;
    }

    public void setCcyname(String ccyname) {
        this.ccyname = ccyname;
    }

    public String getCcrate() {
        return ccrate;
    }

    public void setCcrate(String ccrate) {
        this.ccrate = ccrate;
    }

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }
}
