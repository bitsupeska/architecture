package mn.bitsup.callservice.client.offline.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;

public interface TokenListener {
    void onSuccess(String var1);
    void onError(Response var1);
}
