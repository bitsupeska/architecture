package mn.bitsup.callservice.client.profile;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.RemoteAuthClient;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.data.ProfileData;
import mn.bitsup.callservice.client.profile.data.ProfileDataImpl;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.client.profile.listener.QuestionListener;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.ClientsManager;

public class ProfileClient implements DBSClient {
    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private ProfileData profileData;

    public ProfileClient(DBSDataProvider dbsDataProvider, URI baseUri) {
        this(dbsDataProvider, baseUri, new DefaultProfileParser());
    }

    public ProfileClient(DBSDataProvider dbsDataProvider, URI baseUri, ProfileParser profileParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = baseUri;
        this.profileData = new ProfileDataImpl(profileParser) {
        };
    }

    @Override
    public void setBaseURI(URI uri) {
        this.baseUri = uri;

    }

    @Override
    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    @Override
    public URI getBaseURI() {
        return this.baseUri;
    }

    @Override
    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }

    public void getUser(final UserListener userListener) {
        RemoteAuthClient authClient = ClientsManager.getRemoteAuthClient();
        HashMap<String, String> param = new HashMap<>();
        param.put("uid", authClient.getUid());

        this.profileData.getUser(this.baseUri, this.dbsDataProvider, param, new UserListener() {
            public void onSuccess(User user) {
                userListener.onSuccess(user);
            }

            public void onError(Response errorResponse) {
                userListener.onError(errorResponse);
            }
        });
    }

    public void getUser(final UserListener userListener, User user) {
        HashMap<String, String> param = new HashMap<>();
        param.put("uid", user.getUid());

        this.profileData.getUser(this.baseUri, this.dbsDataProvider, param, new UserListener() {
            public void onSuccess(User user) {
                userListener.onSuccess(user);
            }

            public void onError(Response errorResponse) {
                userListener.onError(errorResponse);
            }
        });
    }

    public void setEmail(final StatusListener listener, HashMap<String, String> param) {
        this.profileData.setEmail(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void setNumber(final StatusListener listener, HashMap<String, String> param) {
        this.profileData.setNumber(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void setAddress(final StatusListener listener, HashMap<String, String> param) {
        this.profileData.setAddress(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void matchPassword(final StatusListener listener, HashMap<String, String> param) {
        this.profileData.matchPassword(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void setPassword(final StatusListener listener, HashMap<String, String> param) {
        this.profileData.setPassword(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void getAnswerQuestion(final QuestionListener listener) {
        this.profileData.getAnswerQuestion(this.baseUri, this.dbsDataProvider, listener);
    }

    public void getAccountList(final AccountListener listener, String param) {
        this.profileData.getAccountList(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void addAccount(final StatusListener listener, Map<String, String> param) {
        this.profileData.addAccount(this.baseUri, this.dbsDataProvider, param, listener);
    }

    public void deleteAccount(final StatusListener listener, Map<String, String> param) {
        this.profileData.deleteAccount(this.baseUri, this.dbsDataProvider, param, listener);
    }
}
