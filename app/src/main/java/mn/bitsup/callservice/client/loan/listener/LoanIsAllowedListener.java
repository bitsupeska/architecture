package mn.bitsup.callservice.client.loan.listener;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanIsAllow;

public interface LoanIsAllowedListener {
    void onSuccess(LoanIsAllow var1);
    void onError(Response var1);
}
