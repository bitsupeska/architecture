package mn.bitsup.callservice.client.info.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class CitizenInfo {
    String requestId;
    int resultCode;
    String resultMessage;

    public static final Parcelable.Creator<CitizenInfo> CREATOR = new Parcelable.Creator<CitizenInfo>() {
        public CitizenInfo createFromParcel(Parcel source) {
            return new CitizenInfo(source);
        }
        public CitizenInfo[] newArray(int size) {
            return new CitizenInfo[size];
        }
    };

    public CitizenInfo(){

    }

    private CitizenInfo(Parcel in) {
        this.requestId = in.readString();
        this.resultCode = in.readInt();
        this.resultMessage = in.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.requestId);
        dest.writeInt(this.resultCode);
        dest.writeString(this.resultMessage);
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

}
