package mn.bitsup.callservice.client.call.data;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.client.call.listener.CallHistoryListener;
import mn.bitsup.callservice.client.call.parser.CallHistoryParser;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.notification.NotificationParser;
import mn.bitsup.callservice.client.notification.dto.Notification;

public class CallHistoryDataImp implements CallHistoryData{
    private CallHistoryParser callHistoryParser;
    private static String TAG = CallHistoryDataImp.class.getName();
    public CallHistoryDataImp(CallHistoryParser callHistoryParser) {
        this.callHistoryParser = callHistoryParser;
    }

    @Override
    public void getHistories(URI baseUri, DBSDataProvider dataProvider, Map<String, String> queryParams, CallHistoryListener listener) {
        try {
            dataProvider.execute(this.buildRequest("GET", baseUri, dataProvider, queryParams), new DBSDataProviderListener() {
                public void onSuccess(Response response) {
                    int responseCode = response.getResponseCode();
                    if (responseCode >= 200 && responseCode < 400) {
                        List<CallHistory> callHistories = callHistoryParser.parseHistories(response.getStringResponse());
                        if (callHistories != null) {
                            Log.e(TAG, "onSuccess: "+callHistories.size());
                            listener.onSuccess(callHistories);
                        } else {
                            Log.e(TAG, "onSuccess: cardReasonList CANNOT PARSE FILE");
                            Response errorResponsex = new Response();
                            errorResponsex.setErrorMessage("cardReasonList CANNOT PARSE FILE");
                            errorResponsex.setResponseCode(500);
                            listener.onError(errorResponsex);
                        }
                    } else {
                        Log.e(TAG, "onError: "+response.getErrorMessage());
                        Response errorResponse = new Response();
                        errorResponse.setErrorMessage(response.getErrorMessage());
                        errorResponse.setResponseCode(responseCode);
                        listener.onError(errorResponse);
                    }
                }

                public void onError(Response errorResponse) {


                    Log.e(TAG, "onError: "+errorResponse.getErrorMessage());
                    listener.onError(errorResponse);
                }
            });

        } catch (URISyntaxException var7) {

            Log.e(TAG, "catchError: "+var7.getReason());
            Response errorResponse = new Response();
            errorResponse.setErrorMessage(var7.getReason());
            errorResponse.setResponseCode(500);
            listener.onError(errorResponse);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    private Request buildRequest(String methodName, URI baseUri, DBSDataProvider dataProvider, Map<String, String> params) throws URISyntaxException, UnsupportedEncodingException {
        Request request = new Request();
        request.setRequestMethod(methodName);
        request.setBody((new Gson()).toJson(params));
        request.setUri(new URI(baseUri + (params != null ? this.buildQuery(params) : "")));
        request.setHeaders(this.getAuthHeaders());
        return request;
    }


    protected String buildQuery(Map<String, String> queryParams) throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("?");
        Iterator var3 = queryParams.entrySet().iterator();

        while (var3.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) var3.next();
            stringBuilder.append(URLEncoder.encode((String) entry.getKey(), "UTF-8")).append("=").append(URLEncoder.encode((String) entry.getValue(), "UTF-8")).append("&");
        }

        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    @NonNull
    private Map<String, String> getAuthHeaders() {
        StorageComponent storageComponent = new StorageComponent();
        Map<String, String> headers = new HashMap<>();
        headers.put(AuthClient.AUTH_HEADER, "Bearer "+ storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        headers.put("Content-Type", "application/json");
        return headers;
    }
}
