package mn.bitsup.callservice.client;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;

public class RefreshService extends Service {
    public static int timer = 0;
    public static CountDownTimer countDownTimer;
    public static int expiresIn = 10000;
    public static boolean isRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            expiresIn = Integer.parseInt(getExpiresIn()) * 1000;
            isRunning = true;
            countDownTimer = new CountDownTimer(expiresIn, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    timer = Integer.parseInt("" + (millisUntilFinished / 1000));
                    Log.e("Timer", "refresh second: " + timer);
                }

                @Override
                public void onFinish() {
                    isRunning = false;
                    onDestroy();
                }
            }.start();
        }catch (Exception ex){
           ex.printStackTrace();
        }
    }

    //                if (timer <= 20) {
//                    countDownTimer.cancel();
//                    refreshToken();
//                }
//    private void refreshToken() {
//        MainApplication.authClient.performAutomatedLogin(new PasscodeAuthListener() {
//            @Override
//            public void onSuccess() {
//                countDownTimer.start();
//            }
//
//            @Override
//            public void onError(Response var1) {
//                countDownTimer.cancel();
//                isRunning = false;
//                Intent intent = new Intent(getApplicationContext(), EnrollmentActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                intent.putExtra(INTENT_EXTRA_FORCED_LOGOUT, true);
//                MainActivity.context.startActivity(intent);
//                Log.e("asd", "refresh " + var1.getErrorMessage());
//            }
//        });
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        timer = 0;
        isRunning = false;
        if(countDownTimer != null){
            countDownTimer.cancel();
        }
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String getExpiresIn() {
        String expires = "300";
        {
            RemoteAuthClient authClient = new RemoteAuthClient(getApplicationContext());
             if(authClient.getExpiresIn() != null){
                 expires = authClient.getExpiresIn();
             }
        }
        return expires;
    }
}
