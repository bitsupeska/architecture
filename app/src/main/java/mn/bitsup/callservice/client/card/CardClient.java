package mn.bitsup.callservice.client.card;

import androidx.annotation.NonNull;
import java.net.URI;
import java.util.List;
import java.util.Map;
import mn.bitsup.callservice.client.card.data.CardData;
import mn.bitsup.callservice.client.card.data.CardDataImp;
import mn.bitsup.callservice.client.card.dto.Card;
import mn.bitsup.callservice.client.card.dto.CardDetail;
import mn.bitsup.callservice.client.card.dto.CardReasonItem;
import mn.bitsup.callservice.client.card.listener.CardDetailListener;
import mn.bitsup.callservice.client.card.listener.CardReasonListener;
import mn.bitsup.callservice.client.card.listener.CardsListener;
import mn.bitsup.callservice.client.card.parser.DefaultCardParser;
import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.util.DBSClient;
import mn.bitsup.callservice.util.CustomParams;
import mn.bitsup.callservice.view.widget.deliver.Deliver;

public class CardClient implements DBSClient {
    private DBSDataProvider dbsDataProvider;
    private URI baseUri;
    private CardData cardData;
    public CardClient(URI baseUri) {
        this((DBSDataProvider) null, baseUri, new DefaultCardParser());
    }

    public CardClient(DBSDataProvider dbsDataProvider, URI uri) {
        this(dbsDataProvider, uri, new DefaultCardParser());

    }

    CardClient(DBSDataProvider dbsDataProvider, URI uri, DefaultCardParser defaultCardParser) {
        this.dbsDataProvider = dbsDataProvider;
        this.baseUri = uri;
        this.cardData = new CardDataImp(defaultCardParser);
    }

    public void getCardList(@NonNull CustomParams var1, @NonNull final CardsListener var2) {
        this.cardData.getCardList(this.baseUri, this.dbsDataProvider, var1, new CardsListener() {
            public void onSuccess(List<Card> var1) {
                var2.onSuccess(var1);
            }

            public void onError(Response var1) {
                var2.onError(var1);
            }
        });
    }

    public void getReasons(@NonNull Map<String, String> queryParams, @NonNull final CardReasonListener cardReasonListener){
        this.cardData.getCardReason(this.baseUri, this.dbsDataProvider, queryParams, new CardReasonListener() {
            @Override
            public void onSuccess(List<CardReasonItem> var1) {
                cardReasonListener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                cardReasonListener.onError(var1);
            }
        });
    }
    public void reissueCard(final StatusListener listener, Deliver deliver) {
        try {
            this.cardData.reissue(this.baseUri, this.dbsDataProvider, deliver, new StatusListener() {
                @Override
                public void onSuccess(StatusResponse response) {
                    listener.onSuccess(response);
                }

                @Override
                public void onError(Response response) {
                    listener.onError(response);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void changeCardStatus(final StatusListener listener, Map<String, String> params) {
        try {
            this.cardData
                    .changeStatus(getBaseURI(), this.dbsDataProvider, params, new StatusListener() {
                        @Override
                        public void onSuccess(StatusResponse response) {
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(Response response) {
                            listener.onError(response);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void cardActive(final StatusListener listener, Map<String, String> params) {
        try {
            this.cardData.cardActive(getBaseURI(), this.dbsDataProvider, params, new StatusListener() {
                        @Override
                        public void onSuccess(StatusResponse response) {
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(Response response) {
                            listener.onError(response);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void cardDetail(final CardDetailListener listener, Map<String, String> params) {
        try {
            this.cardData.cardDetail(getBaseURI(), this.dbsDataProvider, params, new CardDetailListener() {

                @Override
                public void onSuccess(CardDetail var1) {
                    listener.onSuccess(var1);
                }

                @Override
                public void onError(Response var1) {
                    listener.onError(var1);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void changeCardPin(final StatusListener listener, Map<String, String> params) {
        try {
            this.cardData
                    .changePin(getBaseURI(), this.dbsDataProvider, params, new StatusListener() {
                        @Override
                        public void onSuccess(StatusResponse response) {
                            listener.onSuccess(response);
                        }

                        @Override
                        public void onError(Response response) {
                            listener.onError(response);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setBaseURI(URI uri) {
        this.baseUri = uri;
    }

    public void setDataProvider(DBSDataProvider dbsDataProvider) {
        this.dbsDataProvider = dbsDataProvider;
    }

    public URI getBaseURI() {
        return this.baseUri;
    }

    public DBSDataProvider getDBSDataProvider() {
        return this.dbsDataProvider;
    }
}
