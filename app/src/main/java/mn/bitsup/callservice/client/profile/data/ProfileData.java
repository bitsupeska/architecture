package mn.bitsup.callservice.client.profile.data;

import java.net.URI;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.DBSDataProvider;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.client.profile.listener.QuestionListener;
import mn.bitsup.callservice.client.profile.listener.UserListener;

public interface ProfileData {
    void getUser(URI var1, DBSDataProvider var2, Map<String, String> var3, UserListener var4);

    void setEmail(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void setNumber(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void setAddress(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void matchPassword(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void setPassword(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void getAnswerQuestion(URI var1, DBSDataProvider var2, QuestionListener var4);

    void getAccountList(URI var1, DBSDataProvider var2, String var3, AccountListener var4);

    void addAccount(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);

    void deleteAccount(URI var1, DBSDataProvider var2, Map<String, String> var3, StatusListener var4);
}
