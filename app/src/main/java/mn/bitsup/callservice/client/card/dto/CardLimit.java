package mn.bitsup.callservice.client.card.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class CardLimit extends Additions implements Parcelable {
    private String code;
    private String name;
    private String value;
    private String periodTypeName;
    private String resetPeriod;
    private String resetHour;
    private String resetMinute;

    public static final Parcelable.Creator<CardLimit> CREATOR = new Parcelable.Creator<CardLimit>() {
        public CardLimit createFromParcel(Parcel source) {
            return new CardLimit(source);
        }

        public CardLimit[] newArray(int size) {
            return new CardLimit[size];
        }
    };

    private  CardLimit(Parcel in) {
        this.code = in.readString();
        this.name = in.readString();
        this.value = in.readString();
        this.periodTypeName = in.readString();
        this.resetPeriod = in.readString();
        this.resetHour = in.readString();
        this.resetMinute = in.readString();
    }

    public void writeToParcel(Parcel dest) {
        dest.writeString(this.code);
        dest.writeString(this.name);
        dest.writeString(this.value);
        dest.writeString(this.periodTypeName);
        dest.writeString(this.resetPeriod);
        dest.writeString(this.resetHour);
        dest.writeString(this.resetMinute);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPeriodTypeName() {
        return periodTypeName;
    }

    public void setPeriodTypeName(String periodTypeName) {
        this.periodTypeName = periodTypeName;
    }

    public String getResetPeriod() {
        return resetPeriod;
    }

    public void setResetPeriod(String resetPeriod) {
        this.resetPeriod = resetPeriod;
    }

    public String getResetHour() {
        return resetHour;
    }

    public void setResetHour(String resetHour) {
        this.resetHour = resetHour;
    }

    public String getResetMinute() {
        return resetMinute;
    }

    public void setResetMinute(String resetMinute) {
        this.resetMinute = resetMinute;
    }

    public static Creator<CardLimit> getCREATOR() {
        return CREATOR;
    }
}

