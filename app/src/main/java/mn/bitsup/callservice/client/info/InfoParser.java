package mn.bitsup.callservice.client.info;

import java.util.List;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;
import mn.bitsup.callservice.client.info.dto.CurrencyItem;
import mn.bitsup.callservice.client.info.dto.DeliveryDate;
import mn.bitsup.callservice.client.info.dto.DeliveryState;
import mn.bitsup.callservice.client.info.dto.Item;
import mn.bitsup.callservice.client.info.dto.Location;

public interface InfoParser {
    List<Location> parseStateList(String var1);
    List<Location> parseCityList(String var1);
    List<BankItem> parseBankList(String var1);
    List<CurrencyItem> parseCurrencyList(String var1);
    List<DeliveryState> parseDeliveryList(String var1);
    List<DeliveryDate> parseDeliveryDateList(String var1);
    CitizenInfo parseCitizenInfo(String var1);
    Item parseBackgroundItem(String var1);
}
