package mn.bitsup.callservice.client.temporary.dto;

public class Temporary {

      String apartmentHome;
      String apartmentWork;
      String birthDate;
      String cifId;
      String currCode;
      String didSign;
      String direction;
      String districtHome;
      String districtWork;
      String docExpDate;
      String docIssueDate;
      String doorNumberHome;
      String doorNumberWork;
      String email;
      String emailStatus;
      String employmentStatus;
      String familyName;
      String firstName;
      String freezeCode;
      String gender;
      String isHomeAddress;
      String isWorkAddress;
      String lastName;
      String localityHome;
      String localityHomeName;
      String localityWork;
      String position;
      String prodCode;
      String reasonCode;
      String regNum;
      String remarks;
      String sectionHome;
      String sectionWork;
      String townHome;
      String townHomeName;
      String townWork;
      String userName;
      String workName;
      String educationType;
      String educationName;

    public String getApartmentHome() {
        return apartmentHome;
    }

    public void setApartmentHome(String apartmentHome) {
        this.apartmentHome = apartmentHome;
    }

    public String getApartmentWork() {
        return apartmentWork;
    }

    public void setApartmentWork(String apartmentWork) {
        this.apartmentWork = apartmentWork;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getCifId() {
        return cifId;
    }

    public void setCifId(String cifId) {
        this.cifId = cifId;
    }

    public String getCurrCode() {
        return currCode;
    }

    public void setCurrCode(String currCode) {
        this.currCode = currCode;
    }

    public String getDidSign() {
        return didSign;
    }

    public void setDidSign(String didSign) {
        this.didSign = didSign;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getDistrictHome() {
        return districtHome;
    }

    public void setDistrictHome(String districtHome) {
        this.districtHome = districtHome;
    }

    public String getDistrictWork() {
        return districtWork;
    }

    public void setDistrictWork(String districtWork) {
        this.districtWork = districtWork;
    }

    public String getDocExpDate() {
        return docExpDate;
    }

    public void setDocExpDate(String docExpDate) {
        this.docExpDate = docExpDate;
    }

    public String getDocIssueDate() {
        return docIssueDate;
    }

    public void setDocIssueDate(String docIssueDate) {
        this.docIssueDate = docIssueDate;
    }

    public String getDoorNumberHome() {
        return doorNumberHome;
    }

    public void setDoorNumberHome(String doorNumberHome) {
        this.doorNumberHome = doorNumberHome;
    }

    public String getDoorNumberWork() {
        return doorNumberWork;
    }

    public void setDoorNumberWork(String doorNumberWork) {
        this.doorNumberWork = doorNumberWork;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFreezeCode() {
        return freezeCode;
    }

    public void setFreezeCode(String freezeCode) {
        this.freezeCode = freezeCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIsHomeAddress() {
        return isHomeAddress;
    }

    public void setIsHomeAddress(String isHomeAddress) {
        this.isHomeAddress = isHomeAddress;
    }

    public String getIsWorkAddress() {
        return isWorkAddress;
    }

    public void setIsWorkAddress(String isWorkAddress) {
        this.isWorkAddress = isWorkAddress;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocalityHome() {
        return localityHome;
    }

    public void setLocalityHome(String localityHome) {
        this.localityHome = localityHome;
    }

    public String getLocalityWork() {
        return localityWork;
    }

    public void setLocalityWork(String localityWork) {
        this.localityWork = localityWork;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSectionHome() {
        return sectionHome;
    }

    public void setSectionHome(String sectionHome) {
        this.sectionHome = sectionHome;
    }

    public String getSectionWork() {
        return sectionWork;
    }

    public void setSectionWork(String sectionWork) {
        this.sectionWork = sectionWork;
    }

    public String getTownHome() {
        return townHome;
    }

    public void setTownHome(String townHome) {
        this.townHome = townHome;
    }

    public String getTownWork() {
        return townWork;
    }

    public void setTownWork(String townWork) {
        this.townWork = townWork;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getLocalityHomeName() {
        return localityHomeName;
    }

    public void setLocalityHomeName(String localityHomeName) {
        this.localityHomeName = localityHomeName;
    }

    public String getTownHomeName() {
        return townHomeName;
    }

    public void setTownHomeName(String townHomeName) {
        this.townHomeName = townHomeName;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

    public String getEducationType() {
        return educationType;
    }

    public void setEducationType(String educationType) {
        this.educationType = educationType;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }
}
