package mn.bitsup.callservice.client.loan.dto;

public class LoanRelated {
    int code;
    String status;
    String message;
    String contactCifId;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getContactCifId() {
        return contactCifId;
    }

    public void setContactCifId(String contactCifId) {
        this.contactCifId = contactCifId;
    }
}
