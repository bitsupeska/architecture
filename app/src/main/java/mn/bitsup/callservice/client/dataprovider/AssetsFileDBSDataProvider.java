package mn.bitsup.callservice.client.dataprovider;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.apptakk.http_request.HttpRequest;
import com.apptakk.http_request.HttpRequestTask;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.UserInteractionTimers;
import mn.bitsup.callservice.client.dataprovider.listener.DBSDataProviderListener;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.request.Request;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.info.dto.LogItems;
import mn.bitsup.callservice.view.activity.EnrollmentActivity;

public class AssetsFileDBSDataProvider implements DBSDataProvider {
    public static final String INTENT_EXTRA_FORCED_LOGOUT = "INTENT_EXTRA_FORCED_LOGOUT";
    private Context context;
//    private DatabaseReference reference;
    private LogItems logItems = new LogItems();
    private double startTime=0;
    private double endTime=0;

    SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
    Date date = new Date(System.currentTimeMillis());
    public AssetsFileDBSDataProvider(Context context) {
        this.context = context;
    }

    public void execute(Request request, DBSDataProviderListener listener) {
//        int timer = RefreshService.timer;
//        int expires = RefreshService.expiresIn / 2000;
//        requestWorker(request, listener);
//        requestWorker(request, listener);
//        logItems.setUserId(MainApplication.getUser() == null ? "" : MainApplication.getUser().getUid());
//        if (RefreshService.isRunning) {
//            requestWorker(request, listener);
//        } else {
//            if (UserInteractionTimers.isLogged)
//                refreshToken(request, listener);
//            else {
//                requestWorker(request, listener);
//            }
//        }
        requestWorker(request, listener);
    }

//    private void refreshToken(Request request, DBSDataProviderListener listener) {
//        MainApplication.authClient.performAutomatedLogin(new PasscodeAuthListener() {
//            @Override
//            public void onSuccess() {
//                context.startService(new Intent(context, RefreshService.class));
//                requestWorker(request, listener);
//            }
//
//            @Override
//            public void onError(Response var1) {
//                requestWorker(request, listener);
//            }
//        });
//    }


    private void requestWorker(Request request, DBSDataProviderListener listener) {

        Log.e("MWallet", "request url: " + request.getUri().toString());
        Log.e("MWallet", "request url: " + request.getBody());
        startTime = System.currentTimeMillis();
        try {
            if (request.getRequestMethod().equals("GET")) {
                getRequest(request, listener);
            } else if (request.getRequestMethod().equals("POST")) {
                postRequest(request, listener);
            }
            logItems.setRequest(request.getUri().toString());
            logItems.setParam(request.getBody());


        } catch (Exception var3) {
            this.buildErrorResponse(request, "Алдаа гарлаа.", listener, 405);
        }
    }

    private void getRequest(Request request, DBSDataProviderListener listener) {
        new HttpRequestTask(
                new HttpRequest(request.getUri().toString(), HttpRequest.GET, null, null, getAuthHeaders(request.getHeaders())),
                response -> {
                    Log.e("MWallet", "response code: " + response.code);
                    Log.e("MWallet", "response: " + response.body);
                    if (response.code == 200) {
                        buildAndPassResponse(response.body, listener, 200);
                    } else {
                        buildErrorResponse(request, response.body, listener, response.code);
                    }
                }).execute();

    }

    private void postRequest(Request request, DBSDataProviderListener listener) {
        new HttpRequestTask(
                new HttpRequest(request.getUri().toString(), HttpRequest.POST, request.getBody(), null, getAuthHeaders(request.getHeaders())),
                response -> {
                    Log.e("MWallet", "response code: " + response.code);
                    Log.e("MWallet", "response: " + response.body);

                    if (response.code == 200) {
                        buildAndPassResponse(response.body, listener, 200);
                    } else {
                        buildErrorResponse(request, response.body, listener, response.code);
                    }
                }).execute();
    }

    @NonNull
    private Map<String, String> getAuthHeaders(Map<String, String> headers) {
        StorageComponent storageComponent = new StorageComponent();
        headers.put(AuthClient.AUTH_HEADER, "Bearer " + storageComponent.getItem(AuthClient.ACCESS_TOKEN));
        return headers;
    }

    private void buildAndPassResponse(String body, DBSDataProviderListener listener, int code) {
        Log.e("MWallet", "response body: " + body);

        logItems.setResponce(body);
        logItems.setStatus("success");
        endTime = System.currentTimeMillis();
        Date date = new Date(System.currentTimeMillis());
        logItems.setDuration((endTime - startTime)/1000);

        SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss");
//        reference = FirebaseDatabase.getInstance().getReference().child("simple_log_android").child("success").child(formatterDate.format(date)).child(formatTime.format(date));
//        reference.setValue(logItems);
        Response var4;
        (var4 = new Response()).setResponseCode(code);
        var4.setByteResponse(body.getBytes());
        listener.onSuccess(var4);
//        reference.push().setValue(logItems);
    }

    private void buildErrorResponse(Request request, String body, DBSDataProviderListener listener, int code) {
        Response errorResponse = new Response();
        errorResponse.setErrorMessage(body);
        errorResponse.setResponseCode(code);
        String stringUrl = request.getUri().toString();
        Log.e("MWallet", "buildErrorResponse: "+body );
        logItems.setResponce(body);
        logItems.setStatus("error");
        endTime = System.currentTimeMillis();
        logItems.setDuration((endTime - startTime)/1000);
        Date date = new Date(System.currentTimeMillis());
//        reference = FirebaseDatabase.getInstance().getReference().child("simple_log_android").child("failure").child(formatterDate.format(date)).child(formatTime.format(date));
//
//        reference.push().setValue(logItems);
        if (!stringUrl.matches(".*carouselLayout.*") && !stringUrl.matches(".*anonymous.*") && !stringUrl.matches(".*login.*") && code == 401) {
            Log.e("buildErrorResponse", "refresh token expired: " + code);

            ClientsManager.authClient.endSession();
            context.stopService(new Intent(context, RefreshService.class));
            UserInteractionTimers.isLogged = false;
            UserInteractionTimers.endTimer();
            Intent enrollmentIntent = new Intent(context, EnrollmentActivity.class);
            enrollmentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            enrollmentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            enrollmentIntent.putExtra(INTENT_EXTRA_FORCED_LOGOUT, true);
            context.startActivity(enrollmentIntent);
        } else {
            listener.onError(errorResponse);
        }
    }
}
