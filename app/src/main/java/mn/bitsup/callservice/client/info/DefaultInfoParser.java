package mn.bitsup.callservice.client.info;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;
import mn.bitsup.callservice.client.info.dto.CurrencyItem;
import mn.bitsup.callservice.client.info.dto.DeliveryDate;
import mn.bitsup.callservice.client.info.dto.DeliveryState;
import mn.bitsup.callservice.client.info.dto.Item;
import mn.bitsup.callservice.client.info.dto.Location;
import org.json.JSONException;
import org.json.JSONObject;

public class DefaultInfoParser implements InfoParser {

    public DefaultInfoParser() {
    }

    @Override
    public List<Location> parseStateList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONObject("items").getJSONArray("states").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type itemsMapType = new TypeToken<List<Location>>() {}.getType();
        List<Location> locations= new Gson().fromJson(jsonArray, itemsMapType);
        return locations;
    }

    @Override
    public List<Location> parseCityList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONObject("items").getJSONArray("cities").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Type itemsMapType = new TypeToken<List<Location>>() {}.getType();
        List<Location> locations= new Gson().fromJson(jsonArray, itemsMapType);
        return locations;
    }

    @Override
    public List<BankItem> parseBankList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Type itemsMapType = new TypeToken<List<BankItem>>() {}.getType();
        List<BankItem> bankItems = new Gson().fromJson(jsonArray, itemsMapType);
        return bankItems;
    }

    @Override
    public List<CurrencyItem> parseCurrencyList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONArray("ItemsCurrency").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Type itemsMapType = new TypeToken<List<CurrencyItem>>() {}.getType();
        List<CurrencyItem> currencies= new Gson().fromJson(jsonArray, itemsMapType);
        return currencies;
    }

    @Override
    public List<DeliveryState> parseDeliveryList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Type itemsMapType = new TypeToken<List<DeliveryState>>() {}.getType();
        List<DeliveryState> deliveryStateList= new Gson().fromJson(jsonArray, itemsMapType);
        return deliveryStateList;
    }

    @Override
    public List<DeliveryDate> parseDeliveryDateList(String json) {
        String jsonArray = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonArray = obj.getJSONArray("items").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Type itemsMapType = new TypeToken<List<DeliveryDate>>() {}.getType();
        return new Gson().fromJson(jsonArray, itemsMapType);
    }



    @Override
    public CitizenInfo parseCitizenInfo(String json) {
        String jsonObject = "";
        try {
            JSONObject obj = new JSONObject(json);
            jsonObject = obj.getJSONObject("data").toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new Gson().fromJson(jsonObject, CitizenInfo.class);
    }

    @Override
    public Item parseBackgroundItem(String json) {
        Type itemsMapType = new TypeToken<Item>() {}.getType();
        return new Gson().fromJson(json, itemsMapType);
    }

}
