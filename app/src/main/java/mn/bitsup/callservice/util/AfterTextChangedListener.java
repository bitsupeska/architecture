package mn.bitsup.callservice.util;

import android.text.TextWatcher;

/**
 * Created by Erdenedalai R&D B.V on 25/10/2017.
 */

public abstract class AfterTextChangedListener implements TextWatcher {

    @Override
    public final void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // Implementation of this method is forbidden
    }

    @Override
    public final void onTextChanged(CharSequence s, int start, int before, int count) {
        // Implementation of this method is forbidden
    }
}
