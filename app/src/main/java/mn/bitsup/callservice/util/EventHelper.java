package mn.bitsup.callservice.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;

import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.view.activity.GenericActivity;

import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class EventHelper {

    public static void publishActivity(Context context, Fragment fragment, String title) {
        Activity activity = (Activity) Objects.requireNonNull(context);
        GenericActivity.setFragment(fragment);
        Intent intent = new Intent(context, GenericActivity.class);
        intent.putExtra(PAGE_PREFERENCE_TITLE, title);
        activity.startActivityForResult(intent, 1);
        activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    }

    public static void publish(Context context, Fragment fragment) {
        if (getSupportFragmentManager(context) != null) {
            final FragmentTransaction fragmentTransaction = getSupportFragmentManager(context).beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commit();
        }
    }


    public static void publishLoss(Context context, Fragment fragment) {
        if (getSupportFragmentManager(context) != null) {
            final FragmentTransaction fragmentTransaction = getSupportFragmentManager(context).beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }


    public static void publish(Context context, Fragment fragment, String eventName, @Nullable Object data) {
        if (!eventName.equals("")) {
            setEventPayload(eventName, data);
        }

        if (getSupportFragmentManager(context) != null) {
            KeyboardUtils.hideKeyboard((Activity)context);
            final FragmentTransaction fragmentTransaction = getSupportFragmentManager(context).beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.commit();
        }
    }

    public static void publishBack(Context context, Fragment fragment) {

        if (getSupportFragmentManager(context) != null) {
            KeyboardUtils.hideKeyboard((Activity)context);
            final FragmentTransaction fragmentTransaction = getSupportFragmentManager(context).beginTransaction();
            fragmentTransaction.add(R.id.content_frame, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }

    public static void publishStackPop(Context context, Fragment fragment) {
        if (getSupportFragmentManager(context) != null) {
            KeyboardUtils.hideKeyboard((Activity)context);
            final FragmentManager fragmentTransaction = getSupportFragmentManager(context);
            fragmentTransaction.popBackStack();
        }
    }

    public static void publishBack(Context context, Fragment fragment, String eventName, @Nullable Object data) {
        KeyboardUtils.hideKeyboard((Activity)context);
        if (!eventName.equals("")) {
            setEventPayload(eventName, data);
        }

        if (getSupportFragmentManager(context) != null) {
            final FragmentTransaction fragmentTransaction = getSupportFragmentManager(context).beginTransaction();
            fragmentTransaction.add(R.id.content_frame, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }


    public static void setEventPayload(String eventName, @Nullable Object data) {
        StorageComponent storageComponent = new StorageComponent();
        Gson gson = new Gson();
        String gsonData = gson.toJson(data);
        storageComponent.setItem(eventName, gsonData);
    }

    public static <T> T getEventPayload(String eventName, Class<T> classOfT) {
        StorageComponent storageComponent = new StorageComponent();
        String data = storageComponent.getItem(eventName);
        storageComponent.removeItem(eventName);
        Gson gson = new Gson();
        return gson.fromJson(data, classOfT);
    }


    public static FragmentManager getSupportFragmentManager(Context context) {
        if (context instanceof MutableContextWrapper) {
            AppCompatActivity appCompatActivity = ((AppCompatActivity) ((MutableContextWrapper) context)
                    .getBaseContext());
            return appCompatActivity.getSupportFragmentManager();
        }
        if (context instanceof AppCompatActivity) {
            AppCompatActivity appCompatActivity = ((AppCompatActivity) context);
            return appCompatActivity.getSupportFragmentManager();
        }
        return null;
    }
}
