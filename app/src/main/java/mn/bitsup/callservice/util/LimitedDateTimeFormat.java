package mn.bitsup.callservice.util;

import java.text.ParseException;
import java.util.Date;

public interface LimitedDateTimeFormat {
    String format(Date var1);

    Date parse(String var1) throws ParseException;
}
