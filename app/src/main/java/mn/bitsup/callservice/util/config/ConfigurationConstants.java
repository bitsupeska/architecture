package mn.bitsup.callservice.util.config;

/**
 * Created by Erdenedalai R&D B.V on 04/04/2018.
 */
public class ConfigurationConstants {

    public static final String TEST_BEHAVIOR = "testBehaviour";

    private ConfigurationConstants() {
    }
}
