package mn.bitsup.callservice.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Erdenedalai R&D B.V. on 03/10/2018.
 *
 * Helper methods related to {@link Date}
 */
public class DateUtil {

    /**
     * Set the time of the provided {@link Date} to zero.
     * @param date the original {@link Date}. This object is not changed.
     * @return a new instance of {@link Date} with the time set to zero.
     */
    @Nullable
    public static Date setTimeToMidnight(@Nullable final Date date) {
        if (date == null) {
            return null;
        }

        final Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * @param d1
     * @param d2
     * @return the number of days between d2 and d1.
     */
    public static int dateDifferenceInDays(@NonNull Date d1, @NonNull Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return (int) TimeUnit.DAYS.convert(Math.abs(diff), TimeUnit.MILLISECONDS);
    }

    private DateUtil() {
        // Hide constructor
    }
}
