package mn.bitsup.callservice.util.crypto;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;
import androidx.annotation.NonNull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Calendar;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;

/**
 * Created by Erdenedalai R&D B.V. on 26/10/2018.
 *
 * Secure storage for login credentials. RSA 2048 is used to encrypt and decrypt
 * the credentials.
 * This class is temporary, we need this while waiting for SDK to implement OAuth support.
 */
public class LoginCredentialsStorage {

    /**
     * Name of the {@link SharedPreferences} used to store the encrypted username and password.
     */
    private static final String PREFERENCES = "login_credentials";

    /**
     * Key used to store the encrypted username inside {@link SharedPreferences}.
     */
    private static final String PREF_ENC_USERNAME = "enc_username";

    /**
     * Key used to store the encrypted password inside {@link SharedPreferences}.
     */
    @SuppressWarnings("squid:S2068")
    private static final String PREF_ENC_PWD = "enc_pwd";

    /**
     * Name of the keyStore that contains the public/private key pair.
     */
    private static final String KEYSTORE_PROVIDER = "AndroidKeyStore";

    /**
     * Alias of the public/private key used to encrypt username and password.
     */
    private static final String KEY_ALIAS = "loginCredentialsRSA";

    /**
     * Parameters for the asymmetric encryption.
     */
    private static final String RSA_CIPHER = "RSA/ECB/PKCS1Padding";

    /**
     * Algorithm used to generate the public/private encryption keys.
     */
    private static final String KEYS_ALGORITHM = "RSA";

    /**
     * Size of the RSA keys.
     */
    private static final int RSA_BIT_LENGTH = 2048;

    private SharedPreferences prefStorage;
    private KeyStore androidKeyStore;

    public LoginCredentialsStorage(@NonNull final Context context) {
        try {
            loadKeyStore();
            if (!keystoreContainsKeyAlias()) {
                generateAndStoreRSAKeys(context);
            }
            prefStorage = context.getSharedPreferences(PREFERENCES, 0);
        } catch (final Exception e) {
        }
    }

    /**
     * Encrypt a new username and password and stores them persistently.
     * The username and password are provided as char[] array, their
     * content is destroyed when this method returns.
     * @param username
     * @param password
     */
    public void encryptAndStoreCredentialsDestroyInput(final char[] username, final char[] password) {
        try {
            final byte[] encryptedUsername = rsaEncryptDestroyInput(CryptoUtil.toByteArrayDestroyInput(username));
            final String base64EncryptedUsername = Base64.encodeToString(encryptedUsername, Base64.DEFAULT);

            final byte[] encryptedPassword = rsaEncryptDestroyInput(CryptoUtil.toByteArrayDestroyInput(password));
            final String base64EncryptedPassword = Base64.encodeToString(encryptedPassword, Base64.DEFAULT);

            final SharedPreferences.Editor editor = prefStorage.edit();
            editor.putString(PREF_ENC_USERNAME, base64EncryptedUsername);
            editor.putString(PREF_ENC_PWD, base64EncryptedPassword);
            editor.apply();

        } catch (final Exception e) {
        }
    }

    /**
     * @return true if encrypted credentials are already stored, false otherwise.
     */
    public boolean hasEncryptedCredentials() {
        return prefStorage.contains(PREF_ENC_USERNAME) && prefStorage.contains(PREF_ENC_PWD);
    }

    /**
     * Wipes the encrypted credentials that are currently stored along with the encryption keys.
     */
    public void clearEncryptedCredentials() {
        final SharedPreferences.Editor editor = prefStorage.edit();
        editor.remove(PREF_ENC_USERNAME);
        editor.remove(PREF_ENC_PWD);
        editor.apply();
    }

    /**
     * @return the stored username.
     */
    public char[] getUsername() {
        try {
            final String base64EncryptedUsername = prefStorage.getString(PREF_ENC_USERNAME, "");
            byte[] encBinary = Base64.decode(base64EncryptedUsername, Base64.DEFAULT);
            byte[] userName = rsaDecrypt(encBinary);
            return CryptoUtil.toCharArrayDestroyInput(userName);
        } catch (final Exception e) {
        }
        return null;
    }

    /**
     * @return the stored password.
     */
    public char[] getPassword() {
        try {
            final String base64EncryptedUsername = prefStorage.getString(PREF_ENC_PWD, "");
            byte[] encBinary = Base64.decode(base64EncryptedUsername, Base64.DEFAULT);
            byte[] password = rsaDecrypt(encBinary);
            return CryptoUtil.toCharArrayDestroyInput(password);
        } catch (final Exception e) {
        }
        return null;
    }

    private void loadKeyStore() throws Exception {
        androidKeyStore = KeyStore.getInstance(KEYSTORE_PROVIDER);
        androidKeyStore.load(null);
    }

    private boolean keystoreContainsKeyAlias() throws Exception {
        return androidKeyStore.containsAlias(KEY_ALIAS);
    }

    private void generateAndStoreRSAKeys(Context context) throws Exception {
        final Calendar startValidity = Calendar.getInstance();
        final Calendar endValidity = Calendar.getInstance();
        endValidity.add(Calendar.YEAR, 100);

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance(KEYS_ALGORITHM, KEYSTORE_PROVIDER);

        KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                .setAlias(KEY_ALIAS)
                .setKeySize(RSA_BIT_LENGTH)
                .setEndDate(endValidity.getTime())
                .setStartDate(startValidity.getTime())
                .setSerialNumber(BigInteger.ONE)
                .setSubject(new X500Principal("CN = Secured Preference Store, O = Devliving Online"))
                .build();

        keyGen.initialize(spec);
        keyGen.generateKeyPair();
    }

    private PublicKey loadRSAPublicKey() throws Exception {
        if (androidKeyStore.containsAlias(KEY_ALIAS) && androidKeyStore.entryInstanceOf(KEY_ALIAS, KeyStore.PrivateKeyEntry.class)) {
            return androidKeyStore.getCertificate(KEY_ALIAS).getPublicKey();
        }
        return null;
    }

    private PrivateKey loadRSAPrivateKey() throws Exception {
        if (androidKeyStore.containsAlias(KEY_ALIAS) && androidKeyStore.entryInstanceOf(KEY_ALIAS, KeyStore.PrivateKeyEntry.class)) {
            return (PrivateKey) androidKeyStore.getKey(KEY_ALIAS, null);
        }
        return null;
    }

    private byte[] rsaEncryptDestroyInput(final byte[] bytes) throws Exception {
        final Cipher cipher = Cipher.getInstance(RSA_CIPHER);
        cipher.init(Cipher.ENCRYPT_MODE, loadRSAPublicKey());

        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, cipher);
        cipherOutputStream.write(bytes);
        cipherOutputStream.close();

        CryptoUtil.destroyContentsOf(bytes);

        return outputStream.toByteArray();
    }

    private byte[] rsaDecrypt(final byte[] bytes) throws Exception {
        final PrivateKey privateKey = loadRSAPrivateKey();
        final Cipher cipher = Cipher.getInstance(RSA_CIPHER);
        cipher.init(Cipher.DECRYPT_MODE, privateKey);

        final CipherInputStream cipherInputStream = new CipherInputStream(new ByteArrayInputStream(bytes), cipher);

        final ArrayList<Byte> values = new ArrayList<>();
        int nextByte;
        while ((nextByte = cipherInputStream.read()) != -1) {
            values.add((byte) nextByte);
        }

        byte[] dbytes = new byte[values.size()];
        for (int i = 0; i < dbytes.length; i++) {
            dbytes[i] = values.get(i);
        }

        cipherInputStream.close();
        return dbytes;
    }
}
