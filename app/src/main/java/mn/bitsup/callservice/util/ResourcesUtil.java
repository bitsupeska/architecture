package mn.bitsup.callservice.util;

import android.content.res.Resources;
import android.util.TypedValue;

import androidx.annotation.DimenRes;

/**
 * Created by Erdenedalai R&D B.V. on 15/02/2019.
 * Extensions to
 */
public final class ResourcesUtil {

    public static float getDimensionInDp(Resources resources, @DimenRes int dimenRes) {
        final float pxValue = resources.getDimension(dimenRes);
        return pxValue / resources.getDisplayMetrics().density;
    }

    private ResourcesUtil() {
        throw new UnsupportedOperationException("Cannot instantiate " + getClass().getSimpleName());
    }

    public static int dpToPx(int dp,Resources resources) {
        return (int) (dp * resources.getDisplayMetrics().density);
    }
    public static int spToPx(float sp, Resources resources) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, resources.getDisplayMetrics());
    }
}
