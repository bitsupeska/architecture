package mn.bitsup.callservice.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

class SimpleLimitedDateTimeFormat implements LimitedDateTimeFormat {
    private final DateFormat actualDateFormat;

    SimpleLimitedDateTimeFormat(String pattern) {
        this.actualDateFormat = new SimpleDateFormat(pattern, Locale.US);
    }

    SimpleLimitedDateTimeFormat(String pattern, TimeZone zone) {
        this(pattern);
        this.actualDateFormat.setTimeZone(zone);
    }

    public String format(Date date) {
        return this.actualDateFormat.format(date);
    }

    public Date parse(String source) throws ParseException {
        return this.actualDateFormat.parse(source);
    }
}
