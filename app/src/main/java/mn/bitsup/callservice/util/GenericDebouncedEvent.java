package mn.bitsup.callservice.util;

import android.os.SystemClock;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Created by Erdenedalai R&D B.V on 03/12/2018.
 * <p>
 * A Generic Debounced Event
 * Rejects events that are too close together in time.
 */
public class GenericDebouncedEvent<T> {

    private final long minimumInterval;
    private Map<T, Long> lastClickMap;

    private final static long DEFAULT_MIN_INTERVAL = 500;

    public GenericDebouncedEvent() {
        this(DEFAULT_MIN_INTERVAL);
    }

    public GenericDebouncedEvent(long minimumInterval) {
        this.minimumInterval = minimumInterval;
        this.lastClickMap = new WeakHashMap<>();
    }

    /**
     * Check if the event are debounced and return
     *
     * @param object
     * @return true if the event is debounced
     */
    public boolean isDebounced(T object) {
        Long previousClickTimestamp = lastClickMap.get(object);
        long currentTimestamp = SystemClock.uptimeMillis();

        lastClickMap.put(object, currentTimestamp);
        return (previousClickTimestamp == null || (currentTimestamp - previousClickTimestamp > minimumInterval));
    }
}
