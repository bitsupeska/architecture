package mn.bitsup.callservice.util;

import java.text.DecimalFormat;

public class CurrencyConverter {

    private CurrencyConverter() {
    }

    public static String getFormattedCurrencyString(float amount) {
        DecimalFormat formatter = new DecimalFormat("###,###,###,##0");
        if (amount != 0) return formatter.format(Double.parseDouble(amount + ""));
        return "0";
    }

    public static String getFormattedCurrencyString(Double amount) {
        DecimalFormat formatter = new DecimalFormat("###,###,###.00");
        if (amount != 0) return formatter.format(Double.parseDouble(amount + ""));
        return "0";
    }

}
