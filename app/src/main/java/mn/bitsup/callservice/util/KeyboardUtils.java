package mn.bitsup.callservice.util;


import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by Erdenedalai R&D B.V on 22/08/2017.
 * KeyboardUtils to hide the soft input keyboard.
 */

public class KeyboardUtils {

    private KeyboardUtils() {
        //Hide constructor
    }

    /**
     * Method to close possible open keyboard when context and focused view is known.
     *
     * @param context     context to get the InputMethodManager from.
     * @param focusedView view that is focused. If null, nothing happens.
     */
    public static void hideKeyboard(Context context, @Nullable View focusedView) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (focusedView != null && imm != null) {
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }

    }

    /**
     * Method to hide soft keyboard if open.
     *
     * @param activity current activity.
     */
    public static void hideKeyboard(Activity activity) {
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            hideKeyboard(activity, currentFocus);
        }
    }

    /**
     * Method to show the keyboard and focus on the given {@code view}.
     *
     * @param view the view with the focus.
     */
    public static void showKeyboard(@NonNull View view) {
        Context context = view.getContext();
        InputMethodManager imm =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            view.clearFocus();
            view.requestFocus();
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }
}
