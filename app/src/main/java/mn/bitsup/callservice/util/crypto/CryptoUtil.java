package mn.bitsup.callservice.util.crypto;

import androidx.annotation.Nullable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Erdenedalai R&D B.V. on 26/10/2018.
 *
 * Cryptography related helper functionalities.
 */
public class CryptoUtil {
    private static final String HASH_ALGORITHM = "SHA-256";

    /**
     * Return the hash of a char array.
     * After this method has returned, the content of the array provided as a parameter is destroyed.
     *
     * @param charArray
     * @return the hash as a String, or null if an error occurs.
     */
    @Nullable
    public static String hashOfCharArray(final char[] charArray) {
        try {
            final byte[] codeBytes = toByteArrayDestroyInput(charArray);
            final MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
            final String hash = bytesToHex(digest.digest(codeBytes));
            destroyContentsOf(codeBytes);
            return hash;

        } catch (NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static byte[] toByteArrayDestroyInput(final char[] charArray) {
        final byte[] codeBytes = new byte[charArray.length];
        for (int i = 0; i < codeBytes.length; i++) {
            codeBytes[i] = (byte) charArray[i];
        }
        destroyContentsOf(charArray);
        return codeBytes;
    }

    public static char[] toCharArrayDestroyInput(final byte[] byteArray) {
        final char[] charBytes = new char[byteArray.length];
        for (int i = 0; i < charBytes.length; i++) {
            charBytes[i] = (char) byteArray[i];
        }
        destroyContentsOf(byteArray);
        return charBytes;
    }

    public static void destroyContentsOf(byte[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }
    }

    public static void destroyContentsOf(char[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }
    }

    private static String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
