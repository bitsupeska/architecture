package mn.bitsup.callservice.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.format.DateUtils;
import androidx.annotation.NonNull;
import java.text.SimpleDateFormat;
import mn.bitsup.callservice.R;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Util class for project specific date display formats.
 */
public class DateDisplayFormatUtil {

    private static final long JUST_NOW_LIMIT_IN_MINUTES = 5;

    public static String toDateFormat(@NonNull final Date date) {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return fmt.format(date);
    }

    /**
     * The "Simple Date" display format should be used as section titles in transaction lists (eg. Bill Pay, RDC).
     * This display format shows dates for "Tomorrow", "Today", and "Yesterday" as translated strings.
     * Other dates are displayed in the device locale for dates in the current year with showing only
     * the short month and day (eg. Oct 25) and showing the short month, day and year in all other cases (Oct 25, 2017).
     * @param context activity context
     * @param date the date object to display
     * @return formatted simple date format as String
     */
    public static String toSimpleDateFormat(final Context context, @NonNull final Date date) {
        Calendar now = Calendar.getInstance();
        Calendar dateToDisplay = Calendar.getInstance();
        dateToDisplay.setTime(date);
        boolean showYear = !(now.get(Calendar.YEAR) == dateToDisplay.get(Calendar.YEAR));
        return DateTimeFormatter.toDateForDisplayFormat(context, date, true, showYear);
    }

    /**
     * The "Detailed Date" display format should be used to display dates in messages + notifications both in lists and detail views.
     * This display format shows dates within the last 5 minutes as "Just now", and falls back to "Simple Date" in other cases.
     * @param context activity context
     * @param date the date object to display
     * @return formatted simple date format as String
     */
    public static String toDetailedDateFormat(final Context context, @NonNull final Date date) {
        Date now = new Date();
        long diffInMillis = now.getTime() - date.getTime();
        long diffInMinutes = TimeUnit.MINUTES.convert(diffInMillis, TimeUnit.MILLISECONDS);

        if (diffInMinutes < JUST_NOW_LIMIT_IN_MINUTES
                && diffInMillis >= 0) {
            return context.getString(R.string.shared_just_now);
        } else {
            return toSimpleDateFormat(context, date);
        }
    }

    /**
     * The "Detailed Date with Time" display format should be used to display dates in transaction details.
     * If timestamp is not available, it reverts back to "Simple Date")
     * @param context activity context
     * @param date the date object to display
     * @return formatted simple date format as String
     */
    public static String toDatesWithTimeFormat(final Context context, @NonNull final Date date) {
        throw new RuntimeException("Not implemented");
    }

    /**
     * The "Long Date" display format should be used to display dates in a long format.
     * Example: "October 19, 2018"
     * @param context Android context
     * @param date the date object to display
     * @return formatted calendar date format as String
     */
    public static String toLongDateFormat(@NonNull final Context context, @NonNull final Date date) {
        return DateUtils.formatDateTime(context, date.getTime(), DateUtils.FORMAT_SHOW_YEAR);
    }
}
