package mn.bitsup.callservice.util;

import static android.graphics.Typeface.BOLD;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;

import mn.bitsup.callservice.R;

public class TextViewColor {

    public TextViewColor(){
    }

    public static Spannable getColoredText(Context context, String text, int start, int end) {
        Spannable wordtoSpan = new SpannableString(text);
        wordtoSpan.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.primary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new StyleSpan(BOLD), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return wordtoSpan;
    }

    public static Spannable getUnderlineText(Context context, String text, int start, int end) {
        Spannable wordtoSpan = new SpannableString(text);
        wordtoSpan.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.primary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new UnderlineSpan(), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return wordtoSpan;
    }

    public static Spannable getUnderlineTextHint(Context context, String text, int start, int end) {
        Spannable wordtoSpan = new SpannableString(text);
        wordtoSpan.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.blue_grey)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new UnderlineSpan(), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return wordtoSpan;
    }
}
