package mn.bitsup.callservice.util;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;

public class FingerSettings {

    public static KeyStore keyStore;
    public static Cipher cipher;
    public static String KEY_NAME = "fingerPrint";
    public  static PasscodeListener passcodeListener;
    public static CancellationSignal cancellationSignal;

    public static void stopFingerAuth(){
        if(cancellationSignal != null && !cancellationSignal.isCanceled()){
            cancellationSignal.cancel();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static class FingerprintHandler extends FingerprintManager.AuthenticationCallback {
        private Context context;
        public FingerprintHandler(Context mContext) {
            context = mContext;
        }

        public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
            cancellationSignal = new CancellationSignal();
            if (ActivityCompat
                .checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
        }

        @Override
        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Log.e("MBank", "onAuthenticationError: "+errString);
            if(errString.equals("Too many attempts. Fingerprint sensor disabled.")){
                this.update("Хэт олон оролдлго хурууны хээг идэвхгүй болгосон байна.");
            }
        }

        @Override
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        }

        @Override
        public void onAuthenticationFailed() {
            this.update("Хурууны хээ таарахгүй байна.");
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

            ClientsManager.remoteAuthClient.performAutomatedLogin(new PasscodeAuthListener() {
                @Override
                public void onSuccess() {
                    passcodeListener.onSuccess();
                }

                @Override
                public void onError(Response var1) {
                    passcodeListener.onError(var1);
                }
            });
        }

        private void update(String e) {
            Toast.makeText(context, "" + e, Toast.LENGTH_SHORT).show();
        }
    }

    public static void setInflateFingerPrint(Context context, PasscodeListener var1) {
        passcodeListener = var1;
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);
        FingerprintManager fingerprintManager = null;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            fingerprintManager = (FingerprintManager) context.getSystemService(FINGERPRINT_SERVICE);
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (!fingerprintManager.isHardwareDetected()) {
//                Toast.makeText(context, "Таны утас хурууны хээний мэдрэгчгүй байна", Toast.LENGTH_SHORT);
//            } else {
//                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(context, "Хурууны хээгийн зөвшөөрлийг идэвхжүүлээгүй байна", Toast.LENGTH_SHORT);
//
//                } else {
//                    if (!fingerprintManager.hasEnrolledFingerprints()) {
//                        Toast.makeText(context, "Тохиргооноос дор хаяж нэг хурууны хээ бүртгүүлэх хэрэгтэй", Toast.LENGTH_SHORT);
//                    } else {
//                        if (!keyguardManager.isKeyguardSecure()) {
//                            Toast.makeText(context, "Lock дэлгэцийн аюулгүй байдал Тохиргоо идэвхжээгүй", Toast.LENGTH_SHORT);
//                        } else {
//                            generateKey();
//                            if (cipherInit()) {
//                                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
//                                FingerprintHandler helper = new FingerprintHandler(context);
//                                helper.startAuth(fingerprintManager, cryptoObject);
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                KeyGenParameterSpec.Builder(KEY_NAME,
                KeyProperties.PURPOSE_ENCRYPT |
                    KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setUserAuthenticationRequired(true)
                .setEncryptionPaddings(
                    KeyProperties.ENCRYPTION_PADDING_PKCS7)
                .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
            InvalidAlgorithmParameterException
            | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public static boolean cipherInit() {
        try {
            cipher = Cipher
                .getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }
}
