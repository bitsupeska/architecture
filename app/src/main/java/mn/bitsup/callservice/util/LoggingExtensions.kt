@file:JvmName("LoggingUtils")

package mn.bitsup.callservice.util

/*
 * Created by Erdenedalai R&D B.V on 04/08/2019.
 */

val Any.TAG get(): String = when {
    this.javaClass.simpleName.contains("$") -> {
        val fullName = this.javaClass.name
        fullName.substring(fullName.lastIndexOf('.') + 1, fullName.indexOf('$'))
    }
    else -> this.javaClass.simpleName
}
