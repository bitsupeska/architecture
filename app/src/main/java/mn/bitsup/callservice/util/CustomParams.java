package mn.bitsup.callservice.util;

import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CustomParams {
    @NonNull
    final Map<String, String> map;

    public CustomParams() {
        this(new HashMap());
    }

    CustomParams(@NonNull Map<String, String> map) {
        this.map = map;
    }

    public final String get(String key) {
        return (String)this.map.get(key);
    }

    public final boolean containsKey(String key) {
        return this.map.containsKey(key);
    }

    public final Set<String> keySet() {
        return this.map.keySet();
    }
}