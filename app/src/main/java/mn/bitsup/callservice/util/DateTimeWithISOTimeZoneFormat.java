package mn.bitsup.callservice.util;

import androidx.annotation.NonNull;
import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class DateTimeWithISOTimeZoneFormat extends SimpleLimitedDateTimeFormat {
    static final String INTERNAL_TIME_ZONE_PART = "Z";
    private static final char COLON = ':';
    private static final String PATTERN_EXTERNAL_TIME_ZONE_FULL = "[+-][0-9]{2}:[0-9]{2}";
    private static final String PATTERN_INTERNAL_TIME_ZONE = "[+-][0-9]{4}";
    private static final String EXTERNAL_TIME_ZONE_Z = "Z";
    private static final String EXTERNAL_TIME_ZONE_UTC = "+00:00";
    private static final String INTERNAL_TIME_ZONE_UTC = "+0000";

    DateTimeWithISOTimeZoneFormat(String pattern) {
        super(pattern);
        if (!conformsToTimeZonePatternContract(pattern)) {
            throw new DateTimeWithISOTimeZoneFormat.MissingInternalTimeZonePartException("Z");
        }
    }

    DateTimeWithISOTimeZoneFormat(String pattern, @NonNull TimeZone zone) {
        super(pattern, zone);
        if (!conformsToTimeZonePatternContract(pattern)) {
            throw new DateTimeWithISOTimeZoneFormat.MissingInternalTimeZonePartException("Z");
        }
    }

    public final String format(Date date) {
        String internalFormatText = super.format(date);
        return formatTimeZoneForExternalUse(internalFormatText);
    }

    public final Date parse(String source) throws ParseException {
        String reformattedSource = formatTimeZoneForInternalParsing(source);
        return super.parse(reformattedSource);
    }

    private static String formatTimeZoneForExternalUse(String internalSource) {
        String textWithTimeZoneColon = insertTimeZoneColon(internalSource);
        String text = textWithTimeZoneColon;
        if (textWithTimeZoneColon.contains("+00:00")) {
            text = textWithTimeZoneColon.replace("+00:00", "Z");
        }

        return text;
    }

    private static String formatTimeZoneForInternalParsing(String externalSource) throws ParseException {
        return externalSource.contains("Z") ? externalSource.replace("Z", "+0000") : removeTimeZoneColon(externalSource);
    }

    private static String insertTimeZoneColon(String source) {
        String internalTimeZone = extractPattern(source, Pattern.compile("[+-][0-9]{4}"));
        int indexOfTimeZoneColon = "+00:00".indexOf(58);
        String externalTimeZone = insert(internalTimeZone, String.valueOf(':'), indexOfTimeZoneColon);
        return source.replace(internalTimeZone, externalTimeZone);
    }

    private static String removeTimeZoneColon(String source) throws ParseException {
        try {
            String externalTimeZone = extractPattern(source, Pattern.compile("[+-][0-9]{2}:[0-9]{2}"));
            int indexOfTimeZoneColon = externalTimeZone.indexOf(58);
            String internalTimeZone = removeRange(externalTimeZone, indexOfTimeZoneColon, indexOfTimeZoneColon + 1).toString();
            return source.replace(externalTimeZone, internalTimeZone);
        } catch (IllegalStateException var4) {
            throw new ParseException("Source string did not contain expected time zone: " + var4.getMessage(), source.length());
        }
    }

    private static String extractPattern(String source, Pattern pattern) {
        Matcher matcher = pattern.matcher(source);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new IllegalStateException("Did not find pattern " + pattern);
        }
    }

    private static String insert(String source, String insertion, int index) {
        return source.substring(0, index) + insertion + source.substring(index, source.length());
    }

    private static CharSequence removeRange(CharSequence text, int startIndex, int endIndex) {
        if (endIndex < startIndex) {
            throw new IndexOutOfBoundsException(String.format("End index %d is less than start index %d.", endIndex, startIndex));
        } else {
            return (CharSequence)(endIndex == startIndex ? text.subSequence(0, text.length()) : (new StringBuilder(text.length() - (endIndex - startIndex))).append(text, 0, startIndex).append(text, endIndex, text.length()));
        }
    }

    private static boolean conformsToTimeZonePatternContract(String pattern) {
        return pattern.contains("Z") && pattern.indexOf("Z") == pattern.lastIndexOf("Z");
    }

    public static class MissingInternalTimeZonePartException extends RuntimeException {
        private MissingInternalTimeZonePartException(String missingInternalTimeZonePart) {
            super("Expected pattern with time zone part <" + missingInternalTimeZonePart + "> exactly once, but the provided pattern did not have that part.");
        }
    }
}
