package mn.bitsup.callservice.util;

import android.view.View;

/**
 * Created by Erdenedalai R&D B.V on 27/12/2018.
 * <p>
 * A Debounced OnClickListener
 * Rejects clicks that are too close together in time.
 * This class is safe to use as an OnClickListener for multiple views, and will debounce each one separately.
 */
public abstract class DebouncedOnClickListener extends GenericDebouncedEvent<View> implements View.OnClickListener {

    /**
     * Implement this in your subclass instead of onClick
     *
     * @param v The view that was clicked
     */
    public abstract void onDebouncedClick(View v);

    @Override
    final public void onClick(View clickedView) {
        if (isDebounced(clickedView)) {
            onDebouncedClick(clickedView);
        }
    }
}
