package mn.bitsup.callservice.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationInputField {

    public static boolean isCorrectLength(final String password ){
        if(password.length()< 8 || password.length()>30 || password.isEmpty()){
            return false;
        }else {
            return true;
        }
    }
    public static boolean hasDigits(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean hasUpperCase(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[A-Z]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
    public static boolean hasLowerCase(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[a-z]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean hasSpecial(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[$&+,:;=?@#|'<>.^*()%!]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
