package mn.bitsup.callservice.util;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;

import mn.bitsup.callservice.R;

import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Erdenedalai R&D B.V on 20/07/2018.
 * Date formatter for all date formats used by the app.
 */
public final class DateTimeFormatter {
    private static final String TAG = DateTimeFormatter.class.getSimpleName();

    private static final int dateFlags = DateUtils.FORMAT_ABBREV_MONTH;
    private static final int dateWithNoYearFlags = dateFlags | DateUtils.FORMAT_NO_YEAR;

    private static LimitedDateTimeFormat getDateAndTimeAndMilliseconds() {
        return DBSDateTimeFormats.newIsoOffsetDateTimeInstance(true);
    }

    private static LimitedDateTimeFormat getDateAndTime() {
        return DBSDateTimeFormats.newIsoLocalDateTimeInstance();
    }

    private static LimitedDateTimeFormat getDateOnly() {
        return DBSDateTimeFormats.newIsoLocalDateInstance();
    }

    /**
     * Format a {@link Date} into a string, date and time including milliseconds are printed.
     *
     * @param date to format
     * @return the formatted date string in the format "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'".
     */
    public static String toDateAndTimeAndMillisecondsFormat(@NonNull final Date date) {
        return getDateAndTimeAndMilliseconds().format(date);
    }

    /**
     * Parse a formatted date into a {@link Date} object.
     *
     * @param date the formatted date string in the format "yyyy-MM-dd'T'HH:mm:ss.SSSZ".
     * @return Date object or null if parsing fails.
     */
    @Nullable
    public static Date fromDateAndTimeAndMillisecondsFormat(@NonNull final String date) {
        return fromFormat(date, getDateAndTimeAndMilliseconds());
    }

    /**
     * Parse a formatted date into a {@link Date} object.
     *
     * @param date the formatted date string in the format "yyyy-MM-dd'T'HH:mm:ss".
     * @return Date object or null if parsing fails.
     */
    @Nullable
    public static Date fromDateAndTimeFormat(@NonNull final String date) {
        return fromFormat(date, getDateAndTime());
    }

    /**
     * Format a {@link Date} into a string, only the date part is printed.
     *
     * @param date to format
     * @return the formatted date string in the format "yyyy-MM-dd".
     */
    public static String toDateOnlyFormat(@NonNull final Date date) {
        return getDateOnly().format(date);
    }

    /**
     * Parse a formatted date into a {@link Date} object.
     *
     * @param date the formatted date string in the format "yyyy-MM-dd".
     * @return Date object or null if parsing fails.
     */
    @Nullable
    public static Date fromDateOnlyFormat(@NonNull final String date) {
        return fromFormat(date, getDateOnly());
    }

    /**
     * Format a {@link Date} into a string, only the date part is printed.
     * This format is normally used for displaying the date in the UI.
     *
     * @param context    android Context.
     * @param date       to format.
     * @param useStrings if true, strings like "today", "tomorrow" and "yesterday" will be used.
     * @param showYear   if true, the returned date will include the year.
     * @return the formatted date string in the locale specific medium format or the strings today/tomorrow/yesterday.
     */
    public static String toDateForDisplayFormat(final Context context,
                                                @NonNull final Date date,
                                                final boolean useStrings,
                                                final boolean showYear) {
        if (useStrings) {
            if (DateUtils.isToday(date.getTime())) {
                return context.getString(R.string.shared_today);
            } else if (DateUtils.isToday(date.getTime() + DateUtils.DAY_IN_MILLIS)) {
                return context.getString(R.string.shared_yesterday);
            } else if (DateUtils.isToday(date.getTime() - DateUtils.DAY_IN_MILLIS)) {
                return context.getString(R.string.shared_tomorrow);
            }
        }
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
        return fmt.format(date);
    }

    /**
     * Convenience method for {@link DateTimeFormatter#toDateForDisplayFormat(Context, Date, boolean, boolean)}
     * that always shows the year.
     *
     * @return the formatted date string in the locale specific medium format with year or the strings today/tomorrow/yesterday.
     */
    public static String toDateForDisplayFormat(final Context context,
                                                @NonNull final Date date,
                                                final boolean useStrings) {
        return toDateForDisplayFormat(context, date, useStrings, true);
    }

    @Nullable
    private static Date fromFormat(@NonNull final String date, @NonNull final LimitedDateTimeFormat format) {
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    private DateTimeFormatter() {
    }

    public static String dateFormat(String dateText) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            date = df.parse(dateText);
            Log.e("asd", "onCreate: " + date.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert date != null;
        return df.format(date);
    }
}
