package mn.bitsup.callservice.util;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erdenedalai R&D B.V on 03/01/2018.
 * Contains utils methods related to phone such as start phone calls intent
 */

public class PhoneUtils {

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private static final String TAG = PhoneUtils.class.getSimpleName();

    private PhoneUtils() {
    }


    public static void checkCameraPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA)
            != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                Manifest.permission.CAMERA)) {
            } else {
                ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
            }
        }
    }

    public static void checkBioPermissions(Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP_MR1) {
                return;
            }

            PackageInfo packageInfo = context.getPackageManager()
                .getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
            //Get Permissions
            String[] requestedPermissions = packageInfo.requestedPermissions;
            final List<String> permissionsNeeded = new ArrayList<String>();
            for (int i = 0; i < requestedPermissions.length; i++) {
                String requestedPermission = requestedPermissions[i];
                if (!addPermission(permissionsNeeded, requestedPermission, context)) {
                    if (!permissionsNeeded.contains(requestedPermission)) {
                        permissionsNeeded.add(requestedPermission);
                    }
                }
            }

            if (requestedPermissions.length > 0) {
                if (permissionsNeeded.size() > 0) {
                    ArrayList<String> permissionsGroup = new ArrayList<String>();
                    for (int i = 0; i < permissionsNeeded.size(); i++) {
                        String permission = permissionsNeeded.get(i);
                        String group = permission;
                        try {
                            if (permission.contains("android.permission.")) {
                                PermissionInfo permissionInfo = context.getPackageManager()
                                    .getPermissionInfo(permission, PackageManager.GET_META_DATA);
                                CharSequence charSequence = permissionInfo
                                    .loadDescription(context.getPackageManager());
                                String[] split = permissionInfo.group
                                    .split("android.permission-group.");
                                if (split.length > 0) {
                                    group = split[split.length - 1];
                                } else {
                                    group = permissionInfo.group;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!permissionsGroup.contains(group)) {
                            permissionsGroup.add(group);
                        }
                    }
                    ActivityCompat.requestPermissions((Activity) context, permissionsNeeded.toArray(new String[permissionsNeeded.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    return;
                }
                return;
            }
        }catch (Exception e){
        }
    }

    public static boolean addPermission(List<String> permissionsList, String permission, Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return false;
        } else if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission))
                return false;
        }
        return true;
    }


    public static boolean isTimeZoneAutomatic(Context c) {
        return Settings.Global.getInt(c.getContentResolver(), Settings.Global.AUTO_TIME_ZONE, 0)
            == 1;
    }
}
