package mn.bitsup.callservice.util;

import android.text.TextWatcher;

/**
 * Created by Erdenedalai R&D B.V on 17/07/2018.
 *
 * A convenience {@link TextWatcher} extension interface that delivers only
 * {@link TextWatcher#beforeTextChanged(CharSequence, int, int, int)} and
 * {@link TextWatcher#afterTextChanged(android.text.Editable)}
 */

public abstract class BeforeAndAfterTextChangedListener implements TextWatcher {
    @Override
    public final void onTextChanged(CharSequence s, int start, int before, int count) {
        // Implementation of this method is forbidden
    }
}
