package mn.bitsup.callservice.util;

import androidx.annotation.NonNull;
import java.util.Map;

public class MutableParams extends CustomParams {
    public MutableParams() {
    }

    private MutableParams(@NonNull Map<String, String> map) {
        super(map);
    }

    public final void put(String key, String value) {
        this.map.put(key, value);
    }

    public static MutableParams wrap(@NonNull Map<String, String> map) {
        return new MutableParams(map);
    }
}
