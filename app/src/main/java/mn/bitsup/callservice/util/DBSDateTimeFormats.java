package mn.bitsup.callservice.util;

import android.os.Build.VERSION;
import java.util.TimeZone;

public final class DBSDateTimeFormats {
    private static final TimeZone UTC = TimeZone.getTimeZone("UTC");

    public static LimitedDateTimeFormat newIsoLocalDateInstance() {
        return new SimpleLimitedDateTimeFormat("yyyy-MM-dd", UTC);
    }

    public static LimitedDateTimeFormat newIsoLocalTimeInstance() {
        return new SimpleLimitedDateTimeFormat("HH:mm:ss", UTC);
    }

    public static LimitedDateTimeFormat newIsoLocalDateTimeInstance() {
        return newIsoLocalDateTimeInstance(false);
    }

    public static LimitedDateTimeFormat newIsoLocalDateTimeInstance(boolean withMilliseconds) {
        String pattern = "yyyy-MM-dd'T'HH:mm:ss" + getMillisecondsPart(withMilliseconds);
        return new SimpleLimitedDateTimeFormat(pattern, UTC);
    }

    public static LimitedDateTimeFormat newIsoOffsetDateTimeInstance() {
        return newIsoOffsetDateTimeInstance(false);
    }

    public static LimitedDateTimeFormat newIsoOffsetDateTimeInstance(TimeZone zone) {
        return newIsoOffsetDateTimeInstance(false, zone);
    }

    public static LimitedDateTimeFormat newIsoOffsetDateTimeInstance(boolean withMillisecond) {
        String pattern = getIsoOffsetDateTimePattern(withMillisecond);
        return (LimitedDateTimeFormat)(canUseNativeIsoTimeZonePattern() ? new SimpleLimitedDateTimeFormat(pattern) : new DateTimeWithISOTimeZoneFormat(pattern));
    }

    public static LimitedDateTimeFormat newIsoOffsetDateTimeInstance(boolean withMillisecond, TimeZone zone) {
        String pattern = getIsoOffsetDateTimePattern(withMillisecond);
        return (LimitedDateTimeFormat)(canUseNativeIsoTimeZonePattern() ? new SimpleLimitedDateTimeFormat(pattern, zone) : new DateTimeWithISOTimeZoneFormat(pattern, zone));
    }

    private static String getIsoOffsetDateTimePattern(boolean withMilliseconds) {
        return canUseNativeIsoTimeZonePattern() ? "yyyy-MM-dd'T'HH:mm:ss" + getMillisecondsPart(withMilliseconds) + "XXX" : "yyyy-MM-dd'T'HH:mm:ss" + getMillisecondsPart(withMilliseconds) + "Z";
    }

    private static String getMillisecondsPart(boolean withMilliseconds) {
        return withMilliseconds ? ".SSS" : "";
    }

    private static boolean canUseNativeIsoTimeZonePattern() {
        return VERSION.SDK_INT >= 24;
    }

    private DBSDateTimeFormats() {
        throw new UnsupportedOperationException("This class is only used for static factory methods");
    }
}
