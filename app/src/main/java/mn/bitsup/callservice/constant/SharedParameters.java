package mn.bitsup.callservice.constant;

/**
 * Created by gt907588 on 07/04/2016.
 */
public class SharedParameters {
    public final static String KEY_AUTHENTICATION_THRESHOLD = "KEY_AUTHENTICATION_THRESHOLD";
    public final static String KEY_CAPTURE_MODE = "KEY_CAPTURE_MODE";
    public final static String KEY_CAMERA = "KEY_CAMERA";
    public final static String KEY_TORCH = "KEY_TORCH";
    public final static String KEY_OVERLAY = "KEY_OVERLAY";
    public final static String KEY_LOG_LEVEL = "KEY_LOG_LEVEL";

    public final static String URL_ACTIVATION_DATA_SERVER = "URL_ACTIVATION_DATA_SERVER";
    public final static String URL_LKMS_SERVER = "URL_LKMS_SERVER";
    public final static String LKMS_PROFILE = "LKMS_PROFILE";

    public final static String KEY_LOG_DUMP = "KEY_LOG_DUMP";
    public final static String KEY_VIDEO_POST_MORTEN_RECORD_ENABLE = "KEY_VIDEO_POST_MORTEN_RECORD_ENABLE";
    public final static String KEY_VIDEO_POST_MORTEN_RECORD_FOLDER = "KEY_VIDEO_POST_MORTEN_RECORD_FOLDER";
    public final static String KEY_VIDEO_RECORD_ENABLE = "KEY_VIDEO_RECORD_ENABLE";
    public final static String KEY_VIDEO_RECORD_FOLDER = "KEY_VIDEO_RECORD_FOLDER";
    public final static String KEY_LOGS_FOLDER = "KEY_LOGS_FOLDER";
}
