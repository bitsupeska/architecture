package mn.bitsup.callservice.constant;

/**
 * Created by Erdenedalai R&D B.V on 01/05/2018.
 */
public class AppConstants {

    private AppConstants() {
    }

    public static final long LIST_ANIMATION_DURATION = 300;
    public static final long LIST_ANIMATION_STEP = 50;

    public static final String PAGE_PREFERENCE_TITLE = "title";

}
