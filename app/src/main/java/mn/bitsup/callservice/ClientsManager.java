package mn.bitsup.callservice;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import java.net.URI;
import java.net.URISyntaxException;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.RemoteAuthClient;
import mn.bitsup.callservice.client.call.CallClient;
import mn.bitsup.callservice.client.card.CardClient;
import mn.bitsup.callservice.client.dataprovider.CustomAssetsFileDBSDataProvider;
import mn.bitsup.callservice.client.info.InfoClient;
import mn.bitsup.callservice.client.loan.LoanClient;
import mn.bitsup.callservice.client.miniapp.MiniAppClient;
import mn.bitsup.callservice.client.notification.NotificationClient;
import mn.bitsup.callservice.client.offline.OfflineClient;
import mn.bitsup.callservice.client.otp.OTPClient;
import mn.bitsup.callservice.client.profile.ProfileClient;
import mn.bitsup.callservice.client.temporary.TemporaryClient;


//ssh://tfs.mbank.local:22/tfs/DefaultCollection/M-Wallet%20development/_git/android-app


public abstract class ClientsManager {

    private static final String OTP_API = "/be-internal-otp-service/v1.0";

    protected Context context;
    public static AuthClient authClient;
    public static RemoteAuthClient remoteAuthClient;
    public static OfflineClient offlineClient;
    public static ProfileClient profileClient;
    public static OTPClient otpClient;
    public static InfoClient infoClient;
    public static TemporaryClient temporaryClient;
    public static NotificationClient notificationClient;
    public static LoanClient loanClient;
    public static CardClient cardClient;
    public static MiniAppClient miniAppClient;
    public static CallClient callClient;

    ClientsManager(@NonNull final Context context) {
        this.context = context;
    }

    public static void registerClients(Context context) {
        final String baseUri = MainApplication.getServerURL();
        try {
            authClient = new AuthClient(context);
            remoteAuthClient = new RemoteAuthClient(context);
            offlineClient = new OfflineClient(new CustomAssetsFileDBSDataProvider(context),new URI(baseUri));
            profileClient = new ProfileClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri));
            otpClient = new OTPClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri + OTP_API));
            infoClient = new InfoClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri ));
            temporaryClient = new TemporaryClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri ));
            notificationClient = new NotificationClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri ));
            loanClient = new LoanClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri ));
            miniAppClient = new MiniAppClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri));
            callClient = new CallClient(new CustomAssetsFileDBSDataProvider(context), new URI(baseUri));
            Log.e("Client Manager", "registerClients: orloo" );
        } catch (URISyntaxException e) {
            Log.e("Client Manager", "registerClients error: orloo" +e.getMessage());
            e.printStackTrace();
        }
    }

    public static AuthClient getAuthClient() {
        return authClient;
    }

    public static OfflineClient getOfflineClient() {
        return offlineClient;
    }

    public static ProfileClient getProfileClient() {
        return profileClient;
    }

    public static RemoteAuthClient getRemoteAuthClient() {
        return remoteAuthClient;
    }

    public static OTPClient getOtpClient() {
        return otpClient;
    }

    public static InfoClient getInfoClient() {
        return infoClient;
    }

    public static TemporaryClient getTemporaryClient() {
        return temporaryClient;
    }

    public static LoanClient getLoanClient() {
        return loanClient;
    }

    public static MiniAppClient getMiniAppClient(){
        return miniAppClient;
    }

}
