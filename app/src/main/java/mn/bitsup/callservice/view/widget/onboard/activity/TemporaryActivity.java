package mn.bitsup.callservice.view.widget.onboard.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.activity.BaseActivity;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.loan.view.LoanFingerView;
import mn.bitsup.callservice.view.widget.onboard.view.temporary.TPAddressCheckWidgetView;

public class TemporaryActivity extends BaseActivity {

    private FragmentManager fragmentManager;
    private ImageButton backButton, closeButton;
    public static ProgressBar progressToolbar;
    public static RelativeLayout containerToolbar;
    public static WidgetListView widgetListView;
    private Context context;
    private Activity activity;
    public static Temporary temporary;
    public static String type = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.onboard_activity);
        type = getIntent().getStringExtra("type");
        context = TemporaryActivity.this;
        activity = TemporaryActivity.this;
        temporary = new Temporary();
        fragmentManager = getSupportFragmentManager();
        inflatedViews();

    }

    private void inflatedViews() {
        progressToolbar = findViewById(R.id.progressToolbar);
        backButton = findViewById(R.id.backButton);
        closeButton = findViewById(R.id.closeButton);
        containerToolbar = findViewById(R.id.containerToolbar);
        widgetListView = findViewById(R.id.widgetListView);

        setProgressToolbar(10);
        initView();
    }

    private void initView() {
        backButton.setOnClickListener(v -> back());
        closeButton.setOnClickListener(v -> showCancelDialog());

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (MainApplication.getUser().getCif().equals("0")) {
            fragmentTransaction.replace(R.id.content_frame, new LoanFingerView());
        } else {
            setUserInfo();
            fragmentTransaction.replace(R.id.content_frame, new TPAddressCheckWidgetView());
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setUserInfo() {
        User user = MainApplication.getUser();

        temporary.setCifId(user.getCif());
        temporary.setBirthDate(user.getBirthDt());
        temporary.setRegNum(user.getRegNum());
        temporary.setFirstName(user.getFirstName());
        temporary.setLastName(user.getLastName());
        temporary.setFamilyName(user.getFamilyName());
        temporary.setDocExpDate(user.getDocExpDt().replace("T00:00:00.000", ""));
        temporary.setDocIssueDate(user.getDocIssueDt().replace("T00:00:00.000", ""));
        temporary.setDidSign("1");

        if (user.getAddresses() != null) {
            if (user.getAddresses().size() != 0) {
                Addresses mailAddress = null;
                Addresses homeAddress = null;
                for (int i = 0; i < user.getAddresses().size(); i++) {
                    if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                        mailAddress = user.getAddresses().get(i);

                    }
                    if (user.getAddresses().get(i).getAddressType().equals("Home")) {
                        homeAddress = user.getAddresses().get(i);
                    }
                }

                if (homeAddress != null) {
                    temporary.setTownHome(homeAddress.getTownId());
                    temporary.setTownHomeName(homeAddress.getTown());
                    temporary.setLocalityHome(homeAddress.getLocalityId());
                    temporary.setLocalityHomeName(homeAddress.getLocality());
                    temporary.setSectionHome(homeAddress.getStreet() + homeAddress.getSection());
                    temporary.setDistrictHome(homeAddress.getDistrict());
                    temporary.setApartmentHome(homeAddress.getApartment());
                    temporary.setDoorNumberHome(homeAddress.getDoorNumber());
                } else {
                    temporary.setTownHome(mailAddress.getTownId());
                    temporary.setTownHomeName(mailAddress.getTown());
                    temporary.setLocalityHome(mailAddress.getLocalityId());
                    temporary.setLocalityHomeName(mailAddress.getLocality());
                    temporary.setTownHome(mailAddress.getTownId());
                    temporary.setLocalityHome(mailAddress.getLocalityId());
                    temporary.setSectionHome(mailAddress.getStreet() + mailAddress.getSection());
                    temporary.setDistrictHome(mailAddress.getDistrict());
                    temporary.setApartmentHome(mailAddress.getApartment());
                    temporary.setDoorNumberHome(mailAddress.getDoorNumber());
                }
            }
        }

        if (user.getGender().equals("Эрэгтэй")) {
            temporary.setGender("M");
        } else {
            temporary.setGender("F");
        }
    }

    private void showCancelDialog() {
        new AlertDialog.Builder(context)
                .setMessage("Бүртгэлийг зогсоох уу?")
                .setNegativeButton(R.string.shared_alert_button_no, (dialog, which) -> dialog.dismiss())
                .setPositiveButton(R.string.shared_alert_button_yes, (dialog, which) -> {
                    dialog.dismiss();
                    activity.finish();
                }).show();
    }

    private void back() {
        if (!CustomButton.isLoading) {
            if (fragmentManager != null) {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    KeyboardUtils.hideKeyboard(this);
                    fragmentManager.popBackStack();
                    setProgressToolbar(progressToolbar.getProgress() - 13);
                } else {
                    activity.finish();
                }
            }
        }
    }

    public static void addProgress() {
        setProgressToolbar(progressToolbar.getProgress() + 13);
    }

    public static void setProgressToolbar(int width) {
        progressToolbar.setProgress(width);
    }


    @Override
    public void onBackPressed() {
        if (!CustomButton.isLoading) {
            if (fragmentManager != null) {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    KeyboardUtils.hideKeyboard(this);
                    setProgressToolbar(progressToolbar.getProgress() - 10);
                } else {
                    activity.finish();
                }
            } else {
                activity.finish();
            }
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
