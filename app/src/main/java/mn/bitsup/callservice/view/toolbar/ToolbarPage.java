package mn.bitsup.callservice.view.toolbar;

/**
 * Created by Erdenedalai R&D B.V on 03/11/2017.
 * Model defining the toolbar to open for the page.
 */

public enum ToolbarPage {
    PRODUCTS,
    DEFAULT,
    NO_TOOLBAR,
}
