package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class TPEmployeeRoleWidgetView extends Fragment implements
        OnboardView<OnboardContract>, TextWatcher{

    private OnboardContract contract;
    private Context context;
    private UpdateContinueButtonTextWatcher updateContinueButtonTextWatcher = new UpdateContinueButtonTextWatcher();
    private CustomButton buttonContinue;
    private IconSpinner spinnerBranch, spinnerSubBranch;
    private InputField inputJobName, inputJobPosition;
    private List<TemporaryItems> temporaryItems;
    private List<String> branchValue;
    private List<String> branch;
    private List<String> subBranch;
    private List<String> subBranchValue;

    private Temporary payload;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_employee_role, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        spinnerBranch = inflatedView.findViewById(R.id.spinnerJobBranch);
        spinnerSubBranch = inflatedView.findViewById(R.id.spinnerJobSubBranch);
        inputJobName = inflatedView.findViewById(R.id.inputJobName);
        inputJobPosition = inflatedView.findViewById(R.id.inputJobPosition);
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
        inputJobName.addTextChangedListener(updateContinueButtonTextWatcher);
        inputJobPosition.addTextChangedListener(updateContinueButtonTextWatcher);
    }

    private void initView() {
        spinnerBranch.setIconViewVisible(false);
        spinnerSubBranch.setIconViewVisible(false);
        inputJobName.setMaxInputLength(50);
        inputJobName.setSingleLine();
        inputJobPosition.setSingleLine();
        inputJobPosition.setMaxInputLength(50);
        inputJobPosition.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputJobPosition.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                inputJobPosition.hideSoftInput();
                return true;
            }
            return false;
        });

        inputJobName.setIconViewVisible(false);
        inputJobPosition.setIconViewVisible(false);
        buttonContinue.setOnClickListener(v -> publish());
        checkEmployeeStatus();
    }

    private void checkEmployeeStatus() {
        payload = contract.getTemporary();
        switch (payload.getEmploymentStatus()) {
            case "Employed":
                getOccstatlookup();
                break;
            case "FreeLancer":
                spinnerSubBranch.setVisibility(View.GONE);
                spinnerBranch.setVisibility(View.GONE);

                inputJobName.setHintText("Салбар");
                inputJobPosition.setHintText("Эрхэлдэг ажил");
                break;
            default:
                break;
        }
    }

    private void getOccstatlookup() {
        contract.getOccstatlookup(new TemporaryItemsListener() {
            @Override
            public void onSuccess(List<TemporaryItems> temporaryItems) {
                setBranchData(temporaryItems);
            }

            @Override
            public void onError(Response response) {
            }
        });
    }

    private void setBranchData(List<TemporaryItems> temporaryItems) {
        this.temporaryItems = temporaryItems;
        branchValue = new ArrayList<>();
        branch = new ArrayList<>();

        branch.add("Сонгох");
        branchValue.add("");
        for (int i = 0; i < temporaryItems.size(); i++) {
            if (temporaryItems.get(i).getValue() != null && temporaryItems.get(i).getLocaleText() != null) {
                if (temporaryItems.get(i).getValue().length() == 2) {
                    branchValue.add(temporaryItems.get(i).getValue());
                    branch.add(temporaryItems.get(i).getLocaleText());
                }
            }
        }

        spinnerBranch.setItems(branch);

        if (payload.getEmploymentStatus().equals("Employed")) {
            spinnerBranch.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    setSubBranchData();
                    buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
                    KeyboardUtils.hideKeyboard(getActivity());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
                }
            });

            spinnerSubBranch.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
                    KeyboardUtils.hideKeyboard(getActivity());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
                }
            });
        }
    }

    private void setSubBranchData() {
        subBranch = new ArrayList<>();
        subBranchValue = new ArrayList<>();

        subBranch.add("Сонгох");
        subBranchValue.add("");

        int count = 0;
        for (int i = 0; i < temporaryItems.size(); i++) {
            if (temporaryItems.get(i).getValue() != null && temporaryItems.get(i).getLocaleText() != null) {
                if (temporaryItems.get(i).getValue().length() == 4) {
                    if (temporaryItems.get(i).getValue().substring(0, 2).equals(branchValue.get(spinnerBranch.getSelectedPosition()))) {
                        count ++;
                        subBranch.add(temporaryItems.get(i).getLocaleText());
                        subBranchValue.add(temporaryItems.get(i).getValue());
                    }
                }
            }
        }
        if (count != 0) {
            spinnerSubBranch.setItems(subBranch);
            spinnerSubBranch.setVisibility(View.VISIBLE);
        } else {
            subBranch = new ArrayList<>();
            subBranchValue = new ArrayList<>();
            spinnerSubBranch.setVisibility(View.GONE);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if(inputJobName.getValue().length() < 2) inputJobName.setError(R.string.temporary_address_job_name_error + "");
        else inputJobName.setError(null);

        if(inputJobPosition.getValue().length() < 2) inputJobPosition.setError(R.string.temporary_address_job_position_error + "");
        else inputJobPosition.setError(null);

        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    private class UpdateContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
            }
        }
    }

    private boolean isValid() {

        if(subBranchValue == null || subBranchValue.size() == 0){
            return !inputJobName.getValue().equals("")  && !inputJobPosition.getValue().equals("") && spinnerBranch.getSelectedPosition() != 0;
        }else{
            return !inputJobName.getValue().equals("")  && !inputJobPosition.getValue().equals("") && spinnerBranch.getSelectedPosition() != 0 && spinnerSubBranch.getSelectedPosition() != 0;
        }
    }

    private void publish() {
        KeyboardUtils.hideKeyboard(context, inputJobName);
        if (payload.getEmploymentStatus().equals("Employed")) {
            if (branchValue.size() != 0) {
                if (subBranchValue.size() != 0) {
                    payload.setDirection(subBranchValue.get(spinnerSubBranch.getSelectedPosition()));
                } else {
                    payload.setDirection(branchValue.get(spinnerBranch.getSelectedPosition()));
                }
            }
            payload.setWorkName(inputJobName.getValue());
            payload.setPosition(inputJobPosition.getValue());

        } else if (payload.getEmploymentStatus().equals("FreeLancer")) {
            payload.setWorkName(inputJobName.getValue());
            payload.setPosition(inputJobPosition.getValue());
        }

        contract.setTemporary(payload);
        LoanActivity.addProgress();
        EventHelper.publishBack(context, new TPDetailWidgetView());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}