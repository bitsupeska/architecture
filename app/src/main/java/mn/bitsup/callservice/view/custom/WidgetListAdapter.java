package mn.bitsup.callservice.view.custom;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import mn.bitsup.callservice.constant.AppConstants;
import mn.bitsup.callservice.view.animation.ShowFadeInUpAnimator;

public abstract class WidgetListAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private AnimationScrollListener onScrollListener = new AnimationScrollListener();
    private boolean scrolled;
    private int lastPosition = -1;

    protected boolean isScrolled() {
        return scrolled;
    }

    public RecyclerView.OnScrollListener getOnScrollListener() {
        return onScrollListener;
    }

    protected int getLastPosition() {
        return lastPosition;
    }

    protected void setLastPosition(int lastPosition) {
        this.lastPosition = lastPosition;
    }

    protected void animateItemAppearance(RecyclerView.ViewHolder holder, View itemView) {
        if (!isScrolled() && holder.getAdapterPosition() > getLastPosition()) {
            ShowFadeInUpAnimator.animate(itemView, AppConstants.LIST_ANIMATION_STEP * holder.getAdapterPosition(),
                    AppConstants.LIST_ANIMATION_DURATION, itemView.getContext());
            setLastPosition(holder.getAdapterPosition());
        }
    }

    class AnimationScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            scrolled = true;
        }
    }
}
