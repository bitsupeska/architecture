package mn.bitsup.callservice.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.CustomButton;

import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class GenericActivity extends BaseActivity {

    private FragmentManager fragmentManager;
    private Toolbar toolbar;
    private BackButtonStyle DEFAULT_BACK_BTN_STYLE = BackButtonStyle.BACK;
    public static Fragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_activity);
        toolbar = findViewById(R.id.toolbar);
        context = GenericActivity.this;
        activity = GenericActivity.this;
        fragmentManager = getSupportFragmentManager();
        configureToolbar();
        changeTitle(getIntent());
        initView();
    }

    public static void setFragment(Fragment newFragment) {
        fragment = newFragment;
    }

    private void changeTitle(Intent intent) {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            toolbar.setTitle(intent.getStringExtra(PAGE_PREFERENCE_TITLE));
        }
    }


    @Override
    protected BackButtonStyle getBackButtonStyle() {
        return DEFAULT_BACK_BTN_STYLE;
    }

    private void configureToolbar() {
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            configureToolbarNavigation(toolbar);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }
    }

    private void initView() {
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.content_frame, fragment);
            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (!CustomButton.isLoading) {
            KeyboardUtils.hideKeyboard(this);
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                fragment = null;
                this.setResult(Activity.RESULT_OK);
                this.finish();
                this.overridePendingTransition(R.anim.no_animation, R.anim.fade_out);
            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
