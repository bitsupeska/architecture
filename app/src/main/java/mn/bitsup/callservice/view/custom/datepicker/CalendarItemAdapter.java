package mn.bitsup.callservice.view.custom.datepicker;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 03/04/2018.
 * Adapter for each individual cell item in a calendar view
 */
public class CalendarItemAdapter extends RecyclerView.Adapter<CalendarItemAdapter.CalendarItemViewHolder> {

    private static final String TAG = CalendarItemAdapter.class.getSimpleName();
    private final Date minDate;
    private LayoutInflater layoutInflater;
    private List<Date> monthlyDates;
    private Calendar minDateCalendar;
    private ItemClickListener itemClickListener;
    private StyleProvider styler;

    public CalendarItemAdapter(Context context, List<Date> monthlyDates, StyleProvider styler, @Nullable Date minDate) {
        this.monthlyDates = monthlyDates;
        this.minDate = minDate;
        this.minDateCalendar = Calendar.getInstance();
        if (minDate != null) {
            this.minDateCalendar.setTime(minDate);
        }

        this.styler = styler;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CalendarItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.calendar_cell_layout, parent, false);
        return new CalendarItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CalendarItemViewHolder holder, int position) {
        holder.bind(monthlyDates.get(position));
    }

    @Override
    public int getItemCount() {
        return monthlyDates.size();
    }

    class CalendarItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textCellNumber;

        CalendarItemViewHolder(View itemView) {
            super(itemView);
            this.textCellNumber = itemView.findViewById(R.id.calendarDate);
            itemView.setOnClickListener(this);
        }

        public void bind(Date date) {
            Calendar dateCalendar = Calendar.getInstance();
            dateCalendar.setTime(date);
            setCellNumberTextAppearance(dateCalendar);
            int dayValue = dateCalendar.get(Calendar.DAY_OF_MONTH);
            textCellNumber.setText(String.valueOf(dayValue));

            Log.e("day", "day: "+String.valueOf(dayValue));
        }

        private void setCellNumberTextAppearance(Calendar dateCalendar) {
            if(minDate != null && dateCalendar.getTime().before(minDate)){
                ((View)textCellNumber.getParent()).setEnabled(false);
                ((View)textCellNumber.getParent()).setClickable(false);
                ((View)textCellNumber.getParent()).setFocusable(false);
            } else {
                ((View) textCellNumber.getParent()).setEnabled(true);
                ((View) textCellNumber.getParent()).setClickable(true);
                ((View) textCellNumber.getParent()).setFocusable(true);
            }

            if (DateUtils.isToday(dateCalendar.getTime())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textCellNumber.setTextAppearance(styler.onMinDateTextStyleRequested());
                } else {
                    textCellNumber.setTextAppearance(textCellNumber.getContext(), styler.onMinDateTextStyleRequested());
                }
            } else if (minDate != null && dateCalendar.getTime().before(minDate)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textCellNumber.setTextAppearance(styler.onBeforeMinDateTextStyleRequested());
                } else {
                    textCellNumber.setTextAppearance(textCellNumber.getContext(), styler.onBeforeMinDateTextStyleRequested());
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textCellNumber.setTextAppearance(styler.onAfterMinDateTextStyleRequested());
                } else {
                    textCellNumber.setTextAppearance(textCellNumber.getContext(), styler.onAfterMinDateTextStyleRequested());
                }
            }
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

    void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
