package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;;
import android.widget.AdapterView.OnItemSelectedListener;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.Location;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class TPAddAddressWidgetView extends Fragment implements
    OnboardView<OnboardContract>, AdapterView.OnItemSelectedListener {

    private IconSpinner spinnerCityAimag, spinnerDistrictSum;
    private InputField inputKhoroo, inputApartment,inputSection, inputNumber;
    private CustomButton buttonContinue;
    private List<Location> cities;
    private List<Location> districts;
    private UpdateContinueButtonTextWatcher updateContinueButtonTextWatcher = new UpdateContinueButtonTextWatcher();
    private OnboardContract contract;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView =inflater.inflate(R.layout.temporary_address_add, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        spinnerCityAimag = inflatedView.findViewById(R.id.spinnerCity);
        spinnerDistrictSum = inflatedView.findViewById(R.id.spinnerDistrict);
        inputKhoroo = inflatedView.findViewById(R.id.inputKhoroo);
        inputApartment = inflatedView.findViewById(R.id.inputApartment);
        inputNumber = inflatedView.findViewById(R.id.inputNumber);
        inputSection = inflatedView.findViewById(R.id.inputSection);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
    }

    private void initView() {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
        inputKhoroo.addTextChangedListener(updateContinueButtonTextWatcher);
        inputApartment.addTextChangedListener(updateContinueButtonTextWatcher);
        inputSection.addTextChangedListener(updateContinueButtonTextWatcher);
        inputNumber.addTextChangedListener(updateContinueButtonTextWatcher);
        spinnerCityAimag.setIconViewVisible(false);

        spinnerDistrictSum.setIconViewVisible(false);
        inputKhoroo.setIconViewVisible(false);
        inputKhoroo.setSingleLine();
        inputApartment.setSingleLine();
        inputApartment.setIconViewVisible(false);
        inputSection.setIconViewVisible(false);
        inputSection.setSingleLine();
        inputNumber.setSingleLine();
        inputNumber.setIconViewVisible(false);
        inputNumber.setLongClickable(false);
        inputNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputNumber.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                inputNumber.hideSoftInput();
                return true;
            }
            return false;
        });
        loadCityData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initEvent() {
        spinnerCityAimag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtils.hideKeyboard(getActivity());
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
                if(position == 0){
                    List<String> list = new ArrayList<>();
                    spinnerDistrictSum.setItems(list);
                }else{
                    loadDistrictData(cities.get(position-1).getLocationValue());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        buttonContinue.setOnClickListener(v -> publish());

        spinnerDistrictSum.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
                KeyboardUtils.hideKeyboard(getActivity());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
            }
        });
    }

    private void publish() {
        Temporary payload = contract.getTemporary();
        String cityAimagId = cities.get(spinnerCityAimag.getSelectedPosition()-1).getLocationValue();
        String districtSumId = districts.get(spinnerDistrictSum.getSelectedPosition()-1).getLocationValue().substring(2);

        payload.setTownHome(cityAimagId); //townMail
        payload.setLocalityHome(Integer.parseInt(districtSumId)+"");
        payload.setSectionHome(inputKhoroo.getValue());
        payload.setDistrictHome(inputSection.getValue());
        payload.setApartmentHome(inputApartment.getValue());
        payload.setDoorNumberHome(inputNumber.getValue());

        payload.setIsHomeAddress("1");
        contract.setTemporary(payload);
        LoanActivity.addProgress();
        EventHelper.publishBack(context, new TPEducationTypeWidgetView());
    }

    private boolean isValid() {
        return spinnerCityAimag.getSelectedPosition() != 0 && spinnerDistrictSum.getSelectedPosition() != 0 && !inputApartment.getValue().equals("")
                && !inputKhoroo.getValue().equals("")&& !inputSection.getValue().equals("") && !inputNumber.getValue().equals("");
    }

    private void loadCityData() {
        contract.loadCityData(new LocationListener() {
            @Override
            public void onSuccess(List<Location> data) {
                cities = data;
                List<String> list = new ArrayList<>();
                list.add("Сонгох");
                for (int i = 0; i < cities.size(); i++) {
                    list.add(cities.get(i).getLocationText().trim());
                }
                spinnerCityAimag.setItems(list);
            }

            @Override
            public void onError(Response errorResponse) {
                cities = new ArrayList<>();
            }
        });
    }

    private void loadDistrictData(String stateCode) {
        contract.loadDistrictData(new LocationListener() {
            @Override
            public void onSuccess(List<Location> data) {
                districts = data;
                List<String> list = new ArrayList<>();
                list.add("Сонгох");
                for (int i = 0; i < districts.size(); i++) {
                    list.add(districts.get(i).getLocationText());
                }
                spinnerDistrictSum.setItems(list);
            }

            @Override
            public void onError(Response errorResponse) {
                districts = new ArrayList<>();
            }
        }, stateCode);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        KeyboardUtils.hideKeyboard(getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    private class UpdateContinueButtonTextWatcher extends AfterTextChangedListener {

        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
            }
        }
    }
}
