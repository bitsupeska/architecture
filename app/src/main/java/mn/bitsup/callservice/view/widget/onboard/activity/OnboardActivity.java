package mn.bitsup.callservice.view.widget.onboard.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.activity.BaseActivity;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.onboard.view.easy.OBPhoneWidgetView;

public class OnboardActivity extends BaseActivity {

    private ViewGroup mainContainer;
    private FragmentManager fragmentManager;
    public static ImageButton backButton;
    private ImageButton closeButton;
    public static ProgressBar progressToolbar;
    public static RelativeLayout containerToolbar;
    private Context context;
    private Activity activity;
    public static EasyOnboardItem payload;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = OnboardActivity.this;
        activity = OnboardActivity.this;
        stopService(new Intent(this, RefreshService.class));
        startService(new Intent(this, RefreshService.class));

        setContentView(R.layout.onboard_activity);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new OnBackStackChangedListenerImpl());

        inflatedViews();
    }

    private void inflatedViews() {
        mainContainer = findViewById(R.id.content_frame);
        progressToolbar = findViewById(R.id.progressToolbar);
        backButton = findViewById(R.id.backButton);
        closeButton = findViewById(R.id.closeButton);
        containerToolbar = findViewById(R.id.containerToolbar);
        setProgressToolbar(10);
        initView();
        backButton.setVisibility(View.INVISIBLE);
    }

    private void initView() {
        backButton.setOnClickListener(v -> back());
        closeButton.setOnClickListener(v -> showCancelDialog());
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, new OBPhoneWidgetView());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static void hideToolbar() {
        containerToolbar.setVisibility(View.GONE);
    }

    public static void showBackImage(){
        backButton.setVisibility(View.VISIBLE);
    }

    private void showCancelDialog() {
        new AlertDialog.Builder(context)
                .setTitle("Бүртгэл зогсоох")
                .setMessage("Та бүртгэлээ зогсоовол таны оруулсан мэдээллүүд устах болно")
                .setNegativeButton(R.string.shared_alert_button_no, (dialog, which) -> dialog.dismiss())
                .setPositiveButton(R.string.shared_alert_button_yes, (dialog, which) -> {
                    dialog.dismiss();
                    activity.finish();
                }).show();
    }

    private void back() {
        if (!CustomButton.isLoading) {
            if (fragmentManager != null) {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    fragmentManager.popBackStack();
                    KeyboardUtils.hideKeyboard(this);
                    setProgressToolbar(progressToolbar.getProgress() - 10);
                } else {
                    activity.finish();
                }
            } else {
                activity.finish();
            }
        }
    }

    public static void addProgress() {
        setProgressToolbar(progressToolbar.getProgress() + 10);
    }

    public static void setProgressToolbar(int width) {
        progressToolbar.setProgress(width);
    }

    private class OnBackStackChangedListenerImpl implements FragmentManager.OnBackStackChangedListener {
        @Override
        public void onBackStackChanged() {
        }
    }

    @Override
    public void onBackPressed() {
        if (!CustomButton.isLoading) {
            if (fragmentManager != null) {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    KeyboardUtils.hideKeyboard(this);
                    setProgressToolbar(progressToolbar.getProgress() - 10);
                } else {
                    activity.finish();
                }
            } else {
                activity.finish();
            }
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
