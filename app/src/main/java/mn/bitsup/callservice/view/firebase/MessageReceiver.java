//package mn.bitsup.callservice.view.firebase;
//
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Intent;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.os.Build;
//import android.util.Log;
//import android.view.View;
//
//import androidx.core.app.NotificationCompat;
//
//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import mn.bitsup.callservice.R;
//import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
//import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
//import mn.bitsup.callservice.client.dataprovider.response.Response;
//import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
//import mn.bitsup.callservice.view.activity.EnrollmentActivity;
//
//import static mn.bitsup.callservice.ClientsManager.notificationClient;
//import static mn.bitsup.callservice.view.activity.MainActivity.notificationBadge;
//
//public class MessageReceiver extends FirebaseMessagingService {
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//
//        Log.d("MBank", "From: " + remoteMessage.getFrom());
//        Log.d("MBank", "getTitle: " + remoteMessage.getNotification().getTitle());
//        Log.d("MBank", "getBody: " + remoteMessage.getNotification().getBody());
//
//        Intent intent = new Intent(this, EnrollmentActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        String channelId = "Default";
//        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
//                .setSmallIcon(R.drawable.app_icon)
//                .setSound(alarmSound)
//                .setContentTitle(remoteMessage.getNotification().getTitle())
//                .setContentText(remoteMessage.getNotification().getBody())
//                .setAutoCancel(true).setContentIntent(pendingIntent);
//        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
//            manager.createNotificationChannel(channel);
//        }
//        manager.notify(0, builder.build());
//        loadNotificationCount();
//    }
//
//    private void loadNotificationCount() {
//        try {
//            Map<String, String> params = new HashMap<>();
//            StorageComponent storageComponent = new StorageComponent();
//            params.put("cif", storageComponent.getItem("uid"));
//            if (notificationClient != null)
//                notificationClient.getUnreadCount(params, new StatusListener() {
//                    @Override
//                    public void onSuccess(StatusResponse var1) {
//                        if (notificationBadge != null) {
//                            if (!var1.getMessage().equals("0")) {
//                                notificationBadge.setVisibility(View.VISIBLE);
//                                notificationBadge.setText(var1.getMessage());
//                            } else {
//                                notificationBadge.setVisibility(View.GONE);
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onError(Response var2) {
//                        if (notificationBadge != null)
//                            notificationBadge.setVisibility(View.GONE);
//                    }
//                });
//
//        } catch (Exception ex) {
//            Log.e("Notification", "loadNotificationCount: " + ex.toString());
//
//        }
//
//    }
//}
