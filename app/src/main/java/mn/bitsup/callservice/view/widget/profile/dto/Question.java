package mn.bitsup.callservice.view.widget.profile.dto;

import mn.bitsup.callservice.client.profile.dto.QuestionItem;

public class Question {
    private String title;
    private QuestionItem questionItem;

    public Question(String title, QuestionItem questionItem) {
        this.title = title;
        this.questionItem = questionItem;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public QuestionItem getQuestionItem() {
        return questionItem;
    }

    public void setQuestionItem(QuestionItem questionItem) {
        this.questionItem = questionItem;
    }
}
