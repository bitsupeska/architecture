package mn.bitsup.callservice.view.animation;

import android.view.animation.Animation;

/**
 * Created by Erdenedalai R&D B.V on 19/10/2017.
 */

public abstract class AnimationEndListener implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(Animation animation) {
        //THIS IS NOT NECESSARY IN THE SCOPE OF THIS CLASS
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        //THIS IS NOT NECESSARY IN THE SCOPE OF THIS CLASS
    }
}
