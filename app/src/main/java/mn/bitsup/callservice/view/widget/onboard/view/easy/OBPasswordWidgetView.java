package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.util.EmojiFilter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.ValidationInputField;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputFieldPassword;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBPasswordWidgetView extends Fragment implements OnboardView<OnboardContract>, TextWatcher {

    private Context context;
    private OnboardContract contract;
    private CustomButton buttonContinue;
    private InputFieldPassword inputFirstPassword;
    private InputFieldPassword inputSecondPassword;
    private EasyOnboardItem payload;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contract = new OnboardWidget(getContext());
        this.context = getContext();
        this.payload = contract.getPayload();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView =inflater.inflate(R.layout.onboard_password, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputFirstPassword = inflatedView.findViewById(R.id.inputFirstPassword);
        inputSecondPassword = inflatedView.findViewById(R.id.inputSecondPassword);
    }

    private void initView(){

        inputFirstPassword.setIconViewVisible(false);
        inputFirstPassword.addTextChangedListener(this);
        inputFirstPassword.setSingleLine();
        inputFirstPassword.setPasswordType();
        inputFirstPassword.setMaxLength(30);
        inputFirstPassword.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        inputFirstPassword.setFilters(EmojiFilter.getFilter());

        inputSecondPassword.setIconViewVisible(false);
        inputSecondPassword.addTextChangedListener(this);
        inputSecondPassword.setSingleLine();
        inputSecondPassword.setMaxLength(30);
        inputSecondPassword.setPasswordType();
        inputSecondPassword.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputSecondPassword.setFilters(EmojiFilter.getFilter());

        buttonContinue.setLoadingText("");
        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        buttonContinue.setOnClickListener(v -> createUser());
    }

    private void createUser(){
        inputSecondPassword.hideSoftInput();
        if(inputFirstPassword.getValue().equals(inputSecondPassword.getValue())){


            payload.setUserName(payload.getUserName().toLowerCase());
            payload.setUserPassword(inputSecondPassword.getValue());
            contract.setPayload(payload);
            publish();
        }else{
            CustomAlertDialog.showAlertDialog(context, "", context.getResources().getString(R.string.onboard_valid_confirm));
        }
    }

    private void publish(){
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        OnboardActivity.addProgress();
        EventHelper.publishBack(context, new OBTermsWidgetView());
    }

    private void isValidPassword(InputFieldPassword inputField) {
        String password = inputField.getValue();
        if (ValidationInputField.hasDigits(password) && ValidationInputField.hasLowerCase(password) && ValidationInputField.hasUpperCase(password) && ValidationInputField.hasSpecial(password) && ValidationInputField.isCorrectLength(password)) inputField.setError(null);
        else inputField.setError(context.getResources().getString(R.string.onboard_valid_password));
        if (password.toLowerCase().contains(OBUsernameWidgetView.inputUserName.getValue().toLowerCase())) inputField.setError(context.getResources().getString(R.string.onboard_contains_username));
    }

    private void comparePasswords(){
        if(inputFirstPassword.getValue().length() > 0 && inputSecondPassword.getValue().length() > 0){
            if (!inputFirstPassword.getValue().equals(inputSecondPassword.getValue())) {
                if(inputFirstPassword.hasFocus()) inputFirstPassword.setError(context.getResources().getString(R.string.onboard_valid_confirm));
                if(inputSecondPassword.hasFocus()) inputSecondPassword.setError(context.getResources().getString(R.string.onboard_valid_confirm));
            }
        }
    }
    
    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if(inputFirstPassword.getValue().length() > 0) isValidPassword(inputFirstPassword);
        if(inputSecondPassword.getValue().length() > 0) isValidPassword(inputSecondPassword);
        if(inputFirstPassword.getValue().length() > 0 && inputSecondPassword.getValue().length() > 0) comparePasswords();

        if(inputFirstPassword.getError().equals("") && inputSecondPassword.getError().equals("")) buttonContinue.setState(ButtonState.ENABLED, getActivity());
        else buttonContinue.setState(ButtonState.DISABLED, getActivity());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
