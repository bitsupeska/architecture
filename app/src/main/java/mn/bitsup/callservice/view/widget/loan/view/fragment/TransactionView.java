package mn.bitsup.callservice.view.widget.loan.view.fragment;

import android.view.WindowManager;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.HashMap;
import java.util.List;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.widget.loan.adapter.BankListAdapter;
import mn.bitsup.callservice.view.widget.loan.view.dto.BankBottomSheetDialog;
import mn.bitsup.callservice.view.widget.loan.view.dto.BankItemPayload;

public class TransactionView extends BaseRepaymentView {

    public TransactionView(LoanAccountData loanAccountData) {
        super(loanAccountData);
    }


    @Override
    public void loadBankData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("profileSettings", "");
        param.put("qpay", "");
        param.put("transfer", "1");
        this.loanContract.getBanks(new BankListener() {
            @Override
            public void onSuccess(List<BankItem> var1) {
                recyclerItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                OverScrollDecoratorHelper.setUpOverScroll(recyclerItems, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);
                BankListAdapter adapter = new BankListAdapter(getContext(), var1, new BankListAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClicked(BankItemPayload item) {
                        if (amount == 0) {
                            CustomAlertDialog.showAlertDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_button_warning), "Та төлөлт хийх дүнгээ оруулна уу");
                        } else {
                            try {
                                BankBottomSheetDialog bottomSheetDialog = new BankBottomSheetDialog(item.getImageId(), item.getBankItem(), amount, getActivity(),TransactionView.super.loanAccountData.getAccountNo());
                                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                                assert getFragmentManager() != null;
                                bottomSheetDialog.show(getFragmentManager(), "bank bottom");

                            }catch (Exception ex){
                                CustomAlertDialog.showAlertDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title), "Дансны мэдээлэл алга");
                            }

                        }
                    }
                });
                recyclerItems.setAdapter(adapter);
            }

            @Override
            public void onError(Response var1) {

            }
        }, param);
    }
}
