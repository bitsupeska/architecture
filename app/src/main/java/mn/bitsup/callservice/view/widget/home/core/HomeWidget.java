package mn.bitsup.callservice.view.widget.home.core;

import android.content.Context;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.client.call.listener.CallHistoryListener;

import static mn.bitsup.callservice.ClientsManager.callClient;

public class HomeWidget implements HomeContract {

    private Context context;
    public HomeWidget(Context context) {
        this.context = context;
    }

    @Override
    public void getHistories(int currentPage, int size, CallHistoryListener callHistoryListener) {
        Map<String, String> params = new HashMap();
        params.put("page", String.valueOf(currentPage));
        params.put("limit", String.valueOf(size));
        if(callClient == null){
            Toast.makeText(context,"Null baina",Toast.LENGTH_LONG).show();
        }else{
            callClient.getHistories(params,callHistoryListener);

        }
    }
}
