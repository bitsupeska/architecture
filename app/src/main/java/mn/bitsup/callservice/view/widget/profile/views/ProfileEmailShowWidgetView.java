package mn.bitsup.callservice.view.widget.profile.views;

        import android.app.Activity;
        import android.content.Context;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;

        import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;
        import androidx.appcompat.app.AlertDialog;
        import androidx.fragment.app.Fragment;

        import java.util.HashMap;

        import mn.bitsup.callservice.R;
        import mn.bitsup.callservice.client.dataprovider.response.Response;
        import mn.bitsup.callservice.client.offline.dto.Block;
        import mn.bitsup.callservice.client.offline.dto.BlockData;
        import mn.bitsup.callservice.client.offline.listener.BlockListener;
        import mn.bitsup.callservice.util.DeviceAddressUtil;
        import mn.bitsup.callservice.view.activity.GenericActivity;
        import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
        import mn.bitsup.callservice.view.custom.CustomButton;
        import mn.bitsup.callservice.view.custom.DataCell;
        import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
        import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

        import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class ProfileEmailShowWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private CustomButton buttonContinue;
    private DataCell cellEmail;
    private Context context;
    private ProfileContract contract;
    private String username;
    private String macAddress;
    private String ipAddress;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        this.username = contract.getUser().getUid().toLowerCase();
        this.macAddress = DeviceAddressUtil.getMACAddress(context,"wlan0");
        this.ipAddress = DeviceAddressUtil.getIPAddress(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_email_show, container, false);
        inflatedView(inflatedView);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        cellEmail = inflatedView.findViewById(R.id.cellEmail);
    }

    private void initViews() {
        String email = "";
        if (contract.getUser().getEmails() != null)
            email = contract.getUser().getEmails().get(0).getEmailId().toLowerCase();
        if (email != null && !email.equals("")) cellEmail.setValueText(email);
        checkUsed(email);
        buttonContinue.setOnClickListener(v -> checkBlock());
    }

    public void publish() {
        GenericActivity.fragment = new ProfileEditEmailWidgetView();
        Intent intent = new Intent(context, GenericActivity.class);
        intent.putExtra(PAGE_PREFERENCE_TITLE, "И-мэйл шинэчлэх");
        startActivityForResult(intent, 1);
    }

    private void checkUsed(String email) {
        if (email.equals("youremail@example.com")) {
            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
            alertdialog.setMessage(context.getResources().getString(R.string.shared_alert_button_warning)).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertdialog.create();
            alert.setTitle(context.getResources().getString(R.string.profile_information_other_used_changed_warning));
            alert.show();
        }
    }

    boolean isBlocked = false;

    private void checkBlock() {
        HashMap<String, String> blockParams = new HashMap<>();
        blockParams.put("value", username);
        blockParams.put("macAddress", macAddress);
        blockParams.put("ipAddress", ipAddress);

        contract.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block block) {
                if (block.getData() != null && block.getData().size() > 0)
                    for (BlockData blockData : block.getData()) {
                        if (blockData.getBlockinfo().equals(username) && blockData.getBlocktype() == 3 && blockData.getBlockchannel() == 4) {
                            isBlocked = true;
                        }
                    }

                if (isBlocked) {
                    showError("block");
                } else {
                    publish();
                }
            }

            @Override
            public void onError(Response var1) {
                showError("server");
            }
        }, blockParams);
    }

    private void showError(String type) {
        switch (type) {
            case "server":
                NudgeControllerUtil.showErrorNotification(context.getResources().getString(R.string.shared_alert_generic_alert_error_title), context.getResources().getString(R.string.shared_alert_generic_error_server), context);
                break;
            case "block":
                NudgeControllerUtil.showErrorNotification(context.getResources().getString(R.string.shared_alert_generic_alert_error_title), context.getResources().getString(R.string.profile_information_otp_email_blocked), context);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_FIRST_USER) {
                String result = data.getStringExtra("result");
                String resultTitle = data.getStringExtra("resultTitle");
                String resultMessage = data.getStringExtra("resultMessage");
                if (result.equals("success"))
                    NudgeControllerUtil.showSuccessNotification(resultTitle, resultMessage, context);
                else NudgeControllerUtil.showErrorNotification(resultTitle, resultMessage, context);
                initViews();
            }
        }
    }
}
