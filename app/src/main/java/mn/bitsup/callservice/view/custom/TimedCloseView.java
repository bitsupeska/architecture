package mn.bitsup.callservice.view.custom;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.animation.LinearInterpolator;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import mn.bitsup.callservice.R;


/**
 * Created by Erdenedalai R&D B.V. on 12/11/2018.
 *
 * Shows a cancel button surrounded by an animated circle that indicates a timeout.
 */
public class TimedCloseView extends ConstraintLayout {
    private static final int DEFAULT_DURATION_MS = 3000;
    private static final int ANIMATION_ANGLE_START = -90;
    private static final int ANIMATION_ANGLE_END = 360;

    private Paint paint = new Paint();
    private int timeIndicatorDurationMs;
    private int animatedAngleValue;
    private float circleLineWidthPx;
    private ValueAnimator circleAnimator;
    private final RectF arcOval = new RectF();

    public TimedCloseView(final Context context) {
        this(context, null);
    }

    public TimedCloseView(final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TimedCloseView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setAntiAlias(true);
        setWillNotDraw(false);

        LayoutInflater.from(getContext()).inflate(R.layout.view_timed_close, this, true);

        if (attrs != null) {
            initWithStyledAttributes(attrs);
        }
    }

    private void initWithStyledAttributes(@NonNull final AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TimedCloseView, 0, 0);
        timeIndicatorDurationMs = a.getInteger(R.styleable.TimedCloseView_timeIndicatorDurationMs, DEFAULT_DURATION_MS);
        circleLineWidthPx = (float) a.getDimensionPixelSize(R.styleable.TimedCloseView_circleLineWidth, 0);
        final int circleBackgroundColor = a.getColor(R.styleable.TimedCloseView_timeIndicatorColor, Color.TRANSPARENT);
        setTimeIndicatorColor(circleBackgroundColor);
        a.recycle();
    }

    /**
     * Set the color of the time indicator circle.
     * @param color
     */
    public void setTimeIndicatorColor(@ColorInt final int color) {
        this.paint.setColor(color);
        invalidate();
    }

    /**
     * Set the duration in ms of the animation.
     * @param durationMs the duration in milliseconds
     */
    public void setTimeIndicatorDurationMs(final int durationMs) {
        timeIndicatorDurationMs = durationMs;
    }

    /**
     * Start the animation.
     * If an animation is in progress it is restarted.
     */
    public void startAnimation() {
        safeCancelAnimation();
        resetAndRedraw();

        circleAnimator = ValueAnimator.ofInt(animatedAngleValue, 0);
        circleAnimator.setDuration(timeIndicatorDurationMs);
        circleAnimator.addListener(new AnimatorListenerImpl());
        circleAnimator.addUpdateListener(new AnimatorUpdateListenerImpl());
        circleAnimator.setInterpolator(new LinearInterpolator());
        circleAnimator.start();
    }

    /**
     * Start the animation to the initial state.
     */
    public void resetAnimation() {
        safeCancelAnimation();
        resetAndRedraw();
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        arcOval.top = circleLineWidthPx/2f;
        arcOval.bottom = getMeasuredHeight() - circleLineWidthPx/2f;
        arcOval.left = circleLineWidthPx/2f;
        arcOval.right = getMeasuredWidth() - circleLineWidthPx/2f;
    }

    @Override
    protected void onDetachedFromWindow() {
        safeCancelAnimation();
        super.onDetachedFromWindow();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(circleLineWidthPx);

        canvas.drawArc(arcOval, ANIMATION_ANGLE_START, animatedAngleValue, false, paint);
        super.onDraw(canvas);
    }

    private void resetAndRedraw() {
        animatedAngleValue = ANIMATION_ANGLE_END;
        invalidate();
    }

    private void safeCancelAnimation() {
        if (circleAnimator != null) {
            circleAnimator.cancel();
            circleAnimator = null;
        }
    }

    private class AnimatorUpdateListenerImpl implements ValueAnimator.AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(final ValueAnimator valueAnimator) {
            animatedAngleValue = (int) valueAnimator.getAnimatedValue();
            invalidate();
        }
    }

    private class AnimatorListenerImpl implements Animator.AnimatorListener {
        @Override
        public void onAnimationStart(final Animator animator) {
            // Not needed
        }

        @Override
        public void onAnimationEnd(final Animator animator) {
            // Not needed
        }

        @Override
        public void onAnimationCancel(final Animator animator) {
            resetAndRedraw();
        }

        @Override
        public void onAnimationRepeat(final Animator animator) {
            // Not needed
        }
    }
}
