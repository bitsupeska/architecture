//package mn.ashidcapital.simple.view.dialog;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.Color;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//import com.airbnb.lottie.LottieAnimationView;
//import java.util.ArrayList;
//import java.util.Objects;
//import mn.ashidcapital.simple.R;
//import mn.ashidcapital.simple.client.loan.dto.X_RULEDETAIL_L;
//
//public class LoanRolesDialog extends Dialog {
//
//    private TextView textTitle;
//    private String title;
//    private ArrayList<X_RULEDETAIL_L> ruleDetails;
//    private Button buttonOk;
//    private ImageView buttonBack;
//    private LinearLayout containerRules;
//    private Context context;
//
//    public LoanRolesDialog(Context context, String title,  ArrayList<X_RULEDETAIL_L> ruleDetails ) {
//        super(context);
//        this.context = context;
//        this.title = title;
//        this.ruleDetails = ruleDetails;
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        setCancelable(false);
//        setContentView(R.layout.loan_rules_dialog);
//        Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(Color.WHITE));
//        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//
//        LottieAnimationView animationView = findViewById(R.id.animation);
//        animationView.setAnimation("anim_rejection_star.json");
//        animationView.playAnimation();
//
//        buttonBack = findViewById(R.id.buttonBack);
//        buttonOk = findViewById(R.id.buttonOk);
//        textTitle = findViewById(R.id.textTitle);
//        containerRules = findViewById(R.id.containerRules);
//        initEvent();
//        initView();
//    }
//
//    private void initView() {
//        textTitle.setText(title);
//        if(ruleDetails != null) {
//            for (int i = 0; i < ruleDetails.size(); i++) {
//                LayoutInflater vi = (LayoutInflater) context
//                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View v = vi.inflate(R.layout.loan_rules_dialog_listitem, null);
//                TextView textRule = v.findViewById(R.id.textRule);
//                textRule.setText(ruleDetails.get(i).getREASON_EXPLANATION());
//                containerRules.addView(v);
//            }
//        }
//    }
//
//    private void initEvent(){
//        buttonBack.setOnClickListener(v -> dismiss());
//        buttonOk.setOnClickListener(v -> dismiss());
//    }
//}
