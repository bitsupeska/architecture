package mn.bitsup.callservice.view.widget.card.util.formatter;


import androidx.annotation.NonNull;
import java.util.List;

public final class CardNumberFormatter {
    private CardNumberFormatter() {
    }

    @NonNull
    static String pad(@NonNull String cardNumber, @NonNull CardNumberFormat cardNumberFormat) {
        int paddingSize = Math.max(cardNumberFormat.getLength() - cardNumber.length(), 0);
        StringBuilder paddedCardNumberBuilder = new StringBuilder(cardNumber);

        for(int i = 0; i < paddingSize; ++i) {
            paddedCardNumberBuilder.insert(0, cardNumberFormat.getMaskChar());
        }

        return paddedCardNumberBuilder.toString();
    }

    @NonNull
    static String mask(@NonNull String cardNumber, @NonNull CardNumberFormat cardNumberFormat) {
        StringBuilder maskBuilder = new StringBuilder(cardNumber);
        int maskStart = cardNumberFormat.getMaskingInterval().getStart();
        int maskEnd = cardNumberFormat.getMaskingInterval().getEnd();
        String maskCharAsStr = Character.toString(cardNumberFormat.getMaskChar());

        for(int i = maskStart; i < maskEnd; ++i) {
            maskBuilder.replace(i, i + 1, maskCharAsStr);
        }

        return maskBuilder.toString();
    }

    public static String formatCardNumber(@NonNull String cardNumber, @NonNull CardNumberFormat cardNumberFormat) {
        cardNumber = pad(cardNumber, cardNumberFormat);
        cardNumber = mask(cardNumber, cardNumberFormat);
        cardNumber = groupNumbers(cardNumber, cardNumberFormat);
        return cardNumber;
    }

    @NonNull
    static String groupNumbers(@NonNull String cardNumber, @NonNull CardNumberFormat format) {
        StringBuilder result = new StringBuilder(cardNumber);
        List<Integer> groups = format.getGroups();
        String separator = " ";
        int groupIndex = 0;

        for(int separatorIndex = 0; !groups.isEmpty() && (separatorIndex += (Integer)groups.get(groupIndex++ % groups.size())) < result.length(); separatorIndex += separator.length()) {
            result.insert(separatorIndex, separator);
        }

        return result.toString();
    }
}
