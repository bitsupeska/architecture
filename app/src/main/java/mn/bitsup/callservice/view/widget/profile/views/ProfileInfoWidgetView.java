package mn.bitsup.callservice.view.widget.profile.views;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfileInfoWidgetView extends Fragment implements ProfileView<ProfileContract> {

    public static DataCell cellUserName, cellLastName, cellFirstName, cellPhoneNumber, cellEmail, cellAddress;
    private ImageView imagePhoneActive, imageEmailActive, imageAddressActive;
    public static User user;
    private ProfileContract contract;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        cellUserName = inflatedView.findViewById(R.id.cellUserName);
        cellLastName = inflatedView.findViewById(R.id.cellLastName);
        cellFirstName = inflatedView.findViewById(R.id.cellFirstName);
        cellPhoneNumber = inflatedView.findViewById(R.id.cellPhoneNumber);
        cellEmail = inflatedView.findViewById(R.id.cellEmail);
        cellAddress = inflatedView.findViewById(R.id.cellAddress);
        imagePhoneActive = inflatedView.findViewById(R.id.imagePhoneActive);
        imageEmailActive = inflatedView.findViewById(R.id.imageEmailActive);
        imageAddressActive = inflatedView.findViewById(R.id.imageAddressActive);
    }

    private void initViews() {
        String email = "";
        String street = "";
        Addresses homeAddress = null;
        String resultAddress = "";
        String userName = ClientsManager.getAuthClient().getUid();

        if (contract.getUser() != null) {
            String lastName = contract.getUser().getLastName();
            String firstName = contract.getUser().getFirstName();
            String phoneNumber = contract.getUser().getPhoneNo();
            if (phoneNumber == null) contract.getUser().getHandPhone();
            if (userName != null && !userName.equals("")) cellUserName.setValueText(userName);
            if (lastName != null && !lastName.equals("")) cellLastName.setValueText(lastName);
            if (firstName != null && !firstName.equals("")) cellFirstName.setValueText(firstName);
            if (phoneNumber != null && !phoneNumber.equals(""))
                cellPhoneNumber.setValueText(phoneNumber);
        }

        if (contract.getUser().getAddresses() != null) {
            for (int i = 0; i < contract.getUser().getAddresses().size(); i++) {
                if (contract.getUser().getAddresses().get(i).getAddressType().equals("Home")) {
                    homeAddress = contract.getUser().getAddresses().get(i);
                    break;
                }
            }
        }

        if (contract.getUser().getEmails().get(0) != null)
            email = contract.getUser().getEmails().get(0).getEmailId();
        if (email != null && !email.equals("")) cellEmail.setValueText(email.toLowerCase());
        if (homeAddress != null) {

            if (homeAddress.getStreet() != null) {
                street = homeAddress.getStreet();
            }

            String[] addressData = {homeAddress.getTown() == null ? "" : homeAddress.getTown(), homeAddress.getLocality() == null ? "" : homeAddress.getLocality(), street + (homeAddress.getSection() == null ? "" : homeAddress.getSection()),
                    homeAddress.getDistrict() == null ? "" : homeAddress.getDistrict(), homeAddress.getApartment() == null ? "" : homeAddress.getApartment(), homeAddress.getDoorNumber() == null ? "" : homeAddress.getDoorNumber()};

//            for (String address : addressData) {
//                if (address != null && !address.equals("")) {
//                    resultAddress += address + "\n";
//                }
//            }

            resultAddress = addressData[0];
            for (int i = 1; i < addressData.length; i++) {
                if (!addressData[i].equals("") && addressData[i] != null) {
                    resultAddress += "\n" + addressData[i];
                }
            }
            cellAddress.setValueText(resultAddress);
        }

        String didSign = contract.getUser().getDidSign();
        if (didSign != null && didSign.equals("0")) {
            imagePhoneActive.setImageResource(0);
            imageEmailActive.setImageResource(0);
            imageAddressActive.setImageResource(0);
        } else {
            cellPhoneNumber.setOnClickListener(v -> publish("phone"));
            cellEmail.setOnClickListener(v -> publish("email"));
            cellAddress.setOnClickListener(v -> publish("address"));
        }
    }

    private void publish(String direction) {
        switch (direction) {
            case "email":
                EventHelper.publishActivity(context, new ProfileEmailShowWidgetView(), "И-мэйл хаяг");
                break;
            case "phone":
                EventHelper.publishActivity(context, new ProfilePhoneShowWidgetView(), "Гар утасны дугаар");
                break;
            case "address":
                EventHelper.publishActivity(context, new ProfileAddressShowWidgetView(), "Оршин суугаа хаяг");
                break;
        }
    }
}
