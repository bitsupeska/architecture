package mn.bitsup.callservice.view.widget.profile.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.view.activity.GenericActivity;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.ErrorView;
import mn.bitsup.callservice.view.custom.ErrorViewType;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class ProfileConnectAccountWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private Context context;
    private ProfileContract contract;
    private CustomButton buttonContinue;
    private WidgetListView widgetListView;
    private ListView listView;
    private HashMap<Integer, String> bankList = new HashMap<>();
    private List<Account> accountList = new ArrayList<>();
    private ErrorView.ErrorType errorType = ErrorView.ErrorType.EMPTY;
    private final ErrorViewType ERROR_VIEW_TYPE = ErrorViewType.ACCOUNTS;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_connect_account, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        listView = inflatedView.findViewById(R.id.listView);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
    }

    private void initViews() {
        buttonContinue.setOnClickListener(v -> publish());
        fillBankData();
    }

    private void publish() {
        GenericActivity.fragment = new ProfileConnectAccountAddWidgetView();
        Intent intent = new Intent(context, GenericActivity.class);
        intent.putExtra(PAGE_PREFERENCE_TITLE, "Данс холбох");
        startActivityForResult(intent, 1);
    }

    private void deleteAccount(String id) {
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
        alertdialog.setMessage("Та энэ дансаа устгахад итгэлтэй байна уу?").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HashMap<String, String> param = new HashMap<>();
                param.put("id", id);

                contract.deleteAccounts(new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        getAccountList();
                        NudgeControllerUtil.showSuccessNotification(getContext().getResources().getString(R.string.setup_completed_title), "Данс устлаа", context);
                    }

                    @Override
                    public void onError(Response statusResponse) {
                        NudgeControllerUtil.showErrorNotification(context.getResources().getString(R.string.shared_error_failed_title), statusResponse.getErrorMessage(), context);
                    }
                }, param);
                dialog.cancel();
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog alert = alertdialog.create();
        alert.setTitle("Баталгаажуулах");
        alert.show();
    }

    private void getAccountList() {
        contract.getAccountLists(new AccountListener() {
            @Override
            public void onSuccess(List<Account> accounts) {
                if (accounts.size() < 1) {
                    listView.setVisibility(View.GONE);
                    widgetListView.showErrorView(errorType, ERROR_VIEW_TYPE);
                } else {
                    listView.setVisibility(View.VISIBLE);
                    widgetListView.setVisibility(View.GONE);
                    accountList.clear();
                    accountList.addAll(accounts);
                    ListAdapter listAdapter = new ListAdapter();
                    listView.setAdapter(listAdapter);
                    if (accounts.size() >= 3) {
                        buttonContinue.setState(ButtonState.DISABLED, getActivity());
                    } else buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response var1) {
                widgetListView.hideProgress();
                CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> {
                    ((Activity) context).finish();
                });
            }
        }, contract.getUser().getCif());
    }

    private class ListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return accountList.size();
        }

        @Override
        public Object getItem(int position) {
            return accountList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.profile_connect_account_list_item, parent, false);
            TextView textBankName = convertView.findViewById(R.id.textBankName);
            TextView textAccount = convertView.findViewById(R.id.textAccount);
            if (accountList != null)
                convertView.setOnClickListener(v -> deleteAccount(accountList.get(position).getId().toString()));
            textBankName.setText(bankList.get(accountList.get(position).getBank_id()));
            Log.e("uno", "getView: " + accountList.get(position).getBank_id());
            textAccount.setText(accountList.get(position).getAccount_num());

            return convertView;
        }
    }

    private void fillBankData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("profileSettings", "");
        param.put("qpay", "");
        param.put("transfer", "");
        contract.getBanks(new BankListener() {
            @Override
            public void onSuccess(List<BankItem> data) {
                for (int i = 0; i < data.size(); i++) {
                    bankList.put(data.get(i).getId(), data.get(i).getBankname());
                }
                getAccountList();
            }

            @Override
            public void onError(Response var1) {
                Log.e("fillBankData", "onError: " + "couldn't get bank data");
                listView.setVisibility(View.GONE);
                widgetListView.showErrorView(errorType, ERROR_VIEW_TYPE);
            }
        }, param);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_FIRST_USER) {
                String result = data.getStringExtra("result");
                String resultTitle = data.getStringExtra("resultTitle");
                String resultMessage = data.getStringExtra("resultMessage");
                if (result.equals("success"))
                    NudgeControllerUtil.showSuccessNotification(resultTitle, resultMessage, context);
                else NudgeControllerUtil.showErrorNotification(resultTitle, resultMessage, context);
                getAccountList();
            }
        }
    }

}
