package mn.bitsup.callservice.view.widget.card.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.Collections;
import mn.bitsup.callservice.R;

public class MyKeyboard extends LinearLayout implements View.OnClickListener {

    // constructors
    public MyKeyboard(Context context) {
        this(context, null, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    // keyboard keys (buttons)
    private Button mButton1;
    private Button mButton2;
    private Button mButton3;
    private Button mButton4;
    private Button mButton5;
    private Button mButton6;
    private Button mButton7;
    private Button mButton8;
    private Button mButton9;
    private Button mButton0;
    private ImageButton mButtonDelete;

    // This will map the button resource id to the String value that we want to
    // input when that button is clicked.
    SparseArray<String> keyValues = new SparseArray<>();

    // Our communication link to the EditText
    InputConnection inputConnection;

    private void init(Context context, AttributeSet attrs) {

        // initialize buttons
        LayoutInflater.from(context).inflate(R.layout.keyboard, this, true);
        mButton1 = findViewById(R.id.button_1);
        mButton2 = findViewById(R.id.button_2);
        mButton3 = findViewById(R.id.button_3);
        mButton4 = findViewById(R.id.button_4);
        mButton5 = findViewById(R.id.button_5);
        mButton6 = findViewById(R.id.button_6);
        mButton7 = findViewById(R.id.button_7);
        mButton8 = findViewById(R.id.button_8);
        mButton9 = findViewById(R.id.button_9);
        mButton0 = findViewById(R.id.button_0);
        mButtonDelete = findViewById(R.id.button_delete);

        // set button click listeners
        mButton1.setOnClickListener(this);
        mButton2.setOnClickListener(this);
        mButton3.setOnClickListener(this);
        mButton4.setOnClickListener(this);
        mButton5.setOnClickListener(this);
        mButton6.setOnClickListener(this);
        mButton7.setOnClickListener(this);
        mButton8.setOnClickListener(this);
        mButton9.setOnClickListener(this);
        mButton0.setOnClickListener(this);
        mButtonDelete.setOnClickListener(this);

        // map buttons IDs to input strings
        keyValues.put(R.id.button_1, "1");
        keyValues.put(R.id.button_2, "2");
        keyValues.put(R.id.button_3, "3");
        keyValues.put(R.id.button_4, "4");
        keyValues.put(R.id.button_5, "5");
        keyValues.put(R.id.button_6, "6");
        keyValues.put(R.id.button_7, "7");
        keyValues.put(R.id.button_8, "8");
        keyValues.put(R.id.button_9, "9");
        keyValues.put(R.id.button_0, "0");
    }

    @Override
    public void onClick(View v) {

        // do nothing if the InputConnection has not been set yet
        if (inputConnection == null) return;

        if (v.getId() == R.id.button_delete) {
            CharSequence selectedText = inputConnection.getSelectedText(0);
            if (TextUtils.isEmpty(selectedText)) {
                // no selection, so delete previous character
                inputConnection.deleteSurroundingText(1, 0);
            } else {
                // delete the selection
                inputConnection.commitText("", 1);
            }
        } else {
            String value = keyValues.get(v.getId());
            inputConnection.commitText(value, 1);
        }
    }

    // The activity (or some parent or controller) must give us
    // a reference to the current EditText's InputConnection
    public void setInputConnection(InputConnection ic) {
        this.inputConnection = ic;
    }

    public void setKeysToRandom() {

        ArrayList<Integer> randomNumbers = new ArrayList<Integer>(9);

        //Add numbers in order
        for(int i = 0; i<10; i++){

            randomNumbers.add(i);
        }
        //Shuffle order
        Collections.shuffle(randomNumbers);

        //Set text
        mButton1.setText(Integer.toString(randomNumbers.get(1)));
        mButton2.setText(Integer.toString(randomNumbers.get(2)));
        mButton3.setText(Integer.toString(randomNumbers.get(3)));
        mButton4.setText(Integer.toString(randomNumbers.get(4)));
        mButton5.setText(Integer.toString(randomNumbers.get(5)));
        mButton6.setText(Integer.toString(randomNumbers.get(6)));
        mButton7.setText(Integer.toString(randomNumbers.get(7)));
        mButton8.setText(Integer.toString(randomNumbers.get(8)));
        mButton9.setText(Integer.toString(randomNumbers.get(9)));
        mButton0.setText(Integer.toString(randomNumbers.get(0)));


        //Set values
        keyValues.put(R.id.button_1, Integer.toString(randomNumbers.get(1)));
        keyValues.put(R.id.button_2, Integer.toString(randomNumbers.get(2)));
        keyValues.put(R.id.button_3, Integer.toString(randomNumbers.get(3)));
        keyValues.put(R.id.button_4, Integer.toString(randomNumbers.get(4)));
        keyValues.put(R.id.button_5, Integer.toString(randomNumbers.get(5)));
        keyValues.put(R.id.button_6, Integer.toString(randomNumbers.get(6)));
        keyValues.put(R.id.button_7, Integer.toString(randomNumbers.get(7)));
        keyValues.put(R.id.button_8, Integer.toString(randomNumbers.get(8)));
        keyValues.put(R.id.button_9, Integer.toString(randomNumbers.get(9)));
        keyValues.put(R.id.button_0, Integer.toString(randomNumbers.get(0)));
    }

}