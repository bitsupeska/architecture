package mn.bitsup.callservice.view.widget.loan.view.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import io.feeeei.circleseekbar.CircleSeekBar;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.MyScrollView;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;

public class BaseRepaymentView extends Fragment {
    private static String TAG = QpayView.class.getName();
    protected Context context;
    protected LoanAccountData loanAccountData;
    private EditText loanAmount;
    private EditText loanAmountCopy;
    private TextView otherAmount, alertTitle, alertText;
    protected int amount = 0;
    private CircleSeekBar circleSeekBar;
    private int fullAmount;
    private int myAmount;
    List<BankItem> bankItems = new ArrayList<>();
    LoanContract loanContract;
    RecyclerView recyclerItems;
    private LinearLayout alertBackground;
    private LinearLayout alertBackgroundContainer;
    protected ProgressDialog progressDialog;
    private ContinueButtonTextWatcher continueButtonTextWatcher = new ContinueButtonTextWatcher();
    private ContinueButtonTextWatcher1 continueButtonTextWatcher1 = new ContinueButtonTextWatcher1();
    private InputFilter[] fArray;
    private MyScrollView scrollView;
    private LinearLayout productItem;

    public BaseRepaymentView(LoanAccountData loanAccountData) {
        this.loanAccountData = loanAccountData;
        try {
            String s = loanAccountData.getPayOffAmt();
            String str = loanAccountData.getThisMonthPayment();
//            if (str != null && (str.equals("") || str.equals("0"))) {
//                str = loanAccountData.getNextMonthPayment();
//            }
            double dGetPayOffAmt = Double.parseDouble(s);
            double dConvertedYyAmount = Double.parseDouble(str);
            int convertedPayOffAmt = (int) dGetPayOffAmt;
            int convertedMyAmount = (int) dConvertedYyAmount;
            fullAmount = convertedPayOffAmt;
            myAmount = convertedMyAmount;
        } catch (Exception ex) {
            fullAmount = 475008;
            myAmount = 500000;
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.loanContract = new LoanWidget(this.context);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_repayment_transaction, container, false);

        inflatedView(inflatedView);

        return inflatedView;
    }


    private void inflatedView(View inflatedView) {

        recyclerItems = inflatedView.findViewById(R.id.recyclerItems);
        alertBackground = inflatedView.findViewById(R.id.alertBackground);
        loanAmount = inflatedView.findViewById(R.id.loanAmount);
        circleSeekBar = inflatedView.findViewById(R.id.circleSeekBar);
        otherAmount = inflatedView.findViewById(R.id.otherAmount);
        alertTitle = inflatedView.findViewById(R.id.alertTitle);
        alertText = inflatedView.findViewById(R.id.alertText);
        loanAmountCopy = inflatedView.findViewById(R.id.loanAmountCopy);
        alertBackgroundContainer = inflatedView.findViewById(R.id.alertBackgroundContainer);
        scrollView = inflatedView.findViewById(R.id.scrollView);
        productItem = inflatedView.findViewById(R.id.product_item);
        initView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        loadBankData();
        loanAmount.addTextChangedListener(continueButtonTextWatcher);
        loanAmountCopy.setOnClickListener(null);
        loanAmount.setOnClickListener(null);
        loanAmountCopy.setOnTouchListener(null);
        loanAmountCopy.setFocusable(false);
        loanAmount.setOnTouchListener(null);
        loanAmount.setEnabled(false);
        loanAmountCopy.setEnabled(false);

        loanAmountCopy.addTextChangedListener(continueButtonTextWatcher1);
        circleSeekBar.setMaxProcess(fullAmount);
        circleSeekBar.setUnreachedColor(getResources().getColor(R.color.transparent));

        otherAmount.setOnClickListener(v -> {
            Log.e("otherAmount", "inflatedView: focused");
            loanAmount.setVisibility(View.VISIBLE);
            loanAmountCopy.setVisibility(View.GONE);
            loanAmount.removeTextChangedListener(continueButtonTextWatcher);
            loanAmountCopy.setEnabled(true);
            loanAmountCopy.setFocusable(true);
            loanAmountCopy.setCursorVisible(true);
            loanAmount.setEnabled(true);
            loanAmount.setFocusable(true);
            loanAmount.setCursorVisible(true);
            loanAmount.setText("");
            loanAmount.setSelection(loanAmount.getText().length());
            loanAmount.requestFocus();
            loanAmount.addTextChangedListener(continueButtonTextWatcher);
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(loanAmount, InputMethodManager.SHOW_IMPLICIT);
        });


        circleSeekBar.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                loanAmount.setVisibility(View.GONE);
                scrollView.setScrolling(false);
                loanAmountCopy.setVisibility(View.VISIBLE);
                KeyboardUtils.hideKeyboard(getActivity(), loanAmount);
                return true;
            }
            return false;

        });
        recyclerItems.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                scrollView.setScrolling(true);
                return true;
            }
            return false;

        });
        productItem.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                scrollView.setScrolling(true);
                return true;
            }
            return false;

        });

        circleSeekBar.setOnSeekBarChangeListener((seekbar, curValue) ->

        {
            amount = curValue;
            if (curValue == 0) {
                alertBackground.setVisibility(View.GONE);
                gone();
            } else {
                if (curValue > myAmount && fullAmount > curValue) {
                    if (myAmount == 0) {
                        visible("Та илүү төлөлт хийх гэж байнa", "Илүү төлсөн дүн таны дараагийн сарын төлөлтөөс \nхасагдахыг анхаарна уу.");
                    } else {
                        visible("Та илүү төлөлт хийх гэж байнa", "Таны энэ сарын төлбөр төлөгдөж, илүү\n төлбөр сүүлийн сараас хасагдана.");
                    }
                }
                if (fullAmount == curValue) {
                    visible("Та зээлээ хаах гэж байнa", "Доорх дүнг төлснөөс таны зээл  \nхаагдах болно.");
                }
                if (curValue < myAmount) {
                    visible("Та дутуу төлөлт хийх гэж байна", "Дутуу төлснөөр танд маргаашнаас\n торгууль тооцохыг анхаарна уу.");
                }
            }
            ((Runnable) () -> {
                if (loanAmountCopy.getVisibility() == View.VISIBLE) {
                    loanAmountCopy.removeTextChangedListener(continueButtonTextWatcher);
                    int maxProcess = Math.round(circleSeekBar.getMaxProcess() / 10000) * 10000;
                    Log.e("baseRepayment", "initView: " + String.valueOf(curValue != maxProcess));
                    if (curValue < maxProcess) {
                        int value = Math.round(curValue / 10000) * 10000;
                        DecimalFormat formatter = new DecimalFormat("##,###,###");
                        loanAmountCopy.setText(formatter.format(value));
                    } else {
                        loanAmountCopy.setText(CurrencyConverter.getFormattedCurrencyString(fullAmount));
                    }
                }
            }).run();
        });
        Log.e("baseRepayment", "inflatedView: focused" + myAmount);
        int maxProgress = Math.round(fullAmount / 10000) * 10000;
        int curProgress = Math.round(myAmount / 10000) * 10000;
        Log.e("baseRepayment", "inflatedView: focused" + (maxProgress >= myAmount));
        Log.e("baseRepayment", "inflatedView: focused" + curProgress);
        Log.e("baseRepayment", "inflatedView: focused" + myAmount);
        if (maxProgress >= curProgress) {
            initilazeCurProcess(fullAmount);
        } else {
            initilazeCurProcess(myAmount);
        }
    }

    private void initilazeCurProcess(int curValue) {
        ((Runnable) () -> {
            loanAmountCopy.setVisibility(View.GONE);
            loanAmount.setText(CurrencyConverter.getFormattedCurrencyString(curValue));
        }).run();
        circleSeekBar.setCurProcess(curValue);
    }

    public void visible(String title, String text) {
        TransitionManager.beginDelayedTransition(alertBackgroundContainer);
        alertTitle.setText(title);
        alertText.setText(text);
        alertBackground.setVisibility(View.VISIBLE);
    }

    public void gone() {
        TransitionManager.beginDelayedTransition(alertBackgroundContainer);
        alertBackground.setVisibility(View.INVISIBLE);
    }


    protected void loadBankData() {
    }


    private class ContinueButtonTextWatcher implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(final Editable s) {
            try {
                loanAmount.removeTextChangedListener(this);
                if (!s.toString().equals("")) {
                    amount = Integer.parseInt(s.toString().replaceAll(",", ""));
                } else {
                    amount = 0;
                }
                if (!s.toString().equals("") && s.length() == 1) {
                    if (loanAmount.getText().toString().equals("0")) {
                        loanAmount.setText("");
                    }
                }
                if (amount > fullAmount) {
                    amount = fullAmount;
                }
                ((Runnable) () -> {
                    Log.e(TAG, "afterTextChanged: " + amount);
                    if (amount != 0) {
                        loanAmount.setText(CurrencyConverter.getFormattedCurrencyString(amount));
                    } else {
                        loanAmount.setText("0");
                    }
                }).run();
                loanAmount.setSelection(loanAmount.getText().length());
                circleSeekBar.setCurProcess(amount);
                loanAmount.addTextChangedListener(this);
            } catch (Exception e) {
                Log.e("Exception", "afterTextChanged Exception loanAmount: " + e);
            }

        }
    }

    private class ContinueButtonTextWatcher1 implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


        @Override
        public void afterTextChanged(final Editable s) {
            Log.e(TAG, "afterTextChanged:afterTextChangedCopy " + s.toString());
            try {
                loanAmountCopy.removeTextChangedListener(this);
                if (!s.toString().equals("")) {

                    amount = Integer.parseInt(s.toString().replaceAll(",", ""));
                    if (amount > fullAmount) {
                        amount = fullAmount;
                    }
                    loanAmountCopy.setText(CurrencyConverter.getFormattedCurrencyString(amount));
                } else {
                    amount = 0;
                    loanAmountCopy.setText("");
                }
                loanAmountCopy.setSelection(loanAmountCopy.getText().length());

                loanAmountCopy.addTextChangedListener(this);

            } catch (Exception e) {
                Log.e(TAG, "afterTextChanged Exception loanAmountCopy: " + e);
            }
        }
    }
}
