package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.Location;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfileAddressEditWidgetView extends Fragment implements ProfileView<ProfileContract>, TextWatcher, AdapterView.OnItemSelectedListener {

    private IconSpinner spinnerCityAimag, spinnerDistrictSum;
    private InputField inputLocalityName, inputTown, inputPremiseName, inputBuildingLevel;
    private CustomButton buttonContinue;
    private List<Location> cities;
    private List<Location> districts;
    private ProfileContract contract;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        Activity activity = (Activity) getContext();
        try {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_address_edit, container, false);
        inflatedView(inflatedView);
        initViews();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        spinnerCityAimag = inflatedView.findViewById(R.id.spinnerCityAimag);
        spinnerDistrictSum = inflatedView.findViewById(R.id.spinnerDistrictSum);
        inputLocalityName = inflatedView.findViewById(R.id.inputLocalityName);
        inputTown = inflatedView.findViewById(R.id.inputTown);
        inputPremiseName = inflatedView.findViewById(R.id.inputPremiseName);
        inputBuildingLevel = inflatedView.findViewById(R.id.inputBuildingLevel);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
    }

    private void initViews() {
        buttonContinue.setOnClickListener(v -> publish());

        inputLocalityName.addTextChangedListener(this);
        inputTown.addTextChangedListener(this);
        inputPremiseName.addTextChangedListener(this);
        inputBuildingLevel.addTextChangedListener(this);

        inputLocalityName.setMaxLength(30);
        inputLocalityName.setSingleLine();
        inputTown.setMaxLength(30);
        inputTown.setSingleLine();
        inputPremiseName.setMaxLength(30);
        inputPremiseName.setSingleLine();
        inputBuildingLevel.setMaxLength(30);
        inputBuildingLevel.setSingleLine();

        inputLocalityName.setInputType(InputType.TYPE_CLASS_TEXT);
        inputTown.setInputType(InputType.TYPE_CLASS_TEXT);
        inputPremiseName.setInputType(InputType.TYPE_CLASS_TEXT);
        inputBuildingLevel.setInputType(InputType.TYPE_CLASS_TEXT);
        buttonContinue.setState(ButtonState.DISABLED, getActivity());

        spinnerCityAimag.setOnItemSelectedListener(this);
        spinnerDistrictSum.setOnItemSelectedListener(this);

        inputLocalityName.setCancelButton();
        inputTown.setCancelButton();
        inputPremiseName.setCancelButton();
        inputBuildingLevel.setCancelButton();
        inputBuildingLevel.setCancelButton();

        loadCityData();
    }

    private void initEvent() {
        spinnerCityAimag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    List<String> list = new ArrayList<>();
                    spinnerDistrictSum.setItems(list);
                } else {
                    loadDistrictData(cities.get(position - 1).getLocationValue());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        buttonContinue.setOnClickListener(v -> publish());
    }

    private void loadCityData() {
        cities = new ArrayList<>();
        contract.loadCityData(new LocationListener() {
            @Override
            public void onSuccess(List<Location> data) {
                cities = data;
                List<String> list = new ArrayList<>();
                list.add("Сонгох");
                for (int i = 0; i < cities.size(); i++) {
                    list.add(cities.get(i).getLocationText().trim());
                }
                spinnerCityAimag.setItems(list);
            }

            @Override
            public void onError(Response var1) {
                cities = new ArrayList<>();
            }
        });
    }

    private void loadDistrictData(String stateCode) {
        districts = new ArrayList<>();
        contract.loadDistrictData(new LocationListener() {
            @Override
            public void onSuccess(List<Location> data) {
                districts = data;
                List<String> list = new ArrayList<>();
                list.add("Сонгох");
                for (int i = 0; i < districts.size(); i++) {
                    list.add(districts.get(i).getLocationText());
                }
                spinnerDistrictSum.setItems(list);
            }

            @Override
            public void onError(Response errorResponse) {
                districts = new ArrayList<>();
            }
        }, stateCode);
    }

    public void publish() {
        KeyboardUtils.hideKeyboard(context, inputBuildingLevel);
        if (isValid()) setAddress();
    }

    private boolean isValid() {
        return (spinnerCityAimag.getSelectedPosition() != 0 && spinnerDistrictSum.getSelectedPosition() != 0 && !inputLocalityName.getValue().isEmpty() &&
                !inputTown.getValue().isEmpty() && !inputPremiseName.getValue().isEmpty() && !inputBuildingLevel.getValue().isEmpty());
    }

    private void setNullError() {
        InputField[] inputFields = new InputField[]{inputLocalityName, inputTown, inputPremiseName, inputBuildingLevel};

        for (InputField data : inputFields) {
            if (data.getValue().isEmpty()) {
                if (data.hasFocus()) data.setError(context.getResources().getString(R.string.profile_information_edit_address_null_error));
            } else {
                if (data.hasFocus()) data.setError(null);
                else data.setError(null);
            }
        }
    }

    public void setAddress() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        String city_data = districts.get(spinnerDistrictSum.getSelectedPosition() - 1).getLocationValue();
        if (city_data.length() == 3)
            city_data = city_data.substring(city_data.length() - 1);
        else city_data = city_data.substring(city_data.length() - 2);

        HashMap<String, String> param = new HashMap<>();
        param.put("cifId", contract.getUser().getCif());
        param.put("state", cities.get(spinnerCityAimag.getSelectedPosition() - 1).getLocationValue());
        param.put("city", city_data);
        param.put("localityName", inputLocalityName.getValue());
        param.put("town", inputTown.getValue());
        param.put("buildingLevel", inputBuildingLevel.getValue());
        param.put("premiseName", inputPremiseName.getValue());
        param.put("addressType", "Home");

        contract.setAddress(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                if (statusResponse.getStatus().equals("SUCCESS")) {

                    User user = MainApplication.getUser();
                    List<Addresses> addresses = new ArrayList();
                    Addresses homeAddress = new Addresses();
                    homeAddress.setTown(spinnerDistrictSum.getSelectedItem());
                    homeAddress.setLocality(spinnerCityAimag.getSelectedItem());
                    homeAddress.setSection(inputLocalityName.getValue());
                    homeAddress.setDistrict(inputPremiseName.getValue());
                    homeAddress.setApartment(inputBuildingLevel.getValue());
                    homeAddress.setDoorNumber(inputTown.getValue());
                    homeAddress.setAddressType("Home");

                    addresses.add(homeAddress);
                    user.setAddresses(addresses);
                    MainApplication.setUser(user);

                    if (contract.getUser().getAddresses() != null) {
                        for (int i = 0; i < contract.getUser().getAddresses().size(); i++) {
                            if (contract.getUser().getAddresses().get(i).getAddressType().equals("Home")) {
                                homeAddress = contract.getUser().getAddresses().get(i);
                                break;
                            }
                        }
                    }

                    String street = "";
                    if (homeAddress.getStreet() != null) {
                        street = homeAddress.getStreet();
                    }

                    String[] addressData = {homeAddress.getTown(), homeAddress.getLocality(), street + homeAddress.getSection(), homeAddress.getDistrict(), homeAddress.getApartment(), homeAddress.getDoorNumber()};

                    String resultAddress = "";
                    for (String address : addressData) {
                        if (address != null&&!address.equals("")  ) {
                            resultAddress += address + "\n";
                        }
                    }
                    ProfileInfoWidgetView.cellAddress.setValueText(resultAddress);

                    updateDataFinish("success", context.getResources().getString(R.string.setup_completed_title), context.getResources().getString(R.string.profile_information_edit_address_success));
                } else {
                    updateDataFinish("error", (context.getResources().getString(R.string.shared_error_failed_title)), statusResponse.getMessage());
                }
            }

            @Override
            public void onError(Response response) {
                updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), context.getResources().getString(R.string.shared_alert_generic_error_server));
                Log.e("success", "server error");
            }
        }, param);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        setNullError();
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
        KeyboardUtils.hideKeyboard(getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
    }

    public void updateDataFinish(String result, String resultTitle, String resultMessage) {
        Activity activity = ((Activity) Objects.requireNonNull(getContext()));
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        returnIntent.putExtra("resultTitle", resultTitle);
        returnIntent.putExtra("resultMessage", resultMessage);
        activity.setResult(Activity.RESULT_FIRST_USER, returnIntent);
        activity.finish();

        ((Activity) context).finish();
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
    }
}
