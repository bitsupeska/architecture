package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;

/**
 * Created by Erdenedalai R&D B.V on 25/02/2022.
 */
public class WidgetListView extends ConstraintLayout {

    private final RecyclerView recyclerView;
    private final ErrorView errorView;
    private View progressBottomContainer;
    private View progressContainer;
    @Nullable private SwipeRefreshLayout swipeRefreshLayout;
    private boolean endOfListReached;
    private int currentPage = 0;
    private boolean loading;
    private ShouldLoadNextPageListener shouldLoadNextPageListener;
    private boolean paginationEnabled = true;
    public CustomButton retryButton;

    public WidgetListView(Context context) {
        this(context, null);
    }

    public WidgetListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View inflatedView = inflate(context, R.layout.widget_list_view, this);
        recyclerView = inflatedView.findViewById(R.id.recyclerView);
        errorView = inflatedView.findViewById(R.id.errorView);
        errorView.setErrorViewListener(new ErrorViewListener());
        progressBottomContainer = inflatedView.findViewById(R.id.containerProgressBottom);
        progressContainer = inflatedView.findViewById(R.id.containerProgress);
        retryButton = inflatedView.findViewById(R.id.retryButton);
        recyclerView.addOnScrollListener(new BottomScrolledListener());
        applyStyledAttributes(attrs);
    }

    private void applyStyledAttributes(final AttributeSet attrs) {
        final TypedArray a = this.getContext().obtainStyledAttributes(attrs, R.styleable.WidgetListView);
        final int recyclerViewBackgroundColor = a.getColor(R.styleable.WidgetListView_recyclerViewBackgroundColor, Color.TRANSPARENT);
        final int recyclerViewBottomPadding = a.getDimensionPixelSize(R.styleable.WidgetListView_recyclerViewBottomPadding, recyclerView.getPaddingBottom());
        final int errorViewBackgroundColor = a.getColor(R.styleable.WidgetListView_errorViewBackgroundColor, Color.TRANSPARENT);

        recyclerView.setBackgroundColor(recyclerViewBackgroundColor);
        recyclerView.setPadding(recyclerView.getPaddingLeft(),
                recyclerView.getPaddingTop(),
                recyclerView.getPaddingRight(),
                recyclerViewBottomPadding);
        errorView.setBackgroundColor(errorViewBackgroundColor);

        a.recycle();
    }

    /**
     * Initialise the WidgetListView with all the required and non required parameters.
     *
     * @param layoutManager              Mandatory LayoutManager to use with the RecyclerView.
     * @param adapter                    Mandatory Adapter to use with the RecyclerView.
     * @param swipeRefreshLayout         SwipeRefreshLayout to work with the WidgetListView. Should typically be the container surrounding the WidgetListView. May be null.
     * @param shouldLoadNextPageListener listener that is interested whenever the next page should be loaded. Typically this is a subclass in your widget. May be null.
     */
    public void initialize(@NonNull RecyclerView.LayoutManager layoutManager,
                           @NonNull RecyclerView.Adapter adapter,
                           @Nullable SwipeRefreshLayout swipeRefreshLayout,
                           @Nullable ShouldLoadNextPageListener shouldLoadNextPageListener) {
        setSwipeRefreshLayout(swipeRefreshLayout);
        setShouldLoadNextPageListener(shouldLoadNextPageListener);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }


    public void hideButton(){
        retryButton.setVisibility(View.GONE);
    }
    /**
     * Convenience initializer for the WidgetListView that also sets the recycler view's OnScrollListener.
     *
     * @param layoutManager              Mandatory LayoutManager to use with the RecyclerView.
     * @param adapter                    Mandatory Adapter to use with the RecyclerView.
     * @param swipeRefreshLayout         SwipeRefreshLayout to work with the WidgetListView.
     *                                   Should typically be the container surrounding the WidgetListView. May be null.
     * @param shouldLoadNextPageListener listener that is interested whenever the next page should be loaded.
     *                                   Typically this is a subclass in your widget. May be null.
     */
    public void initialize(@NonNull RecyclerView.LayoutManager layoutManager,
                           @NonNull WidgetListAdapter adapter,
                           @Nullable SwipeRefreshLayout swipeRefreshLayout,
                           @Nullable ShouldLoadNextPageListener shouldLoadNextPageListener) {
        initialize(layoutManager, (RecyclerView.Adapter) adapter, swipeRefreshLayout, shouldLoadNextPageListener);
        recyclerView.addOnScrollListener(adapter.getOnScrollListener());
    }

    /**
     * Shows the RecyclerView and hides the ErrorView
     *
     * @return returns the RecyclerView
     */
    public RecyclerView showList() {
        hideProgress();
        errorView.setVisibility(GONE);
        recyclerView.setVisibility(VISIBLE);
        return recyclerView;
    }


    private void showErrorView() {
        hideProgress();
        recyclerView.setVisibility(GONE);
        errorView.setVisibility(VISIBLE);
    }

    /**
     * Shows the ErrorView and hides the RecyclerView
     *
     * @param errorType     the type of error
     * @param errorViewType the ErrorViewType to show the error for
     */
    public void showErrorView(ErrorView.ErrorType errorType, ErrorViewType errorViewType) {
        showErrorView();
        errorView.show(errorType, errorViewType);
    }

    /**
     * Shows the ErrorView and hides the RecyclerView
     *
     * @param response      the error network response
     * @param errorViewType the ErrorViewType to show the error for
     */
    public void showErrorView(Response response, ErrorViewType errorViewType) {
        showErrorView();
        errorView.show(response, errorViewType);
    }

    /**
     * Sets the SwipeRefreshLayout and also initialises the SwipeRefreshListener.
     *
     * @param swipeRefreshLayout SwipeRefreshLayout to work with the WidgetListView. Should typically be the container surrounding the WidgetListView.
     */
    public void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
        initSwipeRefreshLayout();
    }

    /**
     * Shows the bottom progressbar and also sets the loading flag to true.
     */
    public void showBottomProgress() {
        loading = true;
        progressBottomContainer.setVisibility(VISIBLE);
    }

    /**
     * Shows the center progressbar and also sets the loading flag to true.
     */
    public void showCenterProgress() {
        loading = true;
        progressContainer.setVisibility(VISIBLE);
    }

    /**
     * Hides all possible progress bars, also the one from the SwipeRefreshLayout if set. And also disables the loading flag.
     */
    public void hideProgress() {
        progressBottomContainer.setVisibility(GONE);
        progressContainer.setVisibility(GONE);
        if (swipeRefreshLayout != null && loading) {
            swipeRefreshLayout.setRefreshing(false);
        }
        loading = false;
    }

    /**
     * Initialises the SwipeRefreshLayout with proper colors and a new instance of the SwipeRefreshListenerImpl.
     */
    private void initSwipeRefreshLayout() {
        if (swipeRefreshLayout == null) {
            return;
        }
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshListenerImpl());
        swipeRefreshLayout.setColorSchemeResources(R.color.primary);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.almost_white);
    }

    /**
     * @param shouldLoadNextPageListener listener that is interested whenever the next page should be loaded. Typically this is a subclass in your widget.
     */
    public void setShouldLoadNextPageListener(ShouldLoadNextPageListener shouldLoadNextPageListener) {
        this.shouldLoadNextPageListener = shouldLoadNextPageListener;
    }

    public void setEndOfListReached(boolean endOfListReached) {
        this.endOfListReached = endOfListReached;
    }

    /**
     * Enable or disable pagination for this view
     *
     * @param paginationEnabled pagination enabled or not for this view.
     */
    public void setPaginationEnabled(boolean paginationEnabled) {
        this.paginationEnabled = paginationEnabled;
    }

    /**
     * @return true if the list has been scrolled to the end, false otherwise.
     */
    public boolean isEndOfListReached() {
        return endOfListReached;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public boolean isLoading() {
        return loading;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public void resetParams() {
        currentPage = 0;
        endOfListReached = false;
        loading = false;
    }

    private void refreshList() {
        currentPage = 0;
        hideProgress();
        loading = true;
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            showCenterProgress();
        }
        endOfListReached = false;
        if (shouldLoadNextPageListener != null) {
            shouldLoadNextPageListener.shouldLoadNextPage();
        }
    }

    private class SwipeRefreshListenerImpl implements SwipeRefreshLayout.OnRefreshListener {
        @Override
        public void onRefresh() {
            refreshList();
        }
    }

    private class BottomScrolledListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            if (loading || endOfListReached || !paginationEnabled) {
                return;
            }
            if (!recyclerView.canScrollVertically(1)) {
                currentPage++;
                showBottomProgress();
                if (shouldLoadNextPageListener != null) {
                    shouldLoadNextPageListener.shouldLoadNextPage();
                }
            }
        }
    }

    public interface ShouldLoadNextPageListener {
        void shouldLoadNextPage();
    }

    private class ErrorViewListener implements ErrorView.ErrorViewListener {
        @Override
        public void onRetryClicked() {
            refreshList();
        }
    }
}
