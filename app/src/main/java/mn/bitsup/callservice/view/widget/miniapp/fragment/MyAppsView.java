package mn.bitsup.callservice.view.widget.miniapp.fragment;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.miniapp.dto.MiniApp;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ErrorView;
import mn.bitsup.callservice.view.custom.ErrorViewType;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.miniapp.core.MiniAppWidget;
import mn.bitsup.callservice.view.widget.miniapp.core.contract.MiniAppContract;
import mn.bitsup.callservice.view.widget.miniapp.selector.AllAppListAdapter;
import mn.bitsup.callservice.view.widget.miniapp.views.MiniAppWebView;

public class MyAppsView extends Fragment {
    private AllAppListAdapter adapter;
    private GridView gridAppList;
    private FloatingActionButton buttonAdd;
    private MiniAppContract contract;
    private WidgetListView widgetListView;
    private List<MiniApp> appList = new ArrayList<>();
    private ErrorView.ErrorType errorType = ErrorView.ErrorType.EMPTY;
    private final ErrorViewType ERROR_VIEW_TYPE = ErrorViewType.MINIAPP;
    private Context context;

    public MyAppsView() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new MiniAppWidget(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mini_app_mine, container, false);
        inflatedView(view);
        initViews();
        return view;
    }

    private void inflatedView(View inflatedView) {
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        gridAppList = inflatedView.findViewById(R.id.gridAppList);
    }

    private void initViews() {
        getAppList();
    }

    private void getAppList() {
        contract.getMyAppList(new MiniAppListener() {

            @Override
            public void onSuccess(List<MiniApp> apps) {
                if (apps.size() < 1) {
                    gridAppList.setVisibility(View.GONE);
                    widgetListView.showErrorView(errorType, ERROR_VIEW_TYPE);
                } else {
                    gridAppList.setVisibility(View.VISIBLE);
                    widgetListView.setVisibility(View.GONE);
                    appList.clear();
                    appList.addAll(apps);
                    GridAdapter gridAdapter = new GridAdapter();
                    gridAppList.setAdapter(gridAdapter);
                }
            }

            @Override
            public void onError(Response response) {
                showErrorNotification("Уучлаарай", "Сервертэй холбогдоход алдаа гарлаа");
            }
        }, contract.getUser().getUid());
    }

    public void showErrorNotification(@NonNull final String title, @NonNull final String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle, getContext());
    }

    private class GridAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return appList.size();
        }

        @Override
        public Object getItem(int position) {
            return appList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.mini_app_grid_item, parent, false);
            ImageView imageViewAppIcon = convertView.findViewById(R.id.imageViewAppIcon);
            TextView textViewAppName = convertView.findViewById(R.id.textViewAppName);
            if (appList != null)
                convertView.setOnClickListener(v -> openApp(appList.get(position).getAppurl(), appList.get(position).getName()));
            imageViewAppIcon.setBackground(getResources().getDrawable(R.drawable.card));
            Thread thread = new Thread(() -> {
                try {
                    URL url = new URL(appList.get(position).getIconpath());
                    Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    Drawable image = new BitmapDrawable(context.getResources(), bitmap);
                    imageViewAppIcon.setBackground(image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();

            textViewAppName.setText(appList.get(position).getName());
            return convertView;
        }
    }

    private void openApp(String url, String name) {
        EventHelper.publishActivity(context, new MiniAppWebView(url), name);
    }

}
