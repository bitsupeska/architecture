package mn.bitsup.callservice.view.custom;


import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import mn.bitsup.callservice.R;

public class SelectorPageIndicator extends LinearLayout {
    private int activePosition = 0;
    private ArrayList<ImageView> indicatorDots = new ArrayList<>();
    private ViewGroup parent;

    public SelectorPageIndicator(Context context) {
        this(context, null);
    }

    public SelectorPageIndicator(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        parent = (ViewGroup) inflate(getContext(), R.layout.product_selector_indicator, this);
    }

    public void initializeWithPagesCount(int pagesCount) {
        removeAllViews();
        indicatorDots.clear();
        for (int i = 0; i < pagesCount; i++) {
            ImageView pageIndicatorDot = getPageIndicatorDot();
            indicatorDots.add(pageIndicatorDot);
            parent.addView(pageIndicatorDot);
        }
        activatePage(0);
    }

    public void activatePage(int position) {
        int pagesCount = indicatorDots.size();
        int validPosition = position;
        if (position >= pagesCount) {
            validPosition = pagesCount - 1;
        }
        indicatorDots.get(activePosition).setImageResource(R.drawable.circle_grey);
        indicatorDots.get(validPosition).setImageResource(R.drawable.circle_primary_color);
        this.activePosition = validPosition;
    }

    public ImageView getPageIndicatorDot() {
        ImageView imageView = new ImageView(getContext());
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMarginEnd(geIndicatorMargin());
        layoutParams.setMarginStart(geIndicatorMargin());
        imageView.setLayoutParams(layoutParams);
        imageView.setImageResource(R.drawable.circle_grey);
        return imageView;
    }

    private int geIndicatorMargin() {
        return (int) getResources().getDimension(R.dimen.rdc_page_indicator_dot_margin);
    }
}

