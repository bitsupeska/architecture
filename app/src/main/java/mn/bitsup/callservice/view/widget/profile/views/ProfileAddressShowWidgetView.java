package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.view.activity.GenericActivity;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class ProfileAddressShowWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private Context context;
    private ProfileContract contract;
    private CustomButton buttonContinue;
    private DataCell cellAddress;
    private Addresses homeAddress;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater
                .inflate(R.layout.profile_information_address_show, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        cellAddress = inflatedView.findViewById(R.id.cellAddress);
    }

    private void initViews() {
        buttonContinue.setOnClickListener(v -> publish());

        if (contract.getUser().getAddresses() != null) {
            for (int i = 0; i < contract.getUser().getAddresses().size(); i++) {
                if (contract.getUser().getAddresses().get(i).getAddressType().equals("Home")) {
                    homeAddress = contract.getUser().getAddresses().get(i);
                    break;
                }
            }
        }
        if (homeAddress != null) {

            String street = "";
            if (homeAddress.getStreet() != null) {
                street = homeAddress.getStreet();
            }
            String[] addressData = {homeAddress.getTown(), homeAddress.getLocality(), street + homeAddress.getSection(), homeAddress.getDistrict(),
                    homeAddress.getApartment(), homeAddress.getDoorNumber()};

            String resultAddress = "";
            resultAddress = addressData[0];
            for (int i = 1; i < addressData.length; i++) {
                if (!addressData[i].equals("") && addressData[i] != null) {
                    resultAddress += "\n" + addressData[i];
                }
            }
            cellAddress.setValueText(resultAddress);
        }

    }

    public void publish() {
        GenericActivity.fragment = new ProfileAddressEditWidgetView();
        Intent intent = new Intent(context, GenericActivity.class);
        intent.putExtra(PAGE_PREFERENCE_TITLE, "Хаяг шинэчлэх");
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_FIRST_USER) {
                String result = data.getStringExtra("result");
                String resultTitle = data.getStringExtra("resultTitle");
                String resultMessage = data.getStringExtra("resultMessage");
                if (result.equals("success")) {
                    NudgeControllerUtil
                            .showSuccessNotification(resultTitle, resultMessage, context);
                } else {
                    NudgeControllerUtil.showErrorNotification(resultTitle, resultMessage, context);
                }
                initViews();
            }
        }
    }


}
