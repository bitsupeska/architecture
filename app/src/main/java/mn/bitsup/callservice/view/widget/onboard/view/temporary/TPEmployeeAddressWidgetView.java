package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.Location;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class TPEmployeeAddressWidgetView extends Fragment implements
        OnboardView<OnboardContract>, AdapterView.OnItemSelectedListener {

    private OnboardContract contract;
    private Context context;
    private UpdateContinueButtonTextWatcher updateContinueButtonTextWatcher = new UpdateContinueButtonTextWatcher();
    private IconSpinner spinnerCityAimag, spinnerDistrictSum;
    private InputField inputKhoroo, inputApartment, inputNumber, inputSection;
    private CustomButton buttonContinue;
    private List<Location> cities;
    private List<Location> districts;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(getContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_employee_address, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        spinnerCityAimag = inflatedView.findViewById(R.id.spinnerCity);
        spinnerDistrictSum = inflatedView.findViewById(R.id.spinnerDistrict);
        inputSection = inflatedView.findViewById(R.id.inputSection);
        inputKhoroo = inflatedView.findViewById(R.id.inputKhoroo);
        inputApartment = inflatedView.findViewById(R.id.inputApartment);
        inputNumber = inflatedView.findViewById(R.id.inputNumber);
    }

    private void initView() {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        inputKhoroo.addTextChangedListener(updateContinueButtonTextWatcher);
        inputApartment.addTextChangedListener(updateContinueButtonTextWatcher);
        inputNumber.addTextChangedListener(updateContinueButtonTextWatcher);
        inputSection.addTextChangedListener(updateContinueButtonTextWatcher);
        spinnerCityAimag.setIconViewVisible(false);
        spinnerCityAimag.setOnItemSelectedListener(this);
        spinnerDistrictSum.setIconViewVisible(false);
        spinnerCityAimag.setOnItemSelectedListener(this);
        inputKhoroo.setIconViewVisible(false);
        inputKhoroo.setSingleLine();
        inputSection.setIconViewVisible(false);
        inputSection.setSingleLine();

        inputApartment.setSingleLine();
        inputApartment.setIconViewVisible(false);
        inputNumber.setIconViewVisible(false);
        inputNumber.setSingleLine();
        inputNumber.setLongClickable(false);
        inputNumber.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputNumber.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                inputNumber.hideSoftInput();
                return true;
            }
            return false;
        });
        buttonContinue.setOnClickListener(v -> publish());
        loadCityData();
    }

    private boolean isValid() {
        return (inputKhoroo.getValue().length() > 0 && inputSection.getValue().length() > 0 && inputApartment.getValue().length() > 0 && inputNumber.getValue().length() > 0 && spinnerCityAimag.getSelectedPosition() != 0 && spinnerDistrictSum.getSelectedPosition() != 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initEvent() {
        spinnerCityAimag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    List<String> list = new ArrayList<>();
                    spinnerDistrictSum.setItems(list);
                } else {
                    loadDistrictData(cities.get(position - 1).getLocationValue());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        buttonContinue.setOnClickListener(v -> publish());
    }

    private void loadCityData() {
        cities = new ArrayList<>();
        contract.loadCityData(new LocationListener() {
            @Override
            public void onSuccess(List<Location> data) {
                cities = data;
                List<String> list = new ArrayList<>();
                list.add("Сонгох");
                for (int i = 0; i < cities.size(); i++) {
                    list.add(cities.get(i).getLocationText().trim());
                }
                spinnerCityAimag.setItems(list);
            }

            @Override
            public void onError(Response errorResponse) {
            }
        });
    }

    private void loadDistrictData(String stateCode) {
        districts = new ArrayList<>();
        contract.loadDistrictData(new LocationListener() {
            @Override
            public void onSuccess(List<Location> data) {
                districts = data;
                List<String> list = new ArrayList<>();
                list.add("Сонгох");
                for (int i = 0; i < districts.size(); i++) {
                    list.add(districts.get(i).getLocationText());
                }
                spinnerDistrictSum.setItems(list);
            }

            @Override
            public void onError(Response errorResponse) {
            }
        }, stateCode);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        KeyboardUtils.hideKeyboard(getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    private class UpdateContinueButtonTextWatcher extends AfterTextChangedListener {

        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
            }
        }
    }

    private void publish() {
        Temporary payload = contract.getTemporary();
        String cityAimagId = cities.get(spinnerCityAimag.getSelectedPosition() - 1).getLocationValue();

        if (districts != null && districts.size() != 0) {
            String districtSumId = districts.get(spinnerDistrictSum.getSelectedPosition() - 1).getLocationValue().substring(2);
            payload.setLocalityWork(Integer.parseInt(districtSumId) + "");
        }

        payload.setTownWork(cityAimagId);
        payload.setSectionWork(inputKhoroo.getValue());
        payload.setDistrictWork(inputSection.getValue());
        payload.setApartmentWork(inputApartment.getValue());
        payload.setDoorNumberWork(inputNumber.getValue());
        payload.setIsWorkAddress("1");
        contract.setTemporary(payload);
        LoanActivity.addProgress();
        EventHelper.publishBack(context, new TPDetailWidgetView());
    }

}