package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;
import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.OTPView;
import mn.bitsup.callservice.view.custom.OTPViewLisener;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;

public class ForgetOtpWidgetView extends Fragment {
    private User user;
    private TextView textTitle;
    private OTPView otpView;
    private ProgressDialog progressDialog;
    private int checkCount = 3;

    private ForgotPasswordContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = (Activity) getContext();
        assert activity != null;
        try{
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        catch(NullPointerException e){
            Log.e("activity.getWindow()", "exception: " + e);
        }
        this.user = EventHelper.getEventPayload("getUser", User.class);
        contract = new ForgetWidget(Objects.requireNonNull(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_otp, container, false);
        inflatedView(inflatedView);
        initView();
        sendRequest();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        otpView = inflatedView.findViewById(R.id.otpView);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Уншиж байна");
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        progressDialog.show();
        StringBuilder phoneNumber = new StringBuilder(this.user.getHandPhone());
        phoneNumber.setCharAt(3, '*');
        phoneNumber.setCharAt(5, '*');
        phoneNumber.setCharAt(7, '*');
        textTitle.setText("Таны "+phoneNumber + " " + getContext().getResources().getString(R.string.otp_title));
        otpView.setOTPPhoneNumber(this.user.getHandPhone());
        otpView.setLinkOnClickListener(v -> {
            checkBlockIncrement();

        });
        OTPFilledListener otpListener = new OTPFilledListener();
        otpView.setFilledListener(otpListener);
    }

    private void sendRequest() {
        otpView.stopTimer();
        otpView.hideErrorLabel();
        checkCount = 3;
        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", this.user.getHandPhone());

        contract.requestOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();

                if (otp.getStatus().equals("SUCCESS")) {
                    otpView.startTimer();
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, additions);
    }

    private void checkOTP(String code) {
        progressDialog.show();
        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", this.user.getHandPhone());
        additions.put("otp", code);
        contract.checkOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();
                checkCount--;
                if (otp.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    if (checkCount == 0) {
                        otpView.stopTimer();
                        otpView.setLabel();
                        otpView.showErrorLabel("3 удаа буруу оруулсан тул та дахин код илгээнэ үү");
                    } else {
                        otpView.showErrorLabel("Нууц код буруу байна. Танд " + checkCount + " удаагийн оролдлого үлдлээ");
                    }
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
                otpView.emptyLabel();
            }
        }, additions);
    }


    private void publish() {
        otpView.hideKeyboard();
        otpView.stopTimer();
        if (this.user.getCif().equals("0")) {
            EventHelper.publishBack(getContext(), new ForgetResetPassWidgetView(), "getUser", this.user);
        } else {
            EventHelper.publishBack(getContext(), new ForgetFingerWidgetView(), "getUser", this.user);
        }
    }

    private void checkBlockIncrement() {
        progressDialog.setMessage("Уншиж байна");
        progressDialog.show();
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                if (blockIncrement == null) {
                    sendRequest();
                } else {
                    progressDialog.dismiss();
                }
                Log.e("MBank", "block hillee");
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, "10","5");
    }

    public class OTPFilledListener implements OTPViewLisener {

        @Override
        public void onSuccess(String code) {
            checkOTP(code);
        }
    }

}
