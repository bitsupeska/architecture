package mn.bitsup.callservice.view.custom;


public interface WidgetListItemClickListener<T> {

    void onItemClicked(T item);
}
