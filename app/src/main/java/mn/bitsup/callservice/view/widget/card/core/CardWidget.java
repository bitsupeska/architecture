package mn.bitsup.callservice.view.widget.card.core;

import android.content.Context;
import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;
import mn.bitsup.callservice.client.card.listener.CardsListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.util.CustomParams;
import mn.bitsup.callservice.view.widget.card.core.contract.CardContract;
import mn.bitsup.callservice.view.widget.card.dto.UiCardPayload;

import static mn.bitsup.callservice.ClientsManager.offlineClient;
import static mn.bitsup.callservice.ClientsManager.cardClient;
public class CardWidget implements CardContract {

    private Context context;


    public CardWidget(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public void checkBlock(BlockListener blockListener, HashMap<String, String> param) {
        offlineClient.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                blockListener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                blockListener.onError(var1);
            }
        },param);
    }

    @Override
    public void onCardActivationStart(@NonNull UiCardPayload uiCardPayload) {

    }

    public void getCardList(@NonNull CustomParams var1, @NonNull CardsListener var2) {
        cardClient.getCardList(var1, var2);
    }

    @Override
    public void changeCardStatus(StatusListener var1, Map<String, String> var2) {

    }

    @Override
    public void onCardReplacementStart(@NonNull UiCardPayload uiCardPayload) {

    }
}
