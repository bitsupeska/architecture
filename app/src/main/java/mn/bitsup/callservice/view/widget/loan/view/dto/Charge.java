package mn.bitsup.callservice.view.widget.loan.view.dto;

public class Charge {
    private String event;
    private String amt;

    public Charge(String event, String amt) {
        this.event = event;
        this.amt = amt;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }
}
