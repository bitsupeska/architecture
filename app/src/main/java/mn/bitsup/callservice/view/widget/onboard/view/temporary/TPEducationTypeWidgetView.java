package mn.bitsup.callservice.view.widget.onboard.view.temporary;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.temporary.dto.EducationItem;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.listener.EducationListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class TPEducationTypeWidgetView extends Fragment implements OnboardView<OnboardContract>, AdapterView.OnItemSelectedListener {

    private Context context;
    private OnboardContract contract;
    private IconSpinner spinnerEducationType;
    private Hashtable<String, String> educationTypeList = new Hashtable<>();
    private List<String> educationTypeSpinnerList = new ArrayList<>();
    private CustomButton buttonContinue;
    private ProgressDialog progressDialog;
    private Temporary payload;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_education_type, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
        spinnerEducationType = inflatedView.findViewById(R.id.spinnerEducationType);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
    }

    private void initView() {
        fillEducationSpinner();
        buttonContinue.setOnClickListener(v -> publish());
        spinnerEducationType.setOnItemSelectedListener(this);
    }

    private void showProgress(){
        progressDialog.show();
        screenEnabled(true);
    }

    private void dismissProgress(){
        progressDialog.dismiss();
        screenEnabled(false);
    }

    private void fillEducationSpinner() {
        educationTypeSpinnerList.add("Coнгох");
        showProgress();
        contract.getEduType(new EducationListener() {
            @Override
            public void onSuccess(List<EducationItem> educationItems) {
                for(int i = 0; i<educationItems.size(); i++){
                    educationTypeList.put(educationItems.get(i).getLocaletext(), educationItems.get(i).getValue());
                    educationTypeSpinnerList.add(educationItems.get(i).getLocaletext());
                }
                spinnerEducationType.setItems(educationTypeSpinnerList);
                dismissProgress();
                Log.e("fillEducationSpinner", "success: ");
            }

            @Override
            public void onError(Response response) {
                Log.e("fillEducationSpinner", "error: " + response);
            }
        });
    }

    private void publish() {
        LoanActivity.addProgress();
        payload = contract.getTemporary();
        payload.setEducationType(educationTypeList.get(spinnerEducationType.getSelectedItem()));
        contract.setTemporary(payload);
        EventHelper.publishBack(context, new TPEmployeeStatusWidgetView());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        buttonContinue.setState((spinnerEducationType.getSelectedPosition() != 0) ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        buttonContinue.setState((spinnerEducationType.getSelectedPosition() != 0) ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    private void screenEnabled(boolean enable) {
        if (enable) getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
