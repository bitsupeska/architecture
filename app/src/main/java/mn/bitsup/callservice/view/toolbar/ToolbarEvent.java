package mn.bitsup.callservice.view.toolbar;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Erdenedalai R&D B.V on 03/11/2017.
 * ToolbarEvent used to open up the correct toolbar.
 */

public class ToolbarEvent implements Parcelable {


    @Expose
    @SerializedName("toolbarPage")
    private ToolbarPage toolbarPage;

    public ToolbarEvent(ToolbarPage toolbarPage) {
        this.toolbarPage = toolbarPage;
    }

    protected ToolbarEvent(Parcel in) {
        toolbarPage = ToolbarPage.valueOf(in.readString());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(toolbarPage.toString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ToolbarEvent> CREATOR = new Creator<ToolbarEvent>() {
        @Override
        public ToolbarEvent createFromParcel(Parcel in) {
            return new ToolbarEvent(in);
        }

        @Override
        public ToolbarEvent[] newArray(int size) {
            return new ToolbarEvent[size];
        }
    };

    /**
     * @return the toolbarPage. If the toolbarPage is null, it will default to ToolbarPage.NO_TOOLBAR
     */
    @NonNull
    public ToolbarPage getToolbarPage() {
        if (toolbarPage == null) {
            return ToolbarPage.NO_TOOLBAR;
        }
        return toolbarPage;
    }
}
