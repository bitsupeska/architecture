package mn.bitsup.callservice.view.custom.datepicker;

/**
 * Created by Erdenedalai R&D B.V on 03/04/2018.
 * Interface for providing style attributes across different views/adapters
 */

public interface StyleProvider {
    int onDeliveryDateCircleColorRequested();
    int onDeliveryDateTextStyleRequested();
    int onRushDeliveryDateCircleColorRequested();
    int onMinDateTextStyleRequested();
    int onBeforeMinDateTextStyleRequested();
    int onAfterMinDateTextStyleRequested();

}
