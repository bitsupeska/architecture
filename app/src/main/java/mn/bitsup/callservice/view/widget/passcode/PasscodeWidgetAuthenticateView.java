package mn.bitsup.callservice.view.widget.passcode;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.concurrent.Executor;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.util.FingerSettings;
import mn.bitsup.callservice.view.activity.EnrollmentActivity;
import mn.bitsup.callservice.view.activity.MainActivity;
import mn.bitsup.callservice.view.animation.AnimationEndListener;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.widget.passcode.core.PasscodeWidget;
import mn.bitsup.callservice.view.widget.passcode.core.contract.PasscodeContract;
import mn.bitsup.callservice.view.widget.passcode.util.KeyboardUtils;

public class PasscodeWidgetAuthenticateView extends Fragment implements
        PinContainer.OnPinContainerListener {

    private Dialog dialog;
    private int SOFT_FADE_DURATION_MILLIS = 500;
    private long FAKE_LOADING_DELAY = 0;
    private PinContainer pinContainer;
    private TextView textTitle;
    private TextView textMessage;
    private TextView textForgot;
    private ProgressBar progressBar;
    private EditText inputEditText;
    private View container;
    private PasscodeContract contract;
    private StorageComponent storageComponent;
    private PasscodeListener passcodeListener = new PasscodeListenerImpl();
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contract = new PasscodeWidget(getContext());
        this.storageComponent = new StorageComponent();
        this.context = getContext();

        executor = ContextCompat.getMainExecutor(context);
        biometricPrompt = new BiometricPrompt(getActivity(),
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Log.e("biometric", "onAuthenticationError: " + " ERROR" );
                inputEditText.setText("");
                hideProgress(false);
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {

                super.onAuthenticationSucceeded(result);
                Log.e("biometric", "onAuthenticationError: " + " SUCCESS" );
                showProgress();
                refreshToken();

            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }
        });
    }

    private void refreshToken(){
        ClientsManager.authClient.performAutomatedLogin(new PasscodeAuthListener() {
            @Override
            public void onSuccess() {
                getUserData();
            }

            @Override
            public void onError(Response response) {
                hideProgress(false);
                inputEditText.setText("");
                showErrorNotification("Уучлаарай", " Мэдээлэл татах үед алдаа гарлаа");
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.passcode_layout_authenticate, container, false);
        initializeViews(inflatedView);
        return inflatedView;
    }

    protected void initializeViews(@NonNull View inflatedView) {
        getContext().stopService(new Intent(getContext(), RefreshService.class));
        textTitle = inflatedView.findViewById(R.id.textTitle);
        textMessage = inflatedView.findViewById(R.id.textMessage);
        textForgot = inflatedView.findViewById(R.id.textForgot);
        pinContainer = inflatedView.findViewById(R.id.pinContainer);
        progressBar = inflatedView.findViewById(R.id.progressBar);
        inputEditText = inflatedView.findViewById(R.id.textInputEditText);
        container = inflatedView.findViewById(R.id.container);
        inputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        textMessage.setText(getContext().getResources().getString(R.string.passcode_authenticate_message));
        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Хурууны хээ/ Face ID-р нэвтрэх")
                .setSubtitle("")
                .setNegativeButtonText("Буцах")
                .build();

        initializeListeners();
        initializePinContainer();
        hideProgress(false);
        inflatedView.findViewById(R.id.container).setOnClickListener(onScreenClickListener);
        checkFingerPrint();
    }

    private void checkFingerPrint() {
        if (getFingerPrint() != null && getFingerPrint().equals("true")) {
            try{
                biometricPrompt.authenticate(promptInfo);
            }
            catch(Exception e){
                Log.e("uno", "checkFingerPrint: " + "Error" );
            }
            //showFingerAuthDialog();
            //FingerSettings.setInflateFingerPrint(getContext(), passcodeListener);
        }
    }

    private String getFingerPrint() {
        StorageComponent sharedPref = new StorageComponent();
        return sharedPref.getItem("bb.key.is_finger");
    }

    @Override
    public void onResume() {
        super.onResume();
        KeyboardUtils.showKeyboard(inputEditText);
        inputEditText.post(() -> inputEditText.setText(inputEditText.getText()));
    }

    protected void initializeListeners() {
        inputEditText.addTextChangedListener(pinContainer.getAfterTextChangedListener());
        container.setOnClickListener(onScreenClickListener);
        if (textForgot != null) {
            textForgot.setOnClickListener(forgotTextClickListener);
        }
        Log.e("uno", "initializePinContainer: " + "initializeListeners");
    }


    private void initializePinContainer() {
        pinContainer.setGravity(Gravity.CENTER_HORIZONTAL);
        int boxNum = contract.getNumberOfDigits();
        pinContainer.setPins(boxNum, this);
        Log.e("uno", "initializePinContainer: " + "initializePinContainer");
    }


    protected void showProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            animation.setDuration(SOFT_FADE_DURATION_MILLIS);
            progressBar.startAnimation(animation);
        }
    }


    protected void hideProgress(boolean animate) {
        if (progressBar != null) {
            if (animate) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                animation.setDuration(SOFT_FADE_DURATION_MILLIS);
                animation.setAnimationListener(new AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        hideProgress(false);
                    }
                });
                progressBar.startAnimation(animation);
            } else {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }


    protected View.OnClickListener onScreenClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            KeyboardUtils.showKeyboard(inputEditText);
        }
    };

    protected View.OnClickListener forgotTextClickListener = v -> {
    };


    public void onPinCompleted(final char[] pin) {
        if (pin != null && pin.length != 0) {
            showProgress();
            Handler handler = new Handler();
            handler.postDelayed(() -> contract.passcodeEntered(pin, passcodeListener, "AUTHENTICATE"), FAKE_LOADING_DELAY);
        }
    }

    class PasscodeListenerImpl implements PasscodeListener {

        @Override
        public void onSuccess() {
            showProgress();
            getUserData();
        }

        @Override
        public void onError(Response errorResponse) {
            inputEditText.setText("");
            hideProgress(false);

            showErrorNotification("Уучлаарай", errorResponse.getResponseCode() == 0 ? getContext().getResources().getString(R.string.shared_error_no_internet_message) : "Сервертэй холбогдоход алдаа гарлаа");

        }

        @Override
        public void passcodeMatchingFailed() {
            if (passcodeMatchingCount() >= 3) {
                showMatchingErrorDilaog();
            } else {
                inputEditText.setText("");
                hideProgress(false);
                showErrorNotification(getContext().getString(R.string.passcode_does_not_match_title), getContext().getString(R.string.passcode_does_not_match_message));
            }

        }
    }


    private void showMatchingErrorDilaog() {
        inputEditText.setText("");
        hideProgress(false);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
        alertdialog.setMessage("Нууц үг 3 удаа буруу хийгдсэн тул та дахин нэвтэрнэ үү.")
                .setCancelable(false)
                .setPositiveButton("OK", (dialog, which) -> {
                    dialog.cancel();
                    ClientsManager.getAuthClient().endSession();
                    ((Activity) getContext()).finish();
                    Intent enrollmentIntent = new Intent(getContext(), EnrollmentActivity.class);
                    getContext().startActivity(enrollmentIntent);
                });

        alertdialog.show();
    }

    private int passcodeMatchingCount() {
        int count = 0;
        if (storageComponent.getItem("passcode") != null) {
            count = Integer.parseInt(storageComponent.getItem("passcode")) + 1;
            storageComponent.setItem("passcode", "" + count);
        } else {
            storageComponent.setItem("passcode", "1");
            count = 1;
        }

        return count;
    }

    public void showErrorNotification(@NonNull final String title, @NonNull final String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle, getContext());
    }

    private void getUserData() {
        contract.getUser(new UserListener() {
            @Override
            public void onSuccess(User user) {
                hideProgress(false);
                if (user.getStatus().equals("SUCCESSFUL")) {
                    Intent intent = new Intent(getContext(), MainActivity.class);
                    getContext().startActivity(intent);
                    ((Activity) getContext()).finish();
                } else {
                    inputEditText.setText("");
                    showErrorNotification("Уучлаарай", user.getMessage());
                }
            }

            @Override
            public void onError(Response var1) {
                hideProgress(false);
                inputEditText.setText("");
                showErrorNotification("Уучлаарай", " Мэдээлэл татах үед алдаа гарлаа");
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
        FingerSettings.stopFingerAuth();
    }
}
