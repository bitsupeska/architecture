package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.EditorInfo;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBEmailWidgetView extends Fragment implements OnboardView<OnboardContract>,
        TextWatcher {

    private Context context;
    private CustomButton buttonContinue;
    private InputField inputEmail;
    private OnboardContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_email_continue, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputEmail = inflatedView.findViewById(R.id.inputInput);
    }

    private void initView() {
        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        buttonContinue.setOnClickListener(v -> {
            buttonContinue.setState(ButtonState.LOADING,getActivity());
            checkEmail();

        });
        inputEmail.addTextChangedListener(this);
        inputEmail.setIconViewVisible(false);
        inputEmail.setSingleLine();
        inputEmail.setCancelButton();
        inputEmail.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void checkEmail() {
        contract.checkEmail(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                if(statusResponse.getStatus().equals("SUCCESS")){
                    showAlertDialog();
                }else{
                    publish();
                }
            }

            @Override
            public void onError(Response var2) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
            }
        }, inputEmail.getValue());
    }


    private void showAlertDialog() {
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        new AnimationDialog(context, "", context.getResources().getString(R.string.temporary_email_exists_error_message), "Буцах", "anim_rejection_comment.json").show();
    }

    private void publish(){
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        KeyboardUtils.hideKeyboard(context, inputEmail);
        EasyOnboardItem payload = contract.getPayload();
        payload.setEmail(inputEmail.getValue());
        OnboardActivity.addProgress();
        EventHelper.publishBack(context, new OBRegisterWidgetView());
    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (!isValidEmail(editable)) buttonContinue.setState(ButtonState.DISABLED,getActivity());
        else buttonContinue.setState(ButtonState.ENABLED,getActivity());
    }

}
