package mn.bitsup.callservice.view.dialog;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.airbnb.lottie.LottieAnimationView;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.custom.CustomButton;

public class LoanSuccessDialog extends Dialog {

    private TextView textDescription, textTitle;
    private LottieAnimationView animationView;
    private Boolean isFirst = true;
    private AnimationDialogListener listener;
    private CustomButton buttonBack;

    public LoanSuccessDialog(Context context,  AnimationDialogListener listener){
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setContentView(R.layout.loan_success);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        animationView = findViewById(R.id.success_animation);
        textTitle = findViewById(R.id.textTitle);
        textDescription = findViewById(R.id.textDescription);
        buttonBack = findViewById(R.id.buttonBack);

        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if(isFirst){
                    final Handler handler = new Handler();
                    handler.postDelayed(() -> startAnimationSecond(), 2000);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
    }

    private void startAnimationSecond(){
        buttonBack.setVisibility(View.VISIBLE);
        textTitle.setText("Та байгальд ээлтэй\n хүн юм аа!");
        textDescription.setText("Эх дэлхийгээ хайрлаж, цаас хэрэглэхгүй,\n дижитал зээл авсанд баярлалаа!");
        animationView.setAnimation("loan_tree.json");
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animationView.playAnimation();
        buttonBack.setOnClickListener(v ->  dismiss());
    }

    @Override
    protected void onStop() {
        listener.onDismiss();
        super.onStop();
    }
}
