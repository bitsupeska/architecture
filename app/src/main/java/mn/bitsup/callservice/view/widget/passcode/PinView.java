package mn.bitsup.callservice.view.widget.passcode;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 14/09/2017.
 */

public class PinView  extends RelativeLayout {

    private RelativeLayout background;
    private View pin;

    public PinView(@NonNull Context context) {
        this(context, null);
    }

    public PinView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeView();
    }

    public void initializeView() {
        inflate(getContext(), getLayoutResource(), this);

        background = findViewById(R.id.background);
        pin = findViewById(R.id.pin);

        enabled(false);
        removePin();
    }

    public int getLayoutResource() {
        return R.layout.passcode_pin_round;
    }

    /**
     * This method will change the background of the pin container.
     *
     * @param enabled: show enabled effect or not.
     */
    public void enabled(boolean enabled) {
        background.setEnabled(enabled);
    }

    /**
     * This method will show the pin dot.
     */
    public void addPin() {
        pin.setVisibility(VISIBLE);
    }

    /**
     * This method will hide the pin dot.
     */
    public void removePin() {
        pin.setVisibility(GONE);
    }

    /**
     * This method will return the visibility of the pin dot.
     *
     * @return true if pin dot is visible.
     */
    public boolean isPinVisible() {
        return pin.getVisibility() == VISIBLE;
    }

    /**
     * This method will return the enabled status of the pin container.
     *
     * @return true if pin container is enabled.
     */
    public boolean isPinEnabled() {
        return background.isEnabled();
    }

    /**
     * This method defines the size of the PinView plus the spacing.
     *
     * @param size:           Size of the PinView (
     * @param displaySpacing: If true it will add a right margin to the view.
     */
    public void setParams(int size, boolean displaySpacing) {
        int defaultBoxSize = getDefaultBoxSize();
        int newSize = size;

        if (newSize > defaultBoxSize) {
            newSize = defaultBoxSize;
        }

        int left = 0;
        int top = 0;
        int right = displaySpacing ? getPinSpacing() : 0;
        int bottom = 0;

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(newSize, newSize);
        layoutParams.setMargins(left, top, right, bottom);

        this.setLayoutParams(layoutParams);
    }

    public int getPinSpacing() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.pin_spacing);
    }

    public int getDefaultBoxSize() {
        return getContext().getResources().getDimensionPixelSize(R.dimen.pin_background_width);
    }

}
