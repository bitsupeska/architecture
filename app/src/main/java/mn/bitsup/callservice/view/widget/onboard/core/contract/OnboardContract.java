package mn.bitsup.callservice.view.widget.onboard.core.contract;

import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.CheckUserNameListener;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.listener.EducationListener;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;

public interface OnboardContract {

    // Easy
    void checkRegister(String var1, StatusListener var2);

    void checkBaml(String var1, StatusListener var2);

    void requestPCR(String var1, StatusListener var2);

    void checkUsername(String var1, CheckUserNameListener var2);

    void checkToken(TokenListener var1);

    void requestOTP(OTPListener var1, Map<String, String> var2);

    void checkOTP(OTPListener otpListener, Map<String, String> params);

    void getKhurData(KhurDataListener listener, Map<String, String> params);

    void createUser(StatusListener listener, EasyOnboardItem params);

    void getCheckCitizenInfo(CitizenInfoListener var1, Map<String, String> var2);

    void setPayload(EasyOnboardItem var1);

    EasyOnboardItem getPayload();

    // Temporary
    Temporary getTemporary();

    void setTemporary(Temporary var1);

    void getEmpstatlookup(TemporaryItemsListener var1);

    void getOccstatlookup(TemporaryItemsListener var1);

    void loadCityData(LocationListener var1);

    void loadDistrictData(LocationListener var1, String var2);

    void checkEmail(StatusListener var1, String var2);

    void createCif(StatusListener var1, KhurItems var2);

    void createCA(StatusListener var1);

    void setBlock(StatusListener var1, BlockIncrement var2);

    void setBlockKhur(StatusListener var);

    void checkBlockIncrement(BlockIncrementListener var1);

    void getEduType(EducationListener var1);

    void getXyrData(KhurDataListener listener, Map<String, String> params);
}