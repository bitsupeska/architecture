package mn.bitsup.callservice.view.widget.loan.view;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.dto.LoanSalary;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.loan.listener.LoanSalaryListener;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockData;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.KhurReturnImageListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.PhoneUtils;
import mn.bitsup.callservice.util.TextViewColor;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.dialog.KhurInfoDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;
import mn.bitsup.callservice.view.widget.profile.dto.ProfilePayload;
import mn.bitsup.callservice.view.widget.profile.views.ProfilePhoneChangeWidgetView;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class LoanFingerView extends Fragment implements
        OnboardView<OnboardContract> {

    private int count = 3;
    private Context context;
    private OnboardContract onboardContract;
    private LoanContract loanContract;
    private ForgotPasswordContract forgotPasswordContract;
    private User user;
    private TextView textTitle, textLink;
    private boolean isBlocked;
    private String blockMessage;
    private CustomButton buttonContinue;
    private LottieAnimationView animationView;
    private LoanOffer loanOffer;
    private DataListener dataListener = new DataListener();
    private VerifiedListener verifiedListener = new VerifiedListener();
    private VerifiedPhoneListener verifiedPhoneListener = new VerifiedPhoneListener();
    private String salary = "0";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.onboardContract = new OnboardWidget(context);
        this.loanContract = new LoanWidget(context);
        forgotPasswordContract = new ForgetWidget(Objects.requireNonNull(getContext()));
        this.user = MainApplication.getUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_finger_guide, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        textLink = inflatedView.findViewById(R.id.textLink);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        animationView = inflatedView.findViewById(R.id.success_animation);
    }

    private void initView() {
        textTitle.setText(context.getResources().getString(R.string.onboard_finger_guide_title));
        textLink.setText(TextViewColor.getUnderlineText(getContext(),
                context.getResources().getString(R.string.onboard_finger_guide_link), 13, 21));
        PhoneUtils.checkBioPermissions(context);
        setAnimation();
    }

    private void setAnimation() {
        animationView.setAnimation("anim_khur.json");
        animationView.playAnimation();
    }

    private void initEvent() {
        buttonContinue.setOnClickListener(v -> isStoragePermissionGranted());
        textLink.setOnClickListener(v -> showInformation());
    }

    public void isStoragePermissionGranted() {


        if (PhoneUtils.isTimeZoneAutomatic(getContext())) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                        checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE}, 1);
                } else {
                    checkBlock();
                }
            } else { //permission is automatically granted on sdk<23 upon installation
                Log.e("uno", "sdk<23: Permission is granted");
                checkBlock();
            }
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Тохиргоо");
            alertDialog.setMessage("Автомат цагийн бүс(Time zone)-ийг асаана уу.");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    (dialog, which) -> {
                        dialog.dismiss();
                        Intent callGPSSettingIntent = new Intent(Settings.ACTION_DATE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    });
            alertDialog.show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int count = 0;
        for (int i = 0; i < permissions.length; i++)
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) count++;
        if (count == permissions.length) checkBlock();
    }

    private void checkBlock() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> blockParams = new HashMap<>();
        blockParams.put("value", user.getUid());
        blockParams.put("macAddress", DeviceAddressUtil.getMACAddress(context, "wlan0"));
        blockParams.put("ipAddress", DeviceAddressUtil.getIPAddress(true));
        forgotPasswordContract.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                if (!var1.getData().isEmpty()) {
                    for (BlockData block :
                            var1.getData()) {
                        // 2, 3 - khur block shalgah
                        if (block.getBlocktype() == 3
                                && block.getBlockchannel() == 2
                                && block.getBlockinfo().equals(user.getUid())) {
                            isBlocked = true;
                            blockMessage = block.getBlockmsg();
                        }
                    }
                } else {
                    isBlocked = false;
                }
                if (!isBlocked) {
                    checkCif();
                } else {
                    new AnimationDialog(getContext(), "Уучлаарай,",
                            blockMessage,
                            "Буцах", "anim_rejection_clock.json",
                            () -> ((Activity) getContext()).finish()).show();
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }

            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(getContext());
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, blockParams);
    }

    private void checkCif() {
        if (user.getCif().equals("0")) {
            startKhurActivity();
        } else {
            getSalary();
        }
    }

    private void startKhurActivity() {
        if (count > 0) {
            HashMap<String, String> params = new HashMap<>();
            params.put("regNum", MainApplication.getUser().getRegNum());
            onboardContract.getXyrData(new KhurDataListener() {
                @Override
                public void onSuccess(KhurItems var1) {
                    if (var1.getStatus().equals("SUCCESS")) {
                        buttonContinue.setState(ButtonState.LOADING, getActivity());
                        createCif(var1);
                    } else {

                    }
                }

                @Override
                public void onError(Response var1) {
                }
            }, params);
        } else {
            onboardContract.setBlockKhur(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    Log.e("MBank", "username block: " + statusResponse.getMessage());
                }

                @Override
                public void onError(Response response) {
                    CustomAlertDialog.showAlertDialog(context,
                            context.getResources().getString(R.string.shared_alert_generic_error_title),
                            (response.getErrorMessage() != null && !response.getErrorMessage()
                                    .equals("")) ?
                                    response.getErrorMessage() : context.getResources()
                                    .getString(R.string.shared_alert_generic_error_server));
                }
            });
            AnimationDialog dialog = new AnimationDialog(context, "Уучлаарай,",
                    context.getResources().getString(
                            R.string.khur_alert_finger_not_match),
                    context.getResources().getString(R.string.shared_button_back), "error_anim.json",
                    () -> ((Activity) context).finish());
            dialog.show();
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
        }

    }

    private void showInformation() {
        KhurInfoDialog dialog = new KhurInfoDialog(getContext());
        dialog.show();
    }

    private void getKhurData(String image) {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> params = new HashMap<>();
        params.put("regNum", user.getRegNum());
        params.put("image", image);

        onboardContract.getKhurData(new KhurDataListener() {
            @Override
            public void onSuccess(KhurItems khurItems) {

                if (khurItems.getStatus().equals("SUCCESS")) {
                    checkCif(khurItems);
                } else {
                    count--;
                    AnimationDialog dialog = new AnimationDialog(context, "",
                            "Хурууны хээ баталгаажуулалт амжилтгүй. "
                                    + "Танд " + count + " удаагийн боломж үлдлээ.",
                            context.getResources().getString(
                                    R.string.shared_button_retry), "anim_rejection_finger.json");
                    dialog.show();
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response response) {
                Log.e("LoanFinger", "onError: " + response.getErrorMessage());
                CustomAlertDialog.showServerAlertDialog(context);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, params);
    }

    private void checkCif(KhurItems khurItems) {
        if (user.getCif().equals("0")) {
            createCif(khurItems);
        } else {
            getSalary();
        }
    }

    private void checkPhoneVerified() {
        ProfilePayload payload = new ProfilePayload();
        payload.setPublishType("Loan");
        EventHelper.publishBack(context, new ProfilePhoneChangeWidgetView(verifiedPhoneListener),
                "getPhoneData", payload);
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
    }

    private void getSalary() {
        if (user.getPhoneNo() != null && user.getPhoneNo().equals("10000000")) {
            crdpAlertShow("Таны бүртгүүлсэн дугаар идэвхгүй болсон тул дугаараа шинэчилнэ үү");
        } else {
            Map<String, String> params = new HashMap<>();
            params.put("regNum", user.getRegNum());
            params.put("image", "");
            loanContract.getSalary(params, new LoanSalaryListener() {
                @Override
                public void onSuccess(LoanSalary var1) {
                    checkSalary(var1);
                }

                @Override
                public void onError(Response var1) {
                    CustomAlertDialog.showServerAlertDialog(context);
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            });
        }
    }

    private void checkSalary(LoanSalary loanSalary) {
        if (loanSalary.getData() != null) {
            if (loanSalary.getData().getSi_pay_mon_last12() == 0) {
                EventHelper.publishBack(context, new LoanInputSalaryView(verifiedListener));
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            } else {
                getOffer();
            }
        } else {
            CustomAlertDialog.showAlertDialog(context, "", loanSalary.getMessage());
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
        }
    }

    private void getOffer() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        loanContract.getOffer(getLoanOfferParams(), new LoanOfferListener() {
            @Override
            public void onSuccess(LoanOffer loanOffer) {
                if (loanOffer.getCode() == 200) {
                    if (loanOffer.getData().getXyp()) {
                        LoanFingerView.this.loanOffer = loanOffer;
//                        if (!loanOffer.getData().getCRDP()) {
//                            getSalary();
//                        } else {
//                            checkPhoneVerified();
//                        }
                        if (!loanOffer.getData().getCRDP()) {
                            viewOffer(loanOffer);
                        } else {
                            crdpAlertShow("Таны бүртгүүлсэн утасны дугаар таны нэр дээр биш байна. Та дугаараа солино уу");
                        }
                    } else {
                    }

                } else if (loanOffer.getMessage().equals("CRDP Does not Match.")) {
                    crdpAlertShow("Таны бүртгүүлсэн утасны дугаар таны нэр дээр биш байна. Та дугаараа солино уу");
                } else if (loanOffer.getCode() == 302) {
                    viewOffer(loanOffer);
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                } else {
                    CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), loanOffer.getMessage());
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(context);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        });
    }

    private void viewOffer(LoanOffer loanOffer) {
        getActivity().setResult(1);
        getActivity().finish();
    }

    private Map<String, String> getLoanOfferParams() {
        Map<String, String> params = new HashMap<>();
        params.put("cif", user.getCif());
        params.put("frstEntrDttm", user.getMobileDuration());
        params.put("regNum", user.getRegNum());
        params.put("phoneNumber", user.getPhoneNo());
        params.put("reqId", "AONTHAOTNUHNSTOUHTNEHSUNAHOTNUHNAO");
        params.put("salary", salary);
        return params;
    }

    private void createCif(KhurItems khurItems) {
        onboardContract.createCif(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                if (statusResponse.getStatus().equals("SUCCESS")) {
                    getUserInfo();
                } else {
                    CustomAlertDialog.showAlertDialog(context, "", statusResponse.getMessage());
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response var2) {
                CustomAlertDialog.showServerAlertDialog(context);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, khurItems);
    }

    private void getUserInfo() {
        ClientsManager.getProfileClient().getUser(new UserListener() {
            @Override
            public void onSuccess(User statusResponse) {
                if (statusResponse.getCode().equals("200")) {
                    ClientsManager.getRemoteAuthClient().setUser(statusResponse);
                    user = MainApplication.getUser();
                    if (user.getCif() != null && user.getCif().equals("0")) {
                        CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), "Та дахин оролдоно уу");
                        buttonContinue.setState(ButtonState.ENABLED);
                    } else {
                        getSalary();
                    }
                } else {
                    CustomAlertDialog.showAlertDialog(context, "", statusResponse.getMessage());
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response response) {
                Log.e("LoanFinger", "getUserInfo " + response);
                CustomAlertDialog.showServerAlertDialog(context);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        });
    }

    private void crdpAlertShow(String message) {
        DialogInterface.OnClickListener positiveButtonClickListener = (dialog, which) -> {
            checkPhoneVerified();
        };
        new android.app.AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(context.getResources().getString(R.string.shared_alert_generic_alert_error_title))
                .setMessage(message)
                .setPositiveButton("Солих", positiveButtonClickListener)
                .setCancelable(false)
                .show();
    }

    public class VerifiedListener implements StatusListener {

        @Override
        public void onSuccess(StatusResponse var1) {
            if (var1.getMessage() != null && var1.getMessage().equals("")) {
                var1.setMessage("0");
            }
            salary = var1.getMessage();
            getOffer();
        }

        @Override
        public void onError(Response var2) {

        }
    }

    public class VerifiedPhoneListener implements StatusListener {

        @Override
        public void onSuccess(StatusResponse var1) {
            if (user.getPhoneNo() != null && user.getPhoneNo().equals("10000000")) {
                user = MainApplication.getUser();
                getSalary();
            } else {
                user = MainApplication.getUser();
                getOffer();
            }

        }

        @Override
        public void onError(Response var2) {

        }
    }


    public class DataListener implements KhurReturnImageListener {

        @Override
        public void returnImage(String var1) {
            getKhurData(var1);
        }

        @Override
        public void onErrorHandling() {
            Log.e("KhurReturnImageListener", "onErrorHandling");
            CustomAlertDialog.showAlertDialog(context, "Уучлаарай", "Та дахин оролдон уу.");
        }
    }
}