package mn.bitsup.callservice.view.custom;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.widget.DismissibleWidgetView;


public abstract class DismissibleSummaryView extends DismissibleWidgetView {

    private static final String TAG = "DismissibleSummaryView";

    /**
     * @return the title to be displayed by this view.
     */
    @Nullable
    protected abstract String getTitle();

    /**
     * Initializes the title icon's imageview (such as setting background and foreground drawables)
     * @param titleIcon the inflated ImageView object
     */
    protected abstract void initTitleIcon(ImageView titleIcon);

    /**
     * @return the amount of items to be shown by {@link DataCell}
     */
    protected abstract int getItemsCount();

    /**
     * @param position
     * @return the title for the item displayed by the {@link DataCell} at position.
     */
    @StringRes
    protected abstract int getTitleForItemAt(int position);

    /**
     * @param position
     * @return the value for the item displayed by the {@link DataCell} at position.
     */
    @Nullable
    protected abstract String getValueForItemAt(int position);

    /**
     * @return the text shown by the bottom button.
     */
    @Nullable
    protected abstract String getBottomButtonText();

    /**
     * @return true if this view is dismissible, false otherwise.
     */
    protected abstract boolean isDismissAllowed();

    /**
     * Invoked when the bottom button is clicked.
     */
    protected abstract void onBottomButtonClick();

    protected abstract boolean hideContact();

    /**
     * @return true to enable the bottom button, false to disable it.
     */
    protected abstract boolean isBottomButtonEnabled();

    /**
     * @return true to show the indeterminate progress indicator, false to hide it.
     * Note that when the progress indicator is visible the title icon is not and vice versa.
     */
    protected abstract boolean isProgressIndicatorVisible();

    /**
     * @return true to show the view inflated in {@link #inflateViewBelowDataCells(ViewGroup)}
     *         false otherwise
     */
    protected boolean isViewBelowDataCellsVisible() {
        return extraContentContainer.getChildCount() != 0;
    }

    /**
     * @return true when bottom button should be visible, false when should be gone.
     */
    protected boolean isBottomButtonVisible() {
        // Override if needed.
        return true;
    }

    /**
     * @param parent
     * @return an optional extra view to be shown below the list of {@link DataCell}
     */
    @Nullable
    protected View inflateViewBelowDataCells(ViewGroup parent) {
        // Override if needed.
        return null;
    }

    private ImageView titleIconImageView;
    private TextView titleTextView;
    private ImageView buttonClose;
    private LinearLayout dataCellsContainer;
    private LinearLayout extraContentContainer;
    private CustomButton bottomButton;

    public DismissibleSummaryView(final Context context) {
        super(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View parentView =inflater.inflate(R.layout.dismissible_summary_view, container, false);
        titleIconImageView = parentView.findViewById(R.id.titleIconImageView);
        titleTextView = parentView.findViewById(R.id.titleTextView);
        buttonClose = parentView.findViewById(R.id.buttonClose);
        dataCellsContainer = parentView.findViewById(R.id.dataCellsContainer);
        extraContentContainer = parentView.findViewById(R.id.extraContentContainer);
        bottomButton = parentView.findViewById(R.id.bottomButton);

        bottomButton.setOnClickListener(v -> onBottomButtonClick());
        buttonClose.setOnClickListener(v -> dismiss());

        final View extraView = inflateViewBelowDataCells(container);
        if (extraView != null) {
            extraContentContainer.addView(extraView);
        }
        requestViewUpdate();
        return parentView;

    }


    protected void requestViewUpdate() {
        setDismissible(isDismissAllowed());
        initTitleIcon(titleIconImageView);

        titleTextView.setText(getTitle());
        buttonClose.setVisibility(isDismissAllowed() ? VISIBLE : View.INVISIBLE);

        if (isProgressIndicatorVisible()) {
            bottomButton.setState(ButtonState.LOADING,getActivity());
        } else {
            bottomButton.setState(isBottomButtonEnabled() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
        }

        if (isBottomButtonEnabled()) {
            bottomButton.setEnabledText(getBottomButtonText());
        } else {
            bottomButton.setDisabledText(getBottomButtonText());
        }
        bottomButton.setVisibility(isBottomButtonVisible() ? VISIBLE : GONE);

        dataCellsContainer.removeAllViews();
        for (int i = 0; i < getItemsCount(); i++) {
            final DataCell dataCell = new DataCell(getContext());
            dataCell.setHideValueTextWhenEmpty(false);
            dataCell.setHeaderText(getContext().getString(getTitleForItemAt(i)));
            dataCell.setValueText(getValueForItemAt(i));
            dataCell.setValueTextBold();
            dataCellsContainer.addView(dataCell);
            Log.e(TAG, "requestViewUpdate: "+getTitleForItemAt(i) + getValueForItemAt(i) );
            if (i < getItemsCount() - 1) {
                dataCell.showBottomBorder(true);
            }
        }

        extraContentContainer.setVisibility(isViewBelowDataCellsVisible() ? VISIBLE : GONE);
    }

}
