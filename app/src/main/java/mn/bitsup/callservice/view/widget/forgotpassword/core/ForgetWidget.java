package mn.bitsup.callservice.view.widget.forgotpassword.core;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import mn.bitsup.callservice.client.RefreshService;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.dataprovider.util.OfflineCrypto;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.dto.ResetInfo;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.ResetInfoListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;

import static mn.bitsup.callservice.ClientsManager.offlineClient;
import static mn.bitsup.callservice.ClientsManager.otpClient;
import static mn.bitsup.callservice.ClientsManager.profileClient;
import static mn.bitsup.callservice.ClientsManager.temporaryClient;
import static mn.bitsup.callservice.client.AuthClient.ACCESS_TOKEN;

public class ForgetWidget implements ForgotPasswordContract {
    private Context context;


    public ForgetWidget(@NonNull Context context) {
        this.context = context;
    }

    @Override
    public void checkRegister(User user, ResetInfoListener listener) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                HashMap<String, String> params = new HashMap<>();
                params.put("uid", user.getUid());
                params.put("regNum", user.getRegNum());
                params.put("macAddress", DeviceAddressUtil.getMACAddress(context,"wlan0"));
                params.put("ipAddress", DeviceAddressUtil.getIPAddress(true));
                offlineClient.getResetInfo(new ResetInfoListener() {
                    @Override
                    public void onSuccess(ResetInfo var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        listener.onError(var1);
                    }
                }, params);
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
            }
        });
    }

    @Override
    public void requestEmailOTP(OTPListener otpListener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                otpClient.getEmailOTP(params, new OTPListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        otpListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        otpListener.onError(var1);
                    }
                });
            }

            @Override
            public void onError(Response response) {
                otpListener.onError(response);
            }
        });

    }

    @Override
    public void checkToken(TokenListener listener) {
        if (RefreshService.isRunning && RefreshService.timer > 20) {
            listener.onSuccess("");
        } else {
            String data = OfflineCrypto.tokenEncrypt(context);
            if (data != null && !data.equals("")) {
                HashMap<String, String> params = new HashMap<>();
                params.put("data", data);
                offlineClient.convertToken(new TokenListener() {
                    @Override
                    public void onSuccess(String var1) {
                        String ddata = OfflineCrypto.tokenDecrypt(var1, context);
                        if (ddata != null && !ddata.equals("")) {
                            try {
                                JSONObject object = new JSONObject(ddata);
                                StorageComponent storageComponent = new StorageComponent();
                                storageComponent
                                    .setItem(ACCESS_TOKEN, object.getString("access_token"));
                                listener.onSuccess(var1);

                                context.stopService(new Intent(context, RefreshService.class));
                                context.startService(new Intent(context, RefreshService.class));

                            } catch (JSONException e) {
                                Response errorResponse = new Response();
                                errorResponse.setResponseCode(500);
                                listener.onError(errorResponse);
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Response var1) {
                        listener.onError(var1);
                    }
                }, params);
            }
        }
    }

    @Override
    public void requestOTP(OTPListener otpListener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {

                otpClient.getOTP(params, otpListener);
            }

            @Override
            public void onError(Response response) {
                otpListener.onError(response);
//                showErrorDialog();
            }
        });
    }

    @Override
    public void checkOTP(OTPListener otpListener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                otpClient.checkOTP(params, new OTPListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        otpListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        otpListener.onError(var1);
                    }
                });
            }

            @Override
            public void onError(Response response) {
                otpListener.onError(response);
            }
        });
    }

    @Override
    public void setBlock(StatusListener statusListener, int count, String blockType, String blockChannel) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {

                HashMap<String, String> param = new HashMap<>();
                if (blockChannel != null && blockType != null && blockChannel.equals("8") && blockType.equals("4")) {
                    param.put("blockInfo", MainApplication.getUser().getUid());
                    param.put("blockChannel", blockChannel);
                    param.put("blockMsg", "Уучлаарай, Таны ойр дотны хүнээ оруулах оролдлогын тоо дууссан байна. Та 00:00 цагаас хойш дахин оролдоно уу.");
                } else {

                    param.put("blockInfo", DeviceAddressUtil.getMACAddress(context,"wlan0"));
                    param.put("blockChannel", "5");
                    param.put("blockMsg", "Уучлаарай, таны оролдлого хийх эрх хаагдсан байна. Та 00:00 цагаас хойш дахин оролдоно уу.");
                }
                param.put("blockType", blockType);


                offlineClient.setBlock(new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse statusResponse) {
                        statusListener.onSuccess(statusResponse);
                    }

                    @Override
                    public void onError(Response response) {
                        statusListener.onError(response);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                statusListener.onError(response);
            }
        });


    }

    @Override
    public void checkBlockIncrement(BlockIncrementListener listener, String blockType, String blockChannel) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                HashMap<String, String> param = new HashMap<>();
                if (blockType != null && blockChannel != null && blockType.equals("4") && blockChannel.equals("8"))
                    param.put("blockInfo", MainApplication.getUser().getUid());
                else
                    param.put("blockInfo", DeviceAddressUtil.getMACAddress(context,"wlan0"));
                param.put("blockChannel", blockChannel);
                param.put("blockType", blockType);
                offlineClient.checkBlockIncrement(new BlockIncrementListener() {
                    @Override
                    public void onSuccess(BlockIncrement blockIncrement) {

                        Log.e("checkBlockInc", "success");
                        if (blockIncrement.getStatus().equals("SUCCESS")) {
                            checkBlockCount(blockIncrement, blockType, blockChannel, listener);
                        } else {
                            listener.onError(new Response());
                        }
                    }

                    @Override
                    public void onError(Response response) {
                        Log.e("checkBlockInc", "error:" + response);
                        listener.onError(response);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
//                showError/Dialog();
            }
        });

    }

    @Override
    public void resetPassword(StatusListener listener, String password, String Uid) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {

                HashMap<String, String> params = new HashMap<>();
                params.put("uid", Uid);
                params.put("password", password);
                params.put("macAddress", DeviceAddressUtil.getMACAddress(context,"wlan0"));
                params.put("ipAddress", DeviceAddressUtil.getIPAddress(true));
                offlineClient.resetPassword(new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var2) {
                        listener.onError(var2);
                    }
                }, params);
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
//                showErrorDialog();
            }
        });

    }

    @Override
    public void checkEmailOTP(OTPListener otpListener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                otpClient.checkEmailOTP(params, new OTPListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        otpListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        otpListener.onError(var1);

                    }
                });
            }

            @Override
            public void onError(Response response) {
                otpListener.onError(response);
            }
        });
    }

    @Override
    public void getUser(UserListener userListener, User user) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                profileClient.getUser(new UserListener() {
                    @Override
                    public void onSuccess(User var1) {
                        userListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        userListener.onError(var1);
                    }
                }, user);
            }

            @Override
            public void onError(Response response) {
                userListener.onError(response);
            }
        });
    }

    private void checkBlockCount(BlockIncrement blockIncrement, String blockType, String blockCannel, BlockIncrementListener blockIncrementListener) {

        Log.e("checkBlockCount", "getFailed_count(): " + blockIncrement.getData().getFailedCount());
        Log.e("checkBlockCount", "getBlock_limit(): " + blockIncrement.getData().getBlockLimit());

        if (blockIncrement.getData().getFailedCount() >= blockIncrement.getData().getBlockLimit()) {//blockType.equals("10") || blockType.equals("7") ? blockIncrement.getData().getFailedCount() >= blockIncrement.getData().getBlockLimit() - 1 :
            setBlock(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    blockIncrementListener.onSuccess(blockIncrement);
                    if (statusResponse.getStatus().equals("SUCCESS")) {
                        DialogInterface.OnClickListener positiveButtonClickListener = (dialog, which) -> {
                            ((Activity) context).finish();
                        };
                        if (blockType.equals("10")) {
                            new AnimationDialog(context, "Уучлаарай,",
                                    "Та дугаар баталгаажуулах үйлдлийг  " + blockIncrement.getData().getBlockLimit() + " удаа буруу хийсэн тул таны оролдлого хийх эрх хаагдлаа. Та 00:00 цагаас хойш дахин оролдоно уу.",
                                    "Буцах", "anim_rejection_clock.json",
                                    () -> ((Activity) context).finish()).show();
                        } else if (blockType.equals("4")) {
                            new AnimationDialog(context, "Уучлаарай,",
                                    " Таны оруулсын ойр дотны хүний мэдээлэл буруу байгаа тул 00:00 хүртэл таныг блоклож байна.",
                                    "Буцах", "anim_rejection_clock.json",
                                    () -> ((Activity) context).finish()).show();
                        } else {
                            new AnimationDialog(context, "Уучлаарай,",
                                    "Таны оролдлого хийх эрх хаагдлаа. Та 00:00 цагаас хойш дахин оролдоно уу.",
                                    "Буцах", "anim_rejection_clock.json",
                                    () -> ((Activity) context).finish()).show();

                        }
                    } else {
                        blockIncrementListener.onSuccess(null);
                        Log.e("MBank", statusResponse.getMessage());
                    }
                }

                @Override
                public void onError(Response response) {
                    blockIncrementListener.onSuccess(null);
                    Log.e("MBank", "block hiih ued server aldaa garlaa");
//                    showErrorDialog();
                }
            }, blockIncrement.getData().getBlockLimit(), blockType, blockCannel);
        } else {
            blockIncrementListener.onSuccess(blockIncrement);
        }
    }

    @Override
    public void checkBlock(BlockListener blockListener, HashMap<String, String> param) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                offlineClient.checkBlock(new BlockListener() {
                    @Override
                    public void onSuccess(Block var1) {
                        blockListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        blockListener.onError(var1);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                blockListener.onError(response);
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });

    }

    @Override
    public void getKhurData(KhurDataListener listener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                temporaryClient.getKhurData(params, new KhurDataListener() {
                    @Override
                    public void onSuccess(KhurItems var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        listener.onError(var1);
                    }
                });
            }

            @Override
            public void onError(Response var1) {
                listener.onError(var1);
            }
        });

    }

    @Override
    public void setBlockKhur(StatusListener statusListener, String uid) {
        HashMap<String, String> param = new HashMap<>();
        param.put("blockInfo", uid.toLowerCase());
        param.put("blockType", "3");
        param.put("blockChannel", "2");
        param.put("blockMsg", "Хурууны хээ баталгаажуулах оролдлого 3 удаа амжилтгүй болсон байна. Та 00:00 цагаас хойш дахин оролдоно уу.");
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                offlineClient.setBlock(new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse statusResponse) {
                        statusListener.onSuccess(statusResponse);
                    }

                    @Override
                    public void onError(Response response) {
                        statusListener.onError(response);
                    }
                }, param);
            }

            @Override
            public void onError(Response var1) {
                statusListener.onError(var1);
            }
        });
    }
}
