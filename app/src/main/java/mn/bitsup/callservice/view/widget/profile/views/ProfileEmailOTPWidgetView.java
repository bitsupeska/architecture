package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.Emails;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.OTPView;
import mn.bitsup.callservice.view.custom.OTPViewLisener;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.loan.view.LoanRelativeInformation;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.ProfilePayload;

public class ProfileEmailOTPWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private TextView textTitle;
    private OTPView otpViewEmail;
    private ProfilePayload payload;
    private int checkCount = 3;
    private int blockCount = 2;
    private String email = "";
    private ProgressDialog progressDialog;
    private Context context;
    private ProfileContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        this.payload = EventHelper.getEventPayload("getEmailData", ProfilePayload.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_email_otp, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        otpViewEmail = inflatedView.findViewById(R.id.otpViewEmail);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initViews() {
        otpViewEmail.setMessage("0 секунд \n Имэйл ирээгүй юу? Энд дар");
        otpViewEmail.startTimer(180000);
        email = payload.getEmailId();
        textTitle.setText(email + " И-мэйл хаяг руу илгээгдсэн 4 оронтой кодыг оруулна уу");
        otpViewEmail.setLinkOnClickListener(v -> sendRequest());
        OTPFilledListener otpListener = new OTPFilledListener();
        otpViewEmail.setFilledListener(otpListener);
    }

    public class OTPFilledListener implements OTPViewLisener {
        @Override
        public void onSuccess(String code) {
            if (checkCount != 0) {
                checkEmailOTP(code);
            }
        }
    }

    private void sendRequest() {
        if (blockCount != 0) {
            checkCount = 3;
            otpViewEmail.stopTimer();
            otpViewEmail.hideErrorLabel();

            HashMap<String, String> additions = new HashMap<>();
            additions.put("email", email);
            progressDialog.show();
            contract.requestEmailOTP(new OTPListener() {
                @Override
                public void onSuccess(StatusResponse otp) {
                    progressDialog.dismiss();
                    if (otp.getStatus().equals("SUCCESS")) {
                        otpViewEmail.startTimer(180000);
                        blockCount--;
                    }
                }

                @Override
                public void onError(Response var1) {
                    progressDialog.dismiss();
                    CustomAlertDialog.showServerAlertDialog(context);
                }

            }, additions);
        } else {
            otpViewEmail.hideMessage();
            new AnimationDialog(context, "", context.getResources().getString(R.string.otp_error_email), "Буцах", "anim_rejection_clock.json").show();
        }
    }

    private void checkEmailOTP(String code) {
        progressDialog.show();
        HashMap<String, String> additions = new HashMap<>();
        additions.put("email", email);
        additions.put("otp", code);

        contract.checkEmailOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                checkCount--;
                if (otp.getStatus().equals("SUCCESS")) {
                    setEmail();
                } else {
                    progressDialog.dismiss();
                    if (checkCount == 0) {
                        otpViewEmail.stopTimer();
                        otpViewEmail.setLabel();
                        otpViewEmail.showErrorLabel("3 удаа буруу оруулсан тул та дахин код илгээнэ үү");
                        checkBlockIncrement();
                    } else {
                        otpViewEmail.showErrorLabel("Нууц код буруу байна. Танд " + checkCount + " удаагийн оролдлого үлдлээ.\n");
                    }
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
                otpViewEmail.emptyLabel();
            }
        }, additions);
    }

    private void setEmail() {
        HashMap<String, String> param = new HashMap<>();
        param.put("cifId", contract.getUser().getCif());
        param.put("emailId", payload.getEmailId().toLowerCase());
        param.put("emailStatus", "1");
        param.put("emailType", "Work");

        contract.setEmail(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                if (statusResponse.getStatus().equals("SUCCESS")) {
                    User user = contract.getUser();
                    List<Emails> emails = new ArrayList();
                    Emails email = user.getEmails().get(0);
                    email.setEmailId(payload.getEmailId().toLowerCase());
                    email.setEmailStatus("1");
                    emails.add(email);
                    user.setEmails(emails);
                    MainApplication.setUser(user);
                    KeyboardUtils.hideKeyboard(context, otpViewEmail);
                    otpViewEmail.hideKeyboard();
                    otpViewEmail.stopTimer();
                    updateDataFinish("success", context.getResources().getString(R.string.setup_completed_title), context.getResources().getString(R.string.profile_information_edit_email_success));
                } else {
                    updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), statusResponse.getMessage());
                }
            }

            @Override
            public void onError(Response response) {
                updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), context.getResources().getString(R.string.shared_alert_generic_error_server));
            }
        }, param);
        otpViewEmail.hideKeyboard();
    }

    private void updateDataFinish(String result, String resultTitle, String resultMessage) {
        progressDialog.dismiss();
        if (payload.getPublishType() != null && payload.getPublishType().equals("Loan")) {
            EventHelper.publishBack(context, new LoanRelativeInformation());
        } else {
            Log.e("updateDataFinish", "Profile");
            Activity activity = ((Activity) Objects.requireNonNull(getContext()));
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", result);
            returnIntent.putExtra("resultTitle", resultTitle);
            returnIntent.putExtra("resultMessage", resultMessage);
            activity.setResult(Activity.RESULT_FIRST_USER, returnIntent);
            activity.finish();
            ProfileInfoWidgetView.cellEmail.setValueText(payload.getEmailId().toLowerCase());
            ((Activity) context).finish();
        }
    }

    private void checkBlockIncrement() {
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                Log.e("checkBlockIncrement", "onSuccess" + blockIncrement);
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(context);
            }
        }, "email");
    }

}
