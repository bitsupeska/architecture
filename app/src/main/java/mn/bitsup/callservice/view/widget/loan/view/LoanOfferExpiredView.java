package mn.bitsup.callservice.view.widget.loan.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanAccount;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.dto.LoanReasonItem;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.ResourcesUtil;
import mn.bitsup.callservice.view.activity.MainActivity;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.adapter.AccountListAdapter;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class LoanOfferExpiredView extends Fragment implements
        OnboardView<OnboardContract> {

    private CustomButton buttonContinue;
    private Context context;
    private LoanContract contract;
    private User user;
    private TextView textFinalLimit, textAlertTitle, textAlertMessage, textTotalAmount, textRepaymentAmount;
    private LoanOffer loanOffer;
    private LinearLayout viewAlert;
    private LinearLayout container;
    private ImageView imageAlert;
    private ListView listRepayment;
    private WidgetListView widgetListView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SwipeRefreshListener swipeRefreshListener = new SwipeRefreshListener();

    public LoanOfferExpiredView(LoanOffer loanOffer) {
        this.loanOffer = loanOffer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.user = MainApplication.getUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_offer_expired_didsign, container, false);
        inflatedView(inflatedView);
        initView();
        setLoanOffer();
        getLoanAccount();
        return inflatedView;
    }

    private void setLoanOffer() {
        textFinalLimit.setText("₮" + 0);
    }


    private void inflatedView(View inflatedView) {
        swipeRefreshLayout = inflatedView.findViewById(R.id.swipeRefreshLayout);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        textFinalLimit = inflatedView.findViewById(R.id.textFinalLimit);
        container = inflatedView.findViewById(R.id.container);
        viewAlert = inflatedView.findViewById(R.id.viewAlert);
        textAlertTitle = inflatedView.findViewById(R.id.textAlertTitle);
        textAlertMessage = inflatedView.findViewById(R.id.textAlertMessage);
        textTotalAmount = inflatedView.findViewById(R.id.textTotalAmount);
        textRepaymentAmount = inflatedView.findViewById(R.id.textRepaymentAmount);
        imageAlert = inflatedView.findViewById(R.id.imageAlert);
        listRepayment = inflatedView.findViewById(R.id.listRepayment);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
    }

    private void initView() {
        //swipeRefreshLayout.setColorSchemeResources(new int[]{R.color.wl_progress});
        swipeRefreshLayout.setOnRefreshListener(this.swipeRefreshListener);
        if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        StringBuilder reason = new StringBuilder();
        for (LoanReasonItem loanReasonItem : loanOffer.getData().getRejectReasons()) {
            reason.append(loanReasonItem.getReason_desc()).append("\n");
        }
        textAlertTitle.setText("Зээлийн эрх");
        textAlertMessage.setText("Таны өмнөх зээлийн эрхийн хүчинтэй хугацаа дууссан байна.");
        viewAlert.getBackground().setColorFilter(Color.parseColor("#F1BE3C"), PorterDuff.Mode.SRC_ATOP);

        if (loanOffer != null) {
            setFinalLimit();
        }

        buttonContinue.setOnClickListener(view -> {
            EventHelper.publishBack(context, new LoanFingerView());
        });
    }

    private void getLoanAccount() {
        Map<String, String> params = new HashMap<>();
        params.put("cifId", user.getCif());

        contract.getLoanAccount(params, new LoanAccountListener() {
            @Override
            public void onSuccess(LoanAccount response) {
                screenEnabled(true);
                if (response.getCode() == 200) {
                    if (response.getData() != null && response.getData().size() > 0) {
                        setTotalAmount(response.getData());
                        setRepaymentAmount(response.getData());
                        setRepaymentAdapter(response.getData());
                        container.setVisibility(View.VISIBLE);
                        buttonContinue.setVisibility(View.VISIBLE);
                        widgetListView.hideProgress();
                    }
                    container.setVisibility(View.VISIBLE);
                    buttonContinue.setVisibility(View.VISIBLE);
                    widgetListView.hideProgress();
                } else {
                    widgetListView.hideProgress();
                    CustomAlertDialog.showAlertDialog(context, "", response.getMessage());
                }
            }

            @Override
            public void onError(Response response) {
                screenEnabled(true);
                widgetListView.hideProgress();
                CustomAlertDialog.showAlertDialog(context, "", "Зээлийн мэдээлэл татах үед алдаа гарлаа");
            }
        });
    }

    private void setRepaymentAdapter(List<LoanAccountData> loans) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                ResourcesUtil.dpToPx(115 * loans.size(), context.getResources()));
        listRepayment.setLayoutParams(params);
        AccountListAdapter adapter = new AccountListAdapter(context, loans);
        listRepayment.setAdapter(adapter);
    }

    @SuppressLint("SetTextI18n")
    private void setTotalAmount(List<LoanAccountData> loans) {
        double total = 0;
        for (int i = 0; i < loans.size(); i++) {
            if (loans.get(i).getLoanAmt() != null && !loans.get(i).getLoanAmt().equals("")) {
                float amount = Float.parseFloat(loans.get(i).getLoanAmt());
                total = total + amount;
            }
        }
        textTotalAmount.setText("₮" + CurrencyConverter.getFormattedCurrencyString(total));
    }

    @SuppressLint("SetTextI18n")
    private void setRepaymentAmount(List<LoanAccountData> loans) {
        double total = 0;
        for (int i = 0; i < loans.size(); i++) {
            if (loans.get(i).getThisMonthPayment() != null && !loans.get(i).getLoanAmt().equals("")) {
                double thisMonthPayment = Double.parseDouble(loans.get(i).getThisMonthPayment());
                if (thisMonthPayment != 0) {
                    total = total + thisMonthPayment;
                } else {
                    total = total + Double.parseDouble(loans.get(i).getNextMonthPayment());
                }
            }
        }
        textRepaymentAmount.setText("₮" + CurrencyConverter.getFormattedCurrencyString(total));
    }

    private void setFinalLimit() {
        double finalLimit = loanOffer.getData().getFinal_limit();
        if (finalLimit > 0) {
            String finalLimitText = CurrencyConverter.getFormattedCurrencyString(finalLimit);
            textFinalLimit.setText(finalLimitText);
        }
    }

    private class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {
        private SwipeRefreshListener() {
        }

        public void onRefresh() {
            Log.e("dalai", "onRefresh: refreshed");
            EventHelper.publish(getContext(), new LoanLastOfferView());
        }
    }

    private void screenEnabled(boolean enable) {
        if (enable)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        for (int i = 0; i < 3; i++)
            MainActivity.bottomNavigation.getMenu().getItem(i).setEnabled(enable); //navigation
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                EventHelper.publish(getContext(), new LoanLastOfferView(), "loanSuccess", "true");
            } else {
                EventHelper.publish(getContext(), new LoanLastOfferView());
            }
        }
    }
}