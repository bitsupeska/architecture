package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardContractDescActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

import static mn.bitsup.callservice.MainApplication.getUser;

public class TPContractWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private OnboardContract contract;
    private Context context;
    private TextView textDate, textContactInput;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy оны MM сарын dd");
    private CustomButton buttonContinue;
    private ListView listView;
    private ListAdapter adapter;
    private CheckBox checkBoxConfirm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(getContext());
    }

    @Nullable

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_contract, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textDate = inflatedView.findViewById(R.id.textDate);
        textContactInput = inflatedView.findViewById(R.id.textContactInput);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        listView = inflatedView.findViewById(R.id.listView);
        checkBoxConfirm = inflatedView.findViewById(R.id.checkBoxConfirm);
    }

    private void initView() {
        LoanActivity.fillProgress();
        setListData();
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        textDate.setText(df.format(date));

        User user = MainApplication.getUser();

        String lastname = user.getLastName();
        String firstname = user.getFirstName();
        String register = user.getRegNum();

        textContactInput.setText(Html.fromHtml(
                "Нэг талаас  АШИД КАПИТАЛ ББСБ (цаашид “ББСБ” гэх)-, Нөгөө талаас " + "<font color='#11274B'><b>"
                        + lastname + "</b></font>" + " овогтой " + "<font color='#11274B'><b>"
                        + firstname + "</b></font>" + " (Регистрийн дугаар " + "<font color='#11274B'><b>"
                        + register + "</b></font>"
                        + "  цаашид “Харилцагч” гэх) нар дор дурдсан нөхцөлүүдийг харилцан тохиролцож энэхүү Гэрээг (цаашид “Гэрээ” гэх) байгуулав."));

        adapter = new ListAdapter();
        listView.setAdapter(adapter);

        buttonContinue.setState(ButtonState.DISABLED, getActivity());
    }

    private void initEvent() {
        buttonContinue.setOnClickListener(v -> createCA());
        checkBoxConfirm.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonContinue.isLoading()) {
                if (isChecked) {
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                } else {
                    buttonContinue.setState(ButtonState.DISABLED, getActivity());
                }
            }
        });
    }


    private void createCA() {
        String uid = ClientsManager.authClient.getUid();
        Temporary payload = contract.getTemporary();

        payload.setCurrCode("MNT");
        payload.setProdCode("CA101");
        payload.setDidSign("1");
        payload.setFreezeCode("T");
        payload.setReasonCode("10");
        payload.setRemarks("Тайлбар");
        payload.setUserName(uid);
        payload.setEducationName("Default school name");

        User user = MainApplication.getUser();

        if (user.getEmails().size() > 0) {
            if (!user.getEmails().get(0).getEmailId().equals("")) {
                payload.setEmail(user.getEmails().get(0).getEmailId());
                payload.setEmailStatus(user.getEmails().get(0).getEmailStatus());

            } else {
                payload.setEmail("");
                payload.setEmailStatus("0");
            }
        }

        buttonContinue.setState(ButtonState.LOADING, getActivity());
        contract.createCA(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                if (statusResponse.getCode().equals("200")) {
                    User user = getUser();

                    List<Addresses> addresses = new ArrayList();

                    Addresses homeAddress = new Addresses();
                    homeAddress.setTown(payload.getTownHomeName() == null ? "" : payload.getTownHomeName());
                    homeAddress.setTownId(payload.getTownHome() == null ? "" : payload.getTownHome());
                    homeAddress.setLocality(payload.getLocalityHomeName() == null ? "" : payload.getLocalityHomeName());
                    homeAddress.setLocalityId(payload.getLocalityHome() == null ? "" : payload.getLocalityHome());
                    homeAddress.setSection(payload.getSectionHome() == null ? "" : payload.getSectionHome());
                    homeAddress.setDistrict(payload.getDistrictHome() == null ? "" : payload.getDistrictHome());
                    homeAddress.setApartment(payload.getApartmentHome() == null ? "" : payload.getApartmentHome());
                    homeAddress.setDoorNumber(payload.getDoorNumberHome() == null ? "" : payload.getDoorNumberHome());
                    homeAddress.setAddressType("Home");


                    Log.e("TPContractWidgetView", "onSuccess: " + homeAddress);
                    addresses.add(homeAddress);

                    Addresses mailAddress = null;

                    if (user.getAddresses() != null) {
                        for (int i = 0; i < user.getAddresses().size(); i++) {
                            if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                                mailAddress = user.getAddresses().get(i);
                            }
                        }
                    }
                    addresses.add(mailAddress);
                    user.setAddresses(addresses);
                    user.setDidSign("1");
                    MainApplication.setUser(user);

                    new AnimationDialog(getContext(), "Гарын үсэг хүлээгдэж байна,",
                            "Та салбар дээр очиж гарын үсгээ\n" +
                                    "өгснөөр зээл авах боломжтой болно.",
                            "Буцах", "success_anim.json",
                            this::finish
                    ).show();
                } else {
                    CustomAlertDialog.showAlertDialog(context, "Алдаа гарлаа", statusResponse.getMessage());
                }
            }

            private void finish() {
                getActivity().setResult(Activity.RESULT_FIRST_USER);
                getActivity().finish();
            }

            @Override
            public void onError(Response response) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }


    private ArrayList<String> titles;
    private List<Integer> body;

    private void setListData() {
        titles = new ArrayList<>();
        titles.add("Ерөнхий зүйл");
        titles.add("Бүтээгдэхүүн үйлчилгээ үзүүлэх үндсэн нөхцөл");
        titles.add("Банкны эрх, үүрэг");
        titles.add("Харилцагчийн эрх, үүрэг");
        titles.add("Маргаан шийдвэрлэх");
        titles.add("Данс хаах, гэрээ цуцлах, дуусгавар болох");
        titles.add("Хариуцлага");
        titles.add("Бусад зүйл");

        body = new ArrayList<>();
        body.add(R.raw.contract1);
        body.add(R.raw.contract2);
        body.add(R.raw.contract3);
        body.add(R.raw.contract4);
        body.add(R.raw.contract5);
        body.add(R.raw.contract6);
        body.add(R.raw.contract7);
        body.add(R.raw.contract8);

    }

    private String getRawData(int body) throws IOException {
        Resources res = this.context.getResources();
        InputStream in_s = res.openRawResource(body);
        byte[] b = new byte[in_s.available()];
        in_s.read(b);
        return new String(b);
    }

    public class ListAdapter extends BaseAdapter {
        public ListAdapter() {
        }

        @Override
        public int getCount() {
            return titles == null ? 0 : titles.size();
        }

        @Override
        public Object getItem(int position) {
            return titles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.onboard_contract_list_item, null);

            RelativeLayout containerItem = view.findViewById(R.id.containerItem);
            TextView textTitle = view.findViewById(R.id.textTitle);

            textTitle.setText((position + 1) + ". " + titles.get(position));
            containerItem.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), OnboardContractDescActivity.class);
                intent.putExtra("title", (position + 1) + ". " + titles.get(position));
                try {
                    intent.putExtra("body", getRawData(body.get(position)));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                getContext().startActivity(intent);
            });
            return view;
        }
    }
}
