package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.util.StyledAttributesHelper;

/**
 * Created by Erdenedalai R&D B.V on 20/03/2018.
 * DataCell view to be used in several screens.
 * <p>
 * Has several declared styleables:
 * headerText: sets the text of the header field. Defaults to empty if not set (which will hide the view).
 * valueText: sets the text of the value field. Defaults to empty if not set (which will hide the view).
 * bottomBorder: sets border bottom visible or not. Defaults to true if not set.
 * topBorder: sets border top visible or not. Defaults to false if not set.
 * </p>
 */

public class DataCell extends ConstraintLayout {

    private TextView headerTextView;
    private TextView valueTextView;
    private ImageView iconImageView;
    private View iconImageViewSpacer;
    private View borderTop;
    private View borderBottom;
    private boolean hideValueTextWhenEmpty = true;

    public DataCell(Context context) {
        this(context, null);
    }

    public DataCell(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Default behavior of this view is to hide the value text when it is empty.
     * This behavior can be disabled/re-enabled using this method.
     *
     * @param hideWhenEmpty
     */
    public void setHideValueTextWhenEmpty(boolean hideWhenEmpty) {
        this.hideValueTextWhenEmpty = hideWhenEmpty;
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        View inflatedView = inflate(context, R.layout.view_data_cell, this);
        headerTextView = inflatedView.findViewById(R.id.textHeader);
        valueTextView = inflatedView.findViewById(R.id.textValue);
        iconImageView = inflatedView.findViewById(R.id.iconImageView);
        iconImageViewSpacer = inflatedView.findViewById(R.id.iconImageViewSpacer);
        borderTop = inflatedView.findViewById(R.id.topBorder);
        borderBottom = inflatedView.findViewById(R.id.bottomBorder);
        initValues(context, attrs);
    }

    private void initValues(Context context, @Nullable AttributeSet attrs) {
        if (attrs == null) {
            borderTop.setVisibility(INVISIBLE);
            borderBottom.setVisibility(INVISIBLE);
            return;
        }

        TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.DataCell, 0, 0);
        String headerText = StyledAttributesHelper.getStyleableAttributeString(styledAttributes, R.styleable.DataCell_headerText, "-");
        String valueText = StyledAttributesHelper.getStyleableAttributeString(styledAttributes, R.styleable.DataCell_valueText, "-");
        int iconResId = styledAttributes.getResourceId(R.styleable.DataCell_iconRes, -1);
        boolean showTopBorder = styledAttributes.getBoolean(R.styleable.DataCell_topBorder, false);
        boolean showBottomBorder = styledAttributes.getBoolean(R.styleable.DataCell_bottomBorder, true);
        styledAttributes.recycle();

        setTextViewValue(headerTextView, headerText);
        setTextViewValue(valueTextView, valueText);
        borderTop.setVisibility(showTopBorder ? VISIBLE : INVISIBLE);
        borderBottom.setVisibility(showBottomBorder ? VISIBLE : INVISIBLE);
        if (iconResId > 0) {
            setIcon(iconResId);
        }
    }

    private void setTextViewValue(TextView textView, String value) {
        if ((value == null || value.isEmpty()) && hideValueTextWhenEmpty) {
            textView.setVisibility(GONE);
        } else {
            textView.setVisibility(VISIBLE);
            textView.setText(value);
        }
    }

    private void setTextViewValue(TextView textView, SpannableStringBuilder value) {
        if ((value == null || value.length() == 0) && hideValueTextWhenEmpty) {
            textView.setVisibility(GONE);
        } else {
            textView.setVisibility(VISIBLE);
            textView.setText(value);
        }
    }

    public void setHeaderText(String text) {
        setTextViewValue(headerTextView, text);
    }

    public void setValueText(String text) {
        setTextViewValue(valueTextView, text);
    }

    public void setValueText(SpannableStringBuilder text) {
        setTextViewValue(valueTextView, text);
    }

    public void setIcon(int resourceId) {
        enableIconImageView().setImageResource(resourceId);
    }

    /**
     * Shows the iconImageView and the iconImageViewSpacer
     *
     * @return the IconImageView to change to apply any image to.
     */
    public ImageView enableIconImageView() {
        iconImageView.setVisibility(VISIBLE);
        iconImageViewSpacer.setVisibility(VISIBLE);
        return iconImageView;
    }

    public ImageView getIconImageView() {
        return iconImageView;
    }

    public String getHeaderText() {
        return headerTextView.getText().toString();
    }

    public String getValueText() {
        return valueTextView.getText().toString();
    }

    public void setValueTextBold(){
        Typeface typeface = ResourcesCompat.getFont(getContext(), R.font.medium);
        valueTextView.setTypeface(typeface);
    }

    /**
     * @param showTopBorder
     */
    public void showTopBorder(boolean showTopBorder) {
        borderTop.setVisibility(showTopBorder ? VISIBLE : INVISIBLE);
    }

    /**
     * @param showBottomBorder
     */
    public void showBottomBorder(boolean showBottomBorder) {
        borderBottom.setVisibility(showBottomBorder ? VISIBLE : INVISIBLE);
    }
}
