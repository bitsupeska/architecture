package mn.bitsup.callservice.view.widget.card.core.contract;

import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;
import mn.bitsup.callservice.client.card.listener.CardsListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.util.CustomParams;
import mn.bitsup.callservice.view.widget.card.dto.UiCardPayload;

public interface CardContract {

    void checkBlock(BlockListener listener, HashMap<String, String> param);

    void onCardActivationStart(@NonNull UiCardPayload uiCardPayload);

    void getCardList(@NonNull CustomParams var1, @NonNull CardsListener var2);

    void changeCardStatus(StatusListener var1, Map<String, String> var2);

    void onCardReplacementStart(@NonNull UiCardPayload uiCardPayload);


}
