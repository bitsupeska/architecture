package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import java.util.concurrent.Executor;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.view.activity.GenericActivity;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class ProfilePasswordShowWidgetView extends Fragment implements ProfileView<ProfileContract>, CompoundButton.OnCheckedChangeListener {

    private TextView passwordChange, textSwitch;
    private Context context;
    private Switch switchFingerFace;
    private StorageComponent storageComponent;
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private boolean ifChecked;
    private int count = 1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.storageComponent = new StorageComponent();

        executor = ContextCompat.getMainExecutor(context);
        biometricPrompt = new BiometricPrompt(getActivity(),
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                if (errString.equals("Too many attempts. Try again later."))
                    errString = "Хэт олон оролдлого бүртгэгдлээ. Дараа дахин оролдоорой.";
                else if (errString.equals("Too many attempts. Fingerprint sensor disabled."))
                    errString = "Хэт олон оролдсон тул, Хурууны хээ/ Face ID-г идэвхгүй болгосон байна.";
                NudgeControllerUtil.showErrorNotification(context.getResources().getString(R.string.profile_password_biometrics_fail), errString.toString(), context);
                if (ifChecked) {
                    switchFingerFace.setChecked(false);
                } else {
                    switchFingerFace.setChecked(true);
                }
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                NudgeControllerUtil.showSuccessNotification(context.getResources().getString(R.string.setup_completed_title), " Хурууны хээ/ Face ID амжилттай тохирууллаа.", context);
                if (ifChecked) {
                    storageComponent.setItem("bb.key.is_finger", "true");
                    switchFingerFace.setChecked(true);
                } else {
                    storageComponent.setItem("bb.key.is_finger", "false");
                    switchFingerFace.setChecked(false);
                }
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_password_menu, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        passwordChange = inflatedView.findViewById(R.id.passwordChange);
        textSwitch = inflatedView.findViewById(R.id.textSwitch);
        switchFingerFace = inflatedView.findViewById(R.id.switchFingerFace);
    }

    private void initViews() {
        passwordChange.setOnClickListener(v -> publish());
        switchFingerFace.setOnCheckedChangeListener(this);
        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Хурууны хээ/ Face ID-р нэвтрэх")
                .setSubtitle("")
                .setNegativeButtonText("Буцах")
                .build();
        setSwitcher();
        count = 0;
    }

    private void setSwitcher() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            if(!storageComponent.getItem("bb.key.is_finger").isEmpty()){
                String isFinger = storageComponent.getItem("bb.key.is_finger");
                if (isFinger.equals("true")) {
                    switchFingerFace.setChecked(true);
                } else {
                    switchFingerFace.setChecked(false);
                }
            }
        }
    }

    private void throwToSettings() {
        switchFingerFace.setChecked(false);
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
        alertdialog.setMessage("Таны төхөөрөмж дээр хурууны хээ/нүүр царай бүртгэгдээгүй байна.").setCancelable(false).setPositiveButton("Бүртгүүлэх", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(android.provider.Settings.ACTION_SECURITY_SETTINGS));
                dialog.cancel();
            }
        }).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertdialog.create();
        alert.setTitle("Бүртгэгдээгүй");
        alert.show();
    }

    private void checkOSAndHardware() {
        BiometricManager biometricManager = BiometricManager.from(context);
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                try {
                    biometricPrompt.authenticate(promptInfo);
                } catch (Exception e) {
                    Log.e("uno", "checkFingerPrint: " + "Error");
                }
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Toast.makeText(context, "Таны төхөөрөмж хурууны хээ/ нүүр царай баталгаажуулалтыг дэмжихгүй байна.", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Toast.makeText(context, "Таны төхөөрөмж хурууны хээ/ нүүр царай баталгаажуулалт одоогоор боломжгүй байна.", Toast.LENGTH_SHORT).show();
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                throwToSettings();
                break;
        }
    }

    private void setIsFinger(boolean checked) {
        if (checked) {
            if (count != 1) {
                checkOSAndHardware();
                ifChecked = true;
            }
        } else {
            checkOSAndHardware();
            ifChecked = false;
        }
        count = 0;
    }

    private void publish() {
        GenericActivity.fragment = new ProfilePasswordNewWidgetView();
        Intent intent = new Intent(context, GenericActivity.class);
        intent.putExtra(PAGE_PREFERENCE_TITLE, context.getResources().getString(R.string.profile_password_change));
        startActivityForResult(intent, 1);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        setIsFinger(isChecked);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_FIRST_USER) {
                String result = data.getStringExtra("result");
                String resultTitle = data.getStringExtra("resultTitle");
                String resultMessage = data.getStringExtra("resultMessage");
                if (result.equals("success"))
                    NudgeControllerUtil.showSuccessNotification(resultTitle, resultMessage, context);
                else NudgeControllerUtil.showErrorNotification(resultTitle, resultMessage, context);
            }
        }
    }


}
