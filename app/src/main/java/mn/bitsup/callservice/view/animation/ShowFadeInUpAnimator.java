package mn.bitsup.callservice.view.animation;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 18/09/2017.
 */

public class ShowFadeInUpAnimator {

    private ShowFadeInUpAnimator() {
        //Hide constructor
    }

    public static void animate(final View viewToAnimate, long offsetDelay, long duration, Context context) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fade_in_up);
        animation.setStartOffset(offsetDelay);
        animation.setDuration(duration);
        viewToAnimate.startAnimation(animation);
    }
}
