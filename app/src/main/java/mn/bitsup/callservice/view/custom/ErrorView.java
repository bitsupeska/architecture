package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.Nullable;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.util.ResponseCodes;
import mn.bitsup.callservice.client.dataprovider.response.Response;


/**
 * Created by Erdenedalai R&D B.V on 19/09/2017.
 * View to use to show error in the content container.
 */

public class ErrorView extends LinearLayout {

    private static final String TAG = ErrorView.class.getSimpleName();
    protected TextView textErrorTitle;
    protected TextView textErrorMessage;
    protected ImageView icon;
    protected CustomButton retryButton;
    @Nullable
    protected ErrorViewListener listener;

    public ErrorView(Context context) {
        this(context, null);
    }

    public ErrorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        View parentView = inflate(getContext(), R.layout.error_view, this);

        this.textErrorTitle = parentView.findViewById(R.id.textErrorTitle);
        this.textErrorMessage = parentView.findViewById(R.id.textErrorMessage);
        this.icon = parentView.findViewById(R.id.iconImageView);
        this.retryButton = parentView.findViewById(R.id.retryButton);

        setVisibility(GONE);
    }

    public void setErrorViewListener(ErrorViewListener errorViewListener) {
        listener = errorViewListener;
        setRetryButtonListener();
    }

    private void setRetryButtonListener() {
        if (listener != null) {
            retryButton.setOnClickListener(v -> listener.onRetryClicked());
        }
    }

    /**
     * Populate ErrorView by ErrorResponse and set visibility to visible.
     *
     * @param errorResponse ErrorResponse to use to populate the ErrorView with the right values.
     * @param viewType view type to use resources for
     */
    public void show(Response errorResponse, ErrorViewType viewType) {
        int responseCode = errorResponse.getResponseCode();
        if (ResponseCodes.NO_INTERNET.getCode() == responseCode) {
            show(ErrorType.NO_INTERNET, viewType);
        } else {
            show(ErrorType.ERROR, viewType);
        }
    }

    /**
     * Populate ErrorView by ErrorType and set visibility to visible.
     *
     * @param errorType ErrorType to use to populate the ErrorView with the right values.
     * @param viewType view type to use resources for
     */
    public void show(ErrorType errorType, ErrorViewType viewType) {
        if (errorType == null) {
            hide();
            return;
        }
        switch (errorType) {
            case NONE:
                hide();
                break;
            case NO_INTERNET:
                show(viewType.getNoInternetTitle(),
                        viewType.getNoInternetMessage(),
                        viewType.getNoInternetDrawable()
                    );
                break;
            case NO_SEARCH_RESULTS:
                show(viewType.getNoSearchResultsTitle(),
                        viewType.getNoSearchResultsMessage(),
                        viewType.getNoSearchResultsDrawable());
                break;
            case EMPTY:
                show(viewType.getEmptyResultsTitle(),
                        viewType.getEmptyResultsMessage(),
                        viewType.getEmptyResultsDrawable());
                break;
            case ERROR:
            default:
                show(viewType.getErrorTitle(),
                        viewType.getErrorMessage(),
                        viewType.getErrorDrawable()
                        );
        }
    }

    /**
     * Show error view with generic ic_error_warning icon.
     *
     * @param titleResId   resource id of the title to show
     * @param messageResId resource id of the message to show
     */
    public void show(int titleResId, int messageResId) {
        show(titleResId, messageResId, R.drawable.ic_error_warning);
    }

    /**
     * Convenience method for {@link ErrorView#show(int, int, int, boolean)} with retryButtonVisible set to false.
     * @param titleResId   resource id of the title to show
     * @param messageResId resource id of the message to show
     * @param imageResId   resource id of the image to show
     */
    public void show(int titleResId, int messageResId, int imageResId) {
        show(titleResId, messageResId, imageResId, false);
    }

    /**
     * Show error view with values.
     *
     * @param titleResId   resource id of the title to show
     * @param messageResId resource id of the message to show
     * @param imageResId   resource id of the image to show
     * @param retryButtonVisible true when the retry button should be visible
     *                           false otherwise
     */
    public void show(int titleResId, int messageResId, int imageResId, boolean retryButtonVisible) {
        try {
            textErrorTitle.setText(titleResId);
            textErrorMessage.setText(messageResId);
            icon.setImageResource(imageResId);
            retryButton.setVisibility(retryButtonVisible ? VISIBLE : GONE);
        } catch (Resources.NotFoundException e) {
        }
        setVisibility(VISIBLE);
    }

    /**
     * Hides the error view.
     */
    public void hide() {
        setVisibility(GONE);
    }

    public interface ErrorViewListener {

        void onRetryClicked();
    }

    public enum ErrorType {
        NO_INTERNET,
        ERROR,
        NO_SEARCH_RESULTS,
        EMPTY,
        NONE
    }
}
