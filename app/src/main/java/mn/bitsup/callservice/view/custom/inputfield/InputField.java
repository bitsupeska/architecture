package mn.bitsup.callservice.view.custom.inputfield;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Constraints;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.view.util.StyledAttributesHelper;

/**
 * Created by Backbase R&D B.V on 21/06/2018.
 * Custom implementation of an input field using TextInputLayout.
 */
public class InputField extends ConstraintLayout {
    protected ImageButton rightButton;
    protected ImageButton cancelButton;
    protected ImageView iconImageView;
    protected TextInputLayout textInputLayout;
    protected String hintText;
    protected TextInputEditText editText;
    private TextWatcher rightButtonsVisibilityTextWatcher;

    @DrawableRes
    private int iconResId;

    @DrawableRes
    private int rightButtonIconResId;

    @ColorInt
    private int rightButtonIconTint;

    private int editTextOriginalPaddingRight;

    public InputField(Context context) {
        this(context, null);
    }

    public InputField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    @SuppressLint("ResourceType")
    protected void init(Context context, AttributeSet attrs) {
        View inflatedView = inflate(context, R.layout.view_input_field_value, this);
        iconImageView = inflatedView.findViewById(R.id.iconImageView);
        editText = inflatedView.findViewById(R.id.textInputEditText);
        textInputLayout = inflatedView.findViewById(R.id.textInputLayout);
        setTextAppearance();

        if (attrs != null) {
            TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.InputField, 0, 0);
            hintText = StyledAttributesHelper.getStyleableAttributeString(styledAttributes, R.styleable.InputField_inputFieldIconHint, "");
            iconResId = styledAttributes.getResourceId(R.styleable.InputField_inputFieldIconRes, -1);
            rightButtonIconResId = styledAttributes.getResourceId(R.styleable.InputField_inputFieldRightButtonIconRes, -1);
            rightButtonIconTint = styledAttributes.getColor(R.styleable.InputField_inputFieldRightButtonIconTint,
                    getContext().getResources().getColor(R.color.primary));
            styledAttributes.recycle();
        }

        textInputLayout.setHint(hintText);
        if (iconResId > 0) {
            iconImageView.setImageResource(iconResId);
            iconImageView.setVisibility(VISIBLE);
        }else{
            iconImageView.setVisibility(GONE);
        }

        if (rightButtonIconResId > 0) {
            setRightButton(rightButtonIconResId, rightButtonIconTint);
        }

        rightButtonsVisibilityTextWatcher = new RightButtonsVisibilityTextWatcher();
        editText.addTextChangedListener(rightButtonsVisibilityTextWatcher);
        editTextOriginalPaddingRight = editText.getPaddingRight();
//        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

        setOnFocusChangeListener((v, hasFocus) -> updateRightButtonsVisibility());
    }



    protected void setTextAppearance() {
        editText.setTextAppearance(getContext(), R.style.Body1Medium);
    }

    public void setTextColor(int color){
        editText.setTextColor(color);
    }
    public void setCancelButtonDuringEditing(@Nullable final OnClickListener onClickListener) {
        if (hasCancelButton()) {
            return;
        }
        getCancelButton().setImageResource(R.drawable.ic_cancel);
        getCancelButton().setColorFilter(getContext().getResources().getColor(R.color.text_grey));
        getCancelButton().setOnClickListener(v -> {
            editText.setText("");
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
        updateRightButtonsVisibility();
    }

    public void setCancelButton() {
        if (hasCancelButton()) {
            return;
        }
        getCancelButton().setImageResource(R.drawable.ic_cancel);
        getCancelButton().setColorFilter(getContext().getResources().getColor(R.color.text_grey));
        getCancelButton().setOnClickListener(v ->  editText.setText(""));
        updateRightButtonsVisibility();
    }

    /**
     * Remove the cancel button.
     */
    public void removeCancelButton() {
        if (hasCancelButton()) {
            removeRightButtonInternal(cancelButton);
            cancelButton = null;
        }
        updateRightButtonsVisibility();
    }

    /**
     * Shows a button on the right of the input field, a listener can be provided
     * to receive the click event.
     * If there is a previously set right button, it gets replaced.
     * The button is always shown, regardless of whether or not the edit text contains some text or is empty.
     * If there is a cancel button enabled, the cancel button is shown when this view is focused.
     *
     * @param iconResId the res id of the icon to use.
     * @param color     the tint color for the icon.
     */
    public void setRightButton(int iconResId, int color) {
        removeRightButton();
        getRightButton().setImageResource(iconResId);
        getRightButton().setColorFilter(color);
        updateRightButtonsVisibility();
    }


    /**
     * Set the {@link OnClickListener} for the right button.
     * If the right button is not present, or configured as cancel button, this method does nothing.
     *
     * @param onClickListener the listener, use null to remove the listener.
     */
    public void setRightButtonOnClickListener(@Nullable final OnClickListener onClickListener) {

        Log.e("rightButton", "Listener: "+hasRightButton());
        if (hasRightButton()) {
            getRightButton().setOnClickListener(onClickListener);
        }
    }

    /**
     * Remove a previously set right button, if no right button is present this method does nothing.
     */
    public void removeRightButton() {
        if (hasRightButton()) {
            removeRightButtonInternal(rightButton);
            rightButton = null;
        }
    }

    /**
     * Show an error message below the EditText.
     *
     * @param errorMessage the message to show, null to not show an error.
     */
    public void setError(@Nullable final String errorMessage) {
        if (errorMessage != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorMessage);
//            removeCancelButton();
        } else {
            textInputLayout.setErrorEnabled(false);
//            setCancelButton();
        }
    }

    private boolean hasRightButton() {
        return rightButton != null;
    }

    private boolean hasCancelButton() {
        return cancelButton != null;
    }

    private ImageButton getRightButton() {
        if (!hasRightButton()) {
            rightButton = createRightButtonView(R.id.input_field_right_button);
        }
        return rightButton;
    }

    private ImageButton getCancelButton() {
        if (!hasCancelButton()) {
            cancelButton = createRightButtonView(R.id.input_field_cancel_button);
        }
        return cancelButton;
    }

    private void removeRightButtonInternal(final View rightButton) {
        removeView(rightButton);
        editText.setPadding(editText.getPaddingLeft(), editText.getPaddingTop(),
                editTextOriginalPaddingRight, editText.getPaddingBottom());
    }

    private ImageButton createRightButtonView(int id) {
        final ImageButton imageButton = new ImageButton(getContext());
        imageButton.setBackgroundColor(Color.TRANSPARENT);
        imageButton.setId(id);
        imageButton.setPadding(0, 0, 0, 0);

        final int buttonSize = getContext().getResources().getDimensionPixelSize(R.dimen.margin_24dp);
        final ConstraintLayout.LayoutParams lp = new Constraints.LayoutParams(buttonSize, buttonSize);
        addView(imageButton, lp);

        final int buttonMargin = getContext().getResources().getDimensionPixelSize(R.dimen.margin_0dp);
        final int buttonMarginError = getContext().getResources().getDimensionPixelSize(R.dimen.margin_28dp);

        final ConstraintSet constraints = new ConstraintSet();
        constraints.clone(this);
        constraints.connect(imageButton.getId(), ConstraintSet.END, getId(), ConstraintSet.END, buttonMargin);
        if (!textInputLayout.getEditText().equals("")) {
            constraints.connect(imageButton.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP, buttonMarginError);
        }else{
            constraints.connect(imageButton.getId(), ConstraintSet.BOTTOM, getId(), ConstraintSet.BOTTOM, buttonMargin);
        }


        constraints.applyTo(this);

        editText.setPadding(editText.getPaddingLeft(), editText.getPaddingTop(),
                editTextOriginalPaddingRight + buttonSize, editText.getPaddingBottom());

        return imageButton;
    }

    public String getValue() {
        return editText.getText().toString();
    }

    @Override
    public void setEnabled(final boolean enabled) {
        editText.setEnabled(enabled);
    }

    @Override
    public void setOnClickListener(@Nullable final OnClickListener l) {
        editText.setOnClickListener(l);
    }

    public void addTextChangedListener(@NonNull final TextWatcher textWatcher) {
        editText.addTextChangedListener(textWatcher);
    }

    public void removeTextChangedListener(@NonNull final TextWatcher textWatcher) {
        editText.removeTextChangedListener(textWatcher);
    }

    /**
     * Set the text value avoiding an invocation to the specified {@link TextWatcher}
     *
     * @param text
     * @param textWatcher
     */
    public void setTextWithoutNotifyingTextWatcher(@NonNull final String text, @NonNull final TextWatcher textWatcher) {
        editText.removeTextChangedListener(textWatcher);
        editText.setText(text);
        editText.setSelection(text.length());
        editText.addTextChangedListener(textWatcher);
    }

    public void setText(@NonNull final String text) {
        editText.setText(text);
        editText.setSelection(text.length());
    }

    public CharSequence getText() {
        return editText.getText();
    }

    public void setSingleLine() {
        editText.setSingleLine();
    }

    public void setInputType(final int type) {
        editText.setInputType(type);
    }

    public void setFilters(InputFilter[] filters) {
        editText.setFilters(filters);
    }

    public void setSelection(int selection) {
        editText.setSelection(selection);
    }

    public void setImeOptions(int options) {
        editText.setImeOptions(options);
    }

    public void setOnEditorActionListener(final TextView.OnEditorActionListener listener) {
        editText.setOnEditorActionListener(listener);
    }

    @Override
    public void setFocusable(final boolean focusable) {
        editText.setFocusable(focusable);
        editText.setFocusableInTouchMode(focusable);
    }

    @Override
    public final void setOnFocusChangeListener(final OnFocusChangeListener listener) {
        editText.setOnFocusChangeListener((v, hasFocus) -> {
            updateRightButtonsVisibility();
            if (listener != null) {
                listener.onFocusChange(v, hasFocus);
            }
        });
    }

    public void setMaxInputLength(int length) {
        editText.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(length)});
    }

    public void setNextFocusForward(@IdRes int getNextFocusForwardId) {
        editText.setNextFocusForwardId(getNextFocusForwardId);
    }

    public void setDigits(String digits) {
        editText.setKeyListener(DigitsKeyListener.getInstance(digits));
    }

    /**
     * Sets the input field's floating label
     *
     * @param s the string to set the floating label to
     */
    public void setHintText(@Nullable String s) {
        hintText = s;
        textInputLayout.setHint(hintText);
    }

    /**
     * Sets the input field's icon
     *
     * @param iconResId the drawable's resource id
     */
    public void setIcon(@DrawableRes int iconResId) {
        this.iconResId = iconResId;
        iconImageView.setImageResource(iconResId);
        iconImageView.setVisibility(VISIBLE);
    }


    public void setIconColor(int color){
        iconImageView.setColorFilter(getContext().getResources().getColor(color));
    }

    /**
     * Hides the input field's icon
     */
    public void hideIcon() {
        iconImageView.setVisibility(GONE);
    }

    public void setPasswordType(){
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    }

    /**
     * Disable entering text and configure the view to just react to a click event.
     *
     * @param listener the click listener, use null to re enable entering text.
     */
    public void setClickableOnly(@Nullable final OnClickListener listener) {
        if (listener != null) {
            editText.setFocusableInTouchMode(false);
            editText.setClickable(true);
            editText.setOnClickListener(new IntermediateClickListener(listener));
        } else {
            editText.setFocusableInTouchMode(true);
            editText.setClickable(false);
            editText.setOnClickListener(null);
        }
    }

    /**
     * Hde the sift keyboard.
     */
    public void hideSoftInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    /**
     * Show the soft keyboard on this view.
     */
    public void showSoftInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, 0);
        }
    }

    private void updateRightButtonsVisibility() {
        if (editText.isFocused()) {
//            setError(null);
            if (hasCancelButton()) {
                getCancelButton().setVisibility(!TextUtils.isEmpty(getText()) ? View.VISIBLE : View.GONE);
            }

            if (hasRightButton()) {
                getRightButton().setVisibility((!hasCancelButton() || (hasCancelButton() && getCancelButton().getVisibility() != View.VISIBLE)) ? View.VISIBLE : View.GONE);
            }

        } else {
            if (hasCancelButton()) {
                getCancelButton().setVisibility(View.GONE);
            }

            if (hasRightButton()) {
                getRightButton().setVisibility(View.VISIBLE);
            }
        }
    }

    public void setIconViewVisible(boolean visible) {
        if (visible) {
            iconImageView.setVisibility(VISIBLE);
            int size = getResources().getDimensionPixelSize(R.dimen.margin_14dp);
            int sizeZero = getResources().getDimensionPixelSize(R.dimen.margin_0dp);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) textInputLayout.getLayoutParams();
            params.setMarginStart(size);
            params.setMarginEnd(sizeZero);
            textInputLayout.setLayoutParams(params);
        } else {
            iconImageView.setVisibility(GONE);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) textInputLayout.getLayoutParams();
            params.setMarginStart(0);
            params.setMarginEnd(0);
            textInputLayout.setLayoutParams(params);
        }
    }

    public void setMaxLength(int length){
        editText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(length)});
    }

    public void setNumericKeyboard(){
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
    }

    private class RightButtonsVisibilityTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(Editable s) {
            updateRightButtonsVisibility();
        }
    }

    private class IntermediateClickListener implements OnClickListener {

        private OnClickListener listener;

        IntermediateClickListener(OnClickListener listener) {
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.onClick(v);
            v.post(v::requestFocus);
        }
    }

}
