package mn.bitsup.callservice.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.custom.CustomButton;

public class AnimationDialog extends Dialog {

    private AnimationDialogListener listener;
    private TextView textTitle, textDescription;
    private String title, buttonTitle, description, animation;
    private CustomButton buttonOk;
    private ImageView buttonBack;
    private LottieAnimationView animationView;

    public AnimationDialog(Context context, String title, String description, String buttonTitle, String animation, AnimationDialogListener listener) {
        super(context);
        this.listener = listener;
        this.title = title;
        this.buttonTitle = buttonTitle;
        this.description = description;
        this.animation = animation;
    }

    public AnimationDialog(Context context, String title, String description, String buttonTitle, String animation) {
        super(context);
        this.title = title;
        this.buttonTitle = buttonTitle;
        this.description = description;
        this.animation = animation;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_animation);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        animationView = findViewById(R.id.success_animation);
        textTitle = findViewById(R.id.textTitle);
        textDescription = findViewById(R.id.textDescription);
        buttonOk = findViewById(R.id.buttonOk);
        buttonBack = findViewById(R.id.buttonBack);
        initView();
    }

    private void initView() {
        if (!title.equals("")) {
            textTitle.setText(title);
        }
        if (!textDescription.equals("")) {
            textDescription.setText(description);
        }
        if (!buttonTitle.equals("")) {
            buttonOk.setEnabledText(buttonTitle);
        }
        if (listener != null) {
            buttonOk.setOnClickListener(v -> onClick());
        } else {
            buttonOk.setOnClickListener(v -> dismiss());
        }


        if (!animation.equals("")) {
            animationView.setAnimation(animation);
        }
        animationView.playAnimation();

        buttonBack.setOnClickListener(v -> {
            if (listener != null) {
                dismiss();
                listener.onDismiss();
            } else {
                dismiss();
            }
        });
    }

    private void onClick() {
        dismiss();
        listener.onDismiss();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
