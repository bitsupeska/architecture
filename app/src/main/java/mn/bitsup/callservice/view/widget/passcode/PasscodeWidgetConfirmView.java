package mn.bitsup.callservice.view.widget.passcode;

import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.util.FingerSettings;
import mn.bitsup.callservice.view.animation.AnimationEndListener;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.widget.passcode.core.PasscodeWidget;
import mn.bitsup.callservice.view.widget.passcode.core.contract.PasscodeContract;
import mn.bitsup.callservice.view.widget.passcode.util.KeyboardUtils;

public class PasscodeWidgetConfirmView extends  Fragment implements
    PinContainer.OnPinContainerListener{

    private int SOFT_FADE_DURATION_MILLIS = 500;
    private long FAKE_LOADING_DELAY = 0;
    private PinContainer pinContainer;
    private TextView textTitle;
    private TextView textMessage;
    private TextView textForgot;
    private ProgressBar progressBar;
    private EditText inputEditText;
    private View container;
    private PasscodeContract contract;
    private PasscodeListener passcodeListener = new PasscodeListenerImpl();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contract = new PasscodeWidget(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.passcode_layout_authenticate, container, false);
        initializeViews(inflatedView);
        return inflatedView;
    }

    protected void initializeViews(@NonNull View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        textMessage = inflatedView.findViewById(R.id.textMessage);
        textForgot = inflatedView.findViewById(R.id.textForgot);
        pinContainer = inflatedView.findViewById(R.id.pinContainer);
        progressBar = inflatedView.findViewById(R.id.progressBar);
        inputEditText = inflatedView.findViewById(R.id.textInputEditText);
        container = inflatedView.findViewById(R.id.container);
        inputEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
        textTitle.setText(R.string.passcode_confirm_title);
        textMessage.setText(R.string.passcode_confirm_message);

        initializeListeners();
        initializePinContainer();
        hideProgress(false);
        inflatedView.findViewById(R.id.container).setOnClickListener(onScreenClickListener);
    }


    @Override
    public void onResume() {
        super.onResume();
        KeyboardUtils.showKeyboard(inputEditText);
        inputEditText.post(() -> inputEditText.setText(inputEditText.getText()));
    }

    protected void initializeListeners() {
        inputEditText.addTextChangedListener(pinContainer.getAfterTextChangedListener());
        container.setOnClickListener(onScreenClickListener);
        if (textForgot != null) {
            textForgot.setOnClickListener(forgotTextClickListener);
        }
    }


    private void initializePinContainer() {
        pinContainer.setGravity(Gravity.CENTER_HORIZONTAL);
        int boxNum = contract.getNumberOfDigits();
        pinContainer.setPins(boxNum,this);
    }


    protected void showProgress() {
        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            animation.setDuration(SOFT_FADE_DURATION_MILLIS);
            progressBar.startAnimation(animation);
        }
    }


    protected void hideProgress(boolean animate) {
        if (progressBar != null) {
            if (animate) {
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
                animation.setDuration(SOFT_FADE_DURATION_MILLIS);
                animation.setAnimationListener(new AnimationEndListener() {
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        hideProgress(false);
                    }
                });
                progressBar.startAnimation(animation);
            } else {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }



    protected View.OnClickListener onScreenClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            KeyboardUtils.showKeyboard(inputEditText);
        }
    };

    protected View.OnClickListener forgotTextClickListener = v ->{};


    public void onPinCompleted(final char[] pin) {
        if (pin != null && pin.length != 0) {
            showProgress();
            Handler handler = new Handler();
            handler.postDelayed(() -> contract.passcodeEntered(pin, passcodeListener, "CONFIRM"), FAKE_LOADING_DELAY);
        }
    }

    class PasscodeListenerImpl implements PasscodeListener {

        @Override
        public void onSuccess() {

        }

        @Override
        public void onError(Response response) {
            inputEditText.setText("");
            hideProgress(false);
            showErrorNotification(getContext().getString(R.string.shared_alert_generic_alert_error_title), getContext().getString(R.string.shared_alert_generic_error_server));
        }

        @Override
        public void passcodeMatchingFailed() {
            inputEditText.setText("");
            hideProgress(false);
            showErrorNotification(getContext().getString(R.string.passcode_does_not_match_title), getContext().getString(R.string.passcode_does_not_match_message));
        }
    }

    public void showErrorNotification(@NonNull final String title,
                                      @NonNull final String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle,getContext());
    }

    @Override
    public void onStop() {
        super.onStop();
        FingerSettings.stopFingerAuth();
    }
}
