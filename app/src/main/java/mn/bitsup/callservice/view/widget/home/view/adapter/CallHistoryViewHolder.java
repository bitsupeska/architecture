package mn.bitsup.callservice.view.widget.home.view.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.view.custom.DataCell;

public class CallHistoryViewHolder extends RecyclerView.ViewHolder {
    private DataCell cellSales;
    private DataCell cellName;
    private DataCell cellEndDate;
    private CallHistory item;
    public CallHistoryViewHolder(@NonNull View itemView,final CallHistoryListAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);
        cellSales = itemView.findViewById(R.id.cellSales);
        cellName = itemView.findViewById(R.id.cellName);
        cellEndDate = itemView.findViewById(R.id.cellEndDate);
    }

    void bind(final CallHistory callHistory) {
        item = callHistory;
        cellSales.setValueText(callHistory.get_id());
        cellSales.showBottomBorder(false);
        cellName.setValueText("Эрдэнэдалай");
        cellName.showBottomBorder(false);
        cellEndDate.setValueText("2022-06-22 16:00:23");
        cellEndDate.showBottomBorder(false);
    }

    View getItemView() {
        return itemView;
    }
}
