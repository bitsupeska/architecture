package mn.bitsup.callservice.view.widget.card.dto;


public enum LockStatus {
    LOCKED("LOCKED"),
    UNLOCKED("UNLOCKED");

    private final String statusName;

    private LockStatus(String var3) {
        this.statusName = var3;
    }

    public String getLockStatusAsStr() {
        return this.statusName;
    }
}