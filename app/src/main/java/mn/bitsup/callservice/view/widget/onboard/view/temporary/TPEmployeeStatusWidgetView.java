package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class TPEmployeeStatusWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private OnboardContract contract;
    private Context context;
    private IconSpinner spinnerJob;
    private CustomButton buttonContinue;
    private List<TemporaryItems> temporaryItems;
    public static String employeeStatusName = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(getContext());
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_employee_status, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        spinnerJob = inflatedView.findViewById(R.id.spinnerJob);
    }

    private void initView() {
        spinnerJob.setIconViewVisible(false);
        spinnerJob.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                buttonContinue.setState((spinnerJob.getSelectedPosition() != 0) ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                buttonContinue.setState(ButtonState.DISABLED, getActivity());
            }

        });
        getEmpstatlookup();
    }

    private void getEmpstatlookup() {
        buttonContinue.setOnClickListener(v -> publish());
        contract.getEmpstatlookup(new TemporaryItemsListener() {
            @Override
            public void onSuccess(List<TemporaryItems> temporaryItems) {
                setSpinnerData(temporaryItems);
            }

            @Override
            public void onError(Response response) {
            }
        });
    }

    private void setSpinnerData(List<TemporaryItems> temporaryItems) {
        this.temporaryItems = temporaryItems;
        List<String> items = new ArrayList<>();
        items.add("Сонгох");
        for (int i = 0; i < temporaryItems.size(); i++) {
            items.add(temporaryItems.get(i).getLocaletext());
        }
        spinnerJob.setItems(items);
        if (temporaryItems.size() != 0) {
            buttonContinue.setOnClickListener(v -> publish());
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void publish() {
        Temporary temporary = contract.getTemporary();
        if (temporaryItems != null) {
            temporary.setEmploymentStatus(temporaryItems.get(spinnerJob.getSelectedPosition() - 1).getValue());
            employeeStatusName = spinnerJob.getSelectedItem();
            contract.setTemporary(temporary);
            LoanActivity.addProgress();
            switch (temporaryItems.get(spinnerJob.getSelectedPosition() - 1).getValue()) {
                case "Employed":
                    EventHelper.publishBack(context, new TPEmployeeRoleWidgetView());
                    break;
                case "FreeLancer":
                    EventHelper.publishBack(context, new TPEmployeeRoleWidgetView());
                    break;
                default:
                    temporary.setIsWorkAddress("0");
                    contract.setTemporary(temporary);
                    EventHelper.publishBack(context, new TPDetailWidgetView());
                    break;
            }
        }
    }
}