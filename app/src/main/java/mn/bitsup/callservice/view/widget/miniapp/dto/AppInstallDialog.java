package mn.bitsup.callservice.view.widget.miniapp.dto;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.RatingBar;
import android.widget.TextView;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.custom.CustomButton;

public class AppInstallDialog extends Dialog implements
        android.view.View.OnClickListener {

    private RatingBar ratingBar;
    private TextView textName;
    private TextView textInfo;
    private CustomButton button;

    //values
    private Float rating;
    private String name;
    private String info;
    private String id;



    public AppInstallDialog(Activity activity, String rating, String name, String info, String id) {
        super(activity);
        this.rating = Float.valueOf(rating);
        this.name = name;
        this.info = info;
        this.id = id;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mini_app_dialog);
        initViews();

    }

    private void initViews() {
        ratingBar =  findViewById(R.id.rating);
        textName =  findViewById(R.id.textViewAppName);
        textInfo =  findViewById(R.id.textViewAppInfo);
        button =  findViewById(R.id.buttonContinue);

        ratingBar.setRating(Float.valueOf(rating));
        textName.setText(name);
        textInfo.setText(info);
        button.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        installApp(id);
    }

    private void installApp(String id){

    }



}