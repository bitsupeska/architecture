package mn.bitsup.callservice.view.widget.home.view;

import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.common.util.CollectionUtils;

import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.client.call.listener.CallHistoryListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.view.custom.ErrorView;
import mn.bitsup.callservice.view.custom.ErrorViewType;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.home.core.HomeContract;
import mn.bitsup.callservice.view.widget.home.core.HomeWidget;
import mn.bitsup.callservice.view.widget.home.view.adapter.CallHistoryListAdapter;

public class HomeWidgetView extends Fragment {
    private static String TAG = HomeWidgetView.class.getName();
    private Context context;
    private HomeContract contract;
    private InputField inputPhoneNumber;
    private static final ErrorViewType ERROR_VIEW_TYPE = ErrorViewType.NOTIFICATION;
    private static final ErrorViewType SERVER_ERROR_VIEW_TYPE = ErrorViewType.SERVER;

    private SwipeRefreshLayout swipeRefreshLayout;
    public WidgetListView widgetListView;
    private CallHistoryListener callHistoriesListener = new CallHistoryListenerImpl();
    private CallHistoryListAdapter adapter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new HomeWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.home_layout, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        swipeRefreshLayout = inflatedView.findViewById(R.id.swipeRefreshLayout);
        inputPhoneNumber = inflatedView.findViewById(R.id.inputPhoneNumber);
    }

    private void initView() {
        inputPhoneNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
        inputPhoneNumber.setMaxLength(8);
        adapter = new CallHistoryListAdapter(item -> Log.e(TAG, "onItemClicked: daradlaa" ));
        widgetListView.initialize(new LinearLayoutManager(getContext()),
                adapter,
                swipeRefreshLayout,
                new ShouldLoadNextPageListenerImpl());
        getHistories();
    }
    private void getHistories() {
        contract.getHistories(widgetListView.getCurrentPage(), 10, callHistoriesListener);
    }


    private class ShouldLoadNextPageListenerImpl implements WidgetListView.ShouldLoadNextPageListener {
        @Override
        public void shouldLoadNextPage() {
            getHistories();
            Log.e(TAG, "shouldLoadNextPage: daraagiin huudas duudah" );
        }
    }
    private class CallHistoryListenerImpl implements CallHistoryListener {
        @Override
        public void onSuccess(List<CallHistory> list) {
            Log.e("Call histories", "success: " + list.size());
            if (CollectionUtils.isEmpty(list) || list.size() < 10) {
                widgetListView.setEndOfListReached(true);
            }

            if (CollectionUtils.isEmpty(list) && widgetListView.getCurrentPage() == 0) {
                widgetListView.showErrorView(ErrorView.ErrorType.EMPTY, ERROR_VIEW_TYPE);
                return;
            }
            widgetListView.showList();
            if (widgetListView.getCurrentPage() == 0) {
                adapter.updateWith(list);
            } else {
                adapter.appendWith(list);
            }
        }

        @Override
        public void onError(Response response) {
            Log.e("Call histories", "error: " + response);
            widgetListView.showErrorView(response, SERVER_ERROR_VIEW_TYPE);
        }
    }
}
