package mn.bitsup.callservice.view.widget.loan.view.dto;

import mn.bitsup.callservice.client.info.dto.BankItem;

public class BankItemPayload {
    private BankItem bankItem;
    private int imageId;

    public BankItem getBankItem() {
        return bankItem;
    }

    public void setBankItem(BankItem bankItem) {
        this.bankItem = bankItem;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
