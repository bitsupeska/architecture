package mn.bitsup.callservice.view.widget.profile.dto;

public class ProfilePayload {
    private String cifid;
    private String phoneNumber;
    private String phoneType;
    private String mobileDuration;
    private String emailId;
    private String password;
    private String publishType;


    public String getCifid() {
        return cifid;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public String getMobileDuration() {
        return mobileDuration;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setCifid(String cifid) {
        this.cifid = cifid;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public void setMobileDuration(String mobileDuration) {
        this.mobileDuration = mobileDuration;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPublishType() {
        return publishType;
    }

    public void setPublishType(String publishType) {
        this.publishType = publishType;
    }
}
