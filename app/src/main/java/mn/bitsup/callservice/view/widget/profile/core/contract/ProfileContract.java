package mn.bitsup.callservice.view.widget.profile.core.contract;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.client.profile.listener.QuestionListener;

public interface ProfileContract {
    User getUser();

    void setEmail(StatusListener var1, HashMap<String, String> var2);

    void setNumber(StatusListener var1, HashMap<String, String> var2);

    void setAddress(StatusListener var1, HashMap<String, String> var2);

    void matchPassword(StatusListener var1, HashMap<String, String> var2);

    void setPassword(StatusListener var1, HashMap<String, String> var2);

    void requestOTP(OTPListener var1, Map<String, String> var2);

    void checkOTP(OTPListener otpListener, Map<String, String> var2);

    void requestEmailOTP(OTPListener var1, Map<String, String> var2);

    void checkEmailOTP(OTPListener otpListener, Map<String, String> params);

    void loadCityData(LocationListener var1);

    void loadDistrictData(LocationListener var1, String var2);

    void getBanks(BankListener var1, Map<String, String> params);

    void getAccountLists(AccountListener var1, String var2);

    void addAccounts(StatusListener var1, Map<String, String> var2);

    void deleteAccounts(StatusListener var1, Map<String, String> var3);

    void getAnswerQuestions(QuestionListener var1);

    void checkBlockIncrement(BlockIncrementListener var1, String var2);

    void setBlock(StatusListener var1, BlockIncrement var2, String  var3);

    void checkBlock(BlockListener listener, HashMap<String, String> param);

}
