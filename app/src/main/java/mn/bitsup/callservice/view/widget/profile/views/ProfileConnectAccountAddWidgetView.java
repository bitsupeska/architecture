package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfileConnectAccountAddWidgetView extends Fragment implements ProfileView<ProfileContract>, TextWatcher, AdapterView.OnItemSelectedListener {

    private Context context;
    private ProfileContract contract;
    private List<BankItem> bankItem;
    private IconSpinner spinnerBankName;
    private InputField inputOwnerName;
    private InputField inputAccountNumber;
    private CustomButton buttonContinue;
    private ProgressDialog progressDialog;
    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        this.user = MainApplication.getUser();
        Activity activity = (Activity) context;
        try{
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        catch(NullPointerException e){
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_connect_account_add, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        inputOwnerName = inflatedView.findViewById(R.id.inputOwnerName);
        inputAccountNumber = inflatedView.findViewById(R.id.inputAccountNumber);
        spinnerBankName = inflatedView.findViewById(R.id.spinnerBankName);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initViews() {
        progressDialog.show();
        inputOwnerName.setInputType(InputType.TYPE_CLASS_TEXT);
        inputAccountNumber.setInputType(InputType.TYPE_CLASS_TEXT);
        inputAccountNumber.setNumericKeyboard();
        buttonContinue.setOnClickListener(v -> addAccount());
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
        getBankData();

        InputFilter[] filter = new InputFilter[1];
        InputFilter[] filters = new InputFilter[1];
        filter[0] = new InputFilter.LengthFilter(50);
        filters[0] = new InputFilter.LengthFilter(15);

        inputOwnerName.setFilters(filter);
        inputAccountNumber.setFilters(filters);

        inputOwnerName.setInputType(InputType.TYPE_CLASS_TEXT);
        inputOwnerName.setText(user.getFirstName() + " " + user.getLastName());
        inputOwnerName.setEnabled(false);
        inputAccountNumber.addTextChangedListener(this);
        spinnerBankName.setOnItemSelectedListener(this);

        inputOwnerName.setCancelButton();
        inputAccountNumber.setCancelButton();
    }

    private boolean isValid() {
        return (inputAccountNumber.getValue().length() > 8 && spinnerBankName.getSelectedPosition() != 0);
    }

    private void getBankData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("profileSettings", "");
        param.put("qpay", "");
        param.put("transfer", "");
        contract.getBanks(new BankListener() {
            @Override
            public void onSuccess(List<BankItem> data) {
                bankItem = data;
                List<String> list = new ArrayList<String>();
                list.add("Сонгоx");
                for (int i = 0; i < bankItem.size(); i++) {
                    list.add(bankItem.get(i).getBankname());
                }
                if (data.size() != 0) progressDialog.dismiss();
                spinnerBankName.setItems(list);
            }

            @Override
            public void onError(Response response) {
                Log.e("getBankData", "error: " + response);
            }
        }, param);
    }

    private void addAccount() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> param = new HashMap<>();
        param.put("account", inputAccountNumber.getValue());
        param.put("bank", bankItem.get(spinnerBankName.getSelectedPosition() - 1).getId().toString());
        param.put("cif", contract.getUser().getCif());

        contract.addAccounts(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse var1) {
                updateDataFinish("success", context.getResources().getString(R.string.setup_completed_title), context.getResources().getString(R.string.connect_account_add_account_success));
            }

            @Override
            public void onError(Response statusResponse) {
                updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), statusResponse.getErrorMessage());
            }
        }, param);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (inputAccountNumber.hasFocus() && inputAccountNumber.getValue().length() < 8) {
            inputAccountNumber.setError("8-15 оронтой байх хэрэгтэй");
            buttonContinue.setState(ButtonState.DISABLED);
        } else {
            inputAccountNumber.setError(null);
        }
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    private void updateDataFinish(String result, String resultTitle, String resultMessage) {
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
        Activity activity = ((Activity) Objects.requireNonNull(getContext()));
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        returnIntent.putExtra("resultTitle", resultTitle);
        returnIntent.putExtra("resultMessage", resultMessage);
        activity.setResult(Activity.RESULT_FIRST_USER, returnIntent);
        activity.finish();

        ((Activity) context).finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        KeyboardUtils.hideKeyboard(getActivity());

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
