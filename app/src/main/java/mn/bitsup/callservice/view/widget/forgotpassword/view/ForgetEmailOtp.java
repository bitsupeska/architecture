package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.HashMap;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.OTPView;
import mn.bitsup.callservice.view.custom.OTPViewLisener;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.views.ProfileView;

public class ForgetEmailOtp extends Fragment implements ProfileView<ProfileContract> {

    private TextView textTitle;
    private OTPView otpViewEmail;

    private int checkCount = 3;
    private String email = "";
    private ProgressDialog progressDialog;
    private Context context;
    private User user;
    private ForgotPasswordContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ForgetWidget(context);
        this.user = EventHelper.getEventPayload("getUser", User.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_email_otp, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        otpViewEmail = inflatedView.findViewById(R.id.otpViewEmail);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Уншиж байна");
    }

    private void initViews() {
        email = user.getEmails().get(0).getEmailId();
        textTitle.setText(oddIndexHideEmail() + " хаяг руу илгээгдсэн 4 оронтой кодыг оруулна уу");
        otpViewEmail.setLinkOnClickListener(v -> checkBlockIncrement());

        OTPFilledListener otpListener = new OTPFilledListener();
        sendRequest();
        otpViewEmail.setFilledListener(otpListener);
    }

    private StringBuilder oddIndexHideEmail() {
        StringBuilder emailBuild = new StringBuilder(this.email);
        for (int i = 1; i < emailBuild.indexOf("@"); i = i + 2) {
            emailBuild.setCharAt(i, '*');
        }
        return emailBuild;
    }


    public void publish() {
        otpViewEmail.hideKeyboard();
        otpViewEmail.stopTimer();
        EventHelper.publishBack(getContext(), new ForgetFingerWidgetView(), "getUser", this.user);
    }

    public class OTPFilledListener implements OTPViewLisener {

        @Override
        public void onSuccess(String code) {
            if (checkCount != 0) {
                checkEmailOTP(code);
            }
        }
    }

    private void sendRequest() {
        checkCount = 3;
        otpViewEmail.stopTimer();
        otpViewEmail.hideErrorLabel();

        HashMap<String, String> additions = new HashMap<>();
        additions.put("email", email);
        progressDialog.show();

        contract.requestEmailOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();
                if (otp.getStatus().equals("SUCCESS")) {
                    otpViewEmail.startTimer(180000);
                }
            }

            @Override
            public void onError(Response var1) {
                progressDialog.dismiss();
                CustomAlertDialog.showServerAlertDialog(getContext());
            }

        }, additions);
    }

    private void checkEmailOTP(String code) {
        progressDialog.show();
        HashMap<String, String> additions = new HashMap<>();
        additions.put("email", email);
        additions.put("otp", code);

        contract.checkEmailOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();
                checkCount--;
                if (otp.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    if (checkCount == 0) {
                        otpViewEmail.stopTimer();
                        otpViewEmail.setLabel();
                        otpViewEmail.showErrorLabel("3 удаа буруу оруулсан тул та дахин код илгээнэ үү");
                    } else {
                        otpViewEmail.showErrorLabel("Нууц код буруу байна. Танд " + checkCount + " удаагийн оролдлого үлдлээ");
                    }
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
                otpViewEmail.emptyLabel();
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, additions);
    }

    private void checkBlockIncrement() {
        progressDialog.show();
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                if (blockIncrement == null) {
                    sendRequest();
                }else{
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, "7","5");
    }
}