package mn.bitsup.callservice.view.widget.loan.view;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.math.BigDecimal;
import java.util.Currency;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputFieldAmount;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class LoanInputSalaryView extends Fragment implements
        OnboardView<OnboardContract> {

    private StatusListener verifiedListener;

    LoanInputSalaryView(StatusListener verifiedListener) {
        this.verifiedListener = verifiedListener;
    }

    private CustomButton buttonContinue;
    private InputFieldAmount inputSalary;
    private Context context;
    private LoanContract contract;
    private User user;
    private UpdateContinueButtonTextWatcher updateContinueButtonTextWatcher = new UpdateContinueButtonTextWatcher();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.user = MainApplication.getUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_input_salary, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputSalary = inflatedView.findViewById(R.id.inputSalary);
        inputSalary.setIconViewVisible(false);
        inputSalary.setSingleLine();
        inputSalary.setCancelButton();
        inputSalary.setMaxLength(20);
        inputSalary.addTextChangedListener(updateContinueButtonTextWatcher);
    }

    private void initView() {
        if (LoanActivity.progressToolbar != null) {
            LoanActivity.addProgress();
        }

        Currency currency = Currency.getInstance("MNT");
        inputSalary.setCurrency(currency);
        buttonContinue.setOnClickListener(v -> {
            String amount = inputSalary.getValue().replace(",", "");
            StatusResponse statusResponse = new StatusResponse();
            statusResponse.setMessage(amount);
            EventHelper.publishStackPop(context, null);
            verifiedListener.onSuccess(statusResponse);
        });
    }

    private class UpdateContinueButtonTextWatcher extends AfterTextChangedListener {

        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
            }
        }
    }

    private boolean isValid() {
        return inputSalary.getAmount().compareTo(BigDecimal.ZERO) > 0;
    }

}

