package mn.bitsup.callservice.view.custom.inputfield;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.util.AttributeSet;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Locale;
import java.util.regex.Pattern;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.BeforeAndAfterTextChangedListener;


/**
 * Created by Erdenedalai R&D B.V on 21/06/2018.
 * Custom implementation of an input field using TextInputLayout specialised to use for amount values.
 * <p>
 * The amount inserted is automatically formatted using NumberFormat.getCurrencyInstance(). The EditText does not accept copy and paste operations.
 */
public class InputFieldAmount extends InputField {
    private static final String TAG = InputFieldAmount.class.getSimpleName();
    private static final double DEFAULT_MAX_AMOUNT = 999999999.99;

    @Nullable
    private Currency currency;
    @Nullable
    private NumberFormat currencyFormat;
    private DecimalFormat bigDecimalParser;
    private BigDecimal maxValue = BigDecimal.valueOf(DEFAULT_MAX_AMOUNT);
    private String initialAmountText;
    private CurrencyAmountFormatterTextWatcher currencyAmountFormatterTextWatcher;
    private final String decimalSeparator = Character.toString(DecimalFormatSymbols.getInstance(Locale.getDefault()).getDecimalSeparator());
    private final String groupingSeparator = Character.toString(DecimalFormatSymbols.getInstance(Locale.getDefault()).getGroupingSeparator());
    private final Pattern nonDecimalDigits = Pattern.compile("[^\\d" + decimalSeparator + groupingSeparator + "]");

    public InputFieldAmount(Context context) {
        this(context, null);
    }

    public InputFieldAmount(Context context, AttributeSet attrs) {
        super(context, attrs);
        setEnabled(false);
    }

    @Override
    protected void setTextAppearance() {
        editText.setTextAppearance(getContext(), R.style.H2);
    }

    /**
     * @return the amount currently shown, as {@link BigDecimal}, or zero if a parse error occurs.
     */
    public BigDecimal getAmount() {
        final DecimalFormat bigDecimalParser = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
        bigDecimalParser.setParseBigDecimal(true);
        try {
            return (BigDecimal) bigDecimalParser.parse(editText.getText().toString());
        } catch (final ParseException e) {
            e.printStackTrace();
            return new BigDecimal(0);
        }
    }

    /**
     * Sets the value shown by the EditText.
     *
     * @param amount the amount to be shown.
     * @throw IllegalArgumentException if the amount passed is bigger than the maximum allowed amount.
     */
    public void setAmount(@NonNull final BigDecimal amount) {
        if (amount.compareTo(maxValue) <= 0) {
            editText.removeTextChangedListener(currencyAmountFormatterTextWatcher);
            final String formattedValue = getCurrencyFormat().format(amount);
            final String formattedValueDigitsOnly = nonDecimalDigits.matcher(formattedValue).replaceAll("");
            updateTextAndColor(formattedValueDigitsOnly);
            editText.addTextChangedListener(currencyAmountFormatterTextWatcher);
        } else {
            throw new IllegalArgumentException("Value " + amount.toString() + " is greater than maximum allowed amount " + maxValue);
        }
    }

//    /**
//     * Sets the value shown by the EditText.
//     *
//     * @param amount the amount to be shown.
//     */
//    public void setAmount(final double amount) {
//        setAmount(BigDecimal.valueOf(amount));
//    }
//
//    public void setAmount(final Amount amount) {
//        setCurrency(AmountUtil.getCurrency(amount));
//        setAmount(amount.getAmount());
//    }
//
//    public Amount toAmount() {
//        return new Amount(getAmount(), getCurrency().getCurrencyCode());
//    }

    /**
     * Sets the maximum value which can be inserted.
     *
     * @param maxValue the maximum value.
     */
    public void setMaxValue(@NonNull final BigDecimal maxValue) {
        this.maxValue = maxValue;
        try {
            final BigDecimal currentValue = (BigDecimal) getBigDecimalParser().parseObject(editText.getText().toString());
            if (currentValue.compareTo(maxValue) <= 0) {
                return;
            }
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        updateTextAndColor("");
    }

    /**
     * @return true if the amount in the EditText is greater than zero, false otherwise.
     */
    public boolean hasAmountGreaterThanZero() {
        return getAmount().compareTo(BigDecimal.ZERO) > 0;
    }

    private NumberFormat getCurrencyFormat() {
        if (currencyFormat == null) {
            currencyFormat = NumberFormat.getCurrencyInstance();
            currencyFormat.setCurrency(getCurrency());
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) currencyFormat).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol("");
            ((DecimalFormat) currencyFormat).setDecimalFormatSymbols(decimalFormatSymbols);
        }
        return currencyFormat;
    }


    public void setIconColor(int color){
        iconImageView.setColorFilter(getContext().getResources().getColor(color));
    }

    private DecimalFormat getBigDecimalParser() {
        if (bigDecimalParser == null) {
            bigDecimalParser = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
            bigDecimalParser.setParseBigDecimal(true);
        }
        return bigDecimalParser;
    }

    @Override
    protected void init(Context context, AttributeSet attrs) {
        super.init(context, attrs);
        currencyAmountFormatterTextWatcher = new CurrencyAmountFormatterTextWatcher();
        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        editText.addTextChangedListener(currencyAmountFormatterTextWatcher);
        editText.setLongClickable(false);
        editText.setTextIsSelectable(false);
    }

    /**
     * @return the used currency for formatting and icon.
     * If currency was not set explicitly using {@link InputFieldAmount#setCurrency(Currency)} then an {@link IllegalStateException} is thrown.
     */
    @NonNull
    public Currency getCurrency() {
        if (currency == null) {
            throw new IllegalStateException("CurrencyItem was never set.");
        }

        return currency;
    }

    /**
     * Sets the used currency for formatting and icon.
     *
     * @param currency to use for formatting and icon.
     */
    public void setCurrency(@Nullable Currency currency) {
        if (currency == null) {
            hideIcon();
            return;
        }
        this.currency = currency;
        initializeWithCurrency(currency);
    }

    private void initializeWithCurrency(Currency currency) {
        getCurrencyFormat().setCurrency(currency);
        setIcon(getResourceFor(currency));
        setEnabled(true);
        initialAmountText = getCurrencyFormat().format(0.0);
        if (editText.getText() == null
                || editText.getText().toString().isEmpty()) {
            updateTextAndColor(initialAmountText);
        }
    }

    private void updateTextAndColor(String text) {
        if (currency != null) {
//            final String formattedValueDigitsOnly = nonDecimalDigits.matcher(text).replaceAll("");
//            SpannableStringBuilder spannableStringBuilder = CurrencyConverter.toSuperscriptedCharSequence(currency, formattedValueDigitsOnly, null);
            editText.setText(text);
        } else {
            editText.setText(text);
        }
        setTextColor();
    }

    @DrawableRes
    private static int getResourceFor(@NonNull Currency currency) {
        return R.drawable.ic_amount;
    }

    private class CurrencyAmountFormatterTextWatcher extends BeforeAndAfterTextChangedListener {
        private String textBeforeChange;
        private String textAfterChange;

        @Override
        public void beforeTextChanged(final CharSequence charSequence, final int i, final int i1, final int i2) {
            textBeforeChange = nonDecimalDigits.matcher(charSequence.toString()).replaceAll("");
        }

        @Override
        public void afterTextChanged(final Editable editable) {
            textAfterChange = nonDecimalDigits.matcher(editable.toString()).replaceAll("");

            try {
                final BigDecimal newValue = moveDecimalPoint(textBeforeChange, textAfterChange);
                if (newValue.compareTo(maxValue) <= 0) {
                    setAmount(newValue);
                } else {
                    editText.removeTextChangedListener(this);
                    updateTextAndColor(textBeforeChange);
                    editText.addTextChangedListener(this);
                }
                setTextColor();
            } catch (final ParseException e) {
                e.printStackTrace();
            }

            editText.setSelection(editText.getText().length());
        }

        private boolean textWasAddedAtEnd(@NonNull final String oldText, @NonNull final String newText) {
            return newText.length() > oldText.length() && newText.startsWith(oldText);
        }

        private boolean textWasRemovedAtEnd(@NonNull final String oldText, @NonNull final String newText) {
            return newText.length() < oldText.length() && oldText.startsWith(newText);
        }

        private BigDecimal moveDecimalPoint(@NonNull final String oldText, @NonNull final String newText) throws ParseException {
            final boolean firstDigit = newText.length() == 1;
            final int decimalsCount = getCurrencyFormat().getMaximumFractionDigits();
            BigDecimal rawValue = (BigDecimal) getBigDecimalParser().parseObject(newText);

            if (firstDigit) {
                // First digit entered, we start by filling the decimals
                rawValue = rawValue.movePointLeft(decimalsCount);
            } else if (textWasRemovedAtEnd(oldText, newText)) {
                rawValue = rawValue.movePointLeft(decimalsCount - 1);
            } else if (textWasAddedAtEnd(oldText, newText)) {
                rawValue = rawValue.movePointRight(decimalsCount - 1);
            }

            return rawValue.setScale(NumberFormat.getCurrencyInstance().getMaximumFractionDigits(), RoundingMode.FLOOR);
        }
    }

    private void setTextColor() {
        int textColor = ContextCompat.getColor(getContext(),
                getAmount().doubleValue() == 0.0 ? R.color.blue_grey : R.color.almost_black);
        editText.setTextColor(textColor);
    }
}
