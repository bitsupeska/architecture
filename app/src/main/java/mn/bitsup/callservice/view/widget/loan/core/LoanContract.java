package mn.bitsup.callservice.view.widget.loan.core;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.client.loan.listener.LoanCreateInvoiceListener;
import mn.bitsup.callservice.client.loan.listener.LoanIsAllowedListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelCreateListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelListener;
import mn.bitsup.callservice.client.loan.listener.SysDateListener;
import mn.bitsup.callservice.client.profile.listener.AccountListener;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.loan.listener.LoanSalaryListener;

public interface LoanContract {
    void getAccountLists(AccountListener listener, String params);

    void getBanks(BankListener listener, Map<String, String> var2);

    void getCheckCitizenInfo(CitizenInfoListener var1, Map<String, String> var2);

    void getLastOffer(Map<String, String> var1, LoanOfferListener var2);

    void getRelated(LoanRelListener loanRelCreateListener);

    void createRelated(LoanRelCreateListener loanRelListener, Map<String, String> params);

    void getSalary(Map<String, String> var1, LoanSalaryListener var2);

    void getOffer(Map<String, String> var1, LoanOfferListener var2);

    void getLoanAccount(Map<String, String> var1, LoanAccountListener var2);

    void createLoan(Loan loan, StatusListener var2);

    void setLoan(Loan loan);

    Loan getLoan();

    void getLoanDueSchedule(LoanDueScheduleListener var2, Map<String, String> var1);

    void getLoanFindAmortSchedule(LoanDueScheduleListener var2, String var1);

    void createLoanInvoice(Map<String, String> var1, LoanCreateInvoiceListener var2);

    void getSysDate(SysDateListener var2);

    void getLoanAllowed(LoanIsAllowedListener var2, HashMap<String ,String > params);
}
