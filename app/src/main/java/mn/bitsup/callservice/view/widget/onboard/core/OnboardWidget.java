package mn.bitsup.callservice.view.widget.onboard.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.dataprovider.util.OfflineCrypto;
import mn.bitsup.callservice.client.info.InfoClient;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.offline.OfflineClient;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.CheckUsernameItems;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.CheckUserNameListener;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.client.otp.OTPClient;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.TemporaryClient;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.client.temporary.dto.TemporaryCif;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.client.temporary.listener.EducationListener;
import mn.bitsup.callservice.client.temporary.listener.TemporaryItemsListener;
import mn.bitsup.callservice.util.DateTimeFormatter;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.activity.TemporaryActivity;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;

import org.json.JSONException;
import org.json.JSONObject;

public class OnboardWidget implements OnboardContract {

    public static final String ACCESS_TOKEN = "access_token";
    private OfflineClient offlineClient;
    private OTPClient otpClient;
    private InfoClient infoClient;
    private TemporaryClient temporaryClient;
    private Context context;

    public OnboardWidget(@NonNull Context context) {
        this.context = context;
        this.offlineClient = ClientsManager.getOfflineClient();
        this.otpClient = ClientsManager.getOtpClient();
        this.infoClient = ClientsManager.getInfoClient();
        this.temporaryClient = ClientsManager.getTemporaryClient();
    }

    @Override
    public void checkRegister(String register, StatusListener listener) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                Map<String, String> params = new HashMap<>();
                params.put("regNum", register);
                params.put("macaddress", DeviceAddressUtil.getMACAddress(context, "wlan0"));
                params.put("ipaddress", DeviceAddressUtil.getIPAddress(true));
                offlineClient.checkRegister(params, new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var2) {
                        listener.onError(var2);
                        showErrorDialog();
                    }
                });
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                showErrorDialog();
            }
        });
    }

    @Override
    public void checkBaml(String register, StatusListener listener) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                Map<String, String> params = new HashMap<>();
                params.put("regNum", register);
                offlineClient.checkBaml(params, new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var2) {
                        listener.onError(var2);
                        showErrorDialog();
                    }
                });
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                showErrorDialog();
            }
        });
    }

    @Override
    public void requestPCR(String register, StatusListener listener) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                Map<String, String> params = new HashMap<>();
                params.put("regNum", register);
                offlineClient.requestPCR(params, new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var2) {
                        listener.onError(var2);
                        showErrorDialog();
                    }
                });
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                showErrorDialog();
            }
        });
    }

    @Override
    public void checkToken(TokenListener listener) {
        if (RefreshService.isRunning && RefreshService.timer > 20) {
            listener.onSuccess("");
        } else {
            String data = OfflineCrypto.tokenEncrypt(context);
            if (data != null && !data.equals("")) {
                HashMap<String, String> params = new HashMap<>();
                params.put("data", data);
                offlineClient.convertToken(new TokenListener() {
                    @Override
                    public void onSuccess(String var1) {
                        String ddata = OfflineCrypto.tokenDecrypt(var1, context);
                        Log.e("json", "saved token: " + ddata);

                        if (ddata != null && !ddata.equals("")) {
                            try {
                                JSONObject object = new JSONObject(ddata);
                                StorageComponent storageComponent = new StorageComponent();
                                storageComponent.setItem(ACCESS_TOKEN, object.getString("access_token"));
                                listener.onSuccess(var1);

                                context.stopService(new Intent(context, RefreshService.class));
                                context.startService(new Intent(context, RefreshService.class));

                            } catch (JSONException e) {
                                Response errorResponse = new Response();
                                errorResponse.setResponseCode(500);
                                listener.onError(errorResponse);
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onError(Response var1) {
                        listener.onError(var1);
                    }
                }, params);
            }
        }

    }

    @Override
    public void requestOTP(OTPListener otpListener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                otpClient.getOTP(params, new OTPListener() {
                    @Override
                    public void onSuccess(StatusResponse response) {
                        otpListener.onSuccess(response);
                    }

                    @Override
                    public void onError(Response response) {
                        otpListener.onError(response);
                        showErrorDialog();
                    }
                });
            }

            @Override
            public void onError(Response response) {
                showErrorDialog();
                otpListener.onError(response);

            }
        });
    }

    @Override
    public void checkOTP(OTPListener otpListener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                otpClient.checkOTP(params, new OTPListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        otpListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        showErrorDialog();
                        otpListener.onError(var1);

                    }
                });
            }

            @Override
            public void onError(Response response) {
                otpListener.onError(response);
            }
        });
    }


    @Override
    public void getCheckCitizenInfo(CitizenInfoListener listener, Map<String, String> params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {

                infoClient.getCheckCitizenInfo(params, new CitizenInfoListener() {

                    @Override
                    public void onSuccess(CitizenInfo var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        listener.onError(var1);
                        showErrorDialog();

                    }
                });
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                showErrorDialog();
            }
        });
    }

    @Override
    public void getKhurData(KhurDataListener listener, Map<String, String> params) {
        temporaryClient.getKhurData(params, listener);
    }

    @Override
    public void getXyrData(KhurDataListener listener, Map<String, String> params) {
        temporaryClient.getXyrData(params, listener);
    }

    @Override
    public void createUser(StatusListener listener, EasyOnboardItem params) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                offlineClient.createUser(params, new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse response) {
                        listener.onSuccess(response);

                    }

                    @Override
                    public void onError(Response response) {
                        listener.onError(response);
                        CustomAlertDialog.showServerAlertDialog(context);
                    }
                });
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }

    @Override
    public void checkUsername(String username, CheckUserNameListener listener) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                Map<String, String> params = new HashMap<>();
                params.put("userName", username);
                params.put("firstName", getPayload().getFirstName());
                params.put("lastName", getPayload().getLastName());
                offlineClient.checkUsername(params, new CheckUserNameListener() {
                    @Override
                    public void onSuccess(CheckUsernameItems var1) {
                        listener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        showErrorDialog();
                        listener.onError(var1);
                    }
                });
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                showErrorDialog();
            }
        });
    }


    @Override
    public void setBlock(StatusListener statusListener, BlockIncrement blockIncrement) {

        HashMap<String, String> param = new HashMap<>();
        param.put("blockInfo", DeviceAddressUtil.getMACAddress(context, "wlan0"));
        param.put("blockType", "10");
        param.put("blockChannel", "2");
        param.put("blockMsg",
                "Таны өнөөдөр бүртгүүлэх эрх хаагдсан байна. Та 00:00 цагаас хойш дахин оролдоно уу.");
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                offlineClient.setBlock(new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse statusResponse) {
                        statusListener.onSuccess(statusResponse);
                    }

                    @Override
                    public void onError(Response response) {
                        showErrorDialog();
                        statusListener.onError(response);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                statusListener.onError(response);
                showErrorDialog();
            }
        });

    }

    @Override
    public void setBlockKhur(StatusListener statusListener) {
        HashMap<String, String> param = new HashMap<>();
        String uid = ClientsManager.getRemoteAuthClient().getUid();
        param.put("blockInfo", uid.toLowerCase());
        param.put("blockType", "3");
        param.put("blockChannel", "2");
        param.put("blockMsg",
                context.getResources().getString(R.string.khur_alert_finger_not_match));
        offlineClient.setBlock(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                statusListener.onSuccess(statusResponse);
            }

            @Override
            public void onError(Response response) {
                //showErrorDialog();
                statusListener.onError(response);
            }
        }, param);


    }

    @Override
    public void checkBlockIncrement(BlockIncrementListener listener) {
        HashMap<String, String> param = new HashMap<>();
        param.put("blockInfo", DeviceAddressUtil.getMACAddress(context, "wlan0"));
        param.put("blockType", "10");
        param.put("blockChannel", "2");
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                offlineClient.checkBlockIncrement(new BlockIncrementListener() {
                    @Override
                    public void onSuccess(BlockIncrement blockIncrement) {
                        if (blockIncrement.getStatus().equals("SUCCESS")) {
                            checkOtpCount(blockIncrement);
                        } else {
                            listener.onError(new Response());
                        }
                    }

                    @Override
                    public void onError(Response response) {
                        listener.onError(response);
                        showErrorDialog();
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
                showErrorDialog();
            }
        });

    }

    private void checkOtpCount(BlockIncrement blockIncrement) {
        Log.e("checkOtpCount", "getFailed_count(): " + blockIncrement.getData().getFailedCount());
        Log.e("checkOtpCount", "getBlock_limit(): " + blockIncrement.getData().getBlockLimit());
        if (blockIncrement.getData().getFailedCount() >= blockIncrement.getData().getBlockLimit()) {
            setBlock(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    if (statusResponse.getStatus().equals("SUCCESS")) {
                        new AnimationDialog(context, "Уучлаарай,",
                                "Таны өнөөдөр дугаараа баталгаажуулах 3 удаагийн эрх дууслаа. Та 00:00 цагаас хойш дахин оролдоно уу.",
                                "Дахин оролдох", "anim_rejection_clock.json",
                                () -> ((Activity) context).finish()).show();
                    } else {
                        Log.e("MBank", statusResponse.getMessage());
                    }
                }

                @Override
                public void onError(Response response) {
                    Log.e("MBank", "block hiih ued server aldaa garlaa");
                    showErrorDialog();
                }
            }, blockIncrement);
        }
    }

    @Override
    public void checkEmail(StatusListener listener, String param) {
        temporaryClient.checkEmail(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse response) {
                listener.onSuccess(response);
            }

            @Override
            public void onError(Response response) {
                showErrorDialog();
                listener.onError(response);

            }
        }, param);
    }

    @Override
    public void createCif(StatusListener listener, KhurItems khurItems) {
        Log.e("asd", "createCif: "+khurItems.toString() );
        TemporaryCif temporaryCif = new TemporaryCif();
        temporaryCif.setRegNum(khurItems.getRegNum());
        temporaryCif.setFamilyName(khurItems.getSurName());
        temporaryCif.setFirstName(khurItems.getFirstName());
        temporaryCif.setLastName(khurItems.getLastName());
        User user = MainApplication.getUser();
        if (user.getPhoneNo() != null) {
            temporaryCif.setHandPhoneStatus("1");
            temporaryCif.setHandPhone(user.getPhoneNo());
        } else {
            temporaryCif.setHandPhoneStatus("0");
            temporaryCif.setHandPhone("");
        }

        if (user.getEmails().size() > 0) {
            if (!user.getEmails().get(0).getEmailId().equals("")) {
                temporaryCif.setEmail(user.getEmails().get(0).getEmailId());
                temporaryCif.setEmailStatus("0");

            } else {
                temporaryCif.setEmail("");
                temporaryCif.setEmailStatus("0");
            }
        } else {
            temporaryCif.setEmail("");
            temporaryCif.setEmailStatus("0");
        }

        temporaryCif.setBirthDate(DateTimeFormatter.dateFormat(khurItems.getBirthdateastext()));
        temporaryCif.setDocExpDate(khurItems.getPassportExpireDate().replace(" 00:00:00", ""));
        temporaryCif.setDocIssueDate(khurItems.getPassportIssueDate().replace(" 00:00:00", ""));
        temporaryCif.setDidSign("0");
        temporaryCif.setTownHome(khurItems.getAimagCityCode());
        temporaryCif.setLocalityHome(Integer.parseInt(khurItems.getSoumDistrictCode()) + "");
        temporaryCif.setSectionHome(khurItems.getBagKhorooName());

        if (khurItems.getAddressRegionName()!= null && !khurItems.getAddressRegionName().equals("")) {
            temporaryCif.setDistrictHome(khurItems.getAddressRegionName());
        } else {
            if (!(khurItems.getAddressStreetName() == null || khurItems.getAddressStreetName()
                    .equals(""))) {
                temporaryCif.setDistrictHome(khurItems.getAddressStreetName());
            } else {
                temporaryCif.setDistrictHome("");
            }
        }

        temporaryCif.setApartmentHome(khurItems.getAddressApartmentName() == null ? "":khurItems.getAddressApartmentName());
        temporaryCif.setDoorNumberHome(khurItems.getAddressDetail());
        temporaryCif.setUserName(ClientsManager.getRemoteAuthClient().getUid());
        temporaryCif.setTownHome(khurItems.getAimagCityCode());

        if (khurItems.getGender().equals("Эрэгтэй")) {
            temporaryCif.setGender("M");
        } else {
            temporaryCif.setGender("F");
        }

        temporaryClient.createCif(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse response) {
                listener.onSuccess(response);
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
            }
        }, temporaryCif);

    }

    @Override
    public void createCA(StatusListener listener) {
        Temporary temporary = getTemporary();
        temporaryClient.createCA(listener, temporary);
    }

    @Override
    public EasyOnboardItem getPayload() {
        return OnboardActivity.payload;
    }


    @Override
    public void setPayload(EasyOnboardItem payload) {
        OnboardActivity.payload = payload;
    }

    @Override
    public Temporary getTemporary() {
        return TemporaryActivity.temporary;
    }

    @Override
    public void setTemporary(Temporary var1) {
        TemporaryActivity.temporary = var1;
    }

    @Override
    public void getEmpstatlookup(TemporaryItemsListener listener) {
        temporaryClient.getEmpstatlookup(new TemporaryItemsListener() {
            @Override
            public void onSuccess(List<TemporaryItems> var1) {
                listener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                listener.onError(var1);
            }
        });
    }

    @Override
    public void getOccstatlookup(TemporaryItemsListener listener) {
        temporaryClient.getOccstatlookup(new TemporaryItemsListener() {
            @Override
            public void onSuccess(List<TemporaryItems> var1) {
                listener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                listener.onError(var1);
            }
        });
    }

    private void showErrorDialog() {
        CustomAlertDialog.showServerAlertDialog(context);
    }

    //Temporary functions
    @Override
    public void loadCityData(LocationListener listener) {
        this.infoClient.getCityData(listener);
    }

    @Override
    public void loadDistrictData(LocationListener listener, String stateCode) {
        this.infoClient.getDistrictData(listener, stateCode);
    }

    @Override
    public void getEduType(EducationListener listener) {
        this.temporaryClient.getEduType(listener);
    }

}