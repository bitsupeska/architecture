package mn.bitsup.callservice.view.widget.onboard.view.easy;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.Arrays;
import java.util.List;
import mn.bitsup.callservice.R;

import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBDurationWidgetView extends Fragment implements
    OnboardView<OnboardContract>, AdapterView.OnItemSelectedListener {

    private CustomButton buttonContinue;
    private Context context;
    private OnboardContract contract;
    private IconSpinner spinnerPhoneDuration;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView =inflater.inflate(R.layout.onboard_phone_duration, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        spinnerPhoneDuration = inflatedView.findViewById(R.id.spinnerPhoneDuration);
    }

    private void initView(){
        List<String> items = Arrays.asList(context.getResources().getStringArray(R.array.phone_duration_list));
        spinnerPhoneDuration.setItems(items);
        spinnerPhoneDuration.setOnItemSelectedListener(this);
        buttonContinue.setLoadingText("");
        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        buttonContinue.setOnClickListener(v -> publish());
    }

    private void publish(){
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        EasyOnboardItem payload = contract.getPayload();
        payload.setHandPhoneDuration(""+(spinnerPhoneDuration.getSelectedPosition()));
        contract.setPayload(payload);
        OnboardActivity.addProgress();
        EventHelper.publishBack(context, new OBEmailWidgetView());
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(spinnerPhoneDuration.getSelectedPosition() != 0 ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
