package mn.bitsup.callservice.view.widget.card.view;

import static android.view.View.inflate;
import static mn.bitsup.callservice.MainApplication.getUser;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ViewFlipper;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.card.dto.Card;
import mn.bitsup.callservice.client.card.dto.CardItem;
import mn.bitsup.callservice.client.card.listener.CardsListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockData;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.util.CustomParams;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.MutableParams;
import mn.bitsup.callservice.util.ResourcesUtil;
import mn.bitsup.callservice.view.RecyclerViewPager;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.ErrorView;
import mn.bitsup.callservice.view.custom.ErrorViewType;
import mn.bitsup.callservice.view.custom.SelectorPageIndicator;
import mn.bitsup.callservice.view.widget.card.core.CardWidget;
import mn.bitsup.callservice.view.widget.card.core.contract.CardContract;
import mn.bitsup.callservice.view.widget.card.dto.LockStatus;
import mn.bitsup.callservice.view.widget.card.dto.UiCardPayload;
import mn.bitsup.callservice.view.widget.card.selector.CardAdapterItem;
import mn.bitsup.callservice.view.widget.card.selector.CardCarouselAdapter;
import mn.bitsup.callservice.view.widget.card.selector.CardMenuAdapater;
import mn.bitsup.callservice.view.widget.passcode.PasscodeWidgetAuthenticateView;

public class CardManagementWidgetView extends Fragment {
    private static final String TAG = CardManagementWidgetView.class.getSimpleName();

    private CardMenuAdapater menuAdapter;
    private static final int PROGRESS_STATE_VIEW = 0;
    private static final int ERROR_STATE_VIEW = 1;
    private static final int CONTENT_STATE_VIEW = 2;

    private CustomButton bottomButton;
    private Context context;
//    private PasscodeAuthenticateDialog dialog;
    private CardCarouselAdapter cardCarouselAdapter;
    private ErrorView errorView;
    private ViewFlipper viewFlipper;
    private SelectorPageIndicator cardCarouselIndicator;
    private RecyclerViewPager cardCarousel;
    private ListView menuListView;
    private View topDivider;
    private View bottomDivider;
    private LinearLayout container;
    private int currentPosition = 0;
    private int selectedPosition = 0;
    private int activePosition = -2;
    private boolean isPinBlocked = false;
    private boolean isActiveBlocked = false;
    private DialogInterface.OnClickListener positiveButtonClickListener;
    private DialogInterface.OnClickListener negativeButtonClickListener;
    private CardContract contract;


    public CardManagementWidgetView(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contract = new CardWidget(Objects.requireNonNull(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup viewGroup,
        @Nullable Bundle savedInstanceState) {

        View inflatedView = inflate(getContext(), R.layout.card_management_view, viewGroup);
        inflateViews(inflatedView);
        return inflatedView;
    }

    private void inflateViews(View inflatedView) {
        menuListView = inflatedView.findViewById(R.id.menuListView);
        viewFlipper = inflatedView.findViewById(R.id.cardsStateSwitcher);
        container = inflatedView.findViewById(R.id.container);
        cardCarousel = inflatedView.findViewById(R.id.cardCarousel);
        cardCarouselIndicator = inflatedView.findViewById(R.id.cardSelectorPageIndicator);
        bottomButton = inflatedView.findViewById(R.id.buttonActive);
        topDivider = inflatedView.findViewById(R.id.topDivider);
        bottomDivider = inflatedView.findViewById(R.id.bottomDivider);
        errorView = inflatedView.findViewById(R.id.errorView);
        errorView.setErrorViewListener(this::loadCards);
        cardCarouselAdapter = new CardCarouselAdapter(position -> cardCarousel.scrollToPage(position));
        cardCarousel.setAdapter(cardCarouselAdapter);
        cardCarousel.addOnPageChangedListener((int activePosition) -> {
            CardAdapterItem cardAdapterItem = cardCarouselAdapter.getItem(activePosition);
            cardCarouselIndicator.activatePage(activePosition);
            updateCardActions(cardAdapterItem);
        });
    }

    private void updateCardActions(@NonNull final CardAdapterItem cardItem) {
        menuAdapter = new CardMenuAdapater(getContext(), cardItem);
        menuListView.setAdapter(menuAdapter);
        cardCarousel.addOnPageChangedListener((int activePosition) -> {
            selectedPosition = activePosition;
        });
        topDivider.setVisibility(cardItem.isInactive() ? View.GONE : View.VISIBLE);
        bottomDivider.setVisibility(cardItem.isInactive() ? View.GONE : View.VISIBLE);
        menuListView.setOnItemClickListener((parent, view, position, id) -> {
            publish(position, cardItem);
        });
        if (cardItem.getCard().getAdditions() != null) {
            if (Objects.equals(cardItem.getCard().getAdditions().get("CARD_TYPE"), "V")) {
                bottomButton.setVisibility(View.VISIBLE);
                bottomButton.setText(R.string.card_order_button_label);
                menuListView.setVisibility(View.GONE);
                bottomDivider.setVisibility(View.GONE);
                topDivider.setVisibility(View.GONE);
                bottomButton.setOnClickListener(v -> requestReplacement(cardItem));
            } else if (cardItem.isInactive() && Objects
                .equals(cardItem.getCard().getAdditions().get("CARD_TYPE"), "E")) {
                bottomButton.setVisibility(View.VISIBLE);
                bottomButton.setText(R.string.cards_active_button_text);
                menuListView.setVisibility(View.VISIBLE);
                topDivider.setVisibility(View.GONE);
                bottomDivider.setVisibility(View.GONE);
                View.OnClickListener activateCardListener = v -> activateCard(cardItem);
                bottomButton.setOnClickListener(activateCardListener);
            } else if (Objects.equals(cardItem.getCard().getAdditions().get("CARD_TYPE"), "P")) {
                bottomButton.setVisibility(View.GONE);
                menuListView.setVisibility(View.GONE);
                topDivider.setVisibility(View.GONE);
                bottomDivider.setVisibility(View.GONE);
            } else {
                bottomButton.setVisibility(View.GONE);
                menuListView.setVisibility(View.VISIBLE);
            }
        }
    }

    public void updateViewWithUiCard(@NonNull UiCardPayload uiCardPayload) {

    }

    public void showReplacementSuccessMessage(@NonNull String reason) {
        bottomButton.setVisibility(View.GONE);
        cardCarousel.performOnActivePosition(activePosition -> {
            this.activePosition = activePosition;
        });
        String title = getContext().getString(R.string.card_order_success_text);
        NudgeControllerUtil.showSuccessNotification(title, "", getContext());
    }

    private void activateCard(@NonNull final CardAdapterItem cardItem) {
        HashMap<String, String> blockParams = new HashMap<>();
        blockParams.put("value", cardItem.getCard().getGuid());
        blockParams.put("macaddress", DeviceAddressUtil.getMACAddress(getContext(), "wlan0"));
        blockParams.put("ipaddress", DeviceAddressUtil.getIPAddress(true));
        contract.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                if (!var1.getData().isEmpty()) {
                    for (BlockData block :
                            var1.getData()) {
                        if (block.getBlocktype() == 9
                                && block.getBlockchannel() == 9
                                && block.getBlockinfo().equals(cardItem.getCard().getGuid())) {
                            CustomAlertDialog.showAlertDialog(getContext(), getContext().getString(R.string.shared_alert_generic_alert_error_title),
                                    block.getBlockmsg());
                            isActiveBlocked = true;
                        }
                    }
                }
                if (!isActiveBlocked) {
                    @NonNull UiCardPayload uiCardPayload = toUiCardPayload(cardItem);
                    uiCardPayload.setMode("ACTIVE");
                    contract.onCardActivationStart(uiCardPayload);
                }
            }

            @Override
            public void onError(Response var1) {
                NudgeControllerUtil.showErrorNotification(getContext().getString(R.string.shared_alert_generic_alert_error_title),
                        getContext().getString(R.string.shared_alert_generic_error_server), getContext());
            }
        }, blockParams);

    }

    public void showActivationSuccessMessage() {
        cardCarousel.performOnActivePosition(activePosition -> {
            this.activePosition = activePosition;
        });
        bottomButton.setVisibility(View.GONE);
        NudgeControllerUtil.showSuccessNotification(getContext().getResources().getString(R.string.card_active_success_text), "", getContext());
    }

    private void loadCards() {
        viewFlipper.setDisplayedChild(PROGRESS_STATE_VIEW);
        Map<String, String> params = new HashMap<>();
        params.put("cif", getUser().getCif());
        contract.getCardList((CustomParams) MutableParams.wrap(params), new CardsListener() {
            @Override
            public void onSuccess(List<Card> list) {
                List<CardItem> cardItems = new ArrayList<>();
                if (list.isEmpty()) {
                    viewFlipper.setDisplayedChild(ERROR_STATE_VIEW);
                    errorView.show(R.string.cards_empty_title, R.string.cards_empty_message, R.drawable.ic_error_products, false);
                } else {
                    for (Card cardItem : list) {
                        cardItems.add((CardItem) cardItem);
                    }

                    viewFlipper.setDisplayedChild(CONTENT_STATE_VIEW);
                    cardCarouselAdapter.updateWith(cardItems);
                    cardCarouselIndicator.initializeWithPagesCount(list.size());
                    cardCarousel.performOnActivePosition(activePosition -> {
                        cardCarouselIndicator.activatePage(activePosition);
                        CardAdapterItem selectedItem = cardCarouselAdapter.getItem(activePosition);
                        currentPosition = activePosition;
                        updateCardActions(selectedItem);
                    });
                    if (activePosition != -2) {
                        CardAdapterItem selectedItem = cardCarouselAdapter.getItem(activePosition);
                        menuAdapter = new CardMenuAdapater(getContext(), selectedItem);
                        menuListView.setAdapter(menuAdapter);
                        cardCarouselIndicator.activatePage(activePosition);
                        updateCardActions(selectedItem);
                    }
                    if (cardItems.size() == 1) {
                        ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        params.setMargins(ResourcesUtil.dpToPx(40 , getContext().getResources()),0,0,0);
                        cardCarousel.setLayoutParams(params);
                    }
                }
            }

            @Override
            public void onError(Response response) {
                viewFlipper.setDisplayedChild(ERROR_STATE_VIEW);
                errorView.show(response, ErrorViewType.CARDS);
            }
        });
    }

    public void initCardListView() {
        loadCards();

    }

    public void updateViewWithCardAndReason(@NonNull Card card, @NonNull String s) {

    }

    public void updateViewWithCard(@NonNull Card card) {
    }

    private void handleLockUnlock(@NonNull CardAdapterItem cardItem) {
        cardCarouselAdapter.setCardInProgress(cardItem);
        cardCarousel.performOnActivePosition(activePosition -> {
            this.currentPosition = activePosition;
        });
        changeCardStatus(cardItem);
    }

    private void changeCardStatus(CardAdapterItem cardItem) {
        if (cardItem.getCard().getAdditions() != null) {
            final Map<String, String> params = new HashMap<>();
            params.put("guid", cardItem.getCard().getGuid());
            params.put("condition", cardItem.isLocked() ? LockStatus.UNLOCKED.getLockStatusAsStr() : LockStatus.LOCKED.getLockStatusAsStr());
            contract.changeCardStatus(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse var1) {
                    if (selectedPosition == currentPosition) {
                        menuAdapter = new CardMenuAdapater(getContext(), cardItem);
                        menuListView.setAdapter(menuAdapter);
                    }
                    if (cardItem.isLocked())
                        cardCarouselAdapter.unlockCard(cardItem);
                    else {
                        cardCarouselAdapter.lockCard(cardItem);
                    }

                    showSuccess(getContext().getResources().getString(cardItem.isLocked() ? R.string.cards_lock_nudge_success_title : R.string.cards_unlock_nudge_success_title), "");

                }

                @Override
                public void onError(Response var2) {
                    cardCarouselAdapter.unlockCard(cardItem);
                    showError(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server));
                }
            }, params);
        }
    }


    private void showSuccess(String title, String subtitle) {
        NudgeControllerUtil.showSuccessNotification(title, subtitle, getContext());
    }

    private void showError(String title, String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle, getContext());
    }

    private void requestReplacement(@NonNull final CardAdapterItem cardItem) {
        @NonNull UiCardPayload uiCardPayload = toUiCardPayload(cardItem);
        contract.onCardReplacementStart(uiCardPayload);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    private void publish(int position, CardAdapterItem cardAdapterItem) {
        if (!cardAdapterItem.isInactive() && !cardAdapterItem.isInProgress()) {
            switch (position) {
                case 0:
                    if (!cardAdapterItem.isLocked())
                        showEnableDialog(cardAdapterItem.getCard(), position);
                    break;
                case 1:
                    if (!cardAdapterItem.isLocked()) {

                        HashMap<String, String> blockParams = new HashMap<>();
                        blockParams.put("value", cardAdapterItem.getCard().getGuid());
                        blockParams.put("macaddress", DeviceAddressUtil.getMACAddress(getContext(), "wlan0"));
                        blockParams.put("ipaddress", DeviceAddressUtil.getIPAddress(true));
                        contract.checkBlock(new BlockListener() {
                            @Override
                            public void onSuccess(Block var1) {
                                if (!var1.getData().isEmpty()) {
                                    for (BlockData block :
                                            var1.getData()) {
                                        if (block.getBlocktype() == 9
                                                && block.getBlockchannel() == 10
                                                && block.getBlockinfo().equals(cardAdapterItem.getCard().getGuid())) {
                                            CustomAlertDialog.showAlertDialog(getContext(), getContext().getString(R.string.shared_alert_generic_alert_error_title),
                                                    block.getBlockmsg());
                                            isPinBlocked = true;
                                        }
                                    }
                                }
                                if (!isPinBlocked) {
                                    positiveButtonClickListener = (dialog, which) -> showEnableDialog(cardAdapterItem.getCard(), position);
                                    negativeButtonClickListener = (dialog, which) -> dialog.dismiss();
                                    CustomAlertDialog.showAlertDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_button_warning),
                                            getContext().getResources().getString(R.string.card_pin_change_text),
                                            positiveButtonClickListener, negativeButtonClickListener,
                                            getContext().getString(R.string.shared_alert_button_yes),
                                            getContext().getString(R.string.shared_alert_button_no));
                                }
                            }

                            @Override
                            public void onError(Response var1) {
                                NudgeControllerUtil.showErrorNotification(getContext().getString(R.string.shared_alert_generic_alert_error_title),
                                        getContext().getString(R.string.shared_alert_generic_error_server), getContext());
                            }
                        }, blockParams);
                    }
                    break;
                case 2:
                    if (!cardAdapterItem.isLocked()) {
                        positiveButtonClickListener = (dialog, which) -> handleLockUnlock(cardAdapterItem);
                        negativeButtonClickListener = (dialog, which) -> dialog.dismiss();
                        CustomAlertDialog.showAlertDialog(getContext(), getContext().getResources().getString(R.string.card_block_title),
                                getContext().getResources().getString(R.string.card_block_text),
                                positiveButtonClickListener, negativeButtonClickListener,
                                getContext().getString(R.string.shared_alert_button_yes),
                                getContext().getString(R.string.shared_alert_button_no));
                    } else {
                        handleLockUnlock(cardAdapterItem);
                    }
                    break;
//                case 3:
//                    if (contract.getProductAccountList().getProductAccountItems() != null) {
//                        boolean isHave = false;
//                        assert getContract().getProductAccountList().getProductAccountItems() != null;
//                        if (!getContract().getProductAccountList().getProductAccountItems().isEmpty()) {
//                            for (ProductAccountItem productAccountItem : getContract().getProductAccountList().getProductAccountItems()) {
//                                if (productAccountItem.getId().equals(cardAdapterItem.getCard().getMainAccount())) {
//                                    isHave = true;
//                                    if (Double.valueOf(productAccountItem.getAvailableBalance()) <= 5000) {
//                                        NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.card_order_blance_error), "");
//                                        break;
//                                    } else {
//                                        positiveButtonClickListener = (dialog, which) -> requestReplacement(cardAdapterItem);
//                                        negativeButtonClickListener = (dialog, which) -> dialog.dismiss();
//                                        CustomAlertDialog.showAlertDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_button_warning),
//                                                getContext().getResources().getString(R.string.card_reorder_text),
//                                                positiveButtonClickListener, negativeButtonClickListener,
//                                                getContext().getString(R.string.shared_alert_button_ok),
//                                                getContext().getString(R.string.shared_button_back));
//                                        break;
//                                    }
//                                }
//                            }
//                            if (!isHave) {
//                                NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title),
//                                        "Холбогдох данс олдсонгүй");
//                            }
//                        }
//                    } else {
//                        NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title),
//                                "Хэрэглэгчийн мэдээлэл авах үед алдаа гарлаа");
//                    }
//                    break;
            }
        }
    }


    private void showEnableDialog(CardItem cardItem, int position) {
        EventHelper.publish(getContext(), new PasscodeWidgetAuthenticateView());
//
//        dialog = new PasscodeAuthenticateDialog(getContext(), new PasscodeListener() {
//            @Override
//            public void onSuccess() {
//                new Thread(() -> {
//                    hideKeyboard();
//                    if (position == 0) {
//                        EventHelper.publish(EventConstants.CARD_DETAIL, cardItem);
//                    }
//                    if (position == 1) {
//                        UiCardPayload uiCardPayload = new UiCardPayload(cardItem, "PIN");
//                        EventHelper.publish(EventConstants.CARD_PIN_SETUP, uiCardPayload);
//                    }
//                }).start();
//                new Thread(() -> {
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    dialog.dismiss();
//
//                }).start();
//            }
//
//            @Override
//            public void onError(Response response) {
//            }
//
//            @Override
//            public void passcodeMatchingFailed() {
//            }
//        });
//
//        dialog.show();
    }

    @NonNull
    private static UiCardPayload toUiCardPayload(@NonNull CardAdapterItem cardItem) {
        @NonNull CardItem card = cardItem.getCard();
        @ColorRes int backgroundColor = cardItem.getBackgroundColor();
        return new UiCardPayload(card, backgroundColor);
    }


    private void checkBlock(){

    }

}
