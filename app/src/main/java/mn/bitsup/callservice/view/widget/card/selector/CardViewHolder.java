package mn.bitsup.callservice.view.widget.card.selector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Px;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Objects;
import mn.bitsup.callservice.R;


/**
 * Created by Erdenedalai R&D B.V. on 06/02/2019.
 * ViewHolder class for the Card carousel's RecyclerView
 */
public class CardViewHolder extends RecyclerView.ViewHolder {

    private final View backgroundOverlay;
    private final ProgressBar progressBar;
    private final ImageView statusIcon;
    private final TextView statusNameView;
    private final TextView textCardUsername;
    private final TextView textCardNumber;
    private final TextView textCardDate;
    private final RelativeLayout card_center;

    CardViewHolder(@NonNull View cardLayout) {
        super(cardLayout);
        backgroundOverlay = cardLayout.findViewById(R.id.backgroundOverlay);
        card_center = cardLayout.findViewById(R.id.card_center);
        progressBar = cardLayout.findViewById(R.id.progressBar);
        statusIcon = cardLayout.findViewById(R.id.statusIcon);
        statusNameView = cardLayout.findViewById(R.id.statusName);
        textCardUsername = cardLayout.findViewById(R.id.textCardUsername);
        textCardNumber = cardLayout.findViewById(R.id.textCardNumber);
        textCardDate = cardLayout.findViewById(R.id.textCardDate);

    }

    @SuppressLint({"SetTextI18n", "WrongConstant"})
    public void bind(@NonNull CardAdapterItem cardAdapterItem,
                     @NonNull CardCarouselAdapter.OnItemClickListener onItemClickListener) {
        @NonNull Context context = itemView.getContext();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        @Px int card_width = Double.valueOf(width*0.8).intValue();
        @Px int card_height = Double.valueOf(card_width*0.58).intValue();
        itemView.setLayoutParams(new RecyclerView.LayoutParams(card_width, card_height));

        if (cardAdapterItem.isInProgress()) {
            backgroundOverlay.setVisibility(View.VISIBLE);
            statusIcon.setVisibility(View.GONE);
            statusNameView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);

        } else if (cardAdapterItem.isCancelled()) {
            backgroundOverlay.setVisibility(View.VISIBLE);
            statusIcon.setVisibility(View.VISIBLE);
            statusNameView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            statusIcon.setImageResource(R.drawable.ic_do_not_disturb_white_48dp);
            statusNameView.setText(R.string.card_not_active_label);

        } else if (cardAdapterItem.isInactive() || Objects
            .equals(Objects.requireNonNull(cardAdapterItem.getCard().getAdditions()).get("CARD_TYPE"), "P")) {
            backgroundOverlay.setVisibility(View.VISIBLE);
            statusIcon.setVisibility(View.VISIBLE);
            statusNameView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            card_center.setVisibility(View.INVISIBLE);
            statusNameView.setText(R.string.card_not_active_label);
            if(Objects.equals(Objects.requireNonNull(cardAdapterItem.getCard().getAdditions()).get("CARD_TYPE"), "P")){
                statusNameView.setText(R.string.card_ordered);
            }
            statusIcon.setImageResource(R.drawable.ic_do_not_disturb_white_48dp);

        } else if (cardAdapterItem.isLocked() && Objects
            .equals(Objects.requireNonNull(cardAdapterItem.getCard().getAdditions()).get("CARD_TYPE"), "V")){
            backgroundOverlay.setVisibility(View.VISIBLE);
            statusIcon.setVisibility(View.VISIBLE);
            statusNameView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            statusIcon.setImageResource(R.drawable.ic_do_not_disturb_white_48dp);
            statusNameView.setText(R.string.card_not_active_label);

        }  else if (cardAdapterItem.isLocked() && !Objects
            .equals(Objects.requireNonNull(cardAdapterItem.getCard().getAdditions()).get("CARD_TYPE"), "P")){
            backgroundOverlay.setVisibility(View.VISIBLE);
            statusIcon.setVisibility(View.VISIBLE);
            statusNameView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            statusIcon.setImageResource(R.drawable.lock_outline);
            statusNameView.setText(R.string.cards_status_locked);

        } else {
            card_center.setVisibility(View.VISIBLE);
            backgroundOverlay.setVisibility(View.GONE);
            statusIcon.setVisibility(View.GONE);
            statusNameView.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
        textCardUsername.setText(cardAdapterItem.getCard().getNameOfCard());
        textCardNumber.setText(cardAdapterItem.getCard().getPan());
        textCardDate.setText("VALID THRU \n" +cardAdapterItem.getCard().getExpirationDate());
        itemView.setOnClickListener(v -> onItemClickListener.onItemClicked(getAdapterPosition()));
    }
}
