package mn.bitsup.callservice.view.widget.card.selector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.card.dto.CardReasonItem;

public class CardReasonAdapter  extends BaseAdapter {
    private List<CardReasonItem> reasonTypes;
    private Context context;
    private OnItemSelectListener onItemSelectListener;
    public CardReasonAdapter(List<CardReasonItem> reasonTypes, Context mContext, OnItemSelectListener onItemSelectListener) {
        this.reasonTypes = reasonTypes;
        this.context = mContext;
        this.onItemSelectListener = onItemSelectListener;
    }
    @Override
    public int getCount() {
        return reasonTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return reasonTypes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint({"ViewHolder", "InflateParams"})
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        View view = layoutInflater.inflate(R.layout.card_reason_item, null);
        view.setOnClickListener(v -> onItemSelectListener.onReasonTypeSelected(reasonTypes.get(position)));
        TextView textView = view.findViewById(R.id.textView);
        textView.setText(reasonTypes.get(position).getName());
        return view;
    }
    public interface OnItemSelectListener {
        void onReasonTypeSelected(CardReasonItem reasonType);
    }
}
