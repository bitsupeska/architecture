package mn.bitsup.callservice.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.CallSuper;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.databinding.library.baseAdapters.BuildConfig;

//import mn.ashidcapital.simple.BuildConfig;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.UserInteractionTimers;

public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = "BaseActivity";
    public static Context context;
    public static Activity activity;
    private BackButtonStyle backButtonStyle = BackButtonStyle.BACK;

    @CallSuper
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        activity = BaseActivity.this;
        if (!BuildConfig.DEBUG) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.background));
        }

        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
    }

    protected BackButtonStyle getBackButtonStyle() {
        return backButtonStyle;
    }

    protected void configureToolbarNavigation(Toolbar toolbar) {
        switch (getBackButtonStyle()) {
            case CLOSE:
                toolbar.setNavigationIcon(R.drawable.ic_close_cross);
                toolbar.setNavigationContentDescription("Хаах");
                break;
            case BACK:
                Log.e("asd", "back");
                toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
                toolbar.setNavigationContentDescription("Буцах");
                break;
            default:
                //Do nothing
        }
    }

    public enum BackButtonStyle {
        BACK,
        CLOSE
    }

    @Override
    public void onUserInteraction() {
//        if (UserInteractionTimers.isLogged) {
//            UserInteractionTimers.restart();
//        }
//        if (UserInteractionTimers.isRunning) {
//            if (!RefreshService.isRunning) {
//                stopService(new Intent(this, RefreshService.class));
//                refreshToken();
//                startService(new Intent(this, RefreshService.class));
//            }
//        }
    }

    @Override
    protected void onResume() {
//        if (UserInteractionTimers.isLogged)
//            if (UserInteractionTimers.isRunning) {
//                if (!RefreshService.isRunning) {
//                    stopService(new Intent(this, RefreshService.class));
//                    refreshToken();
//                    startService(new Intent(this, RefreshService.class));
//                }
//            }
//        else {
//                if (UserInteractionTimers.isNotFinish) {
//                    UserInteractionTimers.isLogged = false;
//                    UserInteractionTimers.endTimer();
//                    UserInteractionTimers.logOff();
//                }
//            }
        super.onResume();
    }

    private void refreshToken() {
        MainApplication.authClient.performAutomatedLogin(new PasscodeAuthListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Response var1) {
            }
        });
    }
}
