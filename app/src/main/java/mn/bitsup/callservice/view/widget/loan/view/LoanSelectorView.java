package mn.bitsup.callservice.view.widget.loan.view;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.client.loan.dto.SysDate;
import mn.bitsup.callservice.client.loan.listener.SysDateListener;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.MyScrollView;

import io.feeeei.circleseekbar.CircleSeekBar;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public class LoanSelectorView extends Fragment {

    private static final String TAG = "LoanSelectorView";

    private Context context;
    private LoanContract contract;
    private Account account;
    private TextView loan_amount, fee_percent, monthOne, monthTwo, monthThree;
    private CustomButton buttonContinue;
    private DatePickerDialog datePickerDialog;
    private RelativeLayout cardview_repayment_details;
    private Calendar calendar;
    private CircleSeekBar circleSeekBar;
    private TextView repayment_value;
    private TextView textDate;
    private LoanOffer loanOffer;
    private String global_date;
    private Loan loan;
    private Double floatAmount;
    private int amountValue = 0;
    private CardView cardViewMonth1, cardViewMonth2, cardViewMonth3;
    private RelativeLayout relativeMonth1, relativeMonth2, relativeMonth3;
    private MyScrollView scrollView;
    private LinearLayout mainLayout;
    private int selectedMonth = 1;
    private int maxTenor = 1;

    private int currentTenor = 1;
    private String startDate = "";

    public LoanSelectorView(LoanOffer loanOffer) {
        this.loanOffer = loanOffer;
        this.maxTenor = Integer.parseInt(loanOffer.getData().getMax_tenor());
        this.selectedMonth = maxTenor;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.account = EventHelper.getEventPayload("account", Account.class);
        this.loan = contract.getLoan();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_selector_layout, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        circleSeekBar = inflatedView.findViewById(R.id.circleSeekBar);
        loan_amount = inflatedView.findViewById(R.id.loan_amount);
        fee_percent = inflatedView.findViewById(R.id.fee_percent);
        repayment_value = inflatedView.findViewById(R.id.repayment_value);
        monthOne = inflatedView.findViewById(R.id.month_one);
        monthTwo = inflatedView.findViewById(R.id.month_two);
        monthThree = inflatedView.findViewById(R.id.month_three);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        textDate = inflatedView.findViewById(R.id.textDate);
        cardview_repayment_details = inflatedView.findViewById(R.id.cardview_repayment_details);
        cardViewMonth1 = inflatedView.findViewById(R.id.cardView_month1);
        cardViewMonth2 = inflatedView.findViewById(R.id.cardView_month2);
        cardViewMonth3 = inflatedView.findViewById(R.id.cardView_month3);
        relativeMonth1 = inflatedView.findViewById(R.id.relativeMonth1);
        relativeMonth2 = inflatedView.findViewById(R.id.relativeMonth2);
        relativeMonth3 = inflatedView.findViewById(R.id.relativeMonth3);
        scrollView = inflatedView.findViewById(R.id.scrollView);
        mainLayout = inflatedView.findViewById(R.id.main_layout);

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {
        fee_percent.setText(loanOffer.getData().getFinal_rate() / 12 + "%");
        currentTenor = calculateMinTenor();
        setMonthSelection(currentTenor);

        circleSeekBar.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                scrollView.setScrolling(false);
                return true;
            }
            return false;

        });

        mainLayout.setOnTouchListener((v, event) -> {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                scrollView.setScrolling(true);
                return true;
            }
            return false;
        });
        getSysDate();

    }

    private void setMonthSelection(int month) {
        switch (month) {
            case 1:
                if (maxTenor >= month)
                    setMonthOneEnabled();
                break;
            case 2:
                if (maxTenor >= month)
                    setMonthTwoEnabled();
                break;

            case 3:
                if (maxTenor >= month)
                    setMonthThreeEnabled();
                break;
        }
    }


    private void enableCardViewListener(int month) {
        switch (month) {
            case 1:
                monthOne.setAlpha((float) 1);
                monthTwo.setAlpha((float) 0.15);
                monthThree.setAlpha((float) 0.15);
                setMonthOneEnabled();
                cardViewMonth1.setOnClickListener(v -> {
                    setMonthOneEnabled();
                    calcMonthlyPayment(amountValue);
                    selectedMonth = 1;
                });

                cardViewMonth2.setOnClickListener(v -> {
                    setMonthTwoEnabled();
                    calcMonthlyPayment(amountValue);
                    selectedMonth = 2;
                });
                cardViewMonth3.setOnClickListener(v -> {
                    setMonthThreeEnabled();
                    calcMonthlyPayment(amountValue);
                    selectedMonth = 3;
                });

                break;
            case 2:
                monthOne.setAlpha((float) 0.15);
                monthTwo.setAlpha((float) 1);
                monthThree.setAlpha((float) 1);

                if (selectedMonth == 1) {
                    setMonthTwoEnabled();
                    selectedMonth = 2;
                }
                cardViewMonth1.setOnClickListener(null);
                cardViewMonth2.setOnClickListener(v -> {
                    setMonthTwoEnabled();
                    calcMonthlyPayment(amountValue);
                    selectedMonth = 2;
                });
                cardViewMonth3.setOnClickListener(v -> {
                    setMonthThreeEnabled();
                    calcMonthlyPayment(amountValue);
                    selectedMonth = 3;
                });
                break;
            case 3:
                monthOne.setAlpha((float) 0.15);
                monthTwo.setAlpha((float) 0.15);
                monthThree.setAlpha((float) 1);

                if (selectedMonth <= 3) {
                    setMonthThreeEnabled();
                    selectedMonth = 3;
                }
                cardViewMonth1.setOnClickListener(null);
                cardViewMonth2.setOnClickListener(null);
                cardViewMonth3.setOnClickListener(v -> {
                    setMonthThreeEnabled();
                    calcMonthlyPayment(amountValue);
                    selectedMonth = 3;
                });
                break;

        }
    }


    private void openBottomSheet() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());

        HashMap<String, String> param = new HashMap<>();
        param.put("installStartDate", global_date);
        param.put("installType", "M");
        param.put("intRate", "" + loanOffer.getData().getFinal_rate());
        param.put("loanAmt", "" + amountValue);
        param.put("loanDuration", "" + currentTenor);
        param.put("noOfInstall", "" + currentTenor);
        param.put("schemaCode", loanOffer.getData().getPk_product_cd());

        contract.getLoanDueSchedule(new LoanDueScheduleListener() {
            @Override
            public void onSuccess(List<LoanDueScheduleData> data) {
                setLoanData();
                LoanBottomSheetRepaymentPlan bottomSheetRepaymentPlan = LoanBottomSheetRepaymentPlan
                        .newInstance(data, "LoanSelector");
                bottomSheetRepaymentPlan
                        .show(getActivity().getSupportFragmentManager(), "bottom_sheet");
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }

            @Override
            public void onError(Response var1) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, param);
    }

    private void setLoanData() {

        loan.setLoanAmount("" + (amountValue)); // hasna
        loan.setLoanMonthCount("" + currentTenor);
        loan.setRate("" + loanOffer.getData().getFinal_rate()); //hasna
        loan.setRePaymentMonthCount("" + currentTenor); // hasna
        DecimalFormat formatter = new DecimalFormat("0.00");
        loan.setRePaymentScheduledAmount(formatter.format(floatAmount) + ""); //hasna
        Log.e("uno", "setLoanData: " + loan.getRePaymentScheduledAmount());
        contract.setLoan(loan);
    }

    private void configureSeekbar() {
        circleSeekBar.setMaxProcess((int) loanOffer.getData().getFinal_limit());
        setProgressValue((int) loanOffer.getData().getFinal_limit());
        circleSeekBar.setCurProcess((int) loanOffer.getData().getFinal_limit());
        Log.e(TAG, "configureSeekbar: " + loanOffer.getData().getProdMinLimit());
    }

    private void setProgressValue(int value) {
        value = Math.round(value / 10000) * 10000;
        amountValue = value;
        DecimalFormat formatter = new DecimalFormat("##,###,###");
        loan_amount.setText("" + formatter.format(value));

        calculateMinTenor();
        calcMonthlyPayment(amountValue);
    }

    private int calculateMinTenor() {
        double r = loanOffer.getData().getFinal_rate() / 1200;
        double temp0 = (loanOffer.getData().getFinal_limit() * Math.pow((1 + r), loanOffer.getData().getProdMaxDur()) * r) / (Math.pow((1 + r), loanOffer.getData().getProdMaxDur()) - 1);
        double divider = 1 - (amountValue * r / temp0);
        Log.e(TAG, "calculateMinTenor: divider: " + amountValue * r);
        Log.e(TAG, "calculateMinTenor: divider2: " + repayment_value.getText().toString()
                .replaceAll("\\D+", ""));
        double dividerResult = 1 / divider;
        Log.e(TAG, "calculateMinTenor: divider result: " + dividerResult);
        Log.e(TAG, "calculateMinTenor: " + customLog(1 + r, dividerResult));
        int minTenor = (int) Math.ceil(customLog(1 + r, dividerResult));

        if (minTenor < 1) {
            minTenor = 1;
        } else if (minTenor > maxTenor) {
            minTenor = 3;
        }

        if (minTenor >= maxTenor) {
            if (maxTenor > 1)
                minTenor = maxTenor - 1;
        }
        enableCardViewListener(minTenor);
        return minTenor;
    }


    private double customLog(double base, double logNumber) {
        return Math.log(logNumber) / Math.log(base);
    }

    @SuppressLint("SetTextI18n")
    private void calcMonthlyPayment(int amount) {
        try {
            double doubleAmount = Double.parseDouble(amount + "");
            Double p = 1.0;
            long days0 = 1;
            if (currentTenor == 1) {
                days0 = calculateDays0(loan.getBeginDate());
                p = Double.parseDouble(days0 + "") / 365;
            } else {
                p /= 12;
            }
            double rateMonth = loanOffer.getData().getFinal_rate() * p;
            double TMP = 1 + rateMonth / 100;
            double NUM = TMP - 1;
            double DEM = 1 - Math.pow(TMP, -currentTenor);
            Double PMT = doubleAmount * NUM / DEM;
            floatAmount = PMT;
            repayment_value.setText("₮" + CurrencyConverter.getFormattedCurrencyString(PMT));
        } catch (Exception e) {
            CustomAlertDialog.showAlertDialog(context, context.getString(R.string.shared_alert_generic_alert_error_title), "Эргэн төлөх дүн бодох үед алдаа гарлаа");
            buttonContinue.setState(ButtonState.DISABLED);
            Log.e(TAG, "calcMonthlyPayment: " + e.getMessage());
        }
    }

    private long calculateDays0(String endStr) {
        SimpleDateFormat startFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDateValue = null;
        Date endDateValue = null;
        try {
            startDateValue = startFormat.parse(startDate);
            endDateValue = startFormat.parse(endStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = endDateValue.getTime() - startDateValue.getTime();
        Log.e(TAG, "Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    private void getSysDate() {
        contract.getSysDate(new SysDateListener() {
            @Override
            public void onSuccess(SysDate response) {
                if (response.getCode() == 200) {

                    startDate = response.getSysDate();
                    setDate();
//                    if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
                    configureSeekbar();
                    circleSeekBar.setOnSeekBarChangeListener((seekbar, curValue) -> {
                        if (curValue < loanOffer.getData().getProdMinLimit()) {
                            setProgressValue(loanOffer.getData().getProdMinLimit());
                            circleSeekBar.setCurProcess(loanOffer.getData().getProdMinLimit());
                        } else {
                            setProgressValue(curValue);

                        }
                    });
                    buttonContinue.setOnClickListener(v -> openBottomSheet());


                }
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }

    private void setDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = null;
        try {
            convertedDate = dateFormat.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(convertedDate);
        c.add(Calendar.MONTH, 1);

        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = 1 + c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        String stringDay = day + "";
        String stringMonth = month + "";
        String stringYear = year + "";


        if (day < 10) stringDay = "0" + day;
        if (month < 10) stringMonth = "0" + month;

        global_date = stringYear + "-" + stringMonth + "-" + stringDay;
        loan.setBeginDate(global_date);
        loan.setBeginDay(stringDay); //
        textDate.setText("Сар бүрийн " + stringDay + "-нд");

        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        loan.setLastDayOfMonth(c.get(Calendar.DAY_OF_MONTH) + "");
        calcMonthlyPayment(amountValue);
    }

    private void setMonthOneEnabled() {
        if (maxTenor >= 1) {
            currentTenor = 1;
            relativeMonth1.setBackground(getResources().getDrawable(R.drawable.rounded_button));
            relativeMonth2.setBackgroundColor(getResources().getColor(R.color.background));
            relativeMonth3.setBackgroundColor(getResources().getColor(R.color.background));
        }
    }

    private void setMonthTwoEnabled() {
        if (maxTenor >= 2) {
            currentTenor = 2;
            relativeMonth1.setBackgroundColor(getResources().getColor(R.color.background));
            relativeMonth2.setBackground(getResources().getDrawable(R.drawable.rounded_button));
            relativeMonth3.setBackgroundColor(getResources().getColor(R.color.background));
        }
    }

    private void setMonthThreeEnabled() {
        if (maxTenor >= 3) {
            currentTenor = 3;
            relativeMonth1.setBackgroundColor(getResources().getColor(R.color.background));
            relativeMonth2.setBackgroundColor(getResources().getColor(R.color.background));
            relativeMonth3.setBackground(getResources().getDrawable(R.drawable.rounded_button));
        }
    }
}