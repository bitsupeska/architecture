package mn.bitsup.callservice.view.widget.card.selector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import mn.bitsup.callservice.R;

public class CardMenuAdapater extends BaseAdapter {
    private String[] menu_string = {"Картын мэдээлэл", "Картын ПИН Солих", "Карт блоклох", "Карт дахин захиалах"};
    private int[] icon_int = {R.drawable.info_outline, R.drawable.dialpad, R.drawable.lock_outline_grey, R.drawable.message};
    private Context context;
    private CardAdapterItem cardAdapterItem;
    private static String TAG = CardMenuAdapater.class.getName();
    public CardMenuAdapater(Context context, CardAdapterItem cardAdapterItem) {
        this.context = context;
        this.cardAdapterItem = cardAdapterItem;
    }

    @Override
    public int getCount() {
        return menu_string.length;
    }

    @Override
    public String getItem(int position) {
        return menu_string[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItem(String name, int resId, int position) {
        menu_string[position] = name;
        icon_int[position] = resId;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(R.layout.card_menu, parent, false);
        TextView textView = convertView.findViewById(R.id.textView);
        ImageView iconImage = convertView.findViewById(R.id.imageView);
        ImageView arrowButton = convertView.findViewById(R.id.arrowButton);
        Log.e(TAG, "getView: "+cardAdapterItem );
        if (cardAdapterItem != null) {
            if (cardAdapterItem.isInactive()){
                textView.setTextColor(context.getResources().getColor(R.color.text_grey_opacity));
                iconImage.setColorFilter(context.getResources().getColor(R.color.image_grey_opacity));
                arrowButton.setColorFilter(context.getResources().getColor(R.color.image_grey_opacity));
            }else {
                if (position == 0 || position == 1) {
                    textView.setTextColor(cardAdapterItem.isLocked() ? context.getResources().getColor(R.color.text_grey_opacity) : context.getResources().getColor(R.color.text_black));
                    iconImage.setColorFilter(cardAdapterItem.isLocked() ? context.getResources().getColor(R.color.image_grey_opacity) : context.getResources().getColor(R.color.blue_grey));
                    arrowButton.setColorFilter(cardAdapterItem.isLocked() ? context.getResources().getColor(R.color.image_grey_opacity) : context.getResources().getColor(R.color.blue_grey));
                }
            }
            iconImage.setImageResource(icon_int[position]);
            textView.setText(menu_string[position]);
            if (position == 2) {

                if (cardAdapterItem.isLocked()) {
                    iconImage.setImageResource(R.drawable.lock_open);
                    textView.setText("Блок гаргах");
                }
            }
        }


        return convertView;
    }
}
