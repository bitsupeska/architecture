package mn.bitsup.callservice.view.widget.loan.core;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.info.InfoClient;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.loan.LoanClient;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.client.loan.listener.LoanCreateInvoiceListener;
import mn.bitsup.callservice.client.loan.listener.LoanIsAllowedListener;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.loan.listener.LoanSalaryListener;

import java.util.List;

import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelCreateListener;
import mn.bitsup.callservice.client.loan.listener.LoanRelListener;
import mn.bitsup.callservice.client.loan.listener.SysDateListener;
import mn.bitsup.callservice.client.profile.ProfileClient;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public class LoanWidget implements LoanContract {


    private ProfileClient profileClient;
    private Context context;
    private InfoClient infoClient;
    private LoanClient loanClient;

    public LoanWidget(Context context) {
        this.context = context;
        profileClient = ClientsManager.getProfileClient();
        infoClient = ClientsManager.getInfoClient();
        this.loanClient = ClientsManager.getLoanClient();
    }

    @Override
    public void getAccountLists(AccountListener listener, String params) {
        profileClient.getAccountList(new AccountListener() {
            @Override
            public void onSuccess(List<Account> var1) {
                listener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                listener.onError(var1);

            }
        }, params);
    }

    @Override
    public void getBanks(BankListener listener, Map<String, String> params) {
        this.infoClient.getBank(listener, params);
    }

    @Override
    public void getCheckCitizenInfo(CitizenInfoListener listener, Map<String, String> params) {
        infoClient.getCheckCitizenInfo(params, listener);
    }

    @Override
    public void getLastOffer(Map<String, String> var1, LoanOfferListener var2) {
        loanClient.getLastOffer(var1, var2);
    }

    @Override
    public void getRelated(LoanRelListener loanRelCreateListener) {
        loanClient.getRelStatLookup(loanRelCreateListener);
    }


    @Override
    public void createRelated(LoanRelCreateListener loanRelListener, Map<String, String> params) {
        loanClient.addRelStatLookup(params, loanRelListener);
    }

    public void getSalary(Map<String, String> var1, LoanSalaryListener var2) {
        loanClient.getSalary(var1, var2);
    }

    @Override
    public void getOffer(Map<String, String> var1, LoanOfferListener var2) {
        loanClient.getOffer(var1, var2);
    }

    @Override
    public void getLoanAccount(Map<String, String> var1, LoanAccountListener var2) {
        loanClient.getLoanAccount(var1, var2);
    }

    @Override
    public void getLoanDueSchedule(LoanDueScheduleListener var2, Map<String, String> var1) {
        loanClient.getLoanDueSchedule(var1, var2);
    }

    @Override
    public void getLoanFindAmortSchedule(LoanDueScheduleListener var2, String var1) {
        loanClient.getLoanFindAmortSchedule(var1, var2);
    }

    @Override
    public void createLoan(Loan var1, StatusListener var2) {
        loanClient.createLoan(var1, var2);
    }

    @Override
    public void setLoan(Loan loan) {
        LoanActivity.loan = loan;
    }

    @Override
    public Loan getLoan() {
        return LoanActivity.loan;
    }

    @Override
    public void createLoanInvoice(Map<String, String> queryParams, LoanCreateInvoiceListener loanCreateInvoiceListener) {
        this.loanClient.createLoanInvoice(queryParams, loanCreateInvoiceListener);
    }

    @Override
    public void getSysDate(SysDateListener var2) {
        this.loanClient.getSysDate(var2);
    }


    @Override
    public void getLoanAllowed(LoanIsAllowedListener var2, HashMap<String, String> params) {
        this.loanClient.getLoanAllowed(var2, params);
    }
}
