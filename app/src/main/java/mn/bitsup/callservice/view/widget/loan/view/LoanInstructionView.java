package mn.bitsup.callservice.view.widget.loan.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class LoanInstructionView extends Fragment implements
    OnboardView<OnboardContract> {

    private CustomButton buttonContinue;
    private LottieAnimationView animationView;
    private Context context;
    private LoanContract contract;
    private User user;
    private FrameLayout container;
    private WidgetListView widgetListView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.user = MainApplication.getUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_instruction, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        animationView = inflatedView.findViewById(R.id.animation);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        container = inflatedView.findViewById(R.id.container);
        buttonContinue.setEnabledText("Зээлийн эрх шалгах");
        buttonContinue.setOnClickListener(v -> callKhur());
    }

    private void callKhur() {
        Intent intent = new Intent(context, LoanActivity.class);
        intent.putExtra("type", "finger");
        startActivityForResult(intent, 1);
    }

    private void initView() {
        setAnimation();
    }

    private void setAnimation() {
        animationView.setAnimation("loan.json");
        animationView.playAnimation();
    }


    private void loadingProgress() {
        container.setVisibility(View.GONE);
        widgetListView.setVisibility(View.VISIBLE);
        widgetListView.showCenterProgress();
    }

    private void stopProgress() {
        container.setVisibility(View.VISIBLE);
        widgetListView.showCenterProgress();
        widgetListView.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        user = MainApplication.getUser();
        if (requestCode == 1) {
            if (user.getCif() != null && !user.getCif().equals("0")) {
                loadingProgress();
                EventHelper.publish(context, new LoanLastOfferView());
            }
        }
    }
}