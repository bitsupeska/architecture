package mn.bitsup.callservice.view.widget.loan.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.listener.LoanOfferListener;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.RelatedContacts;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

import static mn.bitsup.callservice.view.widget.loan.view.LoanOfferView.loanOffer;

public class LoanDetailsView extends Fragment implements OnboardView<OnboardContract> {

    private Context context;
    private StorageComponent component;
    private LoanContract contract;
    private CustomButton buttonContinue;
    private DataCell cellAmount, cellDuration, cellRate, cellFee, cellMonthlyPayment, cellFullName, cellBankName, cellAccountNumber;
    private Loan loan;
    private User user;
    private Account account;

    LoanDetailsView(Account account) {
        this.account = account;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.loan = contract.getLoan();
        this.component = new StorageComponent();
        this.user = MainApplication.getUser();
        this.loan = getLoanParams();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_details, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        cellAmount = inflatedView.findViewById(R.id.cellAmount);
        cellDuration = inflatedView.findViewById(R.id.cellDuration);
        cellRate = inflatedView.findViewById(R.id.cellRate);
        cellFee = inflatedView.findViewById(R.id.cellFee);
        cellMonthlyPayment = inflatedView.findViewById(R.id.cellMonthlyPayment);
        cellFullName = inflatedView.findViewById(R.id.cellFullName);
        cellBankName = inflatedView.findViewById(R.id.cellBankName);
        cellAccountNumber = inflatedView.findViewById(R.id.cellAccountNumber);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
    }

    private void initView() {
        if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        cellAmount.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Float.parseFloat(loan.getLoanAmount())));
        cellDuration.setValueText(loan.getLoanMonthCount() + " сар");
        cellRate.setValueText((Double.valueOf(loan.getRate()) / 12) + "%");
        if (loanOffer.getData().getCoreFee() != null) {
            cellFee.setValueText("₮ " + loanOffer.getData().getCoreFee().getFixed_amt());
        } else {
            cellFee.setValueText("₮ 0");
        }
        cellMonthlyPayment.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Double.parseDouble(loan.getRePaymentScheduledAmount())));
        cellFullName.setValueText(loan.getFirstName());
        cellBankName.setValueText(account.getBank_name());
        cellAccountNumber.setValueText(account.getAccount_num());
        buttonContinue.setOnClickListener(v -> checkAddress());
    }

    private void checkAddress() {
        Addresses address = new Addresses();
        if (this.user.getAddresses() != null) {
            for (int i = 0; i < this.user.getAddresses().size(); i++) {
                if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                    address = user.getAddresses().get(i);
                }
            }
        }

        if (address.getLocalityId() != null) {
            Log.e("LoanDetails", "localityId: " + address.getLocalityId());
            Log.e("LoanDetails", "localityId: " + address.getLocality());
            if (equalsLocalityId(address.getTownId() + address.getLocalityId())) {
                getLastOffer();
            } else {
                showErrorDialog("Манайх одоогоор зөвхөн Улаанбаатар хотын Баянгол, Баянзүрх, Сонгино хайрхан, Сүхбаатар, Чингэлтэй, Хан-Уул дүүрэг бүртгэлтэй харилцагч нарт үйлчилгээ үзүүлж байгааг анхаарна уу.");
            }
        } else {
            showErrorDialog("Манайх одоогоор зөвхөн Улаанбаатар хотын Баянгол, Баянзүрх, Сонгино хайрхан, Сүхбаатар, Чингэлтэй, Хан-Уул дүүрэг бүртгэлтэй харилцагч нарт үйлчилгээ үзүүлж байгааг анхаарна уу.");
        }
    }

    private Boolean equalsLocalityId(String localityId) {
        switch (localityId) {
            case "117":
            case "1110":
            case "1116":
            case "1119":
            case "1122":
            case "1125":
                return true;
        }
        return false;
    }

    private void getLastOffer() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        String monPayment = loan.getLoanAmount();
        String chosenPeriod = loan.getLoanMonthCount();
        Map<String, String> params = new HashMap<>();
        params.put("cifId", user.getCif());
        params.put("regNum", user.getRegNum());
        params.put("monPayment", monPayment);
        params.put("chosenPeriod", chosenPeriod);

        contract.getLastOffer(params, new LoanOfferListener() {
            @Override
            public void onSuccess(LoanOffer loanOfferData) {
                loanOffer = loanOfferData;
                createLoan();
            }

            @Override
            public void onError(Response response) {

                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }

    private void createLoan() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        contract.createLoan(loan, new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                if (statusResponse.getCode().equals("200")) {
                    setRelated();
                    EventHelper.publish(getContext(), new LoanRemainingOffer());
                } else {
                    showErrorDialog(statusResponse.getMessage());
                }
            }

            @Override
            public void onError(Response var2) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }

    private void setRelated() {
        boolean isHave = false;
        RelatedContacts relatedContacts = new RelatedContacts();
        relatedContacts.setRegnum(loan.getRelatedRegNum());
        relatedContacts.setHandphone(loan.getRelatedHandPhone());
        relatedContacts.setLastname(loan.getRelatedLastName());
        relatedContacts.setFirstname(loan.getRelatedFirstName());
        relatedContacts.setGuardtype(loan.getRelatedWhoIs());

        List<RelatedContacts> updatedRelatedContacts = user.getRelatedContacts();
        if (updatedRelatedContacts == null) {
            updatedRelatedContacts = new ArrayList<>();

            updatedRelatedContacts.add(relatedContacts);
        } else {
            for (int i = 0; i < updatedRelatedContacts.size(); i++) {
                if (updatedRelatedContacts.get(i).getGuardtype().toLowerCase().equals(loan.getRelatedWhoIs().toLowerCase())) {
                    isHave = true;
                    updatedRelatedContacts.set(i, relatedContacts);
                    break;
                }
            }
            if (!isHave) {
                updatedRelatedContacts.add(relatedContacts);
            }
        }
        try {
            user.setRelatedContacts(updatedRelatedContacts);
            MainApplication.setUser(user);
        } catch (Exception e) {
            Log.e("exception", "setRelated: " + e);
        }
    }

    private Loan getLoanParams() {
        loan.setAmountCurrency("MNT");
        loan.setCifId(user.getCif());
        loan.setDmsUser(user.getDmsUser());
        loan.setFirstName(user.getFirstName());
        loan.setLastName(user.getLastName());
        loan.setLoanAmount(loan.getLoanAmount());
        loan.setLoanMonthCount(loan.getLoanMonthCount());
        loan.setMainAccount(""); //702015651000
        loan.setPkSeq("" + LoanOfferView.loanOffer.getData().getPk_seq());
        loan.setProdCode("" + LoanOfferView.loanOffer.getData().getPk_product_cd());
        loan.setRate(loan.getRate()); //hasna
        loan.setRePaymentMonthCount(loan.getRePaymentMonthCount()); // hasna
        loan.setRePaymentScheduledAmount(loan.getRePaymentScheduledAmount()); //hasna
        loan.setRePaymentType("M");
        loan.setRegNum(user.getRegNum());
        loan.setType("M");

        //Bank GW
        loan.setToAccount(account.getAccount_num());
        loan.setToAccName(user.getFirstName());
        loan.setUid(user.getUid());
        loan.setToBank(account.getBank_id() + "");
        loan.setOfferLimit(loanOffer.getData().getFinal_limit() + "");
        return loan;
    }

    private void showErrorDialog(String description) {
        component.setItem("loanStatus", "");
        AnimationDialog dialog = new AnimationDialog(context, "Уучлаарай,", description, "Буцах", "anim_rejection_comment.json");
        dialog.show();
    }
}