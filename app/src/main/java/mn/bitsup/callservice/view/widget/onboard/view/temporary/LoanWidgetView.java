package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.HashMap;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.OfflineClient;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockData;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.dialog.LoanSuccessDialog;
import mn.bitsup.callservice.view.widget.onboard.activity.TemporaryActivity;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class LoanWidgetView extends Fragment implements
    OnboardView<OnboardContract> {

    private CustomButton buttonContinue;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView =inflater.inflate(R.layout.activity_empty, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        buttonContinue.setOnClickListener(v -> beginTemporary());
        buttonContinue.setOnClickListener(v -> showDialog());
    }

    private void showDialog(){
        LoanSuccessDialog dialog = new LoanSuccessDialog(getContext(),
            () -> {
                Log.e("LoanSuccessDialog", "show");
            });
        dialog.show();
    }

    private void initView() {

    }

    private  Boolean isBlocked = false;
    private void beginTemporary(){
        OfflineClient offlineClient = ClientsManager.getOfflineClient();
        HashMap<String, String> blockParams = new HashMap<>();
        blockParams.put("value", DeviceAddressUtil.getMACAddress(getContext(),"wlan0"));
        blockParams.put("macAddress", DeviceAddressUtil.getMACAddress(getContext(),"wlan0"));
        blockParams.put("ipAddress", DeviceAddressUtil.getIPAddress(true));

        offlineClient.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                if (!var1.getData().isEmpty()) {
                    for (BlockData block :var1.getData()) {
                        if (block.getBlocktype() == 3
                            && block.getBlockchannel() == 2
                            && block.getBlockinfo().equals(ClientsManager.getRemoteAuthClient().getUid())) {
                            CustomAlertDialog.showAlertDialog(getContext(), getContext().getString(R.string.shared_alert_generic_alert_error_title), block.getBlockmsg());
                            isBlocked = true;
                        }
                    }
                } else {
                    isBlocked = false;
                }
                if (!isBlocked) {
                    Intent intent = new Intent(getContext(), TemporaryActivity.class);
                    getContext().startActivity(intent);
                }
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, blockParams);
    }
}
