package mn.bitsup.callservice.view.dialog;

public interface AnimationDialogListener {
    void onDismiss();
}
