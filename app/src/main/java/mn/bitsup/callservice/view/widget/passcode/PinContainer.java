package mn.bitsup.callservice.view.widget.passcode;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.AfterTextChangedListener;


public class PinContainer extends LinearLayout {

    private PinContainer.OnPinContainerListener listener;
    private int numberOfBoxes;

    public interface OnPinContainerListener {
        void onPinCompleted(char[] pin);
    }

    public PinContainer(@NonNull Context context) {
        this(context, null);
    }

    public PinContainer(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    private void initialize() {
        setOrientation(LinearLayout.HORIZONTAL);
        setGravity(Gravity.START);
    }

    public void setPins(int numberOfBoxes, @Nullable PinContainer.OnPinContainerListener listener) {
        this.numberOfBoxes = numberOfBoxes;
        this.listener = listener;

        post(() -> generatePinViews());
    }

    public int getNumberOfBoxes() {
        return numberOfBoxes;
    }

    /**
     * This method will calculate the size needed for each box for the user's device.
     *
     * @return size for pinView.
     */
    public int getBoxSize() {
        int width = getWidth();
        int boxes = numberOfBoxes;
        int boxSpace = getResources().getDimensionPixelSize(getPinSpacing());
        int totalBoxSpace = boxSpace * (boxes - 1);

        //remove total box space from calculation
        width = width - totalBoxSpace;

        //calculate the size needed per box
        return width / boxes;
    }

    public int getPinSpacing() {
        return R.dimen.passcode_pin_spacing;
    }

    /**
     * This method will create pinViews for the pinContainer with the correct size for the
     * user's screen.
     */
    private void generatePinViews() {
        //remove all pinViews from container
        this.removeAllViews();

        int boxSize = getBoxSize();

        for (int i = 0; i < numberOfBoxes; i++) {
            //Add right spacing to every view except the last one
            boolean displaySpacing = i < (numberOfBoxes - 1);

            PinView pinView = getNewPinView(getContext());
            pinView.setParams(boxSize, displaySpacing);

            //enabled only the first box when view container is added
            pinView.enabled(i == 0);

            //add view to the container
            this.addView(pinView);
        }
    }

    public PinView getNewPinView(Context context) {
        return new PinView(context);
    }

    public AfterTextChangedListenerImpl getAfterTextChangedListener() {
        return new AfterTextChangedListenerImpl();
    }

    private class AfterTextChangedListenerImpl extends AfterTextChangedListener {

        /**
         * This method will fire every time the user types something on the editText.
         */
        @Override
        public void afterTextChanged(Editable text) {
            refreshBoxes(text.length());

            if (text.length() == getChildCount() && listener != null) {
                char[] pin = new char[text.length()];
                text.getChars(0, text.length(), pin, 0);
                listener.onPinCompleted(pin);
            }
        }

        /**
         * This method will make sure to enabled and show/hide pins depending of the current position
         *
         * @param currentPosition: current text position of user input.
         */
        private void refreshBoxes(int currentPosition) {
            for (int i = 0; i < getChildCount(); i++) {
                PinView pinView = (PinView) getChildAt(i);

                //Highlight the next box unless this is the last one
                if (currentPosition < getChildCount()) {
                    pinView.enabled(currentPosition == i);
                }

                if (i < currentPosition) {
                    pinView.addPin();
                } else {
                    pinView.removePin();
                }
            }
        }
    }
}
