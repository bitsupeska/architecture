package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.HashMap;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBPhoneWidgetView extends Fragment implements OnboardView<OnboardContract> {

    public static String phoneNumber = "";
    private CustomButton buttonContinue;
    private InputField inputPhoneNumber;
    private OnboardContract contract;
    private EasyOnboardItem payload;
    private ContinueButtonTextWatcher continueButtonTextWatcher = new ContinueButtonTextWatcher();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contract = new OnboardWidget(getContext());
        this.payload = new EasyOnboardItem();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_input, container, false);
        inflatedView(inflatedView);
        initView();

        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputPhoneNumber = inflatedView.findViewById(R.id.inputInput);
    }

    private void initView(){
        OnboardActivity.showBackImage();
        inputPhoneNumber.setIconViewVisible(false);
        inputPhoneNumber.setMaxLength(8);
        inputPhoneNumber.setNumericKeyboard();
        inputPhoneNumber.addTextChangedListener(continueButtonTextWatcher);
        inputPhoneNumber.setCancelButton();
        inputPhoneNumber.requestFocus();

        buttonContinue.setLoadingText("");
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
        buttonContinue.setOnClickListener(v -> sendRequest());
    }

    private void publish(){
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        KeyboardUtils.hideKeyboard(getContext(), inputPhoneNumber);
        phoneNumber = inputPhoneNumber.getValue();
        payload.setHandPhone(phoneNumber);
        contract.setPayload(payload);
        OnboardActivity.addProgress();
        EventHelper.publishBack(getContext(), new OBOtpCheckWidgetView());
    }

    private boolean isValid() {
        return inputPhoneNumber.getValue().length() == 8;
    }


    private class ContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
            }
        }
    }

    private void sendRequest() {
        this.buttonContinue.setState(ButtonState.LOADING,getActivity());

        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", inputPhoneNumber.getValue());

        this.contract.requestOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                if(otp.getStatus().equals("SUCCESS")){
                    publish();
                }else{
                    CustomAlertDialog.showAlertDialog(getContext(), "", otp.getMessage());
                }
            }

            @Override
            public void onError(Response errorResponse) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
            }
        }, additions);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
