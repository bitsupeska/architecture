package mn.bitsup.callservice.view.widget.loan.view.dto;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.view.custom.DataCell;

public class BankBottomSheetDialog extends BottomSheetDialogFragment {
    private DataCell cellAmount, cellAccountNumber, cellReceiver, cellTransactionValue;
    private BankItem bankItem;
    private int imageId;
    private int amount;
    private String accountNo;
    private Activity activity;

    public BankBottomSheetDialog(int imageId, BankItem bankItem, int amount, Activity activity, String accountNo) {
        this.imageId = imageId;
        this.bankItem = bankItem;
        this.amount = amount;
        this.activity = activity;
        this.accountNo = accountNo;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bank_bottom, container, false);
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        ImageView imageButton = view.findViewById(R.id.closeButton);
        cellAmount = view.findViewById(R.id.cellAmount);
        cellAccountNumber = view.findViewById(R.id.cellAccountNumber);
        TextView textBankName = view.findViewById(R.id.bankName);
        cellReceiver = view.findViewById(R.id.cellReceiver);
        cellTransactionValue = view.findViewById(R.id.cellTransactionValue);
        ImageView imageBankImage = view.findViewById(R.id.bankImage);
        TextView textAmountCopy = view.findViewById(R.id.textAmountCopy);
        TextView textAccountCopy = view.findViewById(R.id.textAccountCopy);
        TextView textReceiverCopy = view.findViewById(R.id.textReceiverCopy);
        TextView textTransactionValueCopy = view.findViewById(R.id.textTransactionValueCopy);
        imageBankImage.setBackgroundResource(imageId);
        textBankName.setText(this.bankItem.getBankname());
        cellAmount.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(amount));
        cellAccountNumber.setValueText(this.bankItem.getAccountno());
        cellReceiver.setValueText(this.bankItem.getAccountname());
        cellTransactionValue.setValueText(this.accountNo);

        ClipboardManager clipboard = (ClipboardManager) Objects.requireNonNull(getContext()).getSystemService(Context.CLIPBOARD_SERVICE);

        textAmountCopy.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("copyAmount", amount + "");
            clipboard.setPrimaryClip(clip);
//            NudgeControllerUtil.showSuccessNotification(getContext().getResources().getString(R.string.setup_completed_title), cellAmount.getValueText()+" текст хуулагдлаа", getContext());
            Toast.makeText(getContext(), "Амжилттай " + cellAmount.getValueText() + " текст хуулагдлаа", Toast.LENGTH_LONG).show();
        });
        textAccountCopy.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("copyAccount", cellAccountNumber.getValueText());
            clipboard.setPrimaryClip(clip);
//            NudgeControllerUtil.showSuccessNotification(getContext().getResources().getString(R.string.setup_completed_title), cellAccountNumber.getValueText()+" текст хуулагдлаа", getContext());
            Toast.makeText(getContext(), "Амжилттай " + cellAccountNumber.getValueText() + " текст хуулагдлаа", Toast.LENGTH_LONG).show();
        });
        textReceiverCopy.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("copyReceiver", cellReceiver.getValueText());
            clipboard.setPrimaryClip(clip);
//            NudgeControllerUtil.showSuccessNotification(getContext().getResources().getString(R.string.setup_completed_title), cellReceiver.getValueText()+" текст хуулагдлаа", getContext());
            Toast.makeText(getContext(), "Амжилттай " + cellReceiver.getValueText() + " текст хуулагдлаа", Toast.LENGTH_LONG).show();
        });
        textTransactionValueCopy.setOnClickListener(v -> {
            ClipData clip = ClipData.newPlainText("copyTransaction", cellTransactionValue.getValueText());
            clipboard.setPrimaryClip(clip);
//            NudgeControllerUtil.showSuccessNotification(getContext().getResources().getString(R.string.setup_completed_title), cellTransactionValue.getValueText()+" текст хуулагдлаа", getContext());
            Toast.makeText(getContext(), "Амжилттай " + cellTransactionValue.getValueText() + " текст хуулагдлаа", Toast.LENGTH_LONG).show();
        });
        imageButton.setOnClickListener(v -> dismiss());
        return view;
    }
}
