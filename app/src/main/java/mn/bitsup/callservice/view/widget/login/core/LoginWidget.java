package mn.bitsup.callservice.view.widget.login.core;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.listener.PasswordAuthListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.dataprovider.util.OfflineCrypto;
import mn.bitsup.callservice.client.offline.OfflineClient;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.login.core.contract.LoginContract;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginWidget implements LoginContract, PasswordAuthListener {

    private Context context;
    public static final String ACCESS_TOKEN = "access_token";
    private PasswordAuthListener passwordAuthListener;
    private OfflineClient offlineClient;
    private AuthClient authClient;

    public LoginWidget(@NonNull Context context) {
        this.context = context;
        this.authClient = ClientsManager.getAuthClient();
        this.offlineClient = ClientsManager.getOfflineClient();
    }

    public void authenticate(char[] username, char[] password, Map<String, String> headers, PasswordAuthListener passwordAuthListener, String... authTokenNames) {
        this.authenticate(username, password, headers, null, passwordAuthListener, authTokenNames);
    }

    public void authenticate(char[] username, char[] password, Map<String, String> headers, Map<String, String> bodyParameters, PasswordAuthListener passwordAuthListener, String... authTokenNames) {
        this.passwordAuthListener = passwordAuthListener;
        authClient.authenticate(username, password, headers, bodyParameters, this, authTokenNames);
    }


    @Override
    public void checkToken(TokenListener listener) {
        if (RefreshService.isRunning && RefreshService.timer > 20) {
            listener.onSuccess("");
        } else {
                String data = OfflineCrypto.tokenEncrypt(context);
                if (data != null && !data.equals("")) {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("data", data);
                    Log.e("asd", "checkToken: "+params );
                    offlineClient.convertToken(new TokenListener() {
                        @Override
                        public void onSuccess(String var1) {
                            String ddata = OfflineCrypto.tokenDecrypt(var1, context);
                            Log.e("json", "saved token: " + ddata);
                            if (ddata != null && !ddata.equals("")) {
                                try {
                                    JSONObject object = new JSONObject(ddata);
                                    StorageComponent storageComponent = new StorageComponent();
                                    storageComponent
                                            .setItem(ACCESS_TOKEN, object.getString("access_token"));
                                    listener.onSuccess(var1);

                                    context.stopService(new Intent(context, RefreshService.class));
                                    context.startService(new Intent(context, RefreshService.class));

                                } catch (JSONException e) {
                                    Response errorResponse = new Response();
                                    errorResponse.setResponseCode(500);
                                    listener.onError(errorResponse);
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(Response var1) {
                            CustomAlertDialog.showServerAlertDialog(context);
                            listener.onError(var1);
                        }
                    }, params);
                }
        }
    }

    @Override
    public void checkBlock(BlockListener blockListener, HashMap<String, String> param) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {
                offlineClient.checkBlock(new BlockListener() {
                    @Override
                    public void onSuccess(Block var1) {
                        blockListener.onSuccess(var1);
                    }

                    @Override
                    public void onError(Response var1) {
                        blockListener.onError(var1);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                blockListener.onError(response);
            }
        });
    }

    @Override
    public void checkBlockIncrement(BlockIncrementListener listener, String blockType, String blockChannel, String userName) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {

                HashMap<String, String> param = new HashMap<>();
                param.put("blockInfo", userName);
                param.put("blockChannel", blockChannel);
                param.put("blockType", blockType);
                offlineClient.checkBlockIncrement(new BlockIncrementListener() {
                    @Override
                    public void onSuccess(BlockIncrement blockIncrement) {
                        Log.e("checkBlock", "success");
                        if (blockIncrement.getStatus().equals("SUCCESS")) {
                            checkBlockCount(blockIncrement, blockType, listener, userName);
                        } else {
                            listener.onError(new Response());
                        }
                    }

                    @Override
                    public void onError(Response response) {
                        Log.e("checkBlock", "error");
                        listener.onError(response);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
            }
        });

    }

    private void checkBlockCount(BlockIncrement blockIncrement, String blockType, BlockIncrementListener blockIncrementListener, String userName) {
        if (blockIncrement.getData().getFailedCount() >= blockIncrement.getData().getBlockLimit()) {
            setBlock(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    blockIncrementListener.onSuccess(blockIncrement);
                    if (statusResponse.getStatus().equals("SUCCESS")) {
                        new AnimationDialog(context, "Уучлаарай,",
                                "Та нууц үгээ " + blockIncrement.getData().getBlockLimit() + "удаа буруу орууллаа.Та 00:00 цагаас хойш дахин оролдоно уу",
                                "Буцах", "anim_rejection_clock.json").show();

                    }
                }

                @Override
                public void onError(Response response) {
                    blockIncrementListener.onSuccess(null);
                }
            }, blockIncrement.getData().getBlockLimit(), blockType, userName);
        } else {
            blockIncrementListener.onSuccess(blockIncrement);
        }
    }

    @Override
    public void setBlock(StatusListener statusListener, int count, String blockType, String userName) {
        checkToken(new TokenListener() {
            @Override
            public void onSuccess(String var1) {

                HashMap<String, String> param = new HashMap<>();
                param.put("blockInfo", userName);
                param.put("blockChannel", "1");
                param.put("blockType", blockType);
                param.put("blockMsg", "Та нууц үгээ " + count + " удаа буруу оруулсан байна. Нууц үгээ сэргээх эсвэл 00:00 цагаас хойш дахин оролдоно уу.");


                offlineClient.setBlock(new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse statusResponse) {
                        statusListener.onSuccess(statusResponse);
                    }

                    @Override
                    public void onError(Response response) {
                        statusListener.onError(response);
                    }
                }, param);
            }

            @Override
            public void onError(Response response) {
                statusListener.onError(response);
            }
        });


    }

    public void onAuthSuccess(Map<String, List<String>> map) {
        this.passwordAuthListener.onAuthSuccess(map);
        Log.e("auth", "success");
    }

    public void onAuthError(Response response) {
        Log.e("auth", "error");
        this.passwordAuthListener.onAuthError(response);
    }


}
