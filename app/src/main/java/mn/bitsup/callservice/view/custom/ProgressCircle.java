package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.ColorInt;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 30/11/2018.
 * View to display progress.
 * The view consist of a full ring (referred to as path) and a variable progress ring on top of that.
 */
public class ProgressCircle extends View {
    private static final int ANGLE_AT_TWELVE_OCLOCK = 270;
    private static final double CIRCLE_PERCENTAGE_FORMULA_CONSTANT = 3.6;
    private Paint pathPaint = new Paint();
    private Paint progressPaint = new Paint();
    private RectF oval = new RectF();
    protected @ColorInt
    int backgroundPathColor;
    private float pathWidth;
    private float angle;

    public ProgressCircle(Context context) {
        this(context, null);
    }

    public ProgressCircle(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressCircle(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);

        if (attrs != null) {
            initWithStyledAttributes(attrs);
        }
    }

    protected void initWithStyledAttributes(@NonNull AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ProgressCircle, 0, 0);
        backgroundPathColor = a.getColor(R.styleable.ProgressCircle_pathColor, Color.GRAY);
        final int progressColor = a.getColor(R.styleable.ProgressCircle_progressColor, Color.RED);
        pathWidth = (float) a.getDimensionPixelSize(R.styleable.ProgressCircle_pathWidth, 8);
        final int progressPercentage = a.getInteger(R.styleable.ProgressCircle_progressPercentage, 30);

        setPathColor(backgroundPathColor);
        setProgressColor(progressColor);
        setProgressPercentage(progressPercentage);
        a.recycle();
    }

    /**
     * Set the color of the background ring.
     * @param color the color value
     */
    public void setPathColor(@ColorInt final int color) {
        initPaint(pathPaint, color);
    }

    /**
     * Set the color of the progress ring.
     * @param color the color value
     */
    public void setProgressColor(@ColorInt final int color) {
        initPaint(progressPaint, color);
    }

    /**
     * Set the progress ring fill
     * @param progressPercentage the fill amount in percentage
     */
    @Keep
    public void setProgressPercentage(int progressPercentage) {
        angle = (float)(progressPercentage * CIRCLE_PERCENTAGE_FORMULA_CONSTANT);
        invalidate();
    }

    @Keep
    public int getProgressPercentage() {
        return (int) (angle / CIRCLE_PERCENTAGE_FORMULA_CONSTANT);
    }

    private void initPaint(Paint paint, @ColorInt int color) {
        paint.setColor(color);
        paint.setStrokeWidth(pathWidth);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        invalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        canvas.drawCircle(getWidth()/2f, getHeight()/2f , (getWidth()-pathWidth)/2f, pathPaint);
        oval.set(pathWidth/2f, pathWidth/2f, getWidth()-pathWidth/2f, getHeight()-pathWidth/2f);
        canvas.drawArc(oval, ANGLE_AT_TWELVE_OCLOCK, angle, false, progressPaint);
        super.onDraw(canvas);
    }
}
