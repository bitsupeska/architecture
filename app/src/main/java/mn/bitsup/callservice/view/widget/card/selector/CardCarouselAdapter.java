package mn.bitsup.callservice.view.widget.card.selector;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.card.dto.CardItem;

/**
 * Created by Erdenedalai R&D B.V. on 06/02/2019.
 * Adapter class for the Card carousel's RecyclerView
 */
public class CardCarouselAdapter extends RecyclerView.Adapter<CardViewHolder> {

    @NonNull
    private List<CardAdapterItem> cardAdapterItems = new ArrayList<>();
    @NonNull
    private OnItemClickListener itemClickListener;

    public CardCarouselAdapter(@NonNull OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View cardItemView = LayoutInflater
            .from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new CardViewHolder(cardItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        CardAdapterItem cardAdapterItem = cardAdapterItems.get(position);
        holder.bind(cardAdapterItem, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return cardAdapterItems.size();
    }

    public CardAdapterItem getItem(int position) {
        return cardAdapterItems.get(position);
    }

    public void updateWith(@NonNull List<CardItem> cards) {
        List<CardAdapterItem> newCardAdapterItems = CardAdapterItem.toAdapterItems(cards);
        this.cardAdapterItems.clear();
        this.cardAdapterItems.addAll(newCardAdapterItems);
        notifyDataSetChanged();
    }

    private int getPosition(@NonNull CardAdapterItem cardItem) {
        int position = 0;
        for (CardAdapterItem currItem : cardAdapterItems) {
            if (currItem.getCard().getGuid().equals(cardItem.getCard().getGuid())) {
                return position;
            }
            position++;
        }
        return NO_POSITION;
    }

    public void setCardInProgress(@NonNull CardAdapterItem cardItem) {
        int position = getPosition(cardItem);
        cardAdapterItems.get(position).setInProgress(true);
        notifyItemChanged(position);
    }

    public void lockCard(@NonNull CardAdapterItem cardItem) {
        int position = getPosition(cardItem);
        cardAdapterItems.get(position).lock();
        notifyItemChanged(position);
    }

    public void unlockCard(@NonNull CardAdapterItem cardItem) {
        int position = getPosition(cardItem);
        cardAdapterItems.get(position).unlock();
        notifyItemChanged(position);
    }

    public interface OnItemClickListener {

        void onItemClicked(int position);
    }

}
