package mn.bitsup.callservice.view.widget.notification.core.contract;

import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.notification.listener.NotificationListener;
import mn.bitsup.callservice.client.notification.listener.NotificationsListener;
public interface NotificationContract {
    void getNotifications(int currentPage , int size, NotificationsListener notificationsListener);
    void getNotification(Map<String,String> params, NotificationListener notificationListener);
    void notificationArchive(Map<String,String> params, StatusListener statusListener);

}
