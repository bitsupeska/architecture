package mn.bitsup.callservice.view.widget.loan.view;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.loan.adapter.RepaymentScheduleAdapter;
import mn.bitsup.callservice.view.widget.profile.dto.ProfilePayload;
import mn.bitsup.callservice.view.widget.profile.views.ProfileEditEmailWidgetView;

import static mn.bitsup.callservice.view.widget.loan.view.LoanOfferView.loanOffer;

public class LoanBottomSheetRepaymentPlan extends BottomSheetDialogFragment {

    private static final String TAG = "LoanBottomSheetRepaymen";

    private ImageView closeButton;
    private ListView repaymentScheduleListView;
    private CustomButton buttonContinue;
    private TextView totalPayAmount, totalFeeAmount, loan_surcharge;
    private static List<LoanDueScheduleData> loanDueScheduleDataList;
    private RepaymentScheduleAdapter adapter;
    private Context context;
    private User user;
    private static String from;

    public static LoanBottomSheetRepaymentPlan newInstance(List<LoanDueScheduleData> loanDueScheduleData, String path) {
        LoanBottomSheetRepaymentPlan fragment = new LoanBottomSheetRepaymentPlan();
        loanDueScheduleDataList = loanDueScheduleData;
        from = path;
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.loan_bottomsheet_layout, container, false);
        setStyle(STYLE_NORMAL, R.style.CustomShapeAppearanceBottomSheetDialog);
        this.context = getContext();
        this.user = MainApplication.getUser();
        inflateView(layout);
        return layout;
    }

    private void inflateView(View inflatedView) {
        totalPayAmount = inflatedView.findViewById(R.id.total_pay_amount);
        closeButton = inflatedView.findViewById(R.id.closeButton);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        repaymentScheduleListView = inflatedView.findViewById(R.id.repaymentScheduleListView);
        totalFeeAmount = inflatedView.findViewById(R.id.total_fee_amount);
        loan_surcharge = inflatedView.findViewById(R.id.loan_surcharge);
        try {
            setTextAmount(loanDueScheduleDataList);
            adapter = new RepaymentScheduleAdapter(getContext(), loanDueScheduleDataList);
            repaymentScheduleListView.setAdapter(adapter);
        } catch (Exception ex) {
            Toast.makeText(getContext(), "Хөрвүүлэлтийн алдаа гарлаа", Toast.LENGTH_LONG).show();
            buttonContinue.setState(ButtonState.DISABLED, getActivity());
        }
        if (from.equals("LoanSelector")) buttonContinue.setOnClickListener(v -> publish());
        else {
            buttonContinue.setOnClickListener(v -> dismiss());
            buttonContinue.setEnabledText("Буцах");
        }
        closeButton.setOnClickListener(v -> dismiss());
    }

    private void setTextAmount(List<LoanDueScheduleData> data) {
        double coreFee = 0.0;
        double total = 0.0;
        if (loanOffer.getData().getCoreFee() != null) {
            coreFee = Double.parseDouble(loanOffer.getData().getCoreFee().getFixed_amt());
        }

        double fee = 0.0;
        for (LoanDueScheduleData ld : data) {
            total += Double.parseDouble(ld.getInstlAmt());
            fee += Double.parseDouble(ld.getIntAmt());
        }
        total += coreFee;
        loan_surcharge.setText("₮" + CurrencyConverter.getFormattedCurrencyString(coreFee));
        totalPayAmount.setText("₮" + CurrencyConverter.getFormattedCurrencyString(total));
        totalFeeAmount.setText("₮" + CurrencyConverter.getFormattedCurrencyString(fee));
    }

    private void publish() {
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(getContext());
        alertdialog.setMessage("Энэхүү сонгосон нөхцөлөөр зээл авахыг зөвшөөрч байна.")
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkEmailVerified();
                dialog.cancel();
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = alertdialog.create();
        alert.setTitle("Зээлийн өргөдөл");
        alert.show();
    }

    private void checkEmailVerified() {
        if (user.getEmails().get(0).getEmailStatus().equals("1")) { //Active
            EventHelper.publishBack(context, new LoanRelativeInformation());
            dismiss();
        } else {
            ProfilePayload payload = new ProfilePayload();
            payload.setPublishType("Loan");
            payload.setEmailId(user.getEmails().get(0).getEmailId());
            EventHelper.publishBack(context, new ProfileEditEmailWidgetView(), "getEmailData", payload);
            dismiss();
        }
    }

}
