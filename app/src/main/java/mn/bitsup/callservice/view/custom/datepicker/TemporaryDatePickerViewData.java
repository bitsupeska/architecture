package mn.bitsup.callservice.view.custom.datepicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.DateTimeFormatter;

/**
 * Created by Erdenedalai R&D B.V. on 22/02/2019.
 * POJO for payment order scheduling's date picker
 */
public class TemporaryDatePickerViewData implements DatePickerViewData {

    private static final int preselectedDateColor = R.color.primary;

    @Nullable
    private Date preSelectedDate = new Date();

    private Date minDate = new Date();

    @Nullable
    @Override
    public Date getPreSelectedDate() {
        return preSelectedDate;
    }

    public void setPreSelectedDate(@Nullable Date preSelectedDate) {
        this.preSelectedDate = preSelectedDate;
    }

    void setMinDate(Date minDate) {
        this.minDate = minDate;
    }

    @NonNull
    @Override
    public List<DatePickerItem> getDatePickerItems() {
        List<DatePickerItem> pickerItems = new ArrayList<>();
        final String preselectedDateAsString = DateTimeFormatter.toDateAndTimeAndMillisecondsFormat(preSelectedDate);
        pickerItems.add(new DatePickerItem(preselectedDateAsString, preselectedDateColor,false));
        return pickerItems;
    }

    @NonNull
    @Override
    public Date getCalendarMinDate() {
        return minDate;
    }
}
