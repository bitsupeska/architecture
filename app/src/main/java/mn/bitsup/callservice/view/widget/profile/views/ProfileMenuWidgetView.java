package mn.bitsup.callservice.view.widget.profile.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.util.ResourcesUtil;
import mn.bitsup.callservice.view.activity.EnrollmentActivity;
import mn.bitsup.callservice.view.activity.MainActivity;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.client.UserInteractionTimers;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfileMenuWidgetView extends Fragment implements ProfileView<ProfileContract> {
    private ProfileContract contract;
    private MenuAdapter adapter;
    private TextView textName, textProfileLetter;
    private ListView listMenu;
    private Context context;
    private String didSign;
    private User user;
    private String[] wallet_menu = {"Түгээмэл асуулт хариулт", "Миний мэдээлэл", "Данс холбох", "Нэвтрэх нууц үг", "Холбоо барих", "Гарах"};
    private int[] icons = {R.drawable.help_outline, R.drawable.profile, R.drawable.accounts, R.drawable.lock_outline, R.drawable.phone, R.drawable.exit_to_app};

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.user = MainApplication.getUser();
        if (user != null && user.getDidSign() != null) this.didSign = user.getDidSign();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_menu, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        listMenu = inflatedView.findViewById(R.id.listMenu);
        textProfileLetter = inflatedView.findViewById(R.id.textProfileLetter);
        textName = inflatedView.findViewById(R.id.textName);
    }

    private void initViews() {
        if (contract.getUser() != null && contract.getUser().getLastName() != null) {
            String profile_name_letters = contract.getUser().getLastName().substring(0, 1) + "" + contract.getUser().getFirstName().substring(0, 1);
            textName.setText(contract.getUser().getLastName() + " " + contract.getUser().getFirstName());
            textProfileLetter.setText(profile_name_letters.toUpperCase());
        }

        listMenu.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ResourcesUtil.dpToPx(436, context.getResources())));
        adapter = new MenuAdapter();
        listMenu.setAdapter(adapter);

        listMenu.setOnItemClickListener((parent, view, position, id) -> {
            publish(position);
        });

    }


    private class MenuAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return wallet_menu.length;
        }

        @Override
        public Object getItem(int position) {
            return wallet_menu[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.profile_menu_row, parent, false);
            TextView textView = convertView.findViewById(R.id.textView);
            ImageView iconImage = convertView.findViewById(R.id.imageView);

            textView.setText(wallet_menu[position]);
            iconImage.setImageResource(icons[position]);

            if (position == 2) {
                if (didSign == null || didSign.equals("0")) {
                    textView.setTextColor(Color.GRAY);
                    iconImage.setColorFilter(Color.GRAY);
                }
            }
            return convertView;
        }
    }

    private void publish(int position) {
        switch (position) {
            case 0: //Түгээмэл асуулт хариулт
                EventHelper.publishActivity(context, new ProfileHelpWidgetView(), wallet_menu[0]);
                break;
            case 1: //Миний мэдээлэл
                EventHelper.publishActivity(context, new ProfileInfoWidgetView(), wallet_menu[1]);
                break;
            case 2: //Данс холбох
                if (didSign != null && !didSign.equals("0"))
                    EventHelper.publishActivity(context, new ProfileConnectAccountWidgetView(), wallet_menu[2]);
                break;
            case 3: //Нэвтрэх нууц үг
                EventHelper.publishActivity(context, new ProfilePasswordShowWidgetView(), wallet_menu[3]);
                break;
            case 4: //Холбоо барих
                EventHelper.publishActivity(context, new ProfileContactUsWidgetView(), wallet_menu[4]);
                break;
            case 5: //Гарах
                openLogoutDialog();
                break;
        }
    }

    private void openLogoutDialog() {
        new AlertDialog.Builder(context)
                .setTitle(R.string.shared_alert_sign_out_title)
                .setMessage(R.string.shared_alert_sign_out_message)
                .setPositiveButton(R.string.shared_alert_button_yes, (dialog, which) -> {
                    AuthClient authClient = new AuthClient(MainActivity.context);
                    authClient.endSession();
                    UserInteractionTimers.isLogged = false;
                    UserInteractionTimers.endTimer();
                    context.stopService(new Intent(context, RefreshService.class));
                    ((Activity) MainActivity.context).finish();
                    Intent enrollmentIntent = new Intent(MainActivity.context, EnrollmentActivity.class);
                    enrollmentIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    enrollmentIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MainActivity.context.startActivity(enrollmentIntent);
                })
                .setNegativeButton(R.string.shared_alert_button_no,
                        (dialog, which) -> dialog.dismiss()).show();
    }

    public static void showSuccessNotification(String title, String message, Context context) {
        NudgeControllerUtil.showSuccessNotification(title, message, context);
    }

    @Override
    public void onResume() {
        KeyboardUtils.hideKeyboard(getActivity());
        super.onResume();
    }
}
