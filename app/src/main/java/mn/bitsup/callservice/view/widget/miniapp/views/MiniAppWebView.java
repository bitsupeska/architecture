package mn.bitsup.callservice.view.widget.miniapp.views;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;

public class MiniAppWebView extends Fragment {
    private WebView webViewApp;
    private ProgressDialog progressDialog;
    private Context context;
    private String url;

    public MiniAppWebView(String url) {
        this.url = url;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mini_app_web_view, container, false);
        webViewApp = view.findViewById(R.id.webViewApp);
        progressDialog.show();
        webViewApp.getSettings().setJavaScriptEnabled(true);
        webViewApp.setWebViewClient(new WebViewClient() {
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                // Redirect to deprecated method, so you can use it in all SDK versions
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                progressDialog.dismiss();
            }
        });
        webViewApp.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if(webViewApp!=null)
                {
                    progressDialog.dismiss();
                }
            }
        });
        webViewApp.loadUrl(url);
        return view;
    }


    public void showErrorNotification(@NonNull final String title, @NonNull final String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle, getContext());
    }

}
