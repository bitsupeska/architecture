package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.util.ValidationInputField;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBFirstNameWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private OnboardContract contract;
    private Context context;
    private CustomButton buttonContinue;
    private InputField inputFirstName;
    private InputField inputLastName;
    private ContinueButtonTextWatcher continueButtonTextWatcher = new ContinueButtonTextWatcher();
    private LastContinueButtonTextWatcher lastContinueButtonTextWatcher = new LastContinueButtonTextWatcher();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(getContext());
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_firstname, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputFirstName = inflatedView.findViewById(R.id.inputFirstName);
        inputLastName = inflatedView.findViewById(R.id.inputLastName);
    }

    private void initView() {
        inputFirstName.setIconViewVisible(false);
        inputFirstName.setSingleLine();
        inputFirstName.addTextChangedListener(continueButtonTextWatcher);
        inputFirstName.setCancelButton();
        inputFirstName.setMaxLength(50);
        inputFirstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        inputLastName.setIconViewVisible(false);
        inputLastName.setSingleLine();
        inputLastName.addTextChangedListener(lastContinueButtonTextWatcher);
        inputLastName.setCancelButton();
        inputLastName.setMaxLength(50);
        inputLastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        buttonContinue.setOnClickListener(v -> checkCitizenInfo());
//       buttonContinue.setOnClickListener(v -> publish()); //FOR MOCK
    }

    private void checkCitizenInfo() {
        buttonContinue.setState(ButtonState.LOADING,getActivity());
        KeyboardUtils.hideKeyboard(context, inputLastName);
        EasyOnboardItem payload = contract.getPayload();
        Map<String, String> param = new HashMap<>();
        param.put("regNum", payload.getRegNum());
        param.put("firstName", inputFirstName.getValue().trim());
        param.put("lastName", inputLastName.getValue().trim());
        contract.getCheckCitizenInfo(new CitizenInfoListener() {
            @Override
            public void onSuccess(CitizenInfo data) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                if (data.getResultMessage().equals("true")) {
                    payload.setFirstName(inputFirstName.getValue());
                    payload.setLastName(inputLastName.getValue());
                    contract.setPayload(payload);
                    publish();
                } else {
                    new AnimationDialog(context, context.getResources().getString(R.string.shared_alert_generic_error_title), "Уучлаарай, Таны оруулсан мэдээлэл таарахгүй байна", context.getResources().getString(
                            R.string.shared_button_back), "").show();
                }
            }

            @Override
            public void onError(Response var1) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }
        }, param);
    }

    private void publish() {
        EasyOnboardItem payload = contract.getPayload();
        payload.setFirstName(inputFirstName.getValue());
        payload.setLastName(inputLastName.getValue());
        contract.setPayload(payload);
        OnboardActivity.addProgress();
        EventHelper.publishBack(context, new OBUsernameWidgetView());
    }

    private void isValid(InputField inputField) {
        String value = inputField.getValue();
        if (!value.equals("")
                && hasDigits(value)
                && !ValidationInputField.hasLowerCase(value)
                && !ValidationInputField.hasUpperCase(value)
        ) {
            inputField.setError(null);
            if (!inputFirstName.getValue().equals("") && !inputLastName.getValue().equals("")) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
            } else {
                buttonContinue.setState(ButtonState.DISABLED,getActivity());
            }
        } else {
            buttonContinue.setState(ButtonState.DISABLED,getActivity());
            inputField.setError("Зөвхөн крил үсэг оруулж болно");
        }
    }

    private boolean hasDigits(final String name) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[-АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯӨѲҮабвгдеёжзийклмнопрстуфхцчшщъыьэюяѳөү]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

    private class ContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                isValid(inputFirstName);
            }
        }
    }

    private class LastContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                isValid(inputLastName);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
