package mn.bitsup.callservice.view.widget.loan.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.util.CurrencyConverter;

import static mn.bitsup.callservice.view.widget.loan.view.LoanOfferView.loanOffer;

public class RepaymentScheduleAdapter extends BaseAdapter {

    private static final String TAG = "TransactionAdapter";

    private List<LoanDueScheduleData> loanDueScheduleDataList;
    private LayoutInflater inflater;
    private Context context;

    public RepaymentScheduleAdapter(Context context, List<LoanDueScheduleData> loanDueScheduleDataList) {
        this.loanDueScheduleDataList = loanDueScheduleDataList;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return loanDueScheduleDataList.size();
    }


    @Override
    public Object getItem(int position) {
        return loanDueScheduleDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder {

        TextView amount, date;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;

        if (rowView == null) {
            rowView = inflater.inflate(R.layout.repayment_schedule_item, null);
            holder = new ViewHolder();

        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.amount = rowView.findViewById(R.id.amount);
        holder.date = rowView.findViewById(R.id.date);
        double bigDecimalCurrency = Double.parseDouble(loanDueScheduleDataList.get(position).getInstlAmt());
        double coreFee = 0.0;
        if(loanOffer.getData().getCoreFee() != null)
            coreFee= Double.parseDouble(loanOffer.getData().getCoreFee().getFixed_amt());
        if (position == loanDueScheduleDataList.size() - 1) {
            bigDecimalCurrency += coreFee;
        }
        holder.amount.setText("₮" + CurrencyConverter.getFormattedCurrencyString(bigDecimalCurrency));

        holder.date.setText(loanDueScheduleDataList.get(position).getFlowDate().substring(0, 10).replace("-", "."));
        rowView.setTag(holder);
        return rowView;
    }

}
