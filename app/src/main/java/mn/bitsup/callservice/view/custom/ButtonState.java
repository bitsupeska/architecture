package mn.bitsup.callservice.view.custom;

/**
 * Created by Erdenedalai R&D B.V on 28/09/2017.
 * State of the CustomButton
 */

public enum ButtonState {
    DISABLED,
    ENABLED,
    LOADING;

    private ButtonState() {
    }
}
