package mn.bitsup.callservice.view.toolbar;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.activity.BaseActivity;
import mn.bitsup.callservice.view.activity.MainActivity;

import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.profile.views.ProfileMenuWidgetView;

import mn.bitsup.callservice.view.widget.notification.list.NotificationsWidgetView;


public class ToolbarManager {

    private final BaseActivity activity;
    public static Toolbar currentToolbar;
    private Toolbar defaultToolbar;
    private View defaultToolbarContainer;
    private View productSummaryToolbarContainer;
    private View currentToolbarContainerView;

    public ToolbarManager(BaseActivity activity, AppBarLayout appBarLayout) {
        this.activity = activity;
        defaultToolbar = appBarLayout.findViewById(R.id.toolbarDefault);
        defaultToolbarContainer = appBarLayout.findViewById(R.id.toolbarDefaultContainer);
        productSummaryToolbarContainer = appBarLayout.findViewById(R.id.toolbarProductSummary);
    }

    @VisibleForTesting
    public void setToolbar(@NonNull ToolbarEvent toolbarEvent) {
        switch (toolbarEvent.getToolbarPage()) {
            case PRODUCTS:
                showToolbar(productSummaryToolbarContainer, productSummaryToolbarContainer.findViewById(R.id.toolbar));
                updateProductSummaryToolbar();
                break;
            case NO_TOOLBAR:
                hideToolbar();
                break;
            default:
                showToolbar(defaultToolbarContainer, defaultToolbar);
                setToolbarTitle("");
                break;
        }
    }

    @VisibleForTesting
    public static void setToolbarTitle(String title) {
        currentToolbar.setTitle(title);
    }

    private void showToolbar(View newToolbarContainerView, Toolbar newToolbar) {
        if (currentToolbarContainerView != null) {
            currentToolbarContainerView.setVisibility(View.GONE);
        }

        currentToolbarContainerView = newToolbarContainerView;
        currentToolbarContainerView.setVisibility(View.VISIBLE);
        currentToolbar = newToolbar;
        activity.setSupportActionBar(currentToolbar);
    }

    private void hideToolbar() {
        if (currentToolbarContainerView != null) {
            currentToolbarContainerView.setVisibility(View.GONE);
        }
        activity.setSupportActionBar(currentToolbar);
    }

    private void updateProductSummaryToolbar() {
        ImageView buttonNotification = currentToolbar.findViewById(R.id.buttonNotification);
        ImageView buttonSetup = currentToolbar.findViewById(R.id.imageSetup);
        buttonSetup.setOnClickListener(v -> publishProfileMenu());
        buttonNotification.setOnClickListener(v -> EventHelper.publishActivity(MainActivity.context, new NotificationsWidgetView(), "Мэдэгдэл"));
        AppCompatTextView product_summary_toolbar_title = currentToolbar.findViewById(R.id.product_summary_toolbar_title);
        product_summary_toolbar_title.setText("80197667");//MainApplication.getUser().getFirstName().toUpperCase()
    }

    private void publishProfileMenu(){
        EventHelper.publishActivity(MainActivity.context, new ProfileMenuWidgetView(), "Тохиргоо");
    }
}
