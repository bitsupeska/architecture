package mn.bitsup.callservice.view.custom;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.cardview.widget.CardView;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.KeyboardUtils;

/**
 * Created by Erdenedalai R&D B.V on 13/09/2017.
 * Custom button with styling and states.
 */

public class CustomButton extends CardView {

    private ButtonState state = ButtonState.DISABLED;
    private TextView textView;

    private String buttonTextEnabled;
    private String buttonTextDisabled;
    private String buttonTextLoading;
    public static boolean isLoading = false;
    private ProgressBar progressBar;

    private static final int COLOR_LIGHT = 0;
    private static final int COLOR_COLORED = 1;
    private static final int COLOR_GRAY = 2;
    private static final int COLOR_WHITE = 3;

    public CustomButton(@NonNull Context context) {
        this(context, null);
    }

    public CustomButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View inflatedView = inflate(context, R.layout.view_button, this);
        progressBar = findViewById(R.id.progressBar);
        TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.CustomButton, 0, 0);
        int stateInt = styledAttributes.getInt(R.styleable.CustomButton_state, 0);
        getButtonTextValues(styledAttributes);
        textView = inflatedView.findViewById(R.id.textView);
        setState(stateInt);
        colorButton(styledAttributes.getInt(R.styleable.CustomButton_buttonColor, 0));
    }

    public void colorButton(int buttonColor) {
        switch (buttonColor) {
            case COLOR_LIGHT:
                applyColorsToButton(R.color.white, R.color.primary);
                break;
            case COLOR_GRAY:
                applyColorsToButton(R.color.background, R.color.primary);
                break;
            case COLOR_WHITE:
                applyColorsToButton(R.color.background, R.color.text_pink);
                break;
            case COLOR_COLORED:
                applyColorsToButton(R.color.primary, R.color.white);
                break;
        }
    }

    private void applyColorsToButton(int backgroundResId, int textColorResId) {
        setCardBackgroundColor(getContext().getResources().getColor(backgroundResId));
        textView.setTextColor(getContext().getResources().getColor(textColorResId));
        progressBar.getIndeterminateDrawable().setColorFilter(getContext().getResources().getColor(textColorResId), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    private void getButtonTextValues(TypedArray styledAttributes) {
        String buttonTextDefault = getStyleableAttributeString(styledAttributes, R.styleable.CustomButton_text, "");
        buttonTextEnabled = getStyleableAttributeString(styledAttributes, R.styleable.CustomButton_textEnabled, buttonTextDefault);
        buttonTextDisabled = getStyleableAttributeString(styledAttributes, R.styleable.CustomButton_textDisabled, buttonTextDefault);
        buttonTextLoading = getStyleableAttributeString(styledAttributes, R.styleable.CustomButton_textLoading, buttonTextDefault);
    }

    private String getStyleableAttributeString(TypedArray styledAttributes, int styleable, String defaultValue) {
        String retVal = styledAttributes.getString(styleable);
        if (retVal == null) {
            return defaultValue;
        }
        return retVal;
    }

    public void setState(int stateInt) {
        switch (stateInt) {
            case 0:
                setState(ButtonState.ENABLED);
                break;
            case 1:
                setState(ButtonState.LOADING);
                break;
            case 2:
                setState(ButtonState.DISABLED);
                break;
            default:
                setState(ButtonState.ENABLED);
                break;
        }
    }

    public void setState(@NonNull ButtonState state) {
        this.state = state;
        setCustomView();
    }

    public void setState(@NonNull ButtonState state, Activity activity) {
        this.state = state;
        setCustomView(activity);
    }

    private void setCustomView(Activity activity) {
        String text;
        boolean enabled;

        switch (state) {
            case ENABLED:
                setAlpha(1f);
                text = buttonTextEnabled;
                enabled = true;
                disableScreen(false, activity);
                break;
            case LOADING:
                disableScreen(true, activity);
                setAlpha(1f);
                text = "";
                enabled = false;
                break;
            case DISABLED:
                disableScreen(false, activity);
            default:
//                setAlpha(0.4f);
                setAlpha(1f);
                text = buttonTextDisabled;
                disableScreen(false, activity);
                enabled = false;
                break;
        }

        textView.setText(text);

        this.setEnabled(enabled);

        loadingHandler();
    }

    private void setCustomView() {
        String text;
        boolean enabled;

        switch (state) {
            case ENABLED:
                setAlpha(1f);
                text = buttonTextEnabled;
                enabled = true;
                break;
            case LOADING:
                setAlpha(1f);
                text = "";
                enabled = false;
                break;
            case DISABLED:
            default:
//                setAlpha(0.4f);
                setAlpha(1f);
                text = buttonTextDisabled;
                enabled = false;
                break;
        }

        textView.setText(text);

        this.setEnabled(enabled);

        loadingHandler();
    }

    private void disableScreen(boolean enable, Activity activity) {
        if (enable) {
            KeyboardUtils.hideKeyboard(activity);
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void loadingHandler() {
        if (state == ButtonState.LOADING) {
            progressBar.setVisibility(VISIBLE);
            isLoading = true;
            Log.e("button", "loading");

        } else {
            isLoading = false;
            progressBar.setVisibility(GONE);
        }
    }

    public boolean isStateEnabled() {
        return state == ButtonState.ENABLED;
    }

    public boolean isLoading() {
        return state == ButtonState.LOADING;
    }

    /**
     * Sets text both for the enabled and disabled states
     *
     * @param text string resource id
     */
    public void setText(@StringRes int text) {
        setEnabledText(text);
        setDisabledText(text);
    }

    public void setEnabledText(int text) {
        setEnabledText(getContext().getResources().getString(text));
    }

    public void setEnabledText(final String text) {
        buttonTextEnabled = text;
        setCustomView();
    }

    @SuppressWarnings("unused")
    public void setDisabledText(int text) {
        setDisabledText(getContext().getResources().getString(text));
    }

    public void setDisabledText(final String text) {
        buttonTextDisabled = text;
        setCustomView();
    }

    @SuppressWarnings("unused")
    public void setLoadingText(int text) {
        buttonTextLoading = getContext().getResources().getString(text);
        setCustomView();
    }

    @SuppressWarnings("unused")
    public void setLoadingText(String text) {
        buttonTextLoading = text;
        setCustomView();
    }

    public void setTextSize(float textSize) {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
    }
}
