package mn.bitsup.callservice.view.widget.profile.views;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.BranchBottomSheetDialog;

public class ProfileBranchAddress extends Fragment implements ProfileView<ProfileContract>, OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener {
    private Context context;
    private ProfileContract contract;
    private GoogleMap mMap;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.delivery_branch, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
    }

    private void initViews() {
        initMap();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        BranchBottomSheetDialog bottomSheetDialog = new BranchBottomSheetDialog(getContext());
        bottomSheetDialog.show(Objects.requireNonNull(getFragmentManager()), "myBottomSheet");
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        LatLng square = new LatLng(47.918268, 106.920378);
        MarkerOptions markerOptions = new MarkerOptions().position(square).title("Central tower").icon(BitmapDescriptorFactory.fromResource(R.drawable.location_marker));
        mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(square, 15f));
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }
}
