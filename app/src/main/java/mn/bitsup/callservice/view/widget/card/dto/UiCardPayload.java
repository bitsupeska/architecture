package mn.bitsup.callservice.view.widget.card.dto;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import mn.bitsup.callservice.client.card.dto.CardItem;

public class UiCardPayload {
    @NonNull
    private CardItem card;
    private char[] pin;
    @ColorRes
    private int backgroundColor;
    private String mode;
    private int reasonCode=-1;
    public UiCardPayload(@NonNull CardItem card, String mode) {
        this.card = card;
        this.mode = mode;
    }
    public UiCardPayload(@NonNull CardItem card, @ColorRes int backgroundColor) {
        this.card = card;
        this.backgroundColor = backgroundColor;
    }
    public UiCardPayload(@NonNull CardItem card, @ColorRes int backgroundColor,int reasonCode) {
        this.card = card;
        this.backgroundColor = backgroundColor;
        this.reasonCode = reasonCode;
    }
    public UiCardPayload(@NonNull CardItem card,char[] pin) {
        this.card = card;
        this.pin = pin;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    @NonNull
    public CardItem getCard() {
        return card;
    }

    @ColorRes
    public int getBackgroundColor() {
        return backgroundColor;
    }

    public int getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(int reasonCode) {
        this.reasonCode = reasonCode;
    }

    public void setCard(@NonNull CardItem card) {
        this.card = card;
    }

    public char[] getPin() {
        return pin;
    }

    public void setPin(char[] pin) {
        this.pin = pin;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
}
