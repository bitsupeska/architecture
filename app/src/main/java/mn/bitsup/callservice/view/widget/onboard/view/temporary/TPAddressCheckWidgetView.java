package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import static android.content.ContentValues.TAG;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class TPAddressCheckWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private CustomButton buttonYes;
    private CustomButton buttonNo;
    private TextView inputAddress;
    private Context context;
    private OnboardContract contract;
    private User user;
    private Addresses homeAddress;
    private Temporary temporary;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
        this.user = MainApplication.getUser();
        temporary = new Temporary();
        contract.setTemporary(temporary);
        setUserInfo();

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_ask_address, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }


    private void inflatedView(View inflatedView) {
        buttonYes = inflatedView.findViewById(R.id.buttonYes);
        buttonNo = inflatedView.findViewById(R.id.buttonNo);
        inputAddress = inflatedView.findViewById(R.id.inputAddress);
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        this.buttonNo.setOnClickListener(v -> {
            LoanActivity.addProgress();
            EventHelper.publishBack(context, new TPAddAddressWidgetView());
        });
        this.buttonYes.setOnClickListener(v -> {
            LoanActivity.addProgress();
            EventHelper.publishBack(context, new TPEducationTypeWidgetView());
        });


        if (this.user.getAddresses() != null) {
            for (int i = 0; i < this.user.getAddresses().size(); i++) {
                if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                    homeAddress = user.getAddresses().get(i);
                }
                if (this.user.getAddresses().get(i).getAddressType().equals("Home")) {
                    homeAddress = this.user.getAddresses().get(i);
                    break;
                }
            }
        }

        if (homeAddress != null) {
            String[] addressData = {homeAddress.getTown() == null ? "" : homeAddress.getTown(), homeAddress.getLocality() == null ? "" : homeAddress.getLocality(), (homeAddress.getStreet() == null ? "" : homeAddress.getStreet()) + (homeAddress.getSection() == null ? "" : homeAddress.getSection()),
                    homeAddress.getDistrict() == null ? "" : homeAddress.getDistrict(), homeAddress.getApartment() == null ? "" : homeAddress.getApartment(), homeAddress.getDoorNumber() == null ? "" : homeAddress.getDoorNumber()};
            String resultAddress = "";
            for (String address : addressData) {
                if (address != null && !address.equals("")) {
                    resultAddress += address + "\n";
                }
            }
            inputAddress.setText(resultAddress);
        }

    }

    private void setUserInfo() {
        User user = MainApplication.getUser();

        temporary.setCifId(user.getCif());
        temporary.setBirthDate(user.getBirthDt());
        temporary.setRegNum(user.getRegNum());
        temporary.setFirstName(user.getFirstName());
        temporary.setLastName(user.getLastName());
        temporary.setFamilyName(user.getFamilyName());
        if (user.getDocExpDt() != null)
            temporary.setDocExpDate(user.getDocExpDt().replace("T00:00:00.000", ""));
        if (user.getDocIssueDt() != null)
            temporary.setDocIssueDate(user.getDocIssueDt().replace("T00:00:00.000", ""));
        temporary.setDidSign("1");

        if (user.getAddresses() != null) {
            if (user.getAddresses().size() != 0) {
                Addresses mailAddress = null;
                Addresses homeAddress = null;
                for (int i = 0; i < user.getAddresses().size(); i++) {
                    if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                        mailAddress = user.getAddresses().get(i);
                        Log.e(TAG, "setUserInfo: " + mailAddress.toString());

                    }
                    if (user.getAddresses().get(i).getAddressType().equals("Home")) {
                        homeAddress = user.getAddresses().get(i);
                        Log.e(TAG, "setUserInfo: " + homeAddress.toString());
                    }
                }

                if (mailAddress != null) {
                    temporary.setTownHome(mailAddress.getTownId());
                    temporary.setTownHomeName(mailAddress.getTown());
                    temporary.setLocalityHome(mailAddress.getLocalityId());
                    temporary.setLocalityHomeName(mailAddress.getLocality());
                    temporary.setTownHome(mailAddress.getTownId());

                    Log.e(TAG, "setUserInfo: " + (mailAddress.getStreet() == null ? "" : mailAddress.getStreet()) + (mailAddress.getSection() == null ? "" : mailAddress.getSection()));
                    temporary.setSectionHome((mailAddress.getStreet() == null ? "" : mailAddress.getStreet()) + (mailAddress.getSection() == null ? "" : mailAddress.getSection()));
                    temporary.setDistrictHome(mailAddress.getDistrict() == null ? "" : mailAddress.getDistrict());
                    temporary.setApartmentHome(mailAddress.getApartment() == null ? "" : mailAddress.getApartment());
                    temporary.setDoorNumberHome(mailAddress.getDoorNumber() == null ? "" : mailAddress.getDoorNumber());
                } else {
                    temporary.setTownHome(homeAddress.getTownId());
                    temporary.setTownHomeName(homeAddress.getTown());
                    temporary.setLocalityHome(homeAddress.getLocalityId());
                    temporary.setLocalityHomeName(homeAddress.getLocality());
                    Log.e(TAG, "setUserInfo: " + (homeAddress.getStreet() == null ? "" : homeAddress.getStreet()) + (homeAddress.getSection() == null ? "" : homeAddress.getSection()));
                    temporary.setSectionHome((homeAddress.getStreet() == null ? "" : homeAddress.getStreet()) + (homeAddress.getSection() == null ? "" : homeAddress.getSection()));
                    temporary.setDistrictHome(homeAddress.getDistrict() == null ? "" : homeAddress.getDistrict());
                    temporary.setApartmentHome(homeAddress.getApartment()== null ? "" : homeAddress.getApartment());
                    temporary.setDoorNumberHome(homeAddress.getDoorNumber() == null ? "" : homeAddress.getDoorNumber());
                }
            }
        }

        temporary.setIsHomeAddress("1");
        if (user.getGender() != null)
            if (user.getGender().equals("Эрэгтэй")) {
                temporary.setGender("M");
            } else {
                temporary.setGender("F");
            }

        contract.setTemporary(temporary);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
