package mn.bitsup.callservice.view.error;

import androidx.annotation.NonNull;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.data.ErrorCodes;


/**
 * Created by Erdenedalai R&D B.V on 14/09/2017.
 * This is used by the ErrorView class to know which type of error it should display for.
 */
public enum ErrorType {
    EMPTY, OFFLINE, FAILED, SEARCH;

    public static ErrorType fromErrorResponse(@NonNull Response errorResponse) {
        return fromErrorResponseCode(errorResponse.getResponseCode());
    }

    public static ErrorType fromErrorResponseCode(int responseCode) {
        if (responseCode == ErrorCodes.NO_INTERNET.getCode()) {
            return ErrorType.OFFLINE;
        } else {
            return ErrorType.FAILED;
        }
    }
}
