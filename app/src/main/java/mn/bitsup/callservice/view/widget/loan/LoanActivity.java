package mn.bitsup.callservice.view.widget.loan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.activity.BaseActivity;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.loan.view.LoanFingerView;
import mn.bitsup.callservice.view.widget.loan.view.LoanOfferView;
import mn.bitsup.callservice.view.widget.loan.view.LoanSelectorView;
import mn.bitsup.callservice.view.widget.onboard.view.temporary.TPAddressCheckWidgetView;

public class LoanActivity extends BaseActivity {

    private ViewGroup mainContainer;
    private FragmentManager fragmentManager;
    private ImageButton backButton, closeButton;
    public static ProgressBar progressToolbar;
    public static RelativeLayout containerToolbar;
    private Context context;
    private Activity activity;
    private String type = "";
    public static Loan loan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.onboard_activity);
        context = LoanActivity.this;
        activity = LoanActivity.this;
        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new OnBackStackChangedListenerImpl());
        inflatedViews();
    }

    private void inflatedViews() {
        mainContainer = findViewById(R.id.content_frame);
        progressToolbar = findViewById(R.id.progressToolbar);
        backButton = findViewById(R.id.backButton);
        closeButton = findViewById(R.id.closeButton);
        containerToolbar = findViewById(R.id.containerToolbar);

        setProgressToolbar(15);
        initView();

    }

    private void initView() {
        backButton.setOnClickListener(v -> back());
        closeButton.setOnClickListener(v -> {
            if (type.equals("finger")){
                showCancelDialogTempo();
            }else{
                showCancelDialog();
            }
        });

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (type.equals("finger")) {
            fragmentTransaction.replace(R.id.content_frame, new LoanFingerView());
        }
        else if (type.equals("selector")) {
            loan = new Loan();
            fragmentTransaction.replace(R.id.content_frame, new LoanSelectorView(LoanOfferView.loanOffer));
        } else if (type.equals("address")) {
            loan = new Loan();
            fragmentTransaction.replace(R.id.content_frame, new TPAddressCheckWidgetView());
        }

        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static void hideToolbar() {
        containerToolbar.setVisibility(View.GONE);
    }

    private void showCancelDialog() {
        new AlertDialog.Builder(context)
                .setTitle("Зээл авахаа зогсоох")
                .setMessage("Та зээл авах үйлдэл зогсоох гэж байгаадаа итгэлтэй байна уу?")
                .setNegativeButton(R.string.shared_alert_button_no, (dialog, which) -> dialog.dismiss())
                .setPositiveButton(R.string.shared_alert_button_yes, (dialog, which) -> {
                    dialog.dismiss();
                    activity.finish();
                }).show();
    }

    private void showCancelDialogTempo() {
        new AlertDialog.Builder(context)
            .setTitle("Бүртгэл зогсоох")
            .setMessage("Та бүртгэлээ зогсоовол таны оруулсан мэдээллүүд устах болно")
            .setNegativeButton(R.string.shared_alert_button_no, (dialog, which) -> dialog.dismiss())
            .setPositiveButton(R.string.shared_alert_button_yes, (dialog, which) -> {
                dialog.dismiss();
                activity.finish();
            }).show();
    }

    private void back() {
        if (!CustomButton.isLoading) {
            if (fragmentManager != null) {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    KeyboardUtils.hideKeyboard(this);
                    fragmentManager.popBackStack();
                    setProgressToolbar(progressToolbar.getProgress() - 10);
                } else {
                    activity.finish();
                }
            } else {
                activity.finish();
            }
        }
    }


    public static void addProgress() {
        setProgressToolbar(progressToolbar.getProgress() + 15);
    }

    public static void fillProgress() {
        setProgressToolbar(100);
    }

    public static void setProgressToolbar(int width) {
        progressToolbar.setProgress(width);
    }

    private class OnBackStackChangedListenerImpl implements FragmentManager.OnBackStackChangedListener {
        @Override
        public void onBackStackChanged() {
        }
    }

    @Override
    public void onBackPressed() {
        if (!CustomButton.isLoading) {
            if (fragmentManager != null) {
                if (fragmentManager.getBackStackEntryCount() > 1) {
                    KeyboardUtils.hideKeyboard(this);
                    setProgressToolbar(progressToolbar.getProgress() - 10);
                } else {
                    activity.setResult(1);
                    activity.finish();
                }
            } else {
                activity.setResult(1);
                activity.finish();
            }
            super.onBackPressed();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
