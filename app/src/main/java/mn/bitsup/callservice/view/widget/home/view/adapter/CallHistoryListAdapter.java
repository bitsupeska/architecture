package mn.bitsup.callservice.view.widget.home.view.adapter;

import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.call.dto.CallHistory;
import mn.bitsup.callservice.view.custom.WidgetListAdapter;
import mn.bitsup.callservice.view.custom.WidgetListItemClickListener;
public class CallHistoryListAdapter extends WidgetListAdapter<CallHistoryViewHolder> {
    private List<CallHistory> callHistories = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    public CallHistoryListAdapter(@Nullable final OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    @NonNull
    @Override
    public CallHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.call_history_item, parent, false);
        return new CallHistoryViewHolder(listItemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CallHistoryViewHolder holder, int position) {
        holder.bind(callHistories.get(position));
        animateItemAppearance(holder, holder.getItemView());
    }
    public void updateWith(List<CallHistory> callHistories) {
        this.callHistories.clear();
        this.callHistories.addAll(callHistories);
        notifyDataSetChanged();
    }

    public void appendWith(List<CallHistory> callHistories) {
        int positionStart = this.callHistories.size();
        this.callHistories.addAll(callHistories);
        notifyItemRangeInserted(positionStart, callHistories.size());
    }
    @Override
    public int getItemCount() {
        return callHistories.size();
    }


    public interface OnItemClickListener extends WidgetListItemClickListener<CallHistory> {
        void onItemClicked(CallHistory item);
    }
}
