package mn.bitsup.callservice.view.widget.card.util.formatter;


import androidx.annotation.IntRange;
import java.util.ArrayList;
import java.util.List;

public final class CardNumberFormat {
    static final char DEFAULT_MASK_CHAR = '●';
    private static final int DEFAULT_GROUPBY_NUMBER = 4;
    private static final int DEFAULT_LENGTH = 16;
    private static final int MIN_LENGTH = 1;
    private List<Integer> groupList = new ArrayList();
    private char maskChar = 9679;
    private CardNumberFormat.MaskingInterval maskingInterval = new CardNumberFormat.MaskingInterval(0, 0);
    @IntRange(
        from = 1L
    )
    private int length = 16;

    public CardNumberFormat() {
    }

    public void setMaskChar(char maskChar) {
        this.maskChar = maskChar;
    }

    List<Integer> getGroups() {
        return this.groupList;
    }

    CardNumberFormat.MaskingInterval getMaskingInterval() {
        return this.maskingInterval;
    }

    char getMaskChar() {
        return this.maskChar;
    }

    @IntRange(
        from = 1L
    )
    int getLength() {
        return this.length;
    }

    public static final class Formats {
        static List<Integer> groups = new ArrayList();
        static List<Integer> groupsForDinersClub;
        static List<Integer> groupsForAmericanExpress;
        public static final CardNumberFormat VISA_CARD;
        public static final CardNumberFormat MASTER_CARD;
        public static final CardNumberFormat VISA_DEBIT_CARD;
        public static final CardNumberFormat DINERS_CLUB_CARD;
        public static final CardNumberFormat AMERICAN_EXPRESS_CARD;

        private static CardNumberFormat createFromDefaultFormat() {
            return (new CardNumberFormat.FormatBuilder()).withLength(16).withGroups(groups).withMaskingInterval(new CardNumberFormat.MaskingInterval(0, 11)).withMaskChar('●').build();
        }

        private Formats() {
        }

        static {
            groups.add(4);
            groupsForDinersClub = new ArrayList();
            groupsForDinersClub.add(4);
            groupsForDinersClub.add(6);
            groupsForDinersClub.add(4);
            groupsForAmericanExpress = new ArrayList();
            groupsForAmericanExpress.add(4);
            groupsForAmericanExpress.add(6);
            groupsForAmericanExpress.add(5);
            VISA_CARD = createFromDefaultFormat();
            MASTER_CARD = createFromDefaultFormat();
            VISA_DEBIT_CARD = createFromDefaultFormat();
            DINERS_CLUB_CARD = (new CardNumberFormat.FormatBuilder()).withLength(14).withGroups(groupsForDinersClub).withMaskingInterval(new CardNumberFormat.MaskingInterval(0, 9)).withMaskChar('●').build();
            AMERICAN_EXPRESS_CARD = (new CardNumberFormat.FormatBuilder()).withLength(15).withGroups(groupsForAmericanExpress).withMaskingInterval(new CardNumberFormat.MaskingInterval(0, 11)).withMaskChar('●').build();
        }
    }

    public static final class FormatBuilder {
        private CardNumberFormat cardNumberFormat = new CardNumberFormat();

        public FormatBuilder() {
        }

        public CardNumberFormat.FormatBuilder withGroups(List<Integer> groupList) {
            this.cardNumberFormat.groupList = groupList;
            return this;
        }

        public CardNumberFormat.FormatBuilder withMaskingInterval(CardNumberFormat.MaskingInterval maskingInterval) {
            this.cardNumberFormat.maskingInterval = maskingInterval;
            return this;
        }

        public CardNumberFormat.FormatBuilder withMaskChar(char maskChar) {
            this.cardNumberFormat.maskChar = maskChar;
            return this;
        }

        public CardNumberFormat.FormatBuilder withLength(int length) {
            this.cardNumberFormat.length = length;
            return this;
        }

        public CardNumberFormat build() {
            CardNumberFormat cf = new CardNumberFormat();
            cf.groupList = this.cardNumberFormat.groupList;
            cf.maskingInterval = this.cardNumberFormat.maskingInterval;
            cf.maskChar = this.cardNumberFormat.maskChar;
            cf.length = this.cardNumberFormat.length;
            return cf;
        }
    }

    public static final class MaskingInterval {
        private int start;
        private int end;

        public MaskingInterval(int start, int end) {
            this.start = start;
            this.end = end;
        }

        public int getStart() {
            return this.start;
        }

        public int getEnd() {
            return this.end;
        }
    }
}
