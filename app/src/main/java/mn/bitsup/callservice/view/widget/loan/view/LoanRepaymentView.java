package mn.bitsup.callservice.view.widget.loan.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.loan.dto.CustomViewPager;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.view.widget.loan.view.fragment.QpayView;
import mn.bitsup.callservice.view.widget.loan.view.fragment.TransactionView;

public class LoanRepaymentView extends Fragment {
    private Context context;
    private TabLayout tabLayout;
    private CustomViewPager viewPager;
    private LoanAccountData loanAccountData;

    public LoanRepaymentView(LoanAccountData loanAccountData) {
        this.loanAccountData = loanAccountData;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_repayment, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        viewPager = inflatedView.findViewById(R.id.viewPager);
        tabLayout = inflatedView.findViewById(R.id.tabs);
        setupViewPager(viewPager);
        viewPager.setCurrentItem(2);
        tabLayout.setupWithViewPager(viewPager);

    }

    private void initView() {
        disableTabListener();
    }

    private void disableTabListener() {
        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
    }

    //    private void tabSelectHideKeyboard(){
    //        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
//            @Override
//            public void onTabSelected(TabLayout.Tab tab) {
//                KeyboardUtils.hideKeyboard(getActivity());
//            }
//
//            @Override
//            public void onTabUnselected(TabLayout.Tab tab) {
//
//            }
//
//            @Override
//            public void onTabReselected(TabLayout.Tab tab) {
//                KeyboardUtils.hideKeyboard(getActivity());
//            }
//        });
//    }
    private void setupViewPager(CustomViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        Objects.requireNonNull(getChildFragmentManager()).beginTransaction().addToBackStack(null);
        getChildFragmentManager().beginTransaction().commit();
        adapter.addFragment(new QpayView(this.loanAccountData), "QPay");
        adapter.addFragment(new TransactionView(this.loanAccountData), "Шилжүүлэг");
        viewPager.setSwipeable(false);
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            viewPager.setSwipeable(false);
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
