package mn.bitsup.callservice.view.custom.datepicker;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 29/03/2018.
 * Recycler decorator to draw circles around specific dates.
 */
public class CellItemDecorator extends RecyclerView.ItemDecoration {

    private Context context;
    private StyleProvider styler;
    private Paint painter;
    private final int radius;
    private List<DatePickerItem> datePickerItems;

    public CellItemDecorator(Context context, StyleProvider styler, List<DatePickerItem> datePickerItems){
        this.context = context;
        this.styler = styler;
        this.datePickerItems = datePickerItems;
        this.painter = new Paint();
        this.painter.setAntiAlias(true);
        this.radius = (int)context.getResources().getDimension(R.dimen.date_picker_delivery_date_radius);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            List<DatePickerItem> datePickerItems = getDatePickerItem(i);

            for(DatePickerItem pickerItem : datePickerItems){
                if(pickerItem != null){
                    final ViewGroup child = (ViewGroup) parent.getChildAt(i);
                    TextView textView = (TextView) child.getChildAt(0);

                    try {
                        Drawable background = textView.getBackground().mutate();
                        ((GradientDrawable) background).setSize(radius, radius);
                        if (!pickerItem.isFilled()) {
                            ((GradientDrawable) background).setStroke(2, ContextCompat
                                .getColor(context, pickerItem.getDateColor()));
                        } else {
                            ((GradientDrawable) background).setColor(
                                ContextCompat.getColor(context, pickerItem.getDateColor()));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private List<DatePickerItem> getDatePickerItem(int index){
        List<DatePickerItem> datePickerItemsToReturn = new ArrayList<>();
        for (DatePickerItem datePickerItem : datePickerItems){
            if(datePickerItem.getDateIndex() == index){
                datePickerItemsToReturn.add(datePickerItem);
            }
        }
        return datePickerItemsToReturn;
    }
}
