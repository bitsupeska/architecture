package mn.bitsup.callservice.view.widget.passcode.core.contract;

import mn.bitsup.callservice.client.dataprovider.listener.PasscodeListener;
import mn.bitsup.callservice.client.profile.listener.UserListener;

public interface PasscodeContract{

    int getNumberOfDigits();

    void passcodeEntered(char[] var1, PasscodeListener var2, String var3);

    void getUser(UserListener var1);
}
