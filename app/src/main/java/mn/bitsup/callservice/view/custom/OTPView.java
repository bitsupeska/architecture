package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.TextViewColor;
import mn.bitsup.callservice.view.util.KeyboardUtils;

public class OTPView extends LinearLayout implements OnClickListener {
    private EditText otp1;
    private EditText otp2;
    private EditText otp3;
    private EditText otp4;
    private TextView textLink;
    private TextView textError;
    private OTPViewLisener otpViewLisener;
    private CountDownTimer Count;
    private int time = 60000;
    private String message = "0 секунд \n Мессэж ирээгүй юу? Энд дар";
    private String phoneNumber, timedMessage;
    private Context context;

    public OTPView(Context context) {
        this(context, null);
    }

    public OTPView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(context);
    }

    private void init(Context context) {
        View inflatedView = inflate(context, R.layout.view_otp, this);
        textLink = inflatedView.findViewById(R.id.textLink);
        textError = inflatedView.findViewById(R.id.textError);
        otp1 = inflatedView.findViewById(R.id.otp1);
        otp2 = inflatedView.findViewById(R.id.otp2);
        otp3 = inflatedView.findViewById(R.id.otp3);
        otp4 = inflatedView.findViewById(R.id.otp4);

        otp1.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
        otp2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
        otp3.setFilters(new InputFilter[]{new InputFilter.LengthFilter(2)});
        otp4.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1)});

        otp1.addTextChangedListener(new GenericTextWatcher(otp1));
        otp2.addTextChangedListener(new GenericTextWatcher(otp2));
        otp3.addTextChangedListener(new GenericTextWatcher(otp3));
        otp4.addTextChangedListener(new GenericTextWatcher(otp4));

        textLink.setEnabled(false);
        textLink.setText(TextViewColor.getUnderlineTextHint(getContext(), message, message.length() - 7, message.length()));

        createTimer();
        Count.start();
        KeyboardUtils.showKeyboard(otp1);
    }

    public void setOTPPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    void createTimer() {
        Count = new CountDownTimer(time, 1000) {
            public void onTick(long millisUntilFinished) {
                timedMessage = (millisUntilFinished / 1000) + message.substring(1);
                textLink.setEnabled(false);
                textLink.setText(TextViewColor.getUnderlineTextHint(getContext(), timedMessage, timedMessage.length() - 7, timedMessage.length()));
            }

            public void onFinish() {
                textLink.setEnabled(true);
                textLink.setText(TextViewColor.getUnderlineText(getContext(), message, message.length() - 7, message.length()));
                showErrorLabel(context.getResources().getString(R.string.otp_error_timer));
            }
        };
    }

    public void setLabel() {
        textLink.setEnabled(true);
        textLink.setText(TextViewColor.getUnderlineText(getContext(),
                message, message.length() - 7, message.length()));
    }

    public void hideErrorLabel() {
        textError.setVisibility(View.GONE);
        emptyLabel();
        otp1.setBackgroundResource(R.drawable.otp_border);
        otp2.setBackgroundResource(R.drawable.otp_border);
        otp3.setBackgroundResource(R.drawable.otp_border);
        otp4.setBackgroundResource(R.drawable.otp_border);

    }

    public void emptyLabel() {
        otp1.setText("");
        otp2.setText("");
        otp3.setText("");
        otp4.setText("");
        KeyboardUtils.showKeyboard(otp1);
    }

    public void hideMessage() {
        textLink.setVisibility(View.GONE);
    }

    public void showErrorLabel(String error) {
        emptyLabel();
        textError.setVisibility(View.VISIBLE);
        textError.setText(error);
        otp1.setBackgroundResource(R.drawable.otp_border_error);
        otp2.setBackgroundResource(R.drawable.otp_border_error);
        otp3.setBackgroundResource(R.drawable.otp_border_error);
        otp4.setBackgroundResource(R.drawable.otp_border_error);
    }

    public void setFilledListener(OTPViewLisener listener) {
        this.otpViewLisener = listener;
    }

    public void setLinkOnClickListener(@Nullable final OnClickListener onClickListener) {
        textLink.setOnClickListener(onClickListener);
    }

    public void stopTimer() {
        Count.cancel();
    }

    public void startTimer() {
        Count.start();
    }

    public void startTimer(int time) {
        this.time = time;
        Count.cancel();
        createTimer();
        Count.start();
    }

    private void changeBackground(EditText editText) {
        if (editText.getText().toString().trim().equals("")) {
            editText.setBackgroundResource(R.drawable.otp_border);
        } else {
            editText.setBackgroundResource(R.drawable.otp_border_focus);
        }
    }

    public void hideKeyboard() {
        KeyboardUtils.hideKeyboard(otp4);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.otp1:
                otp1.requestFocus();
                KeyboardUtils.showKeyboard(otp1);
                break;
            case R.id.otp2:
                otp2.requestFocus();
                KeyboardUtils.showKeyboard(otp2);
                break;
            case R.id.otp3:
                otp3.requestFocus();
                KeyboardUtils.showKeyboard(otp3);
                break;
            case R.id.otp4:
                otp4.requestFocus();
                KeyboardUtils.showKeyboard(otp4);
                break;
        }

    }

    public void setMessage(String s) {
        message = s;
    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            String text = editable.toString();
            setTextErrorGone();
            switch (view.getId()) {
                case R.id.otp1:
                    changeBackground(otp1);
                    if (otp1.getText().toString().length() == 2) {
                        otp1.setText(text.substring(0, 1));
                        otp2.setText(text.substring(1));
                        otp2.requestFocus();
                    }
                    if (otp1.getText().toString().length() == 1) {
                        otp1.setSelection(1);
                    }
                    break;
                case R.id.otp2:
                    changeBackground(otp2);
                    if (otp2.getText().toString().length() == 2) {
                        otp2.setText(text.substring(0, 1));
                        otp3.setText(text.substring(1));
                        otp3.requestFocus();
                    } else if (otp2.getText().toString().length() == 1) {
                        otp2.setSelection(1);
                        changeBackground(otp2);
                    } else if (text.length() == 0) {
                        otp1.requestFocus();
                        changeBackground(otp2);
                    }
                    break;
                case R.id.otp3:
                    changeBackground(otp3);
                    if (otp3.getText().toString().length() == 2) {
                        otp3.setText(text.substring(0, 1));
                        otp4.setText(text.substring(1));
                        otp4.requestFocus();
                    } else if (otp3.getText().toString().length() == 1) {
                        otp3.setSelection(1);
                    } else if (text.length() == 0) {
                        otp2.requestFocus();
                        changeBackground(otp3);
                    }
                    break;
                case R.id.otp4:
                    changeBackground(otp4);
                    if (otp4.getText().toString().length() == 2) {
                        otp4.setText(text.substring(0, 1));
                    } else if (otp4.getText().toString().length() == 1) {
                        otp4.setSelection(1);
                        String result_otp = otp1.getText().toString().trim() + otp2.getText().toString().trim() + otp3.getText().toString().trim() + otp4.getText().toString().trim();
                        if (result_otp.length() == 4) {
                            otpViewLisener.onSuccess(result_otp);
                        }
                    } else if (otp4.getText().toString().length() == 0) {
                        otp3.requestFocus();
                        changeBackground(otp4);
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    }

    private void setTextErrorGone() {
        String text = otp1.getText().toString().trim() + otp2.getText().toString().trim() + otp3.getText().toString().trim() + otp4.getText().toString().trim();
        if (text.length() == 0) {
            textError.setVisibility(View.GONE);
        }
    }

    public void setTime(int time) {
        this.time = time;
    }
}
