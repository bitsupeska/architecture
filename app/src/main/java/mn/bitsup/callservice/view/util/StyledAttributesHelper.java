package mn.bitsup.callservice.view.util;

import android.content.res.TypedArray;
import androidx.annotation.Nullable;

/**
 * Created by Erdenedalai R&D B.V on 21/06/2018.
 * Helper to get a String from styled attributes or default to given value if not present or if styledAttributes is null
 */
public class StyledAttributesHelper {
    private StyledAttributesHelper() {
        //Hide constructor
    }

    /**
     * @param styledAttributes TypedArray of styled attributes
     * @param styleable        styleable to look for
     * @param defaultValue     value to default to if given styleable not present or if styledAttributes is null
     * @return String from styled attributes or default to given styleable if not present or if styledAttributes is null.
     */
    public static String getStyleableAttributeString(@Nullable TypedArray styledAttributes, int styleable, String defaultValue) {
        if (styledAttributes == null) {
            return defaultValue;
        }
        String retVal = styledAttributes.getString(styleable);
        if (retVal == null) {
            return defaultValue;
        }
        return retVal;
    }
}
