package mn.bitsup.callservice.view.widget.passcode.core;

import android.content.Context;

import androidx.annotation.NonNull;
import mn.bitsup.callservice.client.RemoteAuthClient;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeAuthListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.dataprovider.util.ResponseCodes;
import mn.bitsup.callservice.client.profile.ProfileClient;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.view.widget.passcode.PasscodeWidgetConfirmView;
import mn.bitsup.callservice.view.widget.passcode.core.contract.PasscodeContract;
import mn.bitsup.callservice.client.dataprovider.listener.PasscodeListener;
import mn.bitsup.callservice.view.widget.setupcompleted.SetupCompletedWidgetView;

public class PasscodeWidget implements PasscodeContract {

    private final Context context;
    private ProfileClient profileClient;
    private RemoteAuthClient authClient;

    public PasscodeWidget(@NonNull Context context) {
        this.context = context;
        this.authClient = ClientsManager.getAuthClient();
        this.profileClient = ClientsManager.getProfileClient();
    }


    public int getNumberOfDigits() {
        return 4;
    }

    public void passcodeEntered(char[] passcode, PasscodeListener listener, String mode) {
        switch(mode) {
            case "AUTHENTICATE":
                this.authenticatedPasscode(passcode, listener);
                break;
            case "SETUP":
                this.setupPasscode(passcode);
                break;
            case "CONFIRM":
                this.confirmPasscode(passcode, listener);
                break;
            default:
                throw new IllegalArgumentException("This should not have happened");
        }
    }



    @Override
    public void getUser(UserListener userListener) {
        profileClient.getUser(new UserListener() {
            @Override
            public void onSuccess(User var1) {
                userListener.onSuccess(var1);
                authClient.setUser(var1);
            }

            @Override
            public void onError(Response var1) {
                userListener.onError(var1);
            }
        });
    }

    private void authenticatedPasscode(@NonNull char[] passcode, @NonNull final PasscodeListener listener) {
        authClient.authenticate(passcode, new PasscodeAuthListener() {
            public void onSuccess() {
                listener.onSuccess();

            }
            public void onError(Response errorResponse) {
                if(errorResponse.getResponseCode() == ResponseCodes.ERROR.getCode()){
                    listener.passcodeMatchingFailed();
                }else{
                    listener.onError(errorResponse);
                }

            }
        });
    }

    private void setupPasscode(@NonNull char[] passcode) {
        StorageComponent inMemoryStorage = new StorageComponent();
        inMemoryStorage.setItem("bb.key.passcode.passcode", String.valueOf(passcode));
        EventHelper.publish(context, new PasscodeWidgetConfirmView());
    }

    private void confirmPasscode(@NonNull char[] passcode, @NonNull final PasscodeListener listener) {
         StorageComponent inMemoryStorage = new StorageComponent();
         String setUpPasscode = inMemoryStorage.getItem("bb.key.passcode.passcode");

        if (setUpPasscode != null && setUpPasscode.equals(String.valueOf(passcode))) {
               authClient.setPasscode(passcode, new PasscodeAuthListener() {
                    public void onSuccess() {
                        listener.onSuccess();
                        inMemoryStorage.removeItem("bb.key.passcode.passcode");
                        EventHelper.publish(context, new SetupCompletedWidgetView());
                    }
                    public void onError(Response errorResponse) {
                        listener.onError(errorResponse);
                    }
                });

        } else {
            listener.passcodeMatchingFailed();
        }
    }
}
