package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardContractDescActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBTermsWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private CheckBox checkBoxConfirm;
    private TextView textBody;
    private Context context;
    private CustomButton buttonContinue;
    private OnboardContract contract;
    private EasyOnboardItem payload;
    private ListView listView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
        payload = contract.getPayload();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_terms, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        checkBoxConfirm = inflatedView.findViewById(R.id.checkBoxConfirm);
        textBody = inflatedView.findViewById(R.id.textBody);
        listView = inflatedView.findViewById(R.id.listView);
    }

    private void initView() {
        setListData();
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
        buttonContinue.setOnClickListener(v -> createUser());

        checkBoxConfirm.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!buttonContinue.isLoading()) {
                if (isChecked) {
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                } else {
                    buttonContinue.setState(ButtonState.DISABLED, getActivity());
                }
            }
        });

        ListAdapter adapter = new ListAdapter();
        listView.setAdapter(adapter);
        textBody.setText(textBody.getText().toString() +
                Html.fromHtml(
                        "Нөгөө талаас " + "<font color='#11274B'><b>"
                                + payload.getLastName() + "</b></font>" + " овогтой " + "<font color='#11274B'><b>"
                                + payload.getFirstName() + "</b></font>" + " (Регистрийн дугаар " + "<font color='#11274B'><b>"
                                + payload.getRegNum() + "</b></font>"
                                + "  цаашид “Харилцагч” гэх) нар дор дурдсан нөхцөлүүдийг харилцан тохиролцов."));
    }

    private String getRawData(int body) throws IOException {
        Resources res = context.getResources();
        InputStream in_s = res.openRawResource(body);
        byte[] b = new byte[in_s.available()];
        in_s.read(b);
        return new String(b);
    }

    private void createUser() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        disableScreen(true);

        String macAddress = DeviceAddressUtil.getMACAddress(getContext(), "wlan0");
        String ipAddress = DeviceAddressUtil.getIPAddress(true);
        payload.setMacAddress(macAddress);
        payload.setIpAddress(ipAddress);
        payload.setFireBaseToken("NULL");
        contract.setPayload(payload);

        contract.createUser(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse response) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                if (response.getStatus().equals("SUCCESS")) {
                    disableScreen(false);
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                    OnboardActivity.addProgress();
                    EventHelper.publishBack(context, new OBCongratsWidgetView());
                } else {
                    CustomAlertDialog.showAlertDialog(context, "", response.getMessage());
                }
            }

            @Override
            public void onError(Response var2) {
                disableScreen(false);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, payload);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void disableScreen(boolean enable) {
        if (enable) {
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private ArrayList<String> titles;
    private List<Integer> body;

    private void setListData() {
        titles = new ArrayList<>();
        titles.add("НИЙТЛЭГ ҮНДЭСЛЭЛ");
        titles.add("ХАРИЛЦАГЧ ТА ЭНЭХҮҮ ҮЙЛЧИЛГЭЭНИЙ НӨХЦӨЛИЙГ ЗӨВШӨӨРСНӨӨР ББСБ-Д ДАРААХ ЗӨВШӨӨРӨЛ, ЭРХИЙГ ОЛГОЖ БАЙГАА БОЛНО");
        titles.add("ББСБ НЬ ХАРИЛЦАГЧИЙН МЭДЭЭЛЛИЙГ ГАГЦХҮҮ ДАРААХ ЗОРИЛГООР АШИГЛАНА");
        titles.add("ХАРИЛЦАГЧ ТА ЭНЭХҮҮ ҮЙЛЧИЛГЭЭНИЙ НӨХЦӨЛИЙГ ЗӨВШӨӨРСНӨӨР ДАРААХ ЭРХ, ҮҮРЭГ, ХАРИУЦЛАГЫГ ХҮЛЭЭНЭ");
        titles.add("ББСБ ҮЙЛЧИЛГЭЭГ SIMPLE APP-ЭЭР ДАМЖУУЛАН ҮЗҮҮЛЭХДЭЭ ДАРААХ ЭРХ, ҮҮРЭГ, ХАРИУЦЛАГЫГ ХҮЛЭЭНЭ");
        titles.add("БУСАД НӨХЦӨЛ");
        body = new ArrayList<>();
        body.add(R.raw.contract1);
        body.add(R.raw.contract2);
        body.add(R.raw.contract3);
        body.add(R.raw.contract4);
        body.add(R.raw.contract5);
        body.add(R.raw.contract6);
    }

    public class ListAdapter extends BaseAdapter {
        public ListAdapter() {
        }

        @Override
        public int getCount() {
            return titles == null ? 0 : titles.size();
        }

        @Override
        public Object getItem(int position) {
            return titles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.onboard_contract_list_item, null);

            RelativeLayout containerItem = view.findViewById(R.id.containerItem);
            TextView textTitle = view.findViewById(R.id.textTitle);

            textTitle.setText((position + 1) + ". " + titles.get(position));
            containerItem.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), OnboardContractDescActivity.class);
                intent.putExtra("title", (position + 1) + ". " + titles.get(position));
                try {
                    intent.putExtra("body", getRawData(body.get(position)));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                getContext().startActivity(intent);
            });
            return view;
        }
    }
}
