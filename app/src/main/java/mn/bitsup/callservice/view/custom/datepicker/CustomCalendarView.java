package mn.bitsup.callservice.view.custom.datepicker;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.StyleRes;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import mn.bitsup.callservice.R;

public class CustomCalendarView extends LinearLayout implements CalendarViewAdapter.ItemClickListener {

    private ImageView imagePreviousMonth, imageNextMonth;
    private TextView textDateSummary, textCurrentDate;
    private ViewGroup currentDateContainer;
    private ViewPager calendarItemPager;
    private CalendarViewAdapter calendarItemAdapter;
    private Styler styler;
    private Calendar calendar;
    private Context context;
    private SimpleDateFormat currentDateFormat = DEFAULT_DATE_FORMAT;
    private SimpleDateFormat dateSummaryFormat = DEFAULT_DATE_SUMMARY_FORMAT;
    private ItemClickListener itemClickListener;
    private static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("MMMM yyyy");
    private static final SimpleDateFormat DEFAULT_DATE_SUMMARY_FORMAT = new SimpleDateFormat("MMMM dd, yyyy");

    public CustomCalendarView(Context context, List<DatePickerItem> datePickerItems, Date minDate) {
        this(context, null);
        this.context = context;
        setup(datePickerItems, minDate, null);
    }

    public CustomCalendarView(Context context, List<DatePickerItem> datePickerItems, Date minDate, Date selectedDate) {
        this(context, null);
        this.context = context;
        setup(datePickerItems, minDate, selectedDate);
    }

    CustomCalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void setup(List<DatePickerItem> datePickerItems, Date minDate, Date selectedDate) {
        this.calendar = Calendar.getInstance(Locale.ENGLISH);
        if (selectedDate != null) {
            this.calendar.setTime(selectedDate);
        } else if (minDate != null) {
            this.calendar.setTime(minDate);
        }

        this.styler = new Styler(this);
        this.initializeViews();
        this.setupCalendar(datePickerItems, minDate);
        this.setupListeners();
        this.setCurrentDate(calendar.getTime());
        this.setDateSummary();
        this.applyStyler();
    }

    private void initializeViews() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.custom_calendar_view, this);
        this.imagePreviousMonth = view.findViewById(R.id.prevMonth);
        this.imageNextMonth = view.findViewById(R.id.nextMonth);
        this.textDateSummary = view.findViewById(R.id.dateSummary);
        this.textCurrentDate = view.findViewById(R.id.currentDate);
        this.calendarItemPager = view.findViewById(R.id.calendarItemPager);
        this.currentDateContainer = view.findViewById(R.id.currentDateContainer);
    }

    private void setupListeners() {
        imagePreviousMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar currentCalendar = Calendar.getInstance();
                Calendar currentVisibleCalendar = calendarItemAdapter.getCalendarCurrent();
                if (currentVisibleCalendar.get(Calendar.MONTH) > currentCalendar.get(Calendar.MONTH) || currentVisibleCalendar.get(Calendar.YEAR) > currentCalendar.get(Calendar.YEAR)) {
                    calendarItemPager.setCurrentItem(0);
                }
            }
        });
        imageNextMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarItemPager.setCurrentItem(2);
            }
        });
    }

    private void setupCalendar(List<DatePickerItem> datePickerItems, Date minDate) {
        calendarItemAdapter = new CalendarViewAdapter(context, calendar, styler, datePickerItems, minDate);
        calendarItemAdapter.setItemClickListener(this);
        calendarItemPager.setAdapter(calendarItemAdapter);
        calendarItemPager.setCurrentItem(1);
        calendarItemPager.addOnPageChangeListener(new PagerOnPageChangeListener());
    }

    private void setCurrentDate(Date date) {
        textCurrentDate.setText(currentDateFormat.format(date));
    }

    @SuppressLint("StringFormatInvalid")
    private void setDateSummary() {
        Date today = Calendar.getInstance().getTime();
        textDateSummary.setText(String.format(
                context.getResources().getString(R.string.date_picker_top_title), dateSummaryFormat.format(today)));
    }

    @SuppressWarnings("unused")
    public void setCurrentDateFormat(SimpleDateFormat currentDateFormat) {
        this.currentDateFormat = currentDateFormat;
    }

    @SuppressWarnings("unused")
    public void setDateSummaryFormat(SimpleDateFormat dateSummaryFormat) {
        this.dateSummaryFormat = dateSummaryFormat;
    }

    @SuppressWarnings("unused")
    public void setDateSummaryHidden() {
        this.textDateSummary.setVisibility(View.GONE);
    }

    @SuppressWarnings("unused")
    public Styler getStyler() {
        return styler;
    }

    @SuppressWarnings("unused")
    public void applyStyler() {
        if (styler != null) {
            styler.apply();
        }
    }

    @Override
    public void onItemClick(Date selectedDate) {
        if (itemClickListener != null) {
            itemClickListener.onItemClick(selectedDate);
        }
    }

    private class PagerOnPageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            // not implemented
        }

        @Override
        public void onPageSelected(int position) {
            if (position == 0) {
                calendarItemAdapter.removeView(2);
                calendarItemAdapter.insertView(0);
                calendarItemAdapter.notifyDataSetChanged();
                setCurrentDate(calendarItemAdapter.getCalendarCurrent().getTime());
            } else if (position == 2) {
                calendarItemAdapter.removeView(0);
                calendarItemAdapter.addView();
                calendarItemAdapter.notifyDataSetChanged();
                setCurrentDate(calendarItemAdapter.getCalendarCurrent().getTime());
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            // not implemented
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Date selectedDate);
    }

    public class Styler implements StyleProvider {

        private CustomCalendarView customCalendarView;
        private int daysHeaderBgColor;
        private int daysHeaderTextStyle;
        private int currentDateBgColor;
        private int currentDateTextStyle;
        private int toNextMonthButtonStyle;
        private int toPrevMonthButtonStyle;
        private int minDateTextStyle;
        private int afterMinDateTextStyle;
        private int beforeMinDateTextStyle;
        private int deliveryDateCircleColor;
        private int deliveryDateTextStyle;
        private int rushDeliveryDateCircleColor;

        private Styler(CustomCalendarView customCalendarView) {
            this.customCalendarView = customCalendarView;
            this.setDefaultStyling();
        }

        private void setDefaultStyling() {
            setDaysHeaderBgColor(R.color.date_picker_day_header_bg_color, false);
            setDaysHeaderTextStyle(R.style.CustomDatePickerDayHeaderTextStyle, false);
            setCurrentDateViewBgColor(R.color.date_picker_current_day_view_bg_color, false);
            setCurrentDateTextStyle(R.style.CustomDatePickerCurrentDateTextStyle, false);
            setToNextMonthImageResource(R.drawable.ic_right_arrow, false);
            setToPrevMonthImageResource(R.drawable.ic_left_arrow, false);
            // styles and colors for the adapter
            setMinDateTextStyle(R.style.CustomDatePickerMinDateTextStyle, false);
            setAfterMinDateTextStyle(R.style.CustomDatePickerAfterMinDateTextStyle, false);
            setBeforeMinDateTextStyle(R.style.CustomDatePickerBeforeMinDateTextStyle, false);
            setDeliveryDateCircleColor(R.color.date_picker_delivery_date_color, false);
            setDeliveryDateTextStyle(R.style.CustomDatePickerRushDeliveryTextStyle, false);
            setRushDeliveryDateCircleColor(R.color.date_picker_rush_delivery_date_color, false);
        }

        private void apply() {
            for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                item.applySwipableViewDaysHeaderBgColor(this.daysHeaderBgColor);
            }
            customCalendarView.currentDateContainer.setBackgroundColor(
                ContextCompat.getColor(context, currentDateBgColor));
            this.applyCurrentDateTextStyle();
            this.applyDaysHeaderTextStyle();
            customCalendarView.imageNextMonth.setImageDrawable(
                ContextCompat.getDrawable(context, toNextMonthButtonStyle));
            customCalendarView.imagePreviousMonth.setImageDrawable(
                ContextCompat.getDrawable(context, toPrevMonthButtonStyle));
            this.updateView();
        }

        private void applyCurrentDateTextStyle() {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                customCalendarView.textCurrentDate.setTextAppearance(context, currentDateTextStyle);
            } else {
                customCalendarView.textCurrentDate.setTextAppearance(currentDateTextStyle);
            }
        }

        private void applyDaysHeaderTextStyle() {
            for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                item.applySwipableViewDaysStyle(daysHeaderTextStyle);
            }
        }

        public Styler setDaysHeaderBgColor(@ColorRes int daysHeaderBgColor, boolean redraw) {
            this.daysHeaderBgColor = daysHeaderBgColor;
            if (redraw) {
                for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                    item.applySwipableViewDaysHeaderBgColor(this.daysHeaderBgColor);
                }
                this.updateView();
            }
            return this;
        }

        public Styler setDaysHeaderTextStyle(@StyleRes int daysHeaderTextStyle, boolean redraw) {
            this.daysHeaderTextStyle = daysHeaderTextStyle;
            if (redraw) {
                this.applyDaysHeaderTextStyle();
                this.updateView();
            }
            return this;
        }

        public Styler setCurrentDateViewBgColor(@ColorRes int currentDateBgColor, boolean redraw) {
            this.currentDateBgColor = currentDateBgColor;
            if (redraw) {
                customCalendarView.currentDateContainer.setBackgroundColor(
                    ContextCompat.getColor(context, this.currentDateBgColor));
                this.updateView();
            }
            return this;
        }

        public Styler setCurrentDateTextStyle(@StyleRes int currentDateTextStyle, boolean redraw) {
            this.currentDateTextStyle = currentDateTextStyle;
            if (redraw) {
                this.applyCurrentDateTextStyle();
                this.updateView();
            }
            return this;
        }

        public Styler setMinDateTextStyle(@StyleRes int minDateTextStyle, boolean redraw) {
            this.minDateTextStyle = minDateTextStyle;
            if (redraw) {
                this.updateView();
            }
            return this;
        }

        public Styler setToNextMonthImageResource(@DrawableRes int toNextMonthButtonStyle, boolean redraw) {
            this.toNextMonthButtonStyle = toNextMonthButtonStyle;
            if (redraw) {
                customCalendarView.imageNextMonth.setImageDrawable(
                    ContextCompat.getDrawable(context, toNextMonthButtonStyle));
                this.updateView();
            }
            return this;
        }

        public Styler setToPrevMonthImageResource(@DrawableRes int toPrevMonthButtonStyle, boolean redraw) {
            this.toPrevMonthButtonStyle = toPrevMonthButtonStyle;
            if (redraw) {
                customCalendarView.imagePreviousMonth.setImageDrawable(
                    ContextCompat.getDrawable(context, toPrevMonthButtonStyle));
                updateView();
            }
            return this;
        }

        public Styler setAfterMinDateTextStyle(@StyleRes int afterMinDateTextStyle, boolean redraw) {
            this.afterMinDateTextStyle = afterMinDateTextStyle;
            if (redraw) {
                for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                    item.getCalendarAdapter().notifyDataSetChanged();
                }
            }
            return this;
        }

        public Styler setBeforeMinDateTextStyle(@StyleRes int beforeMinDateTextStyle, boolean redraw) {
            this.beforeMinDateTextStyle = beforeMinDateTextStyle;
            if (redraw) {
                for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                    item.getCalendarAdapter().notifyDataSetChanged();
                }
            }
            return this;
        }

        public Styler setDeliveryDateCircleColor(@ColorRes int deliveryDateColor, boolean redraw) {
            this.deliveryDateCircleColor = deliveryDateColor;
            if (redraw) {
                for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                    item.getCalendarAdapter().notifyDataSetChanged();
                }
            }
            return this;
        }

        public Styler setDeliveryDateTextStyle(@StyleRes int deliveryDateTextStyle, boolean redraw) {
            this.deliveryDateTextStyle = deliveryDateTextStyle;
            if (redraw) {
                for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                    item.getCalendarAdapter().notifyDataSetChanged();
                }
            }
            return this;
        }

        public Styler setRushDeliveryDateCircleColor(@ColorRes int rushDeliveryDateColor, boolean redraw) {
            this.rushDeliveryDateCircleColor = rushDeliveryDateColor;
            if (redraw) {
                for (SwipableCalendarItem item : calendarItemAdapter.getViews()) {
                    item.getCalendarAdapter().notifyDataSetChanged();
                }
            }
            return this;
        }

        private void updateView() {
            customCalendarView.requestLayout();
            customCalendarView.invalidate();
        }

        @Override
        public int onDeliveryDateCircleColorRequested() {
            return deliveryDateCircleColor;
        }

        @Override
        public int onDeliveryDateTextStyleRequested() {
            return deliveryDateTextStyle;
        }

        @Override
        public int onRushDeliveryDateCircleColorRequested() {
            return rushDeliveryDateCircleColor;
        }

        @Override
        public int onMinDateTextStyleRequested() {
            return minDateTextStyle;
        }

        @Override
        public int onBeforeMinDateTextStyleRequested() {
            return beforeMinDateTextStyle;
        }

        @Override
        public int onAfterMinDateTextStyleRequested() {
            return afterMinDateTextStyle;
        }
    }
}
