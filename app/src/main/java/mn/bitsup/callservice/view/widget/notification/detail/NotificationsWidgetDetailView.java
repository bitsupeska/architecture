package mn.bitsup.callservice.view.widget.notification.detail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.notification.dto.Notification;
import mn.bitsup.callservice.client.notification.listener.NotificationListener;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.notification.core.NotificationWidget;
import mn.bitsup.callservice.view.widget.notification.core.contract.NotificationContract;
import mn.bitsup.callservice.view.widget.notification.view.NotificationView;

public class NotificationsWidgetDetailView extends Fragment implements
        NotificationView<NotificationContract> {

    private TextView notificationTitleTextView;
    private TextView dateTextView;
    private TextView notificationMessageTextView;
    private WidgetListView widgetListView;
    private RelativeLayout container;
    private NotificationContract contract;
    private WebView webView;
    private String date;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contract = new NotificationWidget(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.notification_detail, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }


    private void inflatedView(View inflatedView) {
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        container = inflatedView.findViewById(R.id.container);
        notificationTitleTextView = inflatedView.findViewById(R.id.notificationTitleTextView);
        dateTextView = inflatedView.findViewById(R.id.dateTextView);
        notificationMessageTextView = inflatedView.findViewById(R.id.notificationMessageTextView);
        webView = inflatedView.findViewById(R.id.webView);

    }

    private void initView() {
        Intent intent = getActivity().getIntent();
        String id = intent.getStringExtra("id");
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        contract.getNotification(params, new NotificationListener() {
            @Override
            public void onSuccess(Notification var1) {
                widgetListView.hideProgress();
                container.setVisibility(View.VISIBLE);
                notificationTitleTextView.setText(var1.getTitle());
                try {
                    date = convertToNewFormat(var1.getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (date != null) {
                    dateTextView.setText(date);
                } else {
                    dateTextView.setText(var1.getDate());
                }
                notificationMessageTextView.setText(var1.getSubtitle());
                Log.e("getNotification", "success: " + var1.getHtml());
                setWebView(var1.getHtml());
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        });

    }

    private int getScale() {
        try {
            Display display = ((WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            int width = display.getWidth();
            Double val = new Double(width) / new Double(900);
            val = val * 100d;
            return val.intValue();
        } catch (NullPointerException e) {
            Log.e("getScale", "exception " + e);
        }
        return 0;
    }

    private void setWebView(String html) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.setPadding(0, 0, 0, 0);
        webView.setInitialScale(getScale());
        if (html != null)
            webView.loadDataWithBaseURL(null, html.replace("\\n", "").replace("\\t", "").replace("\\\"", ""), "text/html", "UTF-8", null);
    }

    @SuppressLint("SimpleDateFormat")
    private String convertToNewFormat(String dateStr) throws ParseException {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        Date convertedDate = sourceFormat.parse(dateStr);
        if (DateUtils.isToday(convertedDate.getTime())) {
            return "Өнөөдөр " + timeFormat.format(convertedDate);
        } else if (DateUtils.isToday(convertedDate.getTime() + DateUtils.DAY_IN_MILLIS)) {
            return "Өчигдөр " + timeFormat.format(convertedDate);
        } else {
            return destFormat.format(convertedDate);
        }
    }
}


