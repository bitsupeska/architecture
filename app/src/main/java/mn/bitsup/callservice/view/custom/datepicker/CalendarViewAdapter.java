package mn.bitsup.callservice.view.custom.datepicker;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import androidx.viewpager.widget.PagerAdapter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Erdenedalai R&D B.V on 03/04/2018.
 * View pager adapter for multiple calendar views. It only maintains 3 views at a time (prev-current-next)
 */
public class CalendarViewAdapter extends PagerAdapter implements SwipableCalendarItem.ItemClickListener{

    private Context context;
    private List<DatePickerItem> datePickerItems;
    private Calendar calendarPrev;
    private Calendar calendarCurrent;
    private Calendar calendarNext;
    private CustomCalendarView.Styler styler;
    private View toBeRemoved;
    private List<SwipableCalendarItem> views;
    private ItemClickListener itemClickListener;
    private Date minDate;

    public CalendarViewAdapter(Context context, Calendar calendar, CustomCalendarView.Styler styler, List<DatePickerItem> datePickerItems, Date minDate) {
        this.context = context;
        this.views = new ArrayList<>();
        this.datePickerItems = datePickerItems;
        this.calendarCurrent = calendar;
        this.minDate = minDate;
        this.updateCalendars(calendar);
        this.styler = styler;
        this.initializeList();
    }

    private void initializeList(){
        views.add(createSwipableCalendarItem(views.size()));
        views.add(createSwipableCalendarItem(views.size()));
        views.add(createSwipableCalendarItem(views.size()));
    }

    private void updateCalendars(Calendar cal){
        this.calendarPrev = Calendar.getInstance();
        this.calendarPrev.setTime(cal.getTime());
        this.calendarPrev.add(Calendar.MONTH, -1);
        this.calendarCurrent = cal;
        this.calendarNext = Calendar.getInstance();
        this.calendarNext.setTime(cal.getTime());
        this.calendarNext.add(Calendar.MONTH, 1);
    }

    private SwipableCalendarItem createSwipableCalendarItem(int position){
        Calendar current;
        switch (position){
            case 0:
                current = calendarPrev;
                break;
            case 1:
                current = calendarCurrent;
                break;
            case 2:
                current = calendarNext;
                break;
            default:
                current = calendarCurrent;
        }
        SwipableCalendarItem item = new SwipableCalendarItem(context, current, styler, datePickerItems, minDate);
        item.setClickListener(this);
        return item;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        SwipableCalendarItem item = views.get(position);
        collection.addView(item);
        return item;
    }

    public void addView(){
        if (views.size() < 3){
            updateCalendars(calendarNext);
            views.add(createSwipableCalendarItem(views.size()));
        }
    }

    public void insertView(int position){
        if (views.size() < 3) {
            updateCalendars(calendarPrev);
            views.add(position, createSwipableCalendarItem(position));
        }
    }

    public void removeView (int position) {
        if (views.size() > 0 && position >= 0 && position <= views.size()) {
            toBeRemoved = views.remove(position);
        }
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        if (toBeRemoved != null) {
            collection.removeView(toBeRemoved);
        }
    }

    @Override
    public int getCount() {
        return views.size(); // 3 calendar items at a time
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(position);
    }

    @Override
    public int getItemPosition (Object object) {
        int index = views.indexOf (object);
        return index == -1 ? POSITION_NONE : index;
    }

    public List<SwipableCalendarItem> getViews() {
        return views;
    }

    public Calendar getCalendarCurrent() {
        return calendarCurrent;
    }

    @Override
    public void onItemClick(Date selectedDate) {
        if (itemClickListener != null) {
            itemClickListener.onItemClick(selectedDate);
        }
    }

    void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Date selectedDate);
    }
}
