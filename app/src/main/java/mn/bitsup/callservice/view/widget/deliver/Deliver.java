package mn.bitsup.callservice.view.widget.deliver;


import static mn.bitsup.callservice.MainApplication.getUser;

import mn.bitsup.callservice.view.widget.card.dto.UiCardPayload;

public class Deliver {
    private String address;
    private String date;
    private String cardAmount;
    private String time;
    private String timeEng;
    private int timeIndex;
    private int mode = 1;
    private String phoneNumber = (getUser().getPhoneNo());
    private UiCardPayload cardPayload;
    private String type;
    public Deliver(){

    }

    public UiCardPayload getCardPayload() {
        return cardPayload;
    }

    public void setCardPayload(UiCardPayload cardPayload) {
        this.cardPayload = cardPayload;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCardAmount() {
        return cardAmount;
    }

    public void setCardAmount(String cardAmount) {
        this.cardAmount = cardAmount;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getTimeIndex() {
        return timeIndex;
    }

    public void setTimeIndex(int timeIndex) {
        this.timeIndex = timeIndex;
    }

    public String getTimeEng() {
        return timeEng;
    }

    public void setTimeEng(String timeEng) {
        this.timeEng = timeEng;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

