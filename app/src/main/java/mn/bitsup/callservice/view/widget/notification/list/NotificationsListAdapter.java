package mn.bitsup.callservice.view.widget.notification.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.notification.dto.Notification;
import mn.bitsup.callservice.view.custom.WidgetListAdapter;
import mn.bitsup.callservice.view.custom.WidgetListItemClickListener;

/**
 * Created by Erdenedalai R&D B.V on 16/10/2018.
 * Adapter class for the Notifications list's RecyclerView.
 */
public class NotificationsListAdapter extends WidgetListAdapter<NotificationViewHolder> {

    private List<Notification> notifications = new ArrayList<>();
    private OnItemClickListener onItemClickListener;

    public NotificationsListAdapter(@Nullable final OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notifications_list_item, parent, false);
        return new NotificationViewHolder(listItemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {
        holder.bind(notifications.get(position));
        animateItemAppearance(holder, holder.getItemView());
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void updateWith(List<Notification> notifications) {
        this.notifications.clear();
        this.notifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public void appendWith(List<Notification> notifications) {
        int positionStart = this.notifications.size();
        this.notifications.addAll(notifications);
        notifyItemRangeInserted(positionStart, notifications.size());
    }

    /**
     * Removes the first notification with the same identifier from the adapter.
     * If no notification is found with the identifier the adapter is not modified.
     *
     * @param notification the notification to be removed from the adapter.
     */
    public void remove(Notification notification) {
        for (int i = 0; i < notifications.size(); i++) {
            if (notification.getId().equals(notifications.get(i).getId())) {
                notifications.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    /**
     * Update the first notification with the same identifier in the adapter.
     * If no notification is found with the identifier the adapter is not modified.
     *
     * @param notification the notification to be update in the adapter.
     */
    public void update(Notification notification) {
        for (int i = 0; i < notifications.size(); i++) {
            if (notification.getId().equals(notifications.get(i).getId())) {
                notifications.set(i, notification);
                notifyItemChanged(i);
                break;
            }
        }
    }

   public interface OnItemClickListener extends WidgetListItemClickListener<Notification> {
        void onItemLongClicked(Notification item);
    }
}
