package mn.bitsup.callservice.view.widget.card.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import mn.bitsup.callservice.client.card.dto.CardFeeAmount;
import mn.bitsup.callservice.client.card.dto.CardLimit;

public class CardItemPayload {

    private String guid;
    private String pan;
    private String cvv;
    private String cif;
    private String mainAccount;
    private String nameOfCard;
    private String expirationDate;
    private String expiredYear;
    private String plasticType;
    private String signStat;
    private String crdStat;
    private String ecStatus;
    private String pinLen;
    private String productCode;
    private String shipDay;
    @SerializedName("feeAmount")
    @Expose
    private List<CardFeeAmount> feeAmount;

    @SerializedName("cardLimit")
    @Expose
    private List<CardLimit> cardLimit;
    private static final CardItemPayload cardItemPayload = new CardItemPayload();
    public static CardItemPayload getInstance() {
        return cardItemPayload;
    }
    private CardItemPayload() { }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getMainAccount() {
        return mainAccount;
    }

    public void setMainAccount(String mainAccount) {
        this.mainAccount = mainAccount;
    }

    public String getNameOfCard() {
        return nameOfCard;
    }

    public void setNameOfCard(String nameOfCard) {
        this.nameOfCard = nameOfCard;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getExpiredYear() {
        return expiredYear;
    }

    public void setExpiredYear(String expiredYear) {
        this.expiredYear = expiredYear;
    }

    public String getPlasticType() {
        return plasticType;
    }

    public void setPlasticType(String plasticType) {
        this.plasticType = plasticType;
    }

    public String getSignStat() {
        return signStat;
    }

    public void setSignStat(String signStat) {
        this.signStat = signStat;
    }

    public String getCrdStat() {
        return crdStat;
    }

    public void setCrdStat(String crdStat) {
        this.crdStat = crdStat;
    }

    public String getEcStatus() {
        return ecStatus;
    }

    public void setEcStatus(String ecStatus) {
        this.ecStatus = ecStatus;
    }

    public String getPinLen() {
        return pinLen;
    }

    public void setPinLen(String pinLen) {
        this.pinLen = pinLen;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getShipDay() {
        return shipDay;
    }

    public void setShipDay(String shipDay) {
        this.shipDay = shipDay;
    }

    public List<CardFeeAmount> getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(List<CardFeeAmount> feeAmount) {
        this.feeAmount = feeAmount;
    }

    public List<CardLimit> getCardLimit() {
        return cardLimit;
    }

    public void setCardLimit(List<CardLimit> cardLimit) {
        this.cardLimit = cardLimit;
    }

    public static CardItemPayload getOurInstance() {
        return cardItemPayload;
    }
}
