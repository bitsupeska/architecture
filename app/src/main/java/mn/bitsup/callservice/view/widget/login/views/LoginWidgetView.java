package mn.bitsup.callservice.view.widget.login.views;

import static android.content.Context.VIBRATOR_SERVICE;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.listener.PasswordAuthListener;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockData;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.util.DebouncedOnClickListener;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.activity.MainActivity;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.CustomTextBox;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.view.ForgetRegNumWidgetView;
import mn.bitsup.callservice.view.widget.login.core.LoginWidget;
import mn.bitsup.callservice.view.widget.login.core.contract.LoginContract;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.view.easy.OBCongratsWidgetView;
import mn.bitsup.callservice.view.widget.passcode.PasscodeWidgetSetupView;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginWidgetView extends Fragment implements
        LoginView<LoginContract>, TextWatcher, PasswordAuthListener {

    private static final String TAG = LoginWidgetView.class.getName();
    private CustomTextBox editUsername;
    private CustomTextBox editPassword;
    private EditText editTextUsername;
    private EditText editTextPassword;
    private CustomButton buttonLogin;
//    private CustomButton buttonRegister;
    private LinearLayout containerEditTexts;
    private ConstraintLayout constraintLayout;
    private boolean isBlocked;
    private boolean isBlockedEasy = false;
    private String message;
    private LoginContract contract;
    private ProgressDialog progressDialog;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        contract = new LoginWidget(getContext());
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.login_layout, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(@NonNull View inflatedView) {
        constraintLayout = inflatedView.findViewById(R.id.rootview);
        editUsername = inflatedView.findViewById(R.id.username_textbox);
        editPassword = inflatedView.findViewById(R.id.password_textbox);
        buttonLogin = inflatedView.findViewById(R.id.buttonLogin);
//        buttonRegister = inflatedView.findViewById(R.id.buttonRegister);
        containerEditTexts = inflatedView.findViewById(R.id.containerEditTexts);

        inflatedView.findViewById(R.id.buttonLogin).setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                showProgress();
//                final char[] username = editTextUsername.getText().toString().toLowerCase().toCharArray();
//                final char[] password = editableToCharArray(editTextPassword.getText());
//                contract.authenticate(username, password, null, null, LoginWidgetView.this);
                Intent intent = new Intent(getContext(), MainActivity.class);
                getContext().startActivity(intent);
//                EventHelper.publish(getContext(), new PasscodeWidgetSetupView());
            }
        });
        inflatedView.findViewById(R.id.textForgot).setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                checkBlock(1);
            }
        });
    }

    private void checkBlockIncrement() {
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                Log.e(TAG, "onSuccess: " + (blockIncrement != null));
                if (blockIncrement != null) {
                    containerEditTexts.startAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.shakedown));
                    hideProgress();
                    editTextPassword.requestFocus();
                    editTextPassword.selectAll();
                    showErrorNotification("Уучлаарай", "Нэвтрэх нэр эсвэл нууц үг буруу. " + "\n" + " Танд " + (blockIncrement.getData().getBlockLimit() - blockIncrement.getData().getFailedCount()) + " удаагийн боломж үлдлээ");
                }
                buttonLogin.setState(ButtonState.ENABLED, getActivity());
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(getContext());
                buttonLogin.setState(ButtonState.ENABLED, getActivity());
            }
        }, "3", "1", editTextUsername.getText().toString().toLowerCase());
    }

    private void initViews() {
        editUsername.setHintText(R.string.password_username_placeholder);
        editPassword.setHintText(R.string.password_password_placeholder);
        editPassword.visibleButtonAdd();
        editTextUsername = editUsername.getEditText();
        editTextPassword = editPassword.getEditText();
        editTextUsername.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50)});
        editTextPassword.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
        editTextUsername.addTextChangedListener(this);
        editTextPassword.addTextChangedListener(this);
//        buttonRegister.setBackgroundColor(0x00000000);
//        buttonRegister.setOnClickListener(registerButtonClickListener);
        constraintLayout.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
            Rect r = new Rect();
            constraintLayout.getWindowVisibleDisplayFrame(r);
            int screenHeight = constraintLayout.getRootView().getHeight();
            int keypadHeight = screenHeight - r.bottom;
//            if (keypadHeight > screenHeight * 0.15) {
//                buttonRegister.setVisibility(View.GONE);
//            } else {
//                buttonRegister.setVisibility(View.VISIBLE);
//            }
        });
        constraintLayout.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                Log.e(TAG, "onKey: KEYCODE_BACK");
            }
            return false;
        });
    }

    private void showProgress() {
        buttonLogin.setState(ButtonState.LOADING, getActivity());
        editUsername.setFocusable(false);
        editPassword.setFocusable(false);
        editUsername.setFocusableInTouchMode(false);
        editPassword.setFocusableInTouchMode(false);
    }

    private void hideProgress() {
        buttonLogin.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        editUsername.setFocusable(true);
        editPassword.setFocusable(true);
        editUsername.setFocusableInTouchMode(true);
        editPassword.setFocusableInTouchMode(true);
    }

    public void showErrorNotification(@NonNull String title,
                                      @NonNull String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle, getContext());
    }

    private View.OnClickListener registerButtonClickListener = v -> {
//        buttonRegister.setState(ButtonState.LOADING, getActivity());
        checkBlockEasyOnboard();
    };

    private static char[] editableToCharArray(Editable text) {
        char[] pin = new char[text.length()];
        text.getChars(0, text.length(), pin, 0);
        return pin;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //Implementation of method beforeTextChanged not required for BasePasswordWidgetView implementation
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Implementation of method beforeTextChanged not required for BasePasswordWidgetView implementation
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (!buttonLogin.isLoading()) {
            buttonLogin.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        }
    }

    private boolean isValid() {
        return editTextUsername.length() > 0 && editTextPassword.length() > 0;
    }

    @Override
    public void onAuthSuccess(Map<String, List<String>> map) {
        hideProgress();
        EventHelper.publish(getContext(), new PasscodeWidgetSetupView());
    }

    @Override
    public void onAuthError(Response errorResponse) {
        if (errorResponse.getResponseCode() != 0 && errorResponse.getResponseCode() <= 500)
            checkBlockIncrement();
        else {
            hideProgress();
            showErrorNotification("Уучлаарай", errorResponse.getResponseCode() == 0 ? getContext().getResources().getString(R.string.shared_error_no_internet_message) : "Сервертэй холбогдоход алдаа гарлаа");
        }
    }

    private void showErrorMessage(Response errorResponse) {
        String messageLogin = "";
        if (errorResponse.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            try {
                Log.e("wwwe", "wewe");

                JSONObject json = new JSONObject(errorResponse.getErrorMessage());
                messageLogin = json.getString("error");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        String title = getContext().getString(errorResponse.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED ? R.string.password_alert_error_401_title : R.string.shared_alert_generic_error_title);
        String message = errorResponse.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED ? messageLogin : getContext().getString(R.string.shared_alert_generic_error_message);
        showErrorNotification(title, message);
        vibrate(getContext());
    }

    private void vibrate(Context context) {
        Vibrator vibrator = (Vibrator) context.getSystemService(VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(200);
        }
    }

    private void checkBlock(int blockType) {
        if (blockType == 1)
            progressDialog.show();
        HashMap<String, String> blockParams = new HashMap<>();
        if (blockType == 1) {
            blockParams.put("value", DeviceAddressUtil.getMACAddress(getContext(),"wlan0"));
        } else {
            blockParams.put("value", editTextUsername.getText().toString().toLowerCase());
        }

        blockParams.put("macAddress", DeviceAddressUtil.getMACAddress(getContext(), "wlan0"));
        blockParams.put("ipAddress", DeviceAddressUtil.getIPAddress(true));

        contract.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                Log.e("checkBlock", "success: " + var1);
                if (!var1.getData().isEmpty()) {
                    for (BlockData block :
                            var1.getData()) {
                        //type == 1 forget password block shalgah
                        //type !=1 login block shalgah
                        //3,1 loginii block shalgah
                        // 10, 5 - otp block shalgah
                        // 12, 5 - register block shalgah
                        // 7, 5 - email block shalga
                        if (blockType == 1) {
                            if (block.getBlocktype() == 10
                                    && block.getBlockchannel() == 5
                                    && block.getBlockinfo().equals(DeviceAddressUtil.getMACAddress(getContext(),"wlan0"))
                                    || block.getBlocktype() == 12
                                    && block.getBlockchannel() == 5
                                    && block.getBlockinfo().equals(DeviceAddressUtil.getMACAddress(getContext(), "wlan0"))
                                    || block.getBlocktype() == 7
                                    && block.getBlockchannel() == 5
                                    && block.getBlockinfo().equals(DeviceAddressUtil.getMACAddress(getContext(), "wlan0"))) {
                                message = block.getBlockmsg();
                                progressDialog.dismiss();
                                isBlocked = true;
                            }
                        } else {
                            if (block.getBlocktype() == 3
                                    && block.getBlockchannel() == 1
                                    && block.getBlockinfo().equals(editTextUsername.getText().toString().toLowerCase())) {
                                message = block.getBlockmsg();
                                progressDialog.dismiss();
                                isBlocked = true;
                            }
                        }
                    }
                } else {
                    isBlocked = false;
                }
                if (!isBlocked) {
                    if (blockType == 1) {
                        EventHelper.publishActivity(getContext(), new ForgetRegNumWidgetView(), "Нууц үг сэргээх");
                        progressDialog.dismiss();
                    } else {
                        final char[] username = editTextUsername.getText().toString().toLowerCase().toCharArray();
                        final char[] password = editableToCharArray(editTextPassword.getText());
                        contract.authenticate(username, password, null, null, LoginWidgetView.this);

                    }
                } else {
                    progressDialog.dismiss();
                    new AnimationDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title), message, getContext().getResources().getString(R.string.shared_button_back), "anim_rejection_clock.json").show();
                    if (blockType != 1)
                        buttonLogin.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response var1) {
                Log.e(TAG, "onError: " + var1.getResponseCode());
                if (blockType == 1)
                    progressDialog.dismiss();
                if (var1.getResponseCode() == 0) {
                    NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title)
                            , getContext().getResources().getString(R.string.shared_error_no_internet_message), getContext());
                } else {
                    CustomAlertDialog.showServerAlertDialog(getContext());
                }
                if (blockType != 1)
                    buttonLogin.setState(ButtonState.ENABLED, getActivity());
            }
        }, blockParams);
    }

    private void checkBlockEasyOnboard() {
//        HashMap<String, String> blockParams = new HashMap<>();
//        blockParams.put("value", DeviceAddressUtil.getMACAddress(getContext(), "wlan0"));
//        blockParams.put("macAddress", DeviceAddressUtil.getMACAddress(getContext(),"wlan0"));
//        blockParams.put("ipAddress", DeviceAddressUtil.getIPAddress(true));
//
//        contract.checkBlock(new BlockListener() {
//            @Override
//            public void onSuccess(Block var1) {
//                if (!var1.getData().isEmpty()) {
//                    for (BlockData block :
//                            var1.getData()) {
//                        // 10, 2 - otp block shalgah
//                        if (block.getBlocktype() == 10
//                                && block.getBlockchannel() == 2
//                                && block.getBlockinfo().equals(DeviceAddressUtil.getMACAddress(getContext(),"wlan0"))) {
//                            message = block.getBlockmsg();
//                            progressDialog.dismiss();
//                            isBlockedEasy = true;
//                        }
//                    }
//                } else {
//                    isBlockedEasy = false;
//                }
//                if (!isBlockedEasy) {
//                    //progressDialog.dismiss();
//                    Intent intent = new Intent(getContext(), OnboardActivity.class);
//                    startActivityForResult(intent, 1);
//
//                } else {
//                    new AnimationDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title), message, getContext().getResources().getString(R.string.shared_button_back), "anim_rejection_clock.json").show();
//                    buttonRegister.setState(ButtonState.ENABLED, getActivity());
//                }
//            }
//
//            @Override
//            public void onError(Response var1) {
//                if (var1.getResponseCode() == 0) {
//                    NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title)
//                            , getContext().getResources().getString(R.string.shared_error_no_internet_message), getContext());
//                } else {
//                    CustomAlertDialog.showServerAlertDialog(getContext());
//                }
//                buttonRegister.setState(ButtonState.ENABLED, getActivity());
//                Log.e(TAG, "onError: " + var1.getErrorMessage());
//
//            }
//        }, blockParams);
        Intent intent = new Intent(getContext(), OnboardActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_FIRST_USER) {
                if (OBCongratsWidgetView.easyUsername != null)
                    editTextUsername.setText(OBCongratsWidgetView.easyUsername);
                else editTextUsername.setText("");
                editTextPassword.setText("");
                editTextPassword.requestFocus();
            }
//            buttonRegister.setState(ButtonState.ENABLED, getActivity());
        }
    }
}
