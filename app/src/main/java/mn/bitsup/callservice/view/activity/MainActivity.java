package mn.bitsup.callservice.view.activity;

import static mn.bitsup.callservice.view.toolbar.ToolbarPage.PRODUCTS;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.toolbar.ToolbarEvent;
import mn.bitsup.callservice.view.toolbar.ToolbarManager;
import mn.bitsup.callservice.view.widget.card.view.CardMainView;
import mn.bitsup.callservice.view.widget.home.view.HomeWidgetView;

public class MainActivity extends BaseActivity {

    public static Context context;
    public static TextView notificationBadge;
    private ToolbarManager toolbarManager;
    public static BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_main);
        AppBarLayout appBarLayout = findViewById(R.id.appBarLayout);
        notificationBadge = findViewById(R.id.notification_badge);

        toolbarManager = new ToolbarManager(this, appBarLayout);
        bottomNavigation = findViewById(R.id.bottomNavigation);
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);

        toolbarManager.setToolbar(new ToolbarEvent(PRODUCTS));
        replaceFragment(new HomeWidgetView());
    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_account:
                        toolbarManager.setToolbar(new ToolbarEvent(PRODUCTS));
                        replaceFragment(new HomeWidgetView());
                        return true;
                    case R.id.navigation_payment:
                        toolbarManager.setToolbar(new ToolbarEvent(PRODUCTS));
                        replaceFragment(new CardMainView());
                        return true;
                    case R.id.navigation_settings:
                        toolbarManager.setToolbar(new ToolbarEvent(PRODUCTS));
                        replaceFragment(new CardMainView());
                        return true;
                }
                return false;
            };


    public void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
       
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
      
    }
}
