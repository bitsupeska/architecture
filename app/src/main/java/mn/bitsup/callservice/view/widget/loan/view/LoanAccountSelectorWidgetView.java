package mn.bitsup.callservice.view.widget.loan.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public class LoanAccountSelectorWidgetView extends Fragment {
    private ListView listView;
    private RelativeLayout container;
    private WidgetListView widgetListView;
    private Context context;
    private CustomButton buttonContinue;
    private LoanContract contract;
    private HashMap<Integer, String> bankList = new HashMap<>();
    private List<Account> accountList = new ArrayList<>();
    private Account account;
    private User user = MainApplication.getUser();
    private List<BankItem> bankItem;
    private String type = "";

    LoanAccountSelectorWidgetView(Account account, String type) {
        this.type = type;
        this.account = account;
    }

    private int selectedPosition = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_accounts_list, container, false);
        KeyboardUtils.hideKeyboard(getActivity());
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        listView = inflatedView.findViewById(R.id.listView);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        container = inflatedView.findViewById(R.id.container);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());


        buttonContinue.setOnClickListener(v -> {
            {
                KeyboardUtils.hideKeyboard(getActivity());
                EventHelper.publishBack(getContext(), new LoanDetailsView(account));
            }
        });
    }

    private boolean isValid() {
        return selectedPosition != -1;
    }

    private void initView() {
        if(LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        fillBankData();
    }

    private void getAccountList() {
        contract.getAccountLists(new AccountListener() {
            @Override
            public void onSuccess(List<Account> accounts) {
                if (accounts != null) {
                    if (accounts.size() == 0) {
                        publishChangeLoanAddAccount();
                    } else {
                        container.setVisibility(View.VISIBLE);
                        if (accounts.size() == 1) {
                            selectedPosition = 0;
                            account = accounts.get(0);
                        }
                        widgetListView.hideProgress();
                        accountList.addAll(accounts);
                        ListAdapter listAdapter = new ListAdapter();
                        listView.setAdapter(listAdapter);
                    }
                } else {
                    publishChangeLoanAddAccount();
                }
            }

            @Override
            public void onError(Response var1) {
                widgetListView.hideProgress();
                CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> {
                    ((Activity) context).finish();
                });
            }
        }, user.getCif());
    }

    private void publishChangeLoanAddAccount() {
        KeyboardUtils.hideKeyboard(getActivity());
        EventHelper.publishStackPop(getContext(), this);
        EventHelper.publishBack(getContext(), new LoanAccountAdd());
    }

    private void fillBankData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("profileSettings", "");
        param.put("qpay", "");
        param.put("transfer", "");

        contract.getBanks(new BankListener() {
            @Override
            public void onSuccess(List<BankItem> data) {
                bankItem = data;
                if (bankItem == null) {
                    widgetListView.hideProgress();
                } else {
                    if (bankItem.size() == 0) {
                        widgetListView.hideProgress();
                    } else {
                        for (int i = 0; i < bankItem.size(); i++) {
                            bankList.put(bankItem.get(i).getId(), bankItem.get(i).getBankname());
                        }
                    }
                    if (type.equals("related")) {
                        getAccountList();
                    } else {
                        widgetListView.hideProgress();
                        container.setVisibility(View.VISIBLE);
                        accountList.add(account);
                        selectedPosition = 0;
                        ListAdapter listAdapter = new ListAdapter();
                        listView.setAdapter(listAdapter);
                    }
                }

            }

            @Override
            public void onError(Response var1) {
                widgetListView.hideProgress();
                CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> {
                    ((Activity) context).finish();
                });
            }
        }, param);
    }

    private class ListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return accountList.size();
        }

        @Override
        public Object getItem(int position) {
            return accountList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.loan_accouns_item, parent, false);
            TextView textBankName = convertView.findViewById(R.id.textBankName);
            TextView textAccount = convertView.findViewById(R.id.textAccount);
            ImageView selectIcon = convertView.findViewById(R.id.selectIcon);
            textBankName.setText(bankList.get(accountList.get(position).getBank_id()));
            textAccount.setText(accountList.get(position).getAccount_num());
            if (selectedPosition == position) {
                selectIcon.setBackgroundResource((R.drawable.selected_item_circle));
            } else {
                selectIcon.setBackgroundResource(R.drawable.unselected_item);
            }
            buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED,getActivity());
            convertView.setOnClickListener(v -> {
                selectedPosition = position;
                account = accountList.get(position);
                account.setBank_name(bankList.get(accountList.get(position).getBank_id()));
                notifyDataSetChanged();
            });
            return convertView;
        }
    }
}
