package mn.bitsup.callservice.view.widget.notification.core;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.notification.dto.Notification;
import mn.bitsup.callservice.client.notification.listener.NotificationListener;
import mn.bitsup.callservice.client.notification.listener.NotificationsListener;
import mn.bitsup.callservice.view.widget.notification.core.contract.NotificationContract;

import static mn.bitsup.callservice.ClientsManager.notificationClient;

public class NotificationWidget implements NotificationContract {
    public NotificationWidget(@NonNull Context context) {
    }

    @Override
    public void getNotifications(int currentPage, int size, NotificationsListener notificationsListener) {
        Map<String, String> params = new HashMap();
        StorageComponent storageComponent = new StorageComponent();
        params.put("cif", storageComponent.getItem("uid"));
        params.put("offset", String.valueOf(currentPage * size));
//        notificationClient.notifications(params, notificationsListener);
    }

    @Override
    public void getNotification(Map<String, String> params, NotificationListener notificationListener) {
        notificationClient.notificationDetail(params, new NotificationListener() {
            @Override
            public void onSuccess(Notification var1) {
                notificationListener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                notificationListener.onError(var1);

            }
        });
    }

    @Override
    public void notificationArchive(Map<String, String> params, StatusListener statusListener) {
        notificationClient.notificationArchive(params, new StatusListener() {
            @Override
            public void onSuccess(StatusResponse var1) {
                statusListener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                statusListener.onError(var1);

            }
        });
    }
}
