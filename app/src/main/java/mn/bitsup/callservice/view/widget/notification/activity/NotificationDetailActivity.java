package mn.bitsup.callservice.view.widget.notification.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.activity.BaseActivity;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.notification.detail.NotificationsWidgetDetailView;

import static mn.bitsup.callservice.ClientsManager.notificationClient;
import static mn.bitsup.callservice.util.KeyboardUtils.hideKeyboard;

public class NotificationDetailActivity extends BaseActivity {
    private FragmentManager fragmentManager;
    Toolbar toolbar;
    TextView textViewTitle;
    ImageView imageBack;
    ImageView buttonDelete;
    String id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification_activity);
        toolbar = findViewById(R.id.contacts_toolbar);
        textViewTitle = findViewById(R.id.textViewTitle);
        imageBack = findViewById(R.id.imageBack);
        buttonDelete = findViewById(R.id.buttonDelete);
        context = NotificationDetailActivity.this;
        activity = NotificationDetailActivity.this;
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(new OnBackStackChangedListenerImpl());
        inflatedViews();

    }


    private void inflatedViews() {
        textViewTitle.setText("Дэлгэрэнгүй");
        imageBack.setOnClickListener(v -> this.onBackPressed());
        buttonDelete.setOnClickListener(v -> archiveNotification());
        initView();
    }

    private void archiveNotification() {
        DialogInterface.OnClickListener positiveButtonClickListener = (dialog, which) -> {
            archive();
        };
        DialogInterface.OnClickListener negativeButtonClickListener = (dialog, which) -> {
        };

        CustomAlertDialog.showAlertDialog(NotificationDetailActivity.this, "Анхааруулага", "Та мэдэгдлийг устгаснаар дахин харагдахгүй болхийг анхаарна уу", positiveButtonClickListener, negativeButtonClickListener);

    }

    private void initView() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, new NotificationsWidgetDetailView());
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void archive() {
        Map<String, String> params = new HashMap<>();
        params.put("id", id);
        notificationClient.notificationArchive(params, new StatusListener() {
            @Override
            public void onSuccess(StatusResponse var1) {
                if (var1.getCode().equals("200")) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", "Мэдэгдэл устлаа");
                    activity.setResult(Activity.RESULT_OK, returnIntent);
                    activity.finish();
                } else {
                    CustomAlertDialog.showServerAlertDialog(NotificationDetailActivity.this);
                }
            }

            @Override
            public void onError(Response var2) {
                CustomAlertDialog.showServerAlertDialog(NotificationDetailActivity.this);
            }
        });
    }

    private class OnBackStackChangedListenerImpl implements FragmentManager.OnBackStackChangedListener {
        @Override
        public void onBackStackChanged() {
        }
    }

    @Override
    public void onBackPressed() {
        if (!CustomButton.isLoading) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                KeyboardUtils.hideKeyboard(this);
                getSupportFragmentManager().popBackStack();
            } else {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", "Мэдэгдэл устлаа");
                activity.setResult(Activity.RESULT_CANCELED, returnIntent);
                activity.finish();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}