package mn.bitsup.callservice.view.custom;

import android.os.Parcel;
import android.text.TextPaint;
import android.text.style.SuperscriptSpan;
import androidx.annotation.VisibleForTesting;


/**
 * Created by Erdenedalai R&D B.V on 16/08/2017.
 * This class handles the currency conversion.
 */
public class TopAlignSuperscriptSpan extends SuperscriptSpan {
    private float shiftPercentage = 0.0F;
    @VisibleForTesting
    static final float FONT_SCALE = 1.75f;
    public static final Creator<TopAlignSuperscriptSpan> CREATOR = new Creator<TopAlignSuperscriptSpan>() {
        public TopAlignSuperscriptSpan createFromParcel(Parcel in) {
            return new TopAlignSuperscriptSpan(in);
        }

        public TopAlignSuperscriptSpan[] newArray(int size) {
            return new TopAlignSuperscriptSpan[size];
        }
    };

    @SuppressWarnings("WeakerAccess")
    public TopAlignSuperscriptSpan(float shiftPercentage) {
        if (shiftPercentage > 0.0F && shiftPercentage < 1.0F) {
            this.shiftPercentage = shiftPercentage;
        }

    }

    public TopAlignSuperscriptSpan(Parcel in) {
        this.shiftPercentage = in.readFloat();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeFloat(this.shiftPercentage);
    }

    public void updateDrawState(TextPaint textPaint) {
        float ascent = textPaint.ascent();
        textPaint.setTextSize(textPaint.getTextSize() / FONT_SCALE);
        float newAscent = textPaint.getFontMetrics().ascent;
        textPaint.baselineShift = (int) ((float) textPaint.baselineShift + (ascent - ascent * this.shiftPercentage - (newAscent - newAscent * this.shiftPercentage)));
    }

    public void updateMeasureState(TextPaint tp) {
        this.updateDrawState(tp);
    }
}
