package mn.bitsup.callservice.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import java.io.ByteArrayInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import mn.bitsup.callservice.client.RefreshService;
import mn.bitsup.callservice.constant.SharedParameters;
import mn.bitsup.callservice.util.PhoneUtils;
import org.json.JSONException;
import org.json.JSONObject;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.AuthClient;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.dataprovider.util.OfflineCrypto;
import mn.bitsup.callservice.client.info.dto.Item;
import mn.bitsup.callservice.client.info.listener.BackgroundImageListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.widget.home.view.HomeWidgetView;
import mn.bitsup.callservice.view.widget.login.views.LoginWidgetView;
import mn.bitsup.callservice.view.widget.passcode.PasscodeWidgetAuthenticateView;
import static mn.bitsup.callservice.client.AuthClient.ACCESS_TOKEN;

public class EnrollmentActivity extends BaseActivity {

    public static final String INTENT_EXTRA_FORCED_LOGOUT = "INTENT_EXTRA_FORCED_LOGOUT";
    public static final String INTENT_ANOTHER_DEVICE_LOGOUT = "INTENT_ANOTHER_DEVICE_LOGOUT";
    public static final String TAG = EnrollmentActivity.class.getName();
    public static StorageComponent imageStorageComponent = new StorageComponent();
//    public static ImageView discovery_background;
    public static Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_enrollment);
//        discovery_background = findViewById(R.id.discovery_background);
        toolbar = findViewById(R.id.toolbar);

//        changeBackgroundImage();
        //getBackgroundImage();

        PhoneUtils.checkBioPermissions(context);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> EnrollmentActivity.super.onBackPressed());
        toolbar.setVisibility(View.GONE);
//        makeStatusBarTransparent();

        AuthClient authClient = new AuthClient(this);
        if (authClient.isEnrolled()) {
            EventHelper.publish(this, new PasscodeWidgetAuthenticateView());
        } else {
            EventHelper.publish(this, new LoginWidgetView());
        }

        try {
//            doTrustToCertificates();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getIntent() != null && getIntent().getBooleanExtra(INTENT_EXTRA_FORCED_LOGOUT, false)) {
            showForceLogoutInfoDialog();
        }

        if (getIntent() != null && getIntent()
            .getBooleanExtra(INTENT_ANOTHER_DEVICE_LOGOUT, false)) {
            showAnotherLogoutInfoDialog();
        }

    }

    private void showForceLogoutInfoDialog() {
        CustomAlertDialog.showAlertDialog(this, getResources().getString(R.string.shared_alert_session_expired_title), getResources().getString(R.string.shared_alert_session_expired_message));
    }

    private void showAnotherLogoutInfoDialog() {
        CustomAlertDialog.showAlertDialog(this, getResources().getString(R.string.shared_alert_session_another_title), getResources().getString(R.string.shared_alert_session_another_message));
    }



    public void checkToken(TokenListener listener) {
        String data = OfflineCrypto.tokenEncrypt(context);
        if (data != null && !data.equals("")) {
            HashMap<String, String> params = new HashMap<>();
            params.put("data", data);
            ClientsManager.getOfflineClient().convertToken(new TokenListener() {
                @Override
                public void onSuccess(String var1) {
                    String ddata = OfflineCrypto.tokenDecrypt(var1, context);

                    Log.e("json", "saved token: " + ddata);

                    if (ddata != null && !ddata.equals("")) {
                        try {
                            JSONObject object = new JSONObject(ddata);
                            StorageComponent storageComponent = new StorageComponent();
                            storageComponent.setItem(ACCESS_TOKEN, object.getString("access_token"));
                            listener.onSuccess(var1);
                        } catch (JSONException e) {
                            Response errorResponse = new Response();
                            errorResponse.setResponseCode(500);
                            listener.onError(errorResponse);
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onError(Response var1) {
                    listener.onError(var1);
                }
            }, params);
        }
    }


    protected void makeStatusBarTransparent() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(ContextCompat.getColor(this, android.R.color.white));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    String result = data.getStringExtra("result");
                    NudgeControllerUtil.showSuccessNotification(this.getResources().getString(R.string.setup_completed_title)
                            , result, this);
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (!CustomButton.isLoading) {
            super.onBackPressed();
            stopService(new Intent(this, RefreshService.class));
        }
    }


}
