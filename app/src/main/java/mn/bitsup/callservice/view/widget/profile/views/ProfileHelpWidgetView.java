package mn.bitsup.callservice.view.widget.profile.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.QuestionItem;
import mn.bitsup.callservice.client.profile.dto.Questions;
import mn.bitsup.callservice.client.profile.listener.QuestionListener;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.Question;

public class ProfileHelpWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private Context context;
    private ProfileContract contract;
    private ListView listView;
    private String title;
    private WidgetListView widgetListView;
    private List<Question> questionConvertedList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_help, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        listView = inflatedView.findViewById(R.id.listView);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
    }

    private void initViews() {
//        contract.getAnswerQuestions(new QuestionListener() {
//            @Override
//            public void onSuccess(List<Questions> questions) {
//                widgetListView.setVisibility(View.GONE);
//                listView.setVisibility(View.VISIBLE);
//                if (questions != null) {
//                    for (Questions questions1 : questions) {
//                        for (QuestionItem questionItem : questions1.getData()) {
//                            Question question = new Question(questions1.getCategName(), questionItem);
//                            questionConvertedList.add(question);
//                        }
//                    }
//                    if (questions.size() == 0) {
//                        CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), "Одоогоор мэдээлэл ороогүй байна", (dialog, which) -> {
//                            ((Activity) context).finish();
//                        });
//                    } else {
//                        ListAdapter listAdapter = new ListAdapter();
//                        listView.setAdapter(listAdapter);
//                        title = questionConvertedList.get(0).getTitle();
//                    }
//                } else {
//                    CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), "Одоогоор мэдээлэл ороогүй байна", (dialog, which) -> {
//                        ((Activity) context).finish();
//                    });
//                }
//            }
//
//            @Override
//            public void onError(Response var1) {
//                widgetListView.hideProgress();
//                CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> {
//                    ((Activity) context).finish();
//                });
//            }
//        });
    }

    private class ListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return questionConvertedList.size();
        }

        @Override
        public Object getItem(int position) {
            return questionConvertedList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.faq_row_item, parent, false);
            TextView textQuestion = convertView.findViewById(R.id.textQuestion);
            TextView textAnswer = convertView.findViewById(R.id.textAnswer);
            RelativeLayout textLayout = convertView.findViewById(R.id.textLayout);
            TextView textTitle = convertView.findViewById(R.id.textTitle);
            ImageView arrowButton = convertView.findViewById(R.id.arrowButton);
            textAnswer.setText(questionConvertedList.get(position).getQuestionItem().getAnswer());
            textQuestion.setText(questionConvertedList.get(position).getQuestionItem().getQuestion());
            convertView.setOnClickListener(v -> {
                        arrowButton.setRotation(arrowButton.getRotation() - (-1 * 180f));
                        if (textLayout.getVisibility() == View.VISIBLE) {
                            textLayout.setVisibility(View.GONE);
                        } else {
                            TransitionManager.beginDelayedTransition(textLayout);
                            textLayout.setVisibility(View.VISIBLE);
                        }

                    }
            );
            if (title.equals(questionConvertedList.get(position).getTitle())) {
                if (position != 0) {
                    textTitle.setVisibility(View.GONE);
                }

            } else {
                title = questionConvertedList.get(position).getTitle();
                textTitle.setVisibility(View.VISIBLE);
            }
            textTitle.setText(title);
            return convertView;
        }
    }
}
