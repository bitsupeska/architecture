package mn.bitsup.callservice.view.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import androidx.annotation.NonNull;

public class KeyboardUtils {
    private KeyboardUtils() {
    }

    public static void showKeyboard(@NonNull final EditText editText) {

        Log.e("keyboard", "keyboard");
        editText.postDelayed(new Runnable() {
            public void run() {
                Context context = editText.getContext();
                @SuppressLint("WrongConstant")
                InputMethodManager imm = (InputMethodManager)context.getSystemService("input_method");
                if (imm != null) {
                    editText.clearFocus();
                    editText.requestFocus();
                    imm.showSoftInput(editText, 1);
                    editText.setEnabled(true);
                }

            }
        }, 100L);
    }

    public static void hideKeyboard(@NonNull EditText editText) {
        Context context = editText.getContext();
        @SuppressLint("WrongConstant")
        InputMethodManager imm = (InputMethodManager)context.getSystemService("input_method");
        if (imm != null) {
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            editText.clearFocus();
            editText.setEnabled(false);
        }
    }
}
