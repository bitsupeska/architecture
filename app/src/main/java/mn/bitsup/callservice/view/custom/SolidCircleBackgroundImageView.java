package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;

import mn.bitsup.callservice.R;


/**
 * Created by Erdenedalai R&D B.V on 04/09/2018.
 * ImageView used to display an icon on top of a solid colored circle.
 * Use setCircleBackgroundColor() to set the color of the background circle.
 * After setting the background, use this view as a standard {@link android.widget.ImageView}
 * <p>
 * Note:
 * This View is different from {@link de.hdodenhof.circleimageview.CircleImageView} and serves another purpose.
 * The former does not support setting the ScaleType, forcing it to CENTER_CROP which scales the shown image
 * to be as big as the view. It is not possible to change this setting.
 * Use this view when showing a non scaled icon is required.
 */

public class SolidCircleBackgroundImageView extends AppCompatImageView {
    private Paint paint = new Paint();

    public SolidCircleBackgroundImageView(final Context context) {
        this(context, null);
    }

    public SolidCircleBackgroundImageView(final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SolidCircleBackgroundImageView(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.paint.setStyle(Paint.Style.FILL);
        this.paint.setAntiAlias(true);
        setWillNotDraw(false);
        if (attrs != null) {
            initWithStyledAttributes(attrs);
        }
    }

    private void initWithStyledAttributes(@NonNull final AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.SolidCircleBackgroundImageView, 0, 0);
        final int circleBackgroundColor = a.getColor(R.styleable.SolidCircleBackgroundImageView_circleBackgroundColor, Color.TRANSPARENT);
        setCircleBackground(circleBackgroundColor);
        a.recycle();
    }

    /**
     * Set the color of the background circle.
     * @param color
     */
    public void setCircleBackgroundColor(@ColorRes final int color) {
        setCircleBackground(getResources().getColor(color));
    }

    private void setCircleBackground(@ColorInt final int color) {
        this.paint.setColor(color);
        invalidate();
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        canvas.drawCircle(getWidth()/2f, getHeight()/2f ,getWidth()/2f , paint);
        super.onDraw(canvas);
    }
}
