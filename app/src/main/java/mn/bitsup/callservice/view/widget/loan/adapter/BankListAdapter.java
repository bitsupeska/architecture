package mn.bitsup.callservice.view.widget.loan.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.view.custom.WidgetListItemClickListener;
import mn.bitsup.callservice.view.widget.loan.view.dto.BankItemPayload;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.ProductViewHolder> {
    private List<BankItem> models;
    private Context context;
    private int position = 0;
    private OnItemClickListener onItemClickListener;

    public BankListAdapter(Context context, List<BankItem> models, OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.context = context;
        this.models = models;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.bank_item, null);
        return new ProductViewHolder(view);
    }

    private int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder productViewHolder, int i) {
        try {
            productViewHolder.title.setText(models.get(i).getBankname());
            productViewHolder.product_item.setOnClickListener(v -> {
                BankItemPayload bankItemPayload = new BankItemPayload();
                bankItemPayload.setBankItem(models.get(i));
                bankItemPayload.setImageId(changeImage(i));
                this.onItemClickListener.onItemClicked(bankItemPayload);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;
        RelativeLayout product_item;

        ProductViewHolder(@NonNull final View itemView) {
            super(itemView);
            Log.e("BankListAdapter", "ProductViewHolder: " + position);
            title = itemView.findViewById(R.id.product_title);
            product_item = itemView.findViewById(R.id.product_item);
            image = itemView.findViewById(R.id.product_image);
            image.setBackgroundResource(changeImage(position));
            title.setText(models.get(position).getBankname());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(dpToPx(90), dpToPx(72));
            params.setMargins(position == 0 ? dpToPx(37) : dpToPx(16), dpToPx(0), dpToPx(0), dpToPx(0));
            product_item.setLayoutParams(params);
            position++;
        }
    }

    private int changeImage(int position) {
        if (models.get(position).getId() == 1) {
            return R.drawable.khan_bank;
        } else if (models.get(position).getId() == 2) {
            return R.drawable.ic_golomt;//golomt
        } else if (models.get(position).getId() == 3) {
            return R.drawable.tdb;
        } else if (models.get(position).getId() == 4) {
            return R.drawable.khas_bank;
        } else {
            return 0;
        }
    }

    public abstract static class OnItemClickListener implements WidgetListItemClickListener<BankItemPayload> {
    }
}
