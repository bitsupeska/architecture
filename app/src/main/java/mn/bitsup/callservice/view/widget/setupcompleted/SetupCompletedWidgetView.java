package mn.bitsup.callservice.view.widget.setupcompleted;

import static mn.bitsup.callservice.util.KeyboardUtils.hideKeyboard;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricManager;
import androidx.fragment.app.Fragment;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.util.DebouncedOnClickListener;
import mn.bitsup.callservice.view.custom.CustomButton;

public class SetupCompletedWidgetView extends Fragment {

    private Context context;
    private CheckBox checkFingerPrint;
    private StorageComponent inMemoryStorage;
    private CustomButton buttonContinue;
    private SetupCompletedWidget contract;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        inMemoryStorage = new StorageComponent();
        contract = new SetupCompletedWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.setup_complete_layout, container, false);
        hideKeyboard((Activity) getContext());

        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        checkFingerPrint = inflatedView.findViewById(R.id.checkFingerPrint);
        checkFingerPrint.setVisibility(View.INVISIBLE);
        checkIsFinger();

        inflatedView.findViewById(R.id.buttonContinue).setOnClickListener(new DebouncedOnClickListener() {
            @Override
            public void onDebouncedClick(View v) {
                setIsFinger();
                contract.confirmed();
            }
        });

        return inflatedView;
    }

    private void checkIsFinger() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            try {
                BiometricManager biometricManager = BiometricManager.from(context);
                switch (biometricManager.canAuthenticate()) {
                    case BiometricManager.BIOMETRIC_SUCCESS:
                        checkFingerPrint.setVisibility(View.VISIBLE);
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void setIsFinger() {
        if (checkFingerPrint.isChecked()) {
            inMemoryStorage.setItem("bb.key.is_finger", "true");
        } else {
            inMemoryStorage.setItem("bb.key.is_finger", "false");
        }
    }
}
