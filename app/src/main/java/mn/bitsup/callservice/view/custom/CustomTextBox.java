package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Constraints;

import mn.bitsup.callservice.R;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import static mn.bitsup.callservice.util.ResourcesUtil.dpToPx;

/**
 * Created by Erdenedalai R&D B.V on 19/10/2017.
 * Custom TextInputLayout with styleable properties.
 */

public class CustomTextBox extends ConstraintLayout implements TextWatcher, View.OnFocusChangeListener {

    private static final int INPUT_TYPE_TEXT = 0;
    private static final int INPUT_TYPE_PASSWORD = 1;
    private static final int INPUT_TYPE_USERNAME = 2;
    private TextInputLayout textInputLayout;
    private TextInputEditText editText;
    private ImageButton rightButton;
    private ImageButton leftButton;
    private boolean leftButtonVisible = false;
    private int editTextOriginalPaddingRight;
    private int parentId;

    public CustomTextBox(Context context) {
        this(context, null);
    }

    public CustomTextBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View inflatedView = inflate(context, R.layout.view_textbox, this);
        parentId = getId();
        editText = inflatedView.findViewById(R.id.textInputEditText);
        textInputLayout = inflatedView.findViewById(R.id.textInputLayout);
        editText.setId(parentId + editText.getId());
        editText.setPaintFlags(0);
        textInputLayout.setErrorEnabled(false);
        setInputType(context, attrs, editText);
        editTextOriginalPaddingRight = editText.getPaddingRight();
        rightButton = createRightButtonView(parentId + editText.getId(), 0);
        rightButton.setVisibility(GONE);
        rightButton.setImageResource(R.drawable.ic_cancel);
        rightButton.setColorFilter(getResources().getColor(R.color.primary));
        rightButton.setOnClickListener(rightButtonClickListener);
        editText.addTextChangedListener(this);
        editText.setOnFocusChangeListener(this);
    }

    private void updateRightButtonsVisibility() {
        if (Objects.requireNonNull(editText.getText()).toString().length() > 0) {
            rightButton.setVisibility(VISIBLE);
            if (leftButton != null)
                leftButton.setVisibility(VISIBLE);
        } else {
            rightButton.setVisibility(GONE);
            if (leftButton != null)
                leftButton.setVisibility(GONE);
        }
    }

    public void visibleButtonAdd() {
        if (rightButton != null) {
            rightButton.setImageResource(R.drawable.visibility_off);
            leftButton = createRightButtonView(parentId + rightButton.getId(), 32);
            leftButton.setVisibility(GONE);
            leftButton.setImageResource(R.drawable.ic_cancel);
            leftButton.setColorFilter(getResources().getColor(R.color.primary));
            leftButton.setOnClickListener(rightButtonClickListener);
            rightButton.setOnClickListener(leftButtonClickListener);
        }
    }

    private int buttonCount = 0;

    private ImageButton createRightButtonView(int id, int marginRight) {
        final ImageButton imageButton = new ImageButton(getContext());
        imageButton.setBackgroundColor(Color.TRANSPARENT);
        imageButton.setId(id);
        imageButton.setPadding(0, 0, 0, 0);

        final int buttonSize = getContext().getResources().getDimensionPixelSize(R.dimen.margin_32dp);
        marginRight = dpToPx(marginRight, getContext().getResources());
        final ConstraintLayout.LayoutParams lp = new Constraints.LayoutParams(buttonSize, buttonSize);
        addView(imageButton, lp);

        final int buttonMargin = getContext().getResources().getDimensionPixelSize(R.dimen.margin_14dp);

        final ConstraintSet constraints = new ConstraintSet();
        constraints.clone(this);
        constraints.connect(imageButton.getId(), ConstraintSet.END, rightButton == null ? getId() : rightButton.getId(), ConstraintSet.END, marginRight);
        constraints.connect(imageButton.getId(), ConstraintSet.BOTTOM, getId(), ConstraintSet.BOTTOM, buttonMargin);
        constraints.applyTo(this);

        editText.setPadding(editText.getPaddingLeft(), editText.getPaddingTop(),
                editTextOriginalPaddingRight + buttonCount * buttonSize, editText.getPaddingBottom());
        buttonCount++;

        return imageButton;
    }

    public EditText getEditText() {
        return this.editText;
    }

    private void setInputType(Context context, AttributeSet attrs, EditText editText) {
        TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.CustomTextBox, 0, 0);
        int inputTypeChoice = styledAttributes.getInt(R.styleable.CustomTextBox_inputType, 0);
        switch (inputTypeChoice) {
            case INPUT_TYPE_PASSWORD:
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
            case INPUT_TYPE_USERNAME:
            case INPUT_TYPE_TEXT:
                editText.setLines(1);
                editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                break;
            default:
                //Keep default behavior
        }
        styledAttributes.recycle();
    }

    boolean isVisible = false;

    private View.OnClickListener rightButtonClickListener = v -> editText.setText("");
    private View.OnClickListener leftButtonClickListener = v -> {
        if (isVisible) {
            setPasswordType();
        } else {
            setTextType();
        }
        isVisible = !isVisible;
    };

    public void setPasswordType() {
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        editText.setSelection(editText.getText().length());
        rightButton.setImageResource(R.drawable.visibility_off);
    }

    public void setTextType() {
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        editText.setSelection(editText.getText().length());
        rightButton.setImageResource(R.drawable.visibility);
    }

    public void setHintText(int hintText) {
        editText.setHint(getResources().getString(hintText));
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        updateRightButtonsVisibility();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            rightButton.setVisibility(GONE);
            if (leftButton != null)
                leftButton.setVisibility(GONE);
        } else {
            if (editText.getText().length() > 0) {
                rightButton.setVisibility(VISIBLE);

                if (leftButton != null)
                    leftButton.setVisibility(VISIBLE);
            }
        }
    }
}