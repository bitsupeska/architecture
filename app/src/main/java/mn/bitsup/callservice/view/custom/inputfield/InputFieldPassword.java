package mn.bitsup.callservice.view.custom.inputfield;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Constraints;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.view.util.StyledAttributesHelper;

/**
 * Created by Erdenedalai R&D B.V on 21/06/2018.
 * Custom implementation of an input field using TextInputLayout.
 */
public class InputFieldPassword extends ConstraintLayout {
    protected ImageButton rightButton;
    protected ImageButton cancelButton;
    protected ImageView iconImageView;
    protected TextInputLayout textInputLayout;
    protected String hintText;
    protected TextInputEditText editText;
    private TextWatcher rightButtonsVisibilityTextWatcher;
    private Boolean isVisible = true;
    private String error_message = "error message not set.";
    @DrawableRes
    private int iconResId;

    @DrawableRes
    private int rightButtonIconResId;

    @ColorInt
    private int rightButtonIconTint;

    private int editTextOriginalPaddingRight;

    public InputFieldPassword(Context context) {
        this(context, null);
    }

    public InputFieldPassword(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    @SuppressLint("ResourceType")
    protected void init(Context context, AttributeSet attrs) {
        View inflatedView = inflate(context, R.layout.view_input_field_value, this);
        iconImageView = inflatedView.findViewById(R.id.iconImageView);
        editText = inflatedView.findViewById(R.id.textInputEditText);
        textInputLayout = inflatedView.findViewById(R.id.textInputLayout);
        setTextAppearance();
        setRightButton(R.drawable.visibility_off, context.getResources().getColor(R.color.text_grey)); //off

        if (attrs != null) {
            TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.InputField, 0, 0);
            hintText = StyledAttributesHelper.getStyleableAttributeString(styledAttributes, R.styleable.InputField_inputFieldIconHint, "");
            iconResId = styledAttributes.getResourceId(R.styleable.InputField_inputFieldIconRes, -1);
            rightButtonIconResId = styledAttributes.getResourceId(R.styleable.InputField_inputFieldRightButtonIconRes, -1);
            rightButtonIconTint = styledAttributes.getColor(R.styleable.InputField_inputFieldRightButtonIconTint,
                    getContext().getResources().getColor(R.color.primary));
            styledAttributes.recycle();
        }

        textInputLayout.setHint(hintText);
        if (iconResId > 0) {
            iconImageView.setImageResource(iconResId);
            iconImageView.setVisibility(VISIBLE);
        }
        if (rightButtonIconResId > 0) {
            setRightButton(rightButtonIconResId, rightButtonIconTint);
        }

        rightButtonsVisibilityTextWatcher = new RightButtonsVisibilityTextWatcher();
        editText.addTextChangedListener(rightButtonsVisibilityTextWatcher);
        editTextOriginalPaddingRight = editText.getPaddingRight();


        setOnFocusChangeListener((v, hasFocus) -> updateRightButtonsVisibility());
    }

    protected void setTextAppearance() {
        editText.setTextAppearance(getContext(), R.style.Body1Medium);
    }

    public void setImeOptions(int options) {
        editText.setImeOptions(options);
    }

    public void setCancelButtonDuringEditing(@Nullable final OnClickListener onClickListener) {
        if (hasCancelButton()) {
            return;
        }
        getCancelButton(R.dimen.margin_32dp).setImageResource(R.drawable.ic_cancel);
        getCancelButton(R.dimen.margin_32dp).setColorFilter(getContext().getResources().getColor(R.color.text_grey));
        getCancelButton(R.dimen.margin_32dp).setOnClickListener(v -> {
            editText.setText("");
            if (onClickListener != null) {
                onClickListener.onClick(v);
            }
        });
        updateRightButtonsVisibility();
    }

    public void setCancelButton() {
        if (hasCancelButton()) {
            return;
        }
        getCancelButton(R.dimen.margin_32dp).setImageResource(R.drawable.ic_cancel);
        getCancelButton(R.dimen.margin_32dp).setColorFilter(getContext().getResources().getColor(R.color.text_grey));
        getCancelButton(R.dimen.margin_32dp).setOnClickListener(v ->  editText.setText(""));
        updateRightButtonsVisibility();
    }

    /**
     * Remove the cancel button.
     */
    public void removeCancelButton() {
        if (hasCancelButton()) {
            removeRightButtonInternal(cancelButton);
            cancelButton = null;
        }
        updateRightButtonsVisibility();
    }

    /**
     * @author unobatbayar
     * Remain unmask button on validated password.
     */

    private void restoreCancel(){
        getCancelButton(R.dimen.margin_32dp).setImageResource(R.drawable.ic_cancel);
        getCancelButton(R.dimen.margin_32dp).setColorFilter(getContext().getResources().getColor(R.color.text_grey));
        getCancelButton(R.dimen.margin_32dp).setOnClickListener(v ->  editText.setText(""));
    }

    private void remainUnmask(){
        getCancelButton(R.dimen.margin_32dp).setImageResource(R.drawable.ic_check_circle);
        getCancelButton(R.dimen.margin_32dp).setColorFilter( getContext().getResources().getColor(R.color.support_success));
        getCancelButton(R.dimen.margin_32dp).setOnClickListener(null);
        //getCancelButton(R.dimen.margin_32dp).setOnClickListener(v ->  editText.setText(""));
        //updateRightButtonsVisibility();

        //setRightButton(R.drawable.ic_check_circle, getContext().getResources().getColor(R.color.support_success));
    }

    /**
     * Shows a button on the right of the input field, a listener can be provided
     * to receive the click event.
     * If there is a previously set right button, it gets replaced.
     * The button is always shown, regardless of whether or not the edit text contains some text or is empty.
     * If there is a cancel button enabled, the cancel button is shown when this view is focused.
     *
     * @param iconResId the res id of the icon to use.
     * @param color     the tint color for the icon.
     */
    public void setRightButton(int iconResId, int color) {
        removeRightButton();
        getRightButton(R.dimen.margin_0dp).setImageResource(iconResId);
        getRightButton(R.dimen.margin_0dp).setColorFilter(color);
        updateRightButtonsVisibility();
    }

    /**
     * Set the {@link OnClickListener} for the right button.
     * If the right button is not present, or configured as cancel button, this method does nothing.
     *
     * @param onClickListener the listener, use null to remove the listener.
     */
    public void setRightButtonOnClickListener(@Nullable final OnClickListener onClickListener , int dimen) {
        if (hasRightButton() && hasCancelButton()) {
            getRightButton(dimen).setOnClickListener(onClickListener);
        }
    }

    /**
     * Remove a previously set right button, if no right button is present this method does nothing.
     */
    public void removeRightButton() {
        if (hasRightButton()) {
            removeRightButtonInternal(rightButton);
            rightButton = null;
        }
    }

    /**
     * Show an error message below the EditText.
     *
     * @param errorMessage the message to show, null to not show an error.
     */
    public void setError(@Nullable final String errorMessage) {
        if (errorMessage != null) {
            textInputLayout.setErrorEnabled(true);
            textInputLayout.setError(errorMessage);
            restoreCancel();
            setRightButton(R.drawable.visibility_off, getContext().getResources().getColor(R.color.text_grey)); //off
            this.error_message = errorMessage;
        } else {
            textInputLayout.setErrorEnabled(false);
            remainUnmask();
            this.error_message = "";
        }
    }

    public String getError(){
        return this.error_message;
    }


    private boolean hasRightButton() {
        return rightButton != null;
    }

    private boolean hasCancelButton() {
        return cancelButton != null;
    }

    private ImageButton getRightButton(int dimen) {
        if (!hasRightButton()) {
            rightButton = createRightButtonView(R.id.input_field_right_button,  dimen);
        }
        return rightButton;
    }

    private ImageButton getCancelButton(int dimen) {
        if (!hasCancelButton()) {
            cancelButton = createRightButtonView(R.id.input_field_cancel_button,  dimen);
        }
        return cancelButton;
    }

    private void removeRightButtonInternal(final View rightButton) {
        removeView(rightButton);
        editText.setPadding(editText.getPaddingLeft(), editText.getPaddingTop(),
                editTextOriginalPaddingRight, editText.getPaddingBottom());
    }

    private ImageButton createRightButtonView(int id, int dimen) {
        final ImageButton imageButton = new ImageButton(getContext());
        imageButton.setBackgroundColor(Color.TRANSPARENT);
        imageButton.setId(id);
        imageButton.setPadding(0, 0, 0, 0);

        final int buttonSize = getContext().getResources().getDimensionPixelSize(R.dimen.margin_24dp);
        final int endSize = getContext().getResources().getDimensionPixelSize(dimen);
        final LayoutParams lp = new Constraints.LayoutParams(buttonSize, buttonSize);
        addView(imageButton, lp);

        final int buttonMargin = getContext().getResources().getDimensionPixelSize(R.dimen.margin_16dp);
        final int buttonMarginError = getContext().getResources().getDimensionPixelSize(R.dimen.margin_28dp);

        final ConstraintSet constraints = new ConstraintSet();
        constraints.clone(this);
        constraints.connect(imageButton.getId(), ConstraintSet.END, getId(), ConstraintSet.END, endSize);

        if (!textInputLayout.getEditText().equals("")) {
            constraints.connect(imageButton.getId(), ConstraintSet.TOP, getId(), ConstraintSet.TOP, buttonMarginError);
        }else{
            constraints.connect(imageButton.getId(), ConstraintSet.BOTTOM, getId(), ConstraintSet.BOTTOM, buttonMargin);
        }

        constraints.applyTo(this);
        editText.setPadding(editText.getPaddingLeft(), editText.getPaddingTop(),
                editTextOriginalPaddingRight + buttonSize, editText.getPaddingBottom());

        return imageButton;
    }

    public String getValue() {
        return editText.getText().toString();
    }

    @Override
    public void setEnabled(final boolean enabled) {
        editText.setEnabled(enabled);
    }

    @Override
    public void setOnClickListener(@Nullable final OnClickListener l) {
        editText.setOnClickListener(l);
    }

    public void addTextChangedListener(@NonNull final TextWatcher textWatcher) {
        editText.addTextChangedListener(textWatcher);
    }

    public void setText(@NonNull final String text) {
        editText.setText(text);
        editText.setSelection(text.length());
    }

    public CharSequence getText() {
        return editText.getText();
    }

    public void setSingleLine() {
        editText.setSingleLine();
    }

    public void setInputType(final int type) {
        editText.setInputType(type);
    }

    public void setFilters(InputFilter[] filters) {
        editText.setFilters(filters);
    }


    @Override
    public void setFocusable(final boolean focusable) {
        editText.setFocusable(focusable);
        editText.setFocusableInTouchMode(focusable);
    }

    @Override
    public final void setOnFocusChangeListener(final OnFocusChangeListener listener) {
        editText.setOnFocusChangeListener((v, hasFocus) -> {
            updateRightButtonsVisibility();
            if (listener != null) {
                listener.onFocusChange(v, hasFocus);
            }
        });
    }

    public void setMaxInputLength(int length) {
        editText.setFilters(new InputFilter[]{ new InputFilter.LengthFilter(length)});
    }

    public void setDigits(String digits) {
        editText.setKeyListener(DigitsKeyListener.getInstance(digits));
    }

    /**
     * Sets the input field's icon
     *
     * @param iconResId the drawable's resource id
     */
    public void setIcon(@DrawableRes int iconResId) {
        this.iconResId = iconResId;
        iconImageView.setImageResource(iconResId);
        iconImageView.setVisibility(VISIBLE);
    }

    /**
     * Hides the input field's icon
     */
    public void hideIcon() {
        iconImageView.setVisibility(GONE);
    }

    public void setPasswordType(){
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        setRightButton(R.drawable.visibility_off, getContext().getResources().getColor(R.color.text_grey)); //off
        setSelection(editText.getText().length());
    }

    public void setTextType(){
        editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        setRightButton(R.drawable.visibility, getContext().getResources().getColor(R.color.text_grey)); //on
        setSelection(editText.getText().length());
    }

    public void setSelection(int selection) {
        editText.setSelection(selection);
    }

    /**
     * Hde the sift keyboard.
     */
    public void hideSoftInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(getWindowToken(), 0);
        }
    }

    /**
     * Show the soft keyboard on this view.
     */
    public void showSoftInput() {
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.showSoftInput(editText, 0);
        }
    }

    private void updateRightButtonsVisibility() {
        if (editText.isFocused()) {
            setRightButtonOnClickListener(v -> visible(), 0);
            if (hasCancelButton() && hasRightButton()) {
                getCancelButton(R.dimen.margin_32dp).setVisibility(!TextUtils.isEmpty(getText()) ? View.VISIBLE : View.GONE);
                getRightButton(R.dimen.margin_0dp).setVisibility(!TextUtils.isEmpty(getText()) ? View.VISIBLE : View.GONE);
            }
            else if (hasCancelButton()) {
                getCancelButton(R.dimen.margin_0dp).setVisibility(!TextUtils.isEmpty(getText()) ? View.VISIBLE : View.GONE);
            }
            else if (hasRightButton()) {
                getRightButton(R.dimen.margin_0dp).setVisibility(!TextUtils.isEmpty(getText()) ? View.VISIBLE : View.GONE);
            }
        } else {
            if (hasCancelButton() && hasRightButton()) {
                getCancelButton(R.dimen.margin_0dp).setVisibility(View.GONE);
                getRightButton(R.dimen.margin_0dp).setVisibility(View.GONE);
            }
            else if (hasCancelButton()) {
                getCancelButton(R.dimen.margin_0dp).setVisibility(View.GONE);
            }
            else if (hasRightButton()) {
                getRightButton(R.dimen.margin_0dp).setVisibility(View.GONE);
            }
        }
    }

    public void setIconViewVisible(boolean visible) {
        if (visible) {
            iconImageView.setVisibility(VISIBLE);
            int size = getResources().getDimensionPixelSize(R.dimen.margin_16dp);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) textInputLayout.getLayoutParams();
            params.setMarginStart(size);
            params.setMarginEnd(size);
            textInputLayout.setLayoutParams(params);
        } else {
            iconImageView.setVisibility(GONE);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) textInputLayout.getLayoutParams();
            params.setMarginStart(0);
            params.setMarginEnd(0);
            textInputLayout.setLayoutParams(params);
        }
    }

    private void visible() {
        if(isVisible){
            setTextType();
        }else{
            setPasswordType();
        }
        isVisible = !isVisible;
    }

    public void setMaxLength(int length){
        editText.setFilters(new InputFilter[] {new InputFilter.LengthFilter(length)});
    }


    private class RightButtonsVisibilityTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(Editable s) {
            updateRightButtonsVisibility();
        }
    }

}
