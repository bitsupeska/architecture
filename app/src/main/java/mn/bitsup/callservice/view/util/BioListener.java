package mn.bitsup.callservice.view.util;

public interface BioListener {
    void onSuccess();
    void onError();
}
