package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockData;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.dto.ResetInfo;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.ResetInfoListener;
import mn.bitsup.callservice.client.profile.dto.Emails;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.EmojiFilter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;

public class ForgetUserNameWidgetView extends Fragment implements TextWatcher {

    private CustomButton buttonContinue;
    private InputField inputUserName;
    private String regNum;
    private String blockMessage;
    private User resultUser = new User();
    private static String TAG = ForgetUserNameWidgetView.class.getName();
    private ForgotPasswordContract contract;
    private boolean isBlocked;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = (Activity) getContext();
        assert activity != null;
        try {
            activity.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE
                    | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            Log.e("activity.getWindow()", "exception: " + e);
        }
        this.regNum = EventHelper.getEventPayload("registerNum", String.class);
        contract = new ForgetWidget(Objects.requireNonNull(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.reset_username, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputUserName = inflatedView.findViewById(R.id.inputUserName);
    }

    private void initView() {
        buttonContinue.setLoadingText("");
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
        inputUserName.addTextChangedListener(this);
        inputUserName.setMaxLength(30);
        inputUserName.setSingleLine();
        inputUserName.setCancelButton();
        inputUserName.setFilters(EmojiFilter.getFilter());
        buttonContinue.setOnClickListener(v -> checkBlock());
    }

    private void isValid() {

        String username = inputUserName.getValue();
        if (!username.equals("")) {
            if (isCorrectLength(username) && hasDigits(username) && hasLowerCase(username)
                && !hasSpecial(username)) {
                inputUserName.setError(null);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            } else if (hasCrillic(username)) {
                inputUserName.setError("Крилл үсэг байж болохгүй.");
                buttonContinue.setState(ButtonState.DISABLED, getActivity());
            } else if (!isCorrectLength(username)) {
                inputUserName.setError("Таны нэвтрэх нэр 5-30 оронтой байх хэрэгтэй.");
                buttonContinue.setState(ButtonState.DISABLED, getActivity());
            } else if (!hasDigits(username)) {
                inputUserName.setError("Таны нэвтрэх нэрэнд тоо орсон байх хэрэгтэй.");
                buttonContinue.setState(ButtonState.DISABLED, getActivity());
            } else if (hasSpecial(username)) {
                inputUserName.setError("Нэвтрэх нэрэнд тусгай тэмдэгт хэрэглэж болохгүй.");
                buttonContinue.setState(ButtonState.DISABLED, getActivity());
            }
        }
    }

    private boolean hasCrillic(final String name) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[-АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯӨѲҮабвгдеёжзийклмнопрстуфхцчшщъыьэюяѳөү]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

    private static boolean hasSpecial(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[^A-Za-z0-9.]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private static boolean hasLowerCase(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[a-zA-Z]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private static boolean hasDigits(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private static boolean isCorrectLength(final String password) {
        return password.length() >= 5 && password.length() <= 29 && !password.isEmpty();
    }

    private void publish() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());

        User user = new User();
        user.setRegNum(regNum);
        user.setUid(inputUserName.getValue().toLowerCase());
        contract.checkRegister(user, new ResetInfoListener() {
            @Override
            public void onSuccess(ResetInfo var1) {
                switch (var1.getCode()) {
                    case "200":
                        resultUser = var1.getData().get(0);
                        if (resultUser.getCif().equals("0")) {
                            buttonContinue.setState(ButtonState.ENABLED, getActivity());
                            if (resultUser.getHandPhone() == null || (
                                resultUser.getHandPhone() != null && resultUser.getHandPhone()
                                    .equals("10000000"))) {
                                CustomAlertDialog.showAlertDialog(getContext(),
                                    getContext().getResources()
                                        .getString(R.string.shared_alert_generic_alert_error_title),
                                    " Танд бүртгэлтэй дугаар олдсонгүй");
                            } else {
                                publish(new ForgetOtpWidgetView(), resultUser);
                            }
                        } else {
                            contract.getUser(new UserListener() {
                                @Override
                                public void onSuccess(User var1) {
                                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                                    var1.setHandPhone(resultUser.getHandPhone());
                                    List<Emails> emails = var1.getEmails();
                                    if (emails != null && !emails.get(0).getEmailStatus()
                                        .equals("0")) {
                                        publish(new ForgetEmailOrPhone(), var1);
                                    } else {
                                        publish(new ForgetOtpWidgetView(), var1);
                                    }
                                }

                                @Override
                                public void onError(Response var1) {
                                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                                    CustomAlertDialog.showServerAlertDialog(getContext());
                                }
                            }, user);

                        }
                        break;
                    case "301":
                        checkBlockIncrement();
                        CustomAlertDialog.showAlertDialog(getContext(), "Бүртгэлгүй регистр",
                            " Таны оруулсан регистрийн дугаар бүртгэлгүй байна.");
                        buttonContinue.setState(ButtonState.ENABLED, getActivity());
                        break;
                    case "302":
                        checkBlockIncrement();
                        CustomAlertDialog.showAlertDialog(getContext(), "Бүртгэлгүй нэвтрэх нэр",
                            " Таны оруулсан нэвтрэх нэр бүртгэлгүй байна.");
                        buttonContinue.setState(ButtonState.ENABLED, getActivity());
                        break;
                }
            }

            @Override
            public void onError(Response var1) {
                Log.e(TAG, "onError: " + var1.getErrorMessage());
                CustomAlertDialog.showServerAlertDialog(getContext());
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        });
    }

    private void publish(Fragment fragment, User user) {
        EventHelper.publishBack(getContext(), fragment, "getUser", user);
    }

    private void checkBlock() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> blockParams = new HashMap<>();
        blockParams.put("value", inputUserName.getValue().toLowerCase());
        blockParams.put("macAddress", DeviceAddressUtil.getMACAddress(getContext(),"wlan0"));


        blockParams.put("ipAddress", DeviceAddressUtil.getIPAddress(true));
        contract.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                if (!var1.getData().isEmpty()) {
                    for (BlockData block :
                            var1.getData()) {
                        // 2, 3 - khur block shalgah
                        if (block.getBlocktype() == 3
                                && block.getBlockchannel() == 2
                                && block.getBlockinfo().equals(inputUserName.getValue().toLowerCase())) {
                            isBlocked = true;
                            blockMessage = block.getBlockmsg();
                        }
                    }
                } else {
                    isBlocked = false;
                }
                if (!isBlocked) {
                    publish();
                } else {
                    new AnimationDialog(getContext(), "Уучлаарай,",
                            blockMessage,
                            "Буцах", "anim_rejection_clock.json",
                            () -> ((Activity) getContext()).finish()).show();
                }
            }

            @Override
            public void onError(Response var1) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, blockParams);
    }

    private void checkBlockIncrement() {
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                Log.e("MBank", "block hillee");
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, "12", "5");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        isValid();
    }
}