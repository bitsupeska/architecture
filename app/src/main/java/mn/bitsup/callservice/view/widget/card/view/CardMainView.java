package mn.bitsup.callservice.view.widget.card.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.KeyboardUtils;


public class CardMainView extends Fragment {
    ImageView cardImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.card_main_layout, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        cardImage = inflatedView.findViewById(R.id.cardImage);
    }

    private void initView() {

    }

    private void publish(Fragment fragment) {
    }

    @Override
    public void onResume() {
        KeyboardUtils.hideKeyboard(getActivity());
        super.onResume();
    }
}
