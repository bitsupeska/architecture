package mn.bitsup.callservice.view.custom;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 02/10/2018.
 * Enum to be used with {@link ErrorView}
 */
public enum ErrorViewType {

    LOAN(R.string.loan_error_failed_message,
            R.string.shared_alert_generic_error_title,
            R.string.loan_error_empty_message,
            R.drawable.search),

    NOTIFICATION(R.string.notifications_error_failed_message,
            R.string.notifications_error_empty_title,
            R.string.notification_error_empty_message,
            R.drawable.ic_no_messages),
    CARDS(R.string.connect_account_no_data_failed_message,
            R.string.cards_empty_title,
            R.string.cards_empty_message,
            R.drawable.ic_error_products),

    ACCOUNTS(R.string.connect_account_no_data_failed_message,
            R.string.connect_account_no_data_title,
            R.string.connect_account_no_data_error,
            R.drawable.contacts),
    MINIAPP(R.string.connect_account_no_data_failed_message,
            R.string.mini_app_no_installed_apps,
            R.string.connect_account_no_data_error,
            R.drawable.mini_app_not_found),

    SERVER(R.string.server_error_message,
             R.string.server_error_title,
             R.string.server_error_empty_message,
             R.drawable.ic_server_error),;


    @StringRes
    private final int errorMessage;
    @StringRes
    private final int emptyResultsTitle;
    @StringRes
    private final int emptyResultsMessage;
    @DrawableRes
    private final int emptyResultsDrawable;

    /**
     * Constructor to configure feature-specific string and image resources.
     * The following resources are automatically configured for all instances:
     * no internet error message, no internet error message title, no internet image resource,
     * no search results message, no search results title, no search results image resource,
     * error message title.
     *
     * @param errorMessage         generic error message
     * @param emptyResultsTitle    empty list title
     * @param emptyResultsMessage  empty list message
     * @param emptyResultsDrawable empty list image
     */
    ErrorViewType(@StringRes final int errorMessage,
                  @StringRes final int emptyResultsTitle,
                  @StringRes final int emptyResultsMessage,
                  @DrawableRes final int emptyResultsDrawable) {
        this.errorMessage = errorMessage;

        this.emptyResultsTitle = emptyResultsTitle;
        this.emptyResultsMessage = emptyResultsMessage;
        this.emptyResultsDrawable = emptyResultsDrawable;
    }

    @StringRes
    public int getNoInternetTitle() {
        return R.string.shared_error_no_internet_title;
    }

    @StringRes
    public int getNoInternetMessage() {
        return R.string.shared_error_no_internet_message;
    }

    @DrawableRes
    public int getNoInternetDrawable() {
        return R.drawable.ic_error_offline;
    }

    @StringRes
    public int getNoSearchResultsTitle() {
        return R.string.shared_search_error_no_results_title;
    }

    @StringRes
    public int getNoSearchResultsMessage() {
        return R.string.shared_search_error_no_results_message;
    }

    @DrawableRes
    public int getNoSearchResultsDrawable() {
        return R.drawable.ic_error_search;
    }

    @StringRes
    public int getErrorTitle() {
        return R.string.shared_error_failed_title;
    }

    @StringRes
    public int getErrorMessage() {
        return errorMessage;
    }

    @DrawableRes
    public int getErrorDrawable() {
        return emptyResultsDrawable;
    }

    @StringRes
    public int getEmptyResultsTitle() {
        return emptyResultsTitle;
    }

    @StringRes
    public int getEmptyResultsMessage() {
        return emptyResultsMessage;
    }

    @DrawableRes
    public int getEmptyResultsDrawable() {
        return emptyResultsDrawable;
    }
}
