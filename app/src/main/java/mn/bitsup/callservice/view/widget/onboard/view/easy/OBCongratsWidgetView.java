package mn.bitsup.callservice.view.widget.onboard.view.easy;


import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.airbnb.lottie.LottieAnimationView;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBCongratsWidgetView extends Fragment implements
    OnboardView<OnboardContract> {

    private TextView textName;
    private DataCell cellUsername;
    private LottieAnimationView animationView;
    private LottieAnimationView animationViewStar;
    private CustomButton buttonContinue;
    private Context context;
    private OnboardContract contract;
    public static String easyUsername;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contract = new OnboardWidget(getContext());
        this.context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        makeStatusBarPrimary();
        OnboardActivity.hideToolbar();
        View inflatedView =inflater.inflate(R.layout.onboard_congrats, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        textName = inflatedView.findViewById(R.id.textName);
        cellUsername = inflatedView.findViewById(R.id.cellUsername);
        animationView = inflatedView.findViewById(R.id.animation);
        animationViewStar = inflatedView.findViewById(R.id.animationStar);
        OnboardActivity.hideToolbar();
        setAnimation();
    }

    private void initView(){
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        buttonContinue.setOnClickListener(v -> publish());
        easyUsername = contract.getPayload().getUserName();
        textName.setText(contract.getPayload().getLastName() +" "+contract.getPayload().getFirstName());
        cellUsername.setValueText(easyUsername);
    }

    public void makeStatusBarPrimary() {
        Activity activity = ((Activity) context);
        Window window = activity.getWindow();
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            window.setStatusBarColor(ContextCompat.getColor(activity, R.color.primary));
        }
    }

    private void publish() {
        ((Activity) context).setResult(Activity.RESULT_FIRST_USER);
        ((Activity) context).finish();
    }

    private void setAnimation() {
        animationView.setAnimation("anim_welcome_header.json");
        animationView.playAnimation();

        animationViewStar.setAnimation("anim_welcome.json");
        animationViewStar.playAnimation();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((Activity) context).finish();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
