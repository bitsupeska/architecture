package mn.bitsup.callservice.view.widget.card.selector;

import static mn.bitsup.callservice.view.widget.card.dto.LockStatus.LOCKED;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.card.dto.CardItem;
import mn.bitsup.callservice.view.widget.card.dto.LockStatus;
import mn.bitsup.callservice.view.widget.card.util.CardUtil;

/**
 * Created by Erdenedalai R&D B.V. on 23/01/2019.
 * A decorator with current state for a card
 */
public class CardAdapterItem {

    private static int COLOR = R.color.primary;

    @NonNull
    private final CardItem cardItem;
    private boolean isInProgress;
    @ColorRes
    private int backgroundColor;

    private CardAdapterItem(@NonNull CardItem cardItem, @ColorRes int backgroundColor) {
        this.cardItem = cardItem;
        this.backgroundColor = backgroundColor;
        this.isInProgress = false;
    }

    @NonNull
    static List<CardAdapterItem> toAdapterItems(@NonNull List<CardItem> cards) {
        List<CardAdapterItem> cardAdapterItems = new ArrayList<>();
        for (int i = 0; i < cards.size(); i++) {
            int backgroundColor = COLOR;
            CardItem cardItem = cards.get(i);
            cardAdapterItems.add(new CardAdapterItem(cardItem, backgroundColor));
        }
        return cardAdapterItems;
    }

    public boolean isInProgress() {
        return isInProgress;
    }

    void setInProgress(boolean inProgress) {
        isInProgress = inProgress;
    }

    @NonNull
    public CardItem getCard() {
        return cardItem;
    }

    @ColorRes
    public int getBackgroundColor() {
        return backgroundColor;
    }

    @NonNull
    public Object getTag() {
        return getCard().getGuid();
    }

    @NonNull
    String getFormattedExpiryDate() {
        return CardUtil.getFormattedExpiryDate(getCard());
    }

    @NonNull
    String getHolderName() {
        return CardUtil.getHolderName(getCard());
    }

    void lock() {
        getCard().setLockStatus(LOCKED);
        setInProgress(false);
    }

    void unlock() {
        getCard().setLockStatus(LockStatus.UNLOCKED);
        setInProgress(false);
    }

    public boolean isLocked() {
        return getCard().getLockStatus() == LOCKED;
    }

    public boolean isInactive() {
        return CardUtil.STATUS_INACTIVE.equals(getCard().getStatus());
    }

    public boolean isCancelled() {
        return CardUtil.STATUS_CANCELLED.equals(getCard().getStatus());
    }

    public boolean isBroken() {
        return cardItem.getReplacement() != null
                && CardUtil.REASON_BROKEN.equals(cardItem.getReplacement().getReason());
    }

    @NonNull
    String getLowerCaseType() {
        return CardUtil.getLowerCaseType(getCard());
    }

    @NonNull
    String getMaskedNumberWithDots() {
        return CardUtil.getMaskedNumberWithDots(getCard());
    }
}
