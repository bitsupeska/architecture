package mn.bitsup.callservice.view.widget.home.core;
import mn.bitsup.callservice.client.call.listener.CallHistoryListener;
public interface HomeContract {
    void getHistories(int currentPage , int size, CallHistoryListener callHistoryListener);
}
