package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.CheckUsernameItems;
import mn.bitsup.callservice.client.offline.dto.EasyOnboardItem;
import mn.bitsup.callservice.client.offline.listener.CheckUserNameListener;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EmojiFilter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBUsernameWidgetView extends Fragment implements OnboardView<OnboardContract> {
    private Context context;
    private CustomButton buttonContinue;
    public static InputField inputUserName;
    private ContinueButtonTextWatcher continueButtonTextWatcher = new ContinueButtonTextWatcher();
    private ListView listView;
    private TextView textMessage;
    private ListAdapter adapter;
    private LinearLayout containerSuggested;

    private OnboardContract contract;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.contract = new OnboardWidget(getContext());
        this.context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
        @Nullable Bundle savedInstanceState) {
        View inflatedView =inflater.inflate(R.layout.onboard_username, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputUserName = inflatedView.findViewById(R.id.inputInput);
        textMessage = inflatedView.findViewById(R.id.textMessage);
        listView = inflatedView.findViewById(R.id.listView);
        containerSuggested = inflatedView.findViewById(R.id.containerSuggested);
    }

    private void initView(){
        inputUserName.setIconViewVisible(false);
        inputUserName.addTextChangedListener(continueButtonTextWatcher);
        inputUserName.setCancelButton();
        inputUserName.setSingleLine();
        inputUserName.setMaxLength(30);
        inputUserName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        inputUserName.setFilters(EmojiFilter.getFilter());

        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        buttonContinue.setLoadingText("");
        buttonContinue.setOnClickListener(v -> checkUsername());
    }

    private void checkUsername() {
        buttonContinue.setState(ButtonState.LOADING,getActivity());
        contract.checkUsername(inputUserName.getValue(), new CheckUserNameListener() {
            @Override
            public void onSuccess(CheckUsernameItems statusResponse) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                if(statusResponse.getStatus().equals("SUCCESS")){
                    publish();
                }else{
                    inputUserName.setError("Системд бүртгэлтэй нэр байна. Та өөр нэр оруулна уу.");
                    inputUserName.removeCancelButton();
                    setSuggestedList(statusResponse.getRecommendations());
                }
            }
            @Override
            public void onError(Response var2) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
            }
        });
    }

    private void setSuggestedList(String[] list) {
        if(list.length != 0){
            if(adapter == null){
                adapter = new ListAdapter(list);
                listView.setAdapter(adapter);
            }else{
                adapter.notifyDataSetChanged();
            }
            visibleSuggestedView();
        }else{
            hideSuggestedView();
        }
    }

    private void visibleSuggestedView(){
        containerSuggested.setVisibility(View.VISIBLE);
        textMessage.setVisibility(View.GONE);
    }

    private void hideSuggestedView(){
        containerSuggested.setVisibility(View.GONE);
        textMessage.setVisibility(View.VISIBLE);
    }

    private void publish(){
        EasyOnboardItem payload = contract.getPayload();
        payload.setUserName(inputUserName.getValue().toLowerCase());
        contract.setPayload(payload);
        OnboardActivity.addProgress();
        EventHelper.publishBack(context, new OBPasswordWidgetView());
        KeyboardUtils.hideKeyboard(context, inputUserName);
    }

    private void isValid() {
        hideSuggestedView();
        String username = inputUserName.getValue();
        if(!username.equals("")) {
            if (isCorrectLength(username) && hasDigits(username) && hasLowerCase(username)
                && !hasSpecial(username)) {
                inputUserName.setError(null);
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
            }else if(hasCrillic(username)){
                inputUserName.setError("Крилл үсэг байж болохгүй.");
                buttonContinue.setState(ButtonState.DISABLED,getActivity());
            }else if (!isCorrectLength(username)) {
                inputUserName.setError("Таны нэвтрэх нэр 5-30 оронтой байх хэрэгтэй.");
                buttonContinue.setState(ButtonState.DISABLED,getActivity());
            } else if (!hasDigits(username)) {
                inputUserName.setError("Таны нэвтрэх нэрэнд тоо орсон байх хэрэгтэй.");
                buttonContinue.setState(ButtonState.DISABLED,getActivity());
            }
            else if (hasSpecial(username)) {
                inputUserName.setError("Нэвтрэх нэрэнд тусгай тэмдэгт хэрэглэж болохгүй.");
                buttonContinue.setState(ButtonState.DISABLED,getActivity());
            }
        }
    }

    private  boolean hasCrillic(final String name ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[-АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯӨѲҮабвгдеёжзийклмнопрстуфхцчшщъыьэюяѳөү]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();
    }

    public static boolean hasSpecial(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[^A-Za-z0-9.]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean hasLowerCase(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[a-zA-Z]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean hasDigits(final String password ){
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9]).{1,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean isCorrectLength(final String password ){
        if(password.length() < 5 || password.length()>29 || password.isEmpty()){
            return false;
        }else {
            return true;
        }
    }

    private class ContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            isValid();
        }
    }

    public class ListAdapter extends BaseAdapter {

        String[] list;
        public ListAdapter(String[] list){
            this.list = list;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.length ;
        }

        @Override
        public Object getItem(int position) {
            return list[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.onboard_username_list_item, null);

            RelativeLayout containerItem = view.findViewById(R.id.containerItem);
            TextView textTitle = view.findViewById(R.id.textTitle);

            textTitle.setText(list[position]);
            containerItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    inputUserName.setText(list[position]);
                    hideSuggestedView();
                }
            });
            return  view;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
