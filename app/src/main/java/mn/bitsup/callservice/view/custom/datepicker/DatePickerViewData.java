package mn.bitsup.callservice.view.custom.datepicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Erdenedalai R&D B.V. on 21/02/2019.
 */
public interface DatePickerViewData {

    @NonNull
    default List<DatePickerLegendItem> getLegendItems() {
        return new ArrayList<>();
    }

    @NonNull
    default List<DatePickerItem> getDatePickerItems() {
        return new ArrayList<>();
    }

    @NonNull
    default Date getCalendarMinDate() {
        return new Date();
    }

    @Nullable
    Date getPreSelectedDate();
}
