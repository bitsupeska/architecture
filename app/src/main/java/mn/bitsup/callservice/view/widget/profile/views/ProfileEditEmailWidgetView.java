package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.ProfilePayload;

public class ProfileEditEmailWidgetView extends Fragment implements ProfileView<ProfileContract>, TextWatcher {


    private ProfileContract contract;
    private Context context;
    private CustomButton buttonContinue;
    private InputField inputEmail;
    private ProfilePayload payload;
    private String payloadType = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        this.payload = EventHelper.getEventPayload("getEmailData", ProfilePayload.class);
        if (payload != null && payload.getPublishType() != null)
            payloadType = payload.getPublishType();
        Activity activity = (Activity) context;
        try {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_email_edit, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputEmail = inflatedView.findViewById(R.id.inputEmail);

    }

    private void initViews() {
        inputEmail.addTextChangedListener(this);
        inputEmail.setInputType(InputType.TYPE_CLASS_TEXT);
        inputEmail.setCancelButton();
        inputEmail.setSingleLine();

        if (payloadType.equals("Loan")) buttonContinue.setOnClickListener(v -> sendRequest());
        else buttonContinue.setOnClickListener(v -> checkEmail());

        if (payload != null && payload.getEmailId() != null)
            inputEmail.setText(payload.getEmailId().toLowerCase());
        else buttonContinue.setState(ButtonState.DISABLED, getActivity());

    }

    private void checkEmail() {
        if (contract.getUser().getEmails().get(0).getEmailId().toLowerCase().equals(inputEmail.getValue().toLowerCase())) {
            NudgeControllerUtil.showErrorNotification(context.getResources().getString(R.string.shared_alert_generic_alert_error_title), "Таны өөрийн и-мэйл байна", context);
        } else {
            sendRequest();
        }
    }

    public void publish() {
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
        String type = "Profile";
        if (payloadType.equals("Loan")) type = "Loan";
        ProfilePayload payload = new ProfilePayload();
        payload.setEmailId(inputEmail.getValue().toLowerCase());
        payload.setPublishType(type);
        KeyboardUtils.hideKeyboard(context, inputEmail);
        EventHelper.publishBack(context, new ProfileEmailOTPWidgetView(), "getEmailData", payload);
    }

    private void sendRequest() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> additions = new HashMap<>();
        additions.put("email", inputEmail.getValue().toLowerCase());
        contract.requestEmailOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                if (otp.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                    CustomAlertDialog.showAlertDialog(context, "", otp.getMessage());
                }
            }

            @Override
            public void onError(Response var1) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }

        }, additions);
    }


    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (!isValidEmail(editable)) buttonContinue.setState(ButtonState.DISABLED, getActivity());
        else buttonContinue.setState(ButtonState.ENABLED, getActivity());
    }

}
