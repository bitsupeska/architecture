package mn.bitsup.callservice.view.widget.onboard.activity;

import static mn.bitsup.callservice.view.activity.BaseActivity.BackButtonStyle.BACK;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.activity.BaseActivity;

public class OnboardContractDescActivity extends AppCompatActivity {
    private TextView textTitle, textBody;
    public Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.onboard_contract_desc_activity);

        textTitle = findViewById(R.id.textTitle);
        textBody = findViewById(R.id.textBody);
        toolbar = findViewById(R.id.toolbar);
        configureToolbar();
        Intent intent = getIntent();
        textTitle.setText(intent.getStringExtra("title"));
        textBody.setText(intent.getStringExtra("body"));

    }

    @Override
    protected void onStop() {
        super.onStop();
    }
    private void configureToolbar() {
        setSupportActionBar(toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            configureToolbarNavigation(toolbar);
            toolbar.setNavigationOnClickListener(v -> onBackPressed());
        }

    }
    protected void configureToolbarNavigation(Toolbar toolbar) {
        switch (getBackButtonStyle()) {
            case CLOSE:
                toolbar.setNavigationIcon(R.drawable.ic_close_cross);
                toolbar.setNavigationContentDescription(R.string.shared_button_close);
                break;
            case BACK:
                toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
                toolbar.setNavigationContentDescription(R.string.shared_button_back);
                break;
            default:
                //Do nothing
        }
    }
    protected BaseActivity.BackButtonStyle getBackButtonStyle() {
        return BACK;
    }
}
