package mn.bitsup.callservice.view.controller;

import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.custom.NudgeViewContainer;

public final class NudgeControllerUtil {

    private static final int DEFAULT_DURATION_MS = 3000;

    public static void showNotification(@ColorRes final int colorResId,
                                        @DrawableRes final int iconResId,
                                        @NonNull final String title,
                                        @NonNull final String subtitle,
                                        final int durationMs,
                                        @NonNull Context context) {
        Activity activity = (Activity)context;
        NudgeViewContainer nudgeViewContainer = new NudgeViewContainer(activity);

        ViewGroup  decorView = (ViewGroup) activity.getWindow().getDecorView();
        ViewGroup.LayoutParams lp =
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        decorView.addView(nudgeViewContainer, lp);
        nudgeViewContainer.push(
                colorResId,
                iconResId,
                title,
                subtitle,
                durationMs);
    }

    /**
     * Helper method, use this to send the pub/sub event to show an error nudge notification.
     *
     * @param title    title of the notification.
     * @param subtitle subtitle of the notification.
     */
    public static void showErrorNotification(@NonNull final String title,
                                             @NonNull final String subtitle,
                                             @NonNull Context context) {
        showNotification(R.color.support_danger,
                R.drawable.ic_warning,
                title,
                subtitle,
                DEFAULT_DURATION_MS,
                context
        );
    }

    /**
     * Helper method, use this to send the pub/sub event to show a success nudge notification.
     *
     * @param title    title of the notification.
     * @param subtitle subtitle of the notification.
     */
    public static void showSuccessNotification(@NonNull final String title,
                                               @NonNull final String subtitle,
                                               @NonNull Context context) {
        showNotification(R.color.support_success,
                R.drawable.ic_check,
                title,
                subtitle,
                DEFAULT_DURATION_MS,
                context);
    }

    private NudgeControllerUtil() {
        throw new UnsupportedOperationException("Util class should not be instantiated");
    }
}
