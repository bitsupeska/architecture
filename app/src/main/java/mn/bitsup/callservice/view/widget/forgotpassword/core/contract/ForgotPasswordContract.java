package mn.bitsup.callservice.view.widget.forgotpassword.core.contract;


import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.ResetInfoListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;

public interface ForgotPasswordContract {
    void checkRegister(User user, ResetInfoListener listener);

    void checkToken(TokenListener var1);

    void requestOTP(OTPListener var1, Map<String, String> var2);

    void checkOTP(OTPListener otpListener, Map<String, String> params);

    void setBlock(StatusListener statusListener, int count,String blockChannel, String blockType);

    void checkBlockIncrement(BlockIncrementListener var1, String blockType, String blockChannel);

    void resetPassword(StatusListener listener, String password, String Uid);

    void requestEmailOTP(OTPListener otpListener, Map<String, String> params);

    void checkEmailOTP(OTPListener otpListener, Map<String, String> params);

    void getUser(UserListener userListener, User user);

    void checkBlock(BlockListener blockListener, HashMap<String, String> param);

    void getKhurData(KhurDataListener listener, Map<String, String> params);

    void setBlockKhur(StatusListener statusListener,String uid);
}
