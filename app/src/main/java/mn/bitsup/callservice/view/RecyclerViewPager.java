package mn.bitsup.callservice.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Px;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewPager extends RecyclerView {

    private PagerSnapHelper pagerSnapHelper;
    private LinearLayoutManager linearLayoutManager;
    private static String TAG = RecyclerViewPager.class.getName();
    public RecyclerViewPager(@NonNull Context context) {
        super(context);
        setUpAsPager();
    }

    public RecyclerViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setUpAsPager();
    }

    public RecyclerViewPager(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setUpAsPager();
    }

    private void setUpAsPager() {

        linearLayoutManager = new LinearLayoutManager(getContext(), HORIZONTAL, false);
        setLayoutManager(linearLayoutManager);

        addItemDecoration(new FixedWidthItemDecoration());

        pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(this);
    }

    public void setPagerSnapHelper(@NonNull PagerSnapHelper pagerSnapHelper) {
        this.pagerSnapHelper = pagerSnapHelper;
        this.pagerSnapHelper.attachToRecyclerView(this);
    }

    /**
     * A delegate method that scrolls into the required {@code position} smoothly.
     * Use this method instead of {@link #scrollToPosition(int)}.
     * This method calls scroll listeners, unlike {@link #scrollToPosition(int)}
     *
     * @param position the position to scroll smoothly to
     */
    public void scrollToPage(int position) {
        getLayoutManager().smoothScrollToPosition(this, null, position);
    }


    /**
     * Calls the action with the index of the new page
     *
     * @param pageAction your action that depends on the current (new) page index
     */
    public void addOnPageChangedListener(@NonNull PageAction pageAction) {
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    performOnActivePosition(pageAction);
                }
            }
        });
    }

    /**
     * Pass this method any action lambda that depends on the current index.
     * This is a convenience functionality, akin to Optional.
     * It abstracts away {@code null} and {@link #NO_POSITION} checks.
     * Internally, this method depends on methods that may return a null for the current view.
     * Also depends on {@link #getChildAdapterPosition(View)}. It may return {@link #NO_POSITION}.
     *
     * @param pageAction your action that depends on the current page index
     * @see #getChildAdapterPosition(View), PagerSnapHelper#findSnapView
     */
    public void performOnActivePosition(@NonNull PageAction pageAction) {
        @Nullable View view = pagerSnapHelper.findSnapView(getLayoutManager());
        @Nullable Adapter<?> adapter = getAdapter();
        if (view != null) {
            int activePosition = getChildAdapterPosition(view);
            if (activePosition != NO_POSITION) {
                pageAction.performOn(activePosition);
            }
        } else if (adapter != null && adapter.getItemCount() > 0) { // fallback
            pageAction.performOn(0);
        }
    }

    /**
     * A delegate for {@link LinearLayoutManager#getOrientation()}.
     * Fetches the orientation from the internal layout manager.
     * Depends on {@link #getLayoutManager()}
     *
     * @return the current orientation. By default {@link LinearLayoutManager#HORIZONTAL}
     */
    public int getOrientation() {
        return getLayoutManager().getOrientation();
    }

    @NonNull
    @Override
    public LinearLayoutManager getLayoutManager() {
        return linearLayoutManager;
    }

    public void setLayoutManager(@NonNull LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
        super.setLayoutManager(linearLayoutManager);
    }

    public interface PageAction {

        void performOn(int activePosition);
    }

    private static class FixedWidthItemDecoration extends ItemDecoration {

        @Override
        public void getItemOffsets(@NonNull Rect outRect,
            @NonNull View view,
            @NonNull RecyclerView parent,
            @NonNull State state) {

            @Px int recyclerViewWidth = parent.getMeasuredWidth();
            @Px int itemWidth = view.getLayoutParams().width;
            @Px int sideSpace = (recyclerViewWidth - itemWidth) / 2;

            if (parent.getChildAdapterPosition(view) == state.getItemCount() - 1) {
                outRect.right = sideSpace;
            } else if (parent.getChildAdapterPosition(view) == 0) {
                outRect.left = sideSpace;
            } else {
                super.getItemOffsets(outRect, view, parent, state);
            }
        }
    }
}
