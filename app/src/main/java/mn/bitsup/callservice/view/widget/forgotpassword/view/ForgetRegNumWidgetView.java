package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;

public class ForgetRegNumWidgetView extends Fragment {
    private static String TAG = ForgetRegNumWidgetView.class.getName();

    private CustomButton buttonContinue;
    private InputField inputLetters;
    private InputField inputNumbers;
    private String[] letters;
    private int firstPickerPostion = 0;
    private int secondPickerPostion = 0;
    private Context context;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private ContinueButtonTextWatcher continueButtonTextWatcher = new ContinueButtonTextWatcher();
    private ForgotPasswordContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();
        Activity activity = (Activity) getContext();
        assert activity != null;
        try {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            Log.e("activity.getWindow()", "exception: " + e);
        }
        contract = new ForgetWidget(Objects.requireNonNull(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_register, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputLetters = inflatedView.findViewById(R.id.inputLetters);
        inputNumbers = inflatedView.findViewById(R.id.inputNumbers);
    }

    private void initView() {
        letters = Objects.requireNonNull(getContext()).getResources().getStringArray(R.array.letters);

        inputLetters.setIconViewVisible(false);
        inputLetters.setMaxLength(2);
        inputLetters.setText("АА");
        inputLetters.removeCancelButton();
        inputLetters.setIconViewVisible(false);
        inputLetters.setClickableOnly(v -> showRegisterPicker());

        inputNumbers.hideIcon();
        inputNumbers.setNumericKeyboard();
        inputNumbers.setMaxLength(8);
        inputNumbers.setCancelButton();
        inputNumbers.setIconViewVisible(false);
        inputNumbers.addTextChangedListener(continueButtonTextWatcher);

        buttonContinue.setLoadingText("");
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        buttonContinue.setOnClickListener(v -> checkValidation());
    }


    private void showRegisterPicker() {
        final AlertDialog.Builder d = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.view_number_picker, null);
        d.setTitle(getContext().getResources().getString(R.string.onboard_register_title));
        d.setMessage("");
        d.setView(dialogView);

        NumberPicker numberPickerFirst = dialogView.findViewById(R.id.number_picker_first);
        NumberPicker numberPickerSecond = dialogView.findViewById(R.id.number_picker_second);

        numberPickerFirst.setMinValue(0);
        numberPickerFirst.setMaxValue(letters.length - 1);
        numberPickerFirst.setDisplayedValues(letters);
        numberPickerFirst.setValue(firstPickerPostion);

        numberPickerSecond.setMinValue(0);
        numberPickerSecond.setMaxValue(letters.length - 1);
        numberPickerSecond.setDisplayedValues(letters);
        numberPickerSecond.setValue(secondPickerPostion);

        d.setPositiveButton(R.string.shared_button_choice,
                (dialogInterface, i) -> setLetters(numberPickerFirst.getValue(), numberPickerSecond.getValue())
        );
        d.setNegativeButton(R.string.shared_button_back, (dialogInterface, i) -> {
        });

        AlertDialog alertDialog = d.create();
        alertDialog.show();
    }


    private void setLetters(int first, int second) {
        firstPickerPostion = first;
        secondPickerPostion = second;
        inputLetters.setText(letters[first] + letters[second]);
    }

    private void checkValidation() {
        if (Integer.parseInt(inputNumbers.getValue().substring(4, 6)) >= 32) {
            new AnimationDialog(context, context.getResources().getString(R.string.shared_alert_generic_error_title), context.getResources().getString(
                    R.string.onboard_register_error), context.getResources().getString(R.string.shared_button_back), "anim_rejection_comment.json").show();
        } else {
            int age = calculateAge();
            if (age >= 18 && age != 1111) {
                checkRegister();
            } else if (age == 1111) {
                new AnimationDialog(context, context.getResources().getString(R.string.shared_alert_generic_error_title), context.getResources().getString(
                        R.string.onboard_register_error), context.getResources().getString(R.string.shared_button_back), "anim_rejection_comment.json").show();
            } else {
                new AnimationDialog(context, "", context.getResources().getString(R.string.onboard_valid_age), "Буцах", "anim_rejection_comment.json").show();
            }
        }
    }

    private Boolean validRegisterMonth() {
        int month = Integer.parseInt(inputNumbers.getValue().substring(2, 4));
        Log.e("validRegisterMonth", "month: " + month);
        return (month >= 1 && month <= 12 || month >= 21 && month <= 32);
    }

    private Boolean validRegisterDay() {
        return Integer.parseInt(inputNumbers.getValue().substring(4, 6)) < 32;
    }

    private void checkRegister() {
        String registerNumber = letters[firstPickerPostion] + letters[secondPickerPostion] + inputNumbers.getValue();
        KeyboardUtils.hideKeyboard(getContext(), inputNumbers);
        EventHelper.publishBack(getContext(), new ForgetUserNameWidgetView(), "registerNum", registerNumber);

    }

    private boolean isValid() {
        if (inputNumbers.getValue().length() == 8) {
            if (!validRegisterMonth()) {
                inputNumbers.setError("Регистрийн дугаарын сар буруу байна");
                return false;
            } else if (!validRegisterDay()) {
                inputNumbers.setError("Регистрийн дугаарын өдөр буруу байна");
                return false;
            }
            inputNumbers.setError(null);
            return true;
        } else {
            return false;
        }
    }

    private class ContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());

            }
        }
    }

    private int calculateAge() {
        Date date = null;
        int years = 0;
        int months = 0;

        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        String strMonth = inputNumbers.getValue().substring(2, 4);
        String strYear = inputNumbers.getValue().substring(0, 2);

        if ((Integer.parseInt(strMonth) > 0 && Integer.parseInt(strMonth) <= 12) || (Integer.parseInt(strMonth) >= 21 && Integer.parseInt(strMonth) <= 32)) {
            if (Integer.parseInt(strMonth) > 20) {
                strMonth = (Integer.parseInt(strMonth) - 20) + "";
                if (Integer.parseInt(("20" + strYear)) > now.get(Calendar.YEAR)) {
                    return 1111;
                }
                strYear = 20 + strYear;
            } else {
                strYear = "19" + strYear;
            }
        } else {
            return 1111;
        }

        String strDateRegister = strYear + "-" + strMonth + "-" + inputNumbers.getValue().substring(4, 6);
        Log.e("str", "date: " + strDateRegister);

        try {
            date = df.parse(strDateRegister);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(date.getTime());

        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        months = currMonth - birthMonth;
        if (months < 0) {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            years--;
        } else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE)) {
            now.add(Calendar.MONTH, -1);
        } else {
            if (months == 12) {
                years++;
            }
        }
        return years;
    }

}
