package mn.bitsup.callservice.view.widget.login.core.contract;


import java.util.HashMap;
import java.util.Map;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.listener.PasswordAuthListener;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.offline.listener.TokenListener;

public interface LoginContract {

    void authenticate(char[] var1, char[] var2, Map<String, String> var3, PasswordAuthListener var4, String... var5);

    void authenticate(char[] var1, char[] var2, Map<String, String> var3, Map<String, String> var4, PasswordAuthListener var5, String... var6);

    void checkToken(TokenListener var1);

    void checkBlock(BlockListener listener, HashMap<String, String> param);

    void checkBlockIncrement(BlockIncrementListener listener, String blockType, String blockChannel, String userName);

    void setBlock(StatusListener statusListener, int count, String blockType, String userName);
}
