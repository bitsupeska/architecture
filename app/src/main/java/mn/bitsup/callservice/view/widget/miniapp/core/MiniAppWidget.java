package mn.bitsup.callservice.view.widget.miniapp.core;
import android.content.Context;
import androidx.annotation.NonNull;
import java.util.HashMap;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.miniapp.MiniAppClient;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.view.widget.miniapp.core.contract.MiniAppContract;

public class MiniAppWidget implements MiniAppContract {

    private Context context;
    private MiniAppClient miniAppClient;

    public MiniAppWidget(@NonNull Context context) {
        this.context = context;
        this.miniAppClient =  ClientsManager.getMiniAppClient();
    }

    @Override
    public User getUser() {
        return MainApplication.getUser();
    }

    @Override
    public void getAllAppList(MiniAppListener listener, String param) {
        miniAppClient.getAllAppList(listener, param);
    }

    @Override
    public void getMyAppList(MiniAppListener listener, String param) {
        miniAppClient.getMyAppList(listener, param);
    }

    @Override
    public void installApp(StatusListener statusListener, HashMap<String, String> param) {
        miniAppClient.installApp(statusListener, param);
    }
}
