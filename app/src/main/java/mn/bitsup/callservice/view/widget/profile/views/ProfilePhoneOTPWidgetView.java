package mn.bitsup.callservice.view.widget.profile.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.OTPView;
import mn.bitsup.callservice.view.custom.OTPViewLisener;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfilePhoneOTPWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private Context context;
    private ProfileContract contract;
    private TextView textTitle;
    private OTPView otpView;

    private int checkCount = 3;
    private int blockCount = 2;

    private ProgressDialog progressDialog;
    private StatusListener verifiedListener;

    public ProfilePhoneOTPWidgetView() {

    }

    public ProfilePhoneOTPWidgetView(StatusListener verifiedListener) {
        this.verifiedListener = verifiedListener;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_phone_otp, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        otpView = inflatedView.findViewById(R.id.otpView);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initViews() {
        otpView.setLinkOnClickListener(v -> sendRequest());
        ProfilePhoneOTPWidgetView.OTPFilledListener otpListener = new ProfilePhoneOTPWidgetView.OTPFilledListener();
        otpView.setFilledListener(otpListener);
        textTitle.setText(ProfilePhoneChangeWidgetView.phoneNumber + " дугаарт илгээгдсэн 4 оронтой кодыг оруулна уу");
    }

    private void publish() {
        otpView.hideKeyboard();
        otpView.stopTimer();
        EventHelper.publishBack(context, new ProfilePhoneYearWidgetView(verifiedListener));
    }

    private void sendRequest() {
        if (blockCount != 0) {
            checkCount = 3;
            otpView.stopTimer();
            otpView.hideErrorLabel();

            HashMap<String, String> additions = new HashMap<>();
            additions.put("handPhone", ProfilePhoneChangeWidgetView.phoneNumber);
            progressDialog.show();

            contract.requestOTP(new OTPListener() {
                @Override
                public void onSuccess(StatusResponse otp) {
                    progressDialog.dismiss();
                    if (otp.getStatus().equals("SUCCESS")) {
                        otpView.startTimer();
                        blockCount--;
                    }
                }

                @Override
                public void onError(Response var1) {
                    progressDialog.dismiss();
                    CustomAlertDialog.showServerAlertDialog(context);
                }
            }, additions);
        } else {
            otpView.hideMessage();
            new AnimationDialog(context, "", context.getResources().getString(R.string.otp_error_phone), "Буцах", "anim_rejection_clock.json").show();
        }
    }


    private void checkOTP(String code) {
        progressDialog.show();
        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", ProfilePhoneChangeWidgetView.phoneNumber);
        additions.put("otp", code);

        contract.checkOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();
                checkCount--;
                if (otp.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    progressDialog.dismiss();
                    if (checkCount == 0) {
                        otpView.stopTimer();
                        otpView.setLabel();
                        otpView.showErrorLabel("3 удаа буруу оруулсан тул та дахин код илгээнэ үү");
                        checkBlockIncrement();
                    } else {
                        otpView.showErrorLabel("Нууц код буруу байна. Танд " + checkCount + " удаагийн оролдлого үлдлээ");
                    }
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
                otpView.emptyLabel();
                otpView.emptyLabel();
            }
        }, additions);
    }

    public class OTPFilledListener implements OTPViewLisener {
        @Override
        public void onSuccess(String code) {
            if (checkCount != 0) {
                checkOTP(code);
            }
        }
    }

    private void checkBlockIncrement() {
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                Log.e("block", "block hillee");
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(context);
            }
        }, "phone");
    }

}
