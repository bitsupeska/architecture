package mn.bitsup.callservice.view.widget.miniapp.core.contract;

import java.util.HashMap;

import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;
import mn.bitsup.callservice.client.profile.dto.User;

public interface MiniAppContract {

    User getUser();

    void getAllAppList(MiniAppListener var1, String param);

    void getMyAppList(MiniAppListener var1, String param);

    void installApp(StatusListener var1, HashMap<String, String> param);
}
