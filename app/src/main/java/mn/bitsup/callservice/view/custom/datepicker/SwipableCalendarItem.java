package mn.bitsup.callservice.view.custom.datepicker;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.ColorRes;
import androidx.annotation.StyleRes;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V on 03/04/2018.
 * Calendar view representing each month
 */
public class SwipableCalendarItem extends LinearLayout implements CalendarItemAdapter.ItemClickListener{

    private Context context;
    private ViewGroup daysHeaderContainer;
    private RecyclerView calendarGrid;
    private TextView sunday;
    private TextView monday;
    private TextView tuesday;
    private TextView wednesday;
    private TextView thursday;
    private TextView friday;
    private TextView saturday;
    private CalendarItemAdapter calendarAdapter;
    private CellItemDecorator cellItemDecorator;
    private Calendar calendar;
    private List<DatePickerItem> datePickerItems;
    private CustomCalendarView.Styler styler;
    private ItemClickListener clickListener;
    private static final int NUM_OF_COLUMNS = 7;
    private static final int MAX_CALENDAR_COLUMN = 42;
    private Date minDate;

    public SwipableCalendarItem(Context context, Calendar calendar, CustomCalendarView.Styler styler, List<DatePickerItem> datePickerItems, Date minDate) {
        this(context, null);
        this.context = context;
        this.calendar = calendar;
        this.styler = styler;
        this.minDate = minDate;
        this.datePickerItems = datePickerItems;
        this.initializeViews();
        this.setupCalendarGrid();
    }

    SwipableCalendarItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initializeViews() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.swipable_calendar_item, this);
        calendarGrid = view.findViewById(R.id.calendarGrid);
        daysHeaderContainer = view.findViewById(R.id.daysLabelContainer);
        sunday = view.findViewById(R.id.sun);
        monday = view.findViewById(R.id.mon);
        tuesday = view.findViewById(R.id.tue);
        wednesday = view.findViewById(R.id.wed);
        thursday = view.findViewById(R.id.thu);
        friday = view.findViewById(R.id.fri);
        saturday = view.findViewById(R.id.sat);
    }

    public void setupCalendarGrid(){
        List<Date> dayValuesInCalendar = initCalendarData();
        List<DatePickerItem> pickerItems = updateDatePickerItemsList(dayValuesInCalendar);
        calendarGrid.setLayoutManager(new GridLayoutManager(context, NUM_OF_COLUMNS));
        calendarAdapter = new CalendarItemAdapter(context, dayValuesInCalendar, styler, minDate);
        calendarAdapter.setItemClickListener(this);
        calendarGrid.setAdapter(calendarAdapter);
        if (cellItemDecorator != null){
            calendarGrid.removeItemDecoration(cellItemDecorator);
        }
        cellItemDecorator = new CellItemDecorator(context, styler, pickerItems);
        calendarGrid.addItemDecoration(cellItemDecorator);
    }

    private List<DatePickerItem> updateDatePickerItemsList(List<Date> dayValuesInCalendar) {
        List<DatePickerItem> datePickerItemsToReturn = new ArrayList<>(getCloneVersion(datePickerItems));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        for(DatePickerItem datePickerItem : datePickerItemsToReturn){
            try {
                Date date = simpleDateFormat.parse(datePickerItem.getDate());
                datePickerItem.setDateIndex(getDatePositionInAdapter(dayValuesInCalendar, date));
            } catch (ParseException e) {
            }
        }
        return datePickerItemsToReturn;
    }

    private List<DatePickerItem> getCloneVersion(List<DatePickerItem> datePickerItems) {
        List<DatePickerItem> datePickerItemToReturn = new ArrayList<>();
        for(DatePickerItem datePickerItem : datePickerItems){
            datePickerItemToReturn.add(datePickerItem.getClone());
        }
        return datePickerItemToReturn;
    }

    private List<Date> initCalendarData(){
        List<Date> dayValuesInCalendar = new ArrayList<>();
        Calendar calendarCopy = (Calendar)calendar.clone();
        calendarCopy.setTime(calendar.getTime());
        calendarCopy.set(Calendar.DAY_OF_MONTH, 1);
        int firstDayOfTheMonth = calendarCopy.get(Calendar.DAY_OF_WEEK) - 1;
        calendarCopy.add(Calendar.DAY_OF_MONTH, -firstDayOfTheMonth);
        while(dayValuesInCalendar.size() < MAX_CALENDAR_COLUMN){
            dayValuesInCalendar.add(calendarCopy.getTime());
            calendarCopy.add(Calendar.DAY_OF_MONTH, 1);
        }
        return dayValuesInCalendar;
    }

    @Override
    public void onItemClick(View view, int position) {
        if(clickListener != null){
            Date selectedDate = initCalendarData().get(position);
            clickListener.onItemClick(selectedDate);
        }
    }

    private int getDatePositionInAdapter(List<Date> dayValuesInCalendar, Date date){
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        for (int i=0; i < dayValuesInCalendar.size(); i++){
            Calendar checkCalendar = Calendar.getInstance();
            checkCalendar.setTime(dayValuesInCalendar.get(i));
            if(DateUtils.isSameDay(dateCalendar, checkCalendar)) {
                return i;
            }
        }
        return -1;
    }

    public CalendarItemAdapter getCalendarAdapter() {
        return calendarAdapter;
    }

    public void applySwipableViewDaysStyle(@StyleRes int daysHeaderTextStyle){
        if (Build.VERSION.SDK_INT < 23) {
            sunday.setTextAppearance(context, daysHeaderTextStyle);
            monday.setTextAppearance(context, daysHeaderTextStyle);
            tuesday.setTextAppearance(context, daysHeaderTextStyle);
            wednesday.setTextAppearance(context, daysHeaderTextStyle);
            thursday.setTextAppearance(context, daysHeaderTextStyle);
            friday.setTextAppearance(context, daysHeaderTextStyle);
            saturday.setTextAppearance(context, daysHeaderTextStyle);
        } else {
            sunday.setTextAppearance(daysHeaderTextStyle);
            monday.setTextAppearance(daysHeaderTextStyle);
            tuesday.setTextAppearance(daysHeaderTextStyle);
            wednesday.setTextAppearance(daysHeaderTextStyle);
            thursday.setTextAppearance(daysHeaderTextStyle);
            friday.setTextAppearance(daysHeaderTextStyle);
            saturday.setTextAppearance(daysHeaderTextStyle);
        }
    }

    public void applySwipableViewDaysHeaderBgColor(@ColorRes int daysHeaderBgColor) {
        if (Build.VERSION.SDK_INT < 23) {
            daysHeaderContainer.setBackgroundColor(ContextCompat.getColor(context, daysHeaderBgColor));
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(Date selectedDate);
    }
}
