package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import java.util.HashMap;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.KhurReturnImageListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.util.PhoneUtils;
import mn.bitsup.callservice.util.TextViewColor;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.dialog.KhurInfoDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;

public class ForgetFingerWidgetView extends Fragment {
    private int count = 3;
    private TextView textTitle;
    private TextView textLink;
    private CustomButton buttonContinue;
    private LottieAnimationView animationView;
    private Context context;
    private DataListener dataListener = new DataListener();
    private ForgotPasswordContract contract;
    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ForgetWidget(context);
        this.user = EventHelper.getEventPayload("getUser", User.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_finger_guide, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        textLink = inflatedView.findViewById(R.id.textLink);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        animationView = inflatedView.findViewById(R.id.success_animation);
    }

    private void initView() {
        textTitle.setText("Хурууны хээгээ уншуулна уу");
        textLink.setText(TextViewColor.getUnderlineText(getContext(), context.getResources().getString(R.string.onboard_finger_guide_link), 13, 22));
        PhoneUtils.checkBioPermissions(context);
        setAnimation();
    }

    private void setAnimation() {
        animationView.setAnimation("anim_khur.json");
        animationView.playAnimation();
    }

    private void initEvent() {
        buttonContinue.setOnClickListener(v -> startKhurActivity());
        textLink.setOnClickListener(v -> showInformation());
    }

    private void startKhurActivity() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        if (count > 0) {
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
        } else {
            contract.setBlockKhur(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    Log.e("setBlockKhur", "success: " + statusResponse.getStatus());
                    AnimationDialog dialog = new AnimationDialog(context, "Уучлаарай,",
                            "Хурууны хээ баталгаажуулах оролдлого 3 удаа амжилтгүй боллоо. Та 00:00 цагаас хойш дахин оролдоно уу.",
                            context.getResources().getString(R.string.shared_button_back), "anim_rejection_clock.json",
                            () -> ((Activity) context).finish());
                    dialog.show();
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }

                @Override
                public void onError(Response response) {
                    CustomAlertDialog.showServerAlertDialog(getContext());
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }, this.user.getUid());
        }
    }

    private void showInformation() {
        KhurInfoDialog dialog = new KhurInfoDialog(getContext());
        dialog.show();
    }

    private void getKhurData(String image) {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        String regNum = this.user.getRegNum();
        HashMap<String, String> params = new HashMap<>();
        params.put("regNum", regNum);
        params.put("image", image);

        contract.getKhurData(new KhurDataListener() {
            @Override
            public void onSuccess(KhurItems khurItems) {
                if (khurItems.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    count--;
                    AnimationDialog dialog = new AnimationDialog(context, "", "Хурууны хээ баталгаажуулалт амжилтгүй. "
                            + "Танд " + count + " удаагийн боломж үлдлээ.", context.getResources().getString(
                            R.string.shared_button_retry), "anim_rejection_finger.json");
                    dialog.show();
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                }
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showAlertDialog(context, R.string.shared_alert_generic_error_title, R.string.shared_alert_generic_error_server);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, params);
    }


    private void publish() {
        EventHelper.publishBack(getContext(), new ForgetResetPassWidgetView(), "getUser", this.user);
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
    }

    public class DataListener implements KhurReturnImageListener {
        @Override
        public void returnImage(String var1) {
            getKhurData(var1);
        }

        @Override
        public void onErrorHandling() {
            CustomAlertDialog.showAlertDialog(context, "Уучлаарай", "Та дахин оролдон уу.");
        }
    }

    @Override
    public void onResume() {
        KeyboardUtils.hideKeyboard(getActivity());
        super.onResume();
    }
}
