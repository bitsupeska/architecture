package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.ProfilePayload;

public class ProfilePhoneChangeWidgetView extends Fragment implements ProfileView<ProfileContract>, TextWatcher {

    private Context context;
    private ProfileContract contract;
    private CustomButton buttonContinue;
    private InputField inputPhoneNumber;
    private ProfilePayload payload;
    public static String phoneNumber = "";
    private StatusListener verifiedListener;

    public ProfilePhoneChangeWidgetView() {

    }

    public ProfilePhoneChangeWidgetView(StatusListener verifiedListener) {
        this.verifiedListener = verifiedListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        this.payload = EventHelper.getEventPayload("getPhoneData", ProfilePayload.class);
        Activity activity = (Activity) context;
        try {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_phone_number_edit, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputPhoneNumber = inflatedView.findViewById((R.id.inputPhoneNumber));
    }

    private void initViews() {
        buttonContinue.setOnClickListener(v -> checkOldNumber());
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
        inputPhoneNumber.setNumericKeyboard();
        inputPhoneNumber.addTextChangedListener(this);

        int phone_max_length = 8;
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(phone_max_length);
        inputPhoneNumber.setFilters(filters);
        inputPhoneNumber.setCancelButton();
    }

    private boolean isValidNumber(CharSequence target) {
        return !TextUtils.isEmpty(target) && target.length() == 8;
    }

    public void publish() {
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
        phoneNumber = inputPhoneNumber.getValue();
        String publishType = "Profile";
        if (payload != null && payload.getPublishType() != null)
            publishType = payload.getPublishType();
        ProfilePayload payload = new ProfilePayload();
        payload.setPhoneNumber(phoneNumber);
        payload.setPublishType(publishType);
        KeyboardUtils.hideKeyboard(context, inputPhoneNumber);
        EventHelper.publishBack(context, new ProfilePhoneOTPWidgetView(verifiedListener), "getPhoneData", payload);
    }

    private void checkOldNumber() {
        if (contract.getUser().getPhoneNo().equals(inputPhoneNumber.getValue())) {
            NudgeControllerUtil.showErrorNotification(context.getResources().getString(R.string.shared_alert_generic_alert_error_title), "Таны бүртгүүлсэн дугаар байна. Та өөр дугаар оруулна уу.", context);
        } else {
            sendRequest();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (inputPhoneNumber.equals(contract.getUser().getPhoneNo())) {
            inputPhoneNumber.setError("2 утасны дугаар адилхан байж болохгүй");
            buttonContinue.setState(ButtonState.DISABLED, getActivity());
        } else if (isValidNumber(editable)) {
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
            inputPhoneNumber.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    inputPhoneNumber.hideSoftInput();
                    sendRequest();
                    return true;
                }
                return false;
            });
        } else {
            buttonContinue.setState(ButtonState.DISABLED, getActivity());
        }
    }

    private void sendRequest() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", inputPhoneNumber.getValue());

        contract.requestOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                if (otp.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                    CustomAlertDialog.showAlertDialog(context, "", otp.getMessage());
                }
            }

            @Override
            public void onError(Response errorResponse) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, additions);
    }
}
