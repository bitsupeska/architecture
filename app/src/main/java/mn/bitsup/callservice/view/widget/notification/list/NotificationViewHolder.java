package mn.bitsup.callservice.view.widget.notification.list;

import android.annotation.SuppressLint;
import android.graphics.PorterDuff;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.notification.dto.Notification;

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    private final TextView dateTextView;
    private final ImageView imageView;
    private final TextView notificationTitleTextView;
    private final TextView notificationPreviewTextView;
    private Notification item;


    NotificationViewHolder(View itemView, final NotificationsListAdapter.OnItemClickListener onItemClickListener) {
        super(itemView);
        notificationTitleTextView = itemView.findViewById(R.id.notificationTitleTextView);
        dateTextView = itemView.findViewById(R.id.dateTextView);
        imageView = itemView.findViewById(R.id.imageView);
        notificationPreviewTextView = itemView.findViewById(R.id.notificationPreviewTextView);
        itemView.setOnClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClicked(item);
            }
        });
        itemView.setOnLongClickListener(v -> {
            if (onItemClickListener != null) {
                onItemClickListener.onItemLongClicked(item);
                return true;
            }
            return false;
        });
    }

    void bind(final Notification notification) {
        item = notification;
        notificationTitleTextView.setText(notification.getTitle());
        notificationPreviewTextView.setText(notification.getSubtitle());
        String createdOn = null;
        try {
            createdOn = convertToNewFormat(notification.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (createdOn != null) {
            dateTextView.setText((CharSequence) createdOn);
        } else {
            dateTextView.setText(notification.getDate());
        }

        @DrawableRes int iconDrawable;
        @ColorRes int colorResource;


        if (notification.getHas_read().equals("1")) {
            iconDrawable = R.drawable.ic_drafts;
            colorResource = R.color.almost_black;
            imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(iconDrawable));
            notificationTitleTextView.setTextColor(notificationTitleTextView.getContext().getResources().getColor(R.color.almost_black));
            imageView.setColorFilter(imageView.getContext().getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_IN);
            getItemView().setContentDescription("Уншсан");
        } else {
            iconDrawable = R.drawable.ic_email;
            colorResource = R.color.primary;
            imageView.setImageDrawable(imageView.getContext().getResources().getDrawable(iconDrawable));
            notificationTitleTextView.setTextColor(notificationTitleTextView.getContext().getResources().getColor(R.color.primary));
            imageView.setColorFilter(imageView.getContext().getResources().getColor(colorResource), PorterDuff.Mode.SRC_IN);
            getItemView().setContentDescription("Уншаагүй");
        }
    }

    View getItemView() {
        return itemView;
    }

    @SuppressLint("SimpleDateFormat")
    private String convertToNewFormat(String dateStr) throws ParseException {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        Date convertedDate = sourceFormat.parse(dateStr);
        if (DateUtils.isToday(convertedDate.getTime())) {
            return "Өнөөдөр " + timeFormat.format(convertedDate);
        } else if (DateUtils.isToday(convertedDate.getTime() + DateUtils.DAY_IN_MILLIS)) {
            return "Өчигдөр " + timeFormat.format(convertedDate);
        } else {
            return destFormat.format(convertedDate);
        }
    }
}

