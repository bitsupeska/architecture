package mn.bitsup.callservice.view.widget.profile.views;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfileContactUsWidgetView extends Fragment implements ProfileView<ProfileContract> {

    private static final int REQUEST_CALL = 1;
    private Context context;
    private TextView textView, textView1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_contact_us, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textView = inflatedView.findViewById(R.id.textView);
        textView1 = inflatedView.findViewById(R.id.textView1);
    }

    private void initViews() {
        textView.setOnClickListener(v -> openDialog());
        textView1.setOnClickListener(v -> EventHelper.publishActivity(context, new ProfileBranchAddress(), "Салбар, цэгүүдийн байршил"));
    }

    private void openDialog() {
        new AlertDialog.Builder(context)
                .setTitle("Туслах")
                .setMessage("1800-2828")
                .setPositiveButton("Залгах", (dialog, which) -> makePhoneCall())
                .setNegativeButton("Буцах", (dialog, which) -> dialog.dismiss()).show();
    }

    private void makePhoneCall() {
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_CALL);
        } else {
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "18002828")));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CALL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makePhoneCall();
            }
        }
    }
}
