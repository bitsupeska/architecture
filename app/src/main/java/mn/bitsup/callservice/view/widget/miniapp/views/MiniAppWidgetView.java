package mn.bitsup.callservice.view.widget.miniapp.views;

import android.content.Context;
import android.content.MutableContextWrapper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.widget.miniapp.core.MiniAppWidget;
import mn.bitsup.callservice.view.widget.miniapp.core.contract.MiniAppContract;
import mn.bitsup.callservice.view.widget.miniapp.fragment.AllAppsView;
import mn.bitsup.callservice.view.widget.miniapp.fragment.MyAppsView;


public class MiniAppWidgetView extends Fragment implements MiniAppView<MiniAppContract> {

    private MiniAppContract contract;
    private Context context;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new MiniAppWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.mini_app, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        viewPager = inflatedView.findViewById(R.id.viewPager);
        tabLayout = inflatedView.findViewById(R.id.tabs);
    }

    private void initViews() {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        Objects.requireNonNull(getSupportFragmentManager()).beginTransaction().addToBackStack(null);
        getSupportFragmentManager().beginTransaction().commit();
        adapter.addFragment(new MyAppsView(), "Миний Апп");
        adapter.addFragment(new AllAppsView(), "Бүх Апп");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Nullable
    private FragmentManager getSupportFragmentManager() {
        if (getContext() instanceof MutableContextWrapper) {
            AppCompatActivity appCompatActivity = ((AppCompatActivity) ((MutableContextWrapper) getContext()).getBaseContext());
            return appCompatActivity.getSupportFragmentManager();
        }
        if (getContext() instanceof AppCompatActivity) {
            AppCompatActivity appCompatActivity = ((AppCompatActivity) getContext());
            return appCompatActivity.getSupportFragmentManager();
        }
        return null;
    }

    @Override
    public void onResume() {
        KeyboardUtils.hideKeyboard(getActivity());
        super.onResume();
    }


}
