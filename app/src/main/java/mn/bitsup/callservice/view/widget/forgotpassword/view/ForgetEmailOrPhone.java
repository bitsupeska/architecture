package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.CustomButton;

public class ForgetEmailOrPhone extends Fragment {
    private CustomButton buttonEmail;
    private CustomButton buttonPhone;
    private User user;
    private static String TAG = ForgetUserNameWidgetView.class.getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.user = EventHelper.getEventPayload("getUser", User.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.email_or_phone, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonPhone = inflatedView.findViewById(R.id.buttonPhone);
        buttonEmail = inflatedView.findViewById(R.id.buttonEmail);
    }

    private void initView() {


        buttonPhone.setOnClickListener(v -> publish(new ForgetOtpWidgetView()));
        buttonEmail.setOnClickListener(v -> publish(new ForgetEmailOtp()));

    }

    private void publish(Fragment fragment) {
        EventHelper.publishBack(getContext(), fragment, "getUser", user);
    }

    @Override
    public void onResume() {
        KeyboardUtils.hideKeyboard(getActivity());
        super.onResume();
    }
}
