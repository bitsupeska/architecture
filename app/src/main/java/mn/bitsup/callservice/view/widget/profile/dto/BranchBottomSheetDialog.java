package mn.bitsup.callservice.view.widget.profile.dto;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
public class BranchBottomSheetDialog extends BottomSheetDialogFragment {
    private Context context;
    private CustomButton buttonContinue;

    public BranchBottomSheetDialog(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.branch_bottom, container, false);

        ImageView imageButton = view.findViewById(R.id.closeButton);
        imageButton.setOnClickListener(v -> dismiss());
        return view;
    }

    private void dismiss(DialogInterface dialog) {
        buttonContinue.setState(ButtonState.ENABLED,getActivity());
        dialog.dismiss();
    }
}


