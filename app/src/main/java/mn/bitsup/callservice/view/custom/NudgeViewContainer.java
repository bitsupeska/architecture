package mn.bitsup.callservice.view.custom;

import android.animation.Animator;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import java.util.Stack;

import mn.bitsup.callservice.R;

/**
 * Created by Erdenedalai R&D B.V. on 08/11/2018.
 *
 * A container view added on top of the activity's view. It displays
 * nudge notifications.
 * The current implementation does not deal with stacked notifications,
 * they are stacked but only in the code. Some logic needs to be added to
 * animate the views that are not on the top of the stack.
 */
public class NudgeViewContainer extends FrameLayout {
    private static final int STATUS_BAR_HEIGHT_POST_M_DP = 24;
    private static final int STATUS_BAR_HEIGHT_PRE_M_DP = 25;
    private static final int ENTER_EXIT_ANIMATION_DURATION_MS = 400;

    /**
     * How much swiping is needed in order to dismiss a nudge.
     * Value goes from 0 to 1 and it is relative to the nudge view height.
     * A value of 0.5 means that a swipe-up of 50% the height of the view
     * will trigger the dismiss.
     */
    private static final float NUDGE_DISMISS_THRESHOLD = 0.5f;

    /**
     * How much "over pull" can be performed by the user.
     * Over pull happens when the nudge view is swiped towards the bottom,
     * when the user releases the finger the nudge bounces back to its original
     * position.
     * Increase this factor for more over pull, decrease for less overpull.
     * Minimum value is zero.
     */
    private static final float NUDGE_OVERPULL_FACTOR = 0.4f;

    private Stack<View> stackedViews = new Stack<>();
    private Handler dismissHandler = new Handler(Looper.getMainLooper());

    public NudgeViewContainer(@NonNull final Context context) {
        this(context, null);
    }

    public NudgeViewContainer(@NonNull final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NudgeViewContainer(@NonNull final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final int hPadding = getResources().getDimensionPixelSize(R.dimen.padding_8dp);
        setPadding(hPadding, 0, hPadding, 0);
    }

    public void push(@ColorRes final int colorResId,
                     @DrawableRes final int iconResId,
                     @NonNull final String title,
                     @NonNull final String subtitle,
                     final int durationMs) {

        final View view = LayoutInflater.from(getContext()).inflate(R.layout.view_nudge, this, false);
        final SolidCircleBackgroundImageView ivIcon = view.findViewById(R.id.ivIcon);
        final TextView tvTitle = view.findViewById(R.id.tvTitle);
        final ConstraintLayout nudgeContainer = view.findViewById(R.id.nudge_container);
        final TextView tvSubtitle = view.findViewById(R.id.tvSubtitle);

        ivIcon.setCircleBackgroundColor(colorResId);
        ivIcon.setImageResource(iconResId);
        tvTitle.setText(title);
        if(subtitle == null){
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(nudgeContainer);
            constraintSet.connect(R.id.tvTitle,ConstraintSet.TOP,ConstraintSet.PARENT_ID,ConstraintSet.TOP,0);
            constraintSet.connect(R.id.tvTitle,ConstraintSet.BOTTOM,ConstraintSet.PARENT_ID,ConstraintSet.BOTTOM,0);
            constraintSet.applyTo(nudgeContainer);
            tvSubtitle.setVisibility(GONE);
        }
        tvSubtitle.setText(subtitle);

        view.setTag(durationMs);
        stackedViews.push(view);
        addView(view);

        showTopOfStackAnimated(true);
        scheduleCurrentNudgeDismiss();
    }

    public int getNudgesCount() {
        return stackedViews.size();
    }

    private void scheduleCurrentNudgeDismiss() {
        final View view = stackedViews.peek();
        final int durationMs = (int) view.getTag();
        dismissHandler.postDelayed(() -> removeTopOfStackAnimated(), durationMs);
    }

    private void cancelCurrentNudgeDismiss() {
        dismissHandler.removeCallbacksAndMessages(null);
        resetTimedCloseAnimationOnTopOfStack();
    }

    private void showTopOfStackAnimated(final boolean resetPosition) {
        final View view = stackedViews.peek();
        final int durationMs = (int) view.getTag();
        if (resetPosition) {
            view.setTranslationY(getNegativeYTranslationForHiddenView(view));
        }
        view.setOnTouchListener(new NudgeViewTouchListener());
        view.animate()
                .translationY(getPositiveYTranslationForVisibleView())
                .setInterpolator(new OvershootInterpolator())
                .setDuration(ENTER_EXIT_ANIMATION_DURATION_MS)
                .start();

        final TimedCloseView timedCloseView = view.findViewById(R.id.timedCloseView);
        timedCloseView.setTimeIndicatorDurationMs(durationMs);
        timedCloseView.setOnClickListener(new DismissNudgeClickListener());
        timedCloseView.startAnimation();
    }

    private void removeTopOfStackAnimated() {
        final View view = stackedViews.pop();
        view.setOnTouchListener(null);
        view.animate()
                .translationY(getNegativeYTranslationForHiddenView(view))
                .setDuration(ENTER_EXIT_ANIMATION_DURATION_MS)
                .setListener(new EndAnimationListener() {
                    @Override
                    public void onAnimationEnd(final Animator animation) {
                        removeView(view);
                    }
                }).start();
    }

    private int getNegativeYTranslationForHiddenView(final View view) {
        int viewHeight = view.getHeight();
        if (viewHeight == 0) {
            viewHeight = getContext().getResources().getDimensionPixelSize(R.dimen.nudge_view_min_height);
        }

        return -(getStatusBarHeight() + viewHeight);
    }

    private int getPositiveYTranslationForVisibleView() {
        return getStatusBarHeight();
    }

    private void resetTimedCloseAnimationOnTopOfStack() {
        final View view = stackedViews.peek();
        final TimedCloseView timedCloseView = view.findViewById(R.id.timedCloseView);
        timedCloseView.resetAnimation();
    }

    private class NudgeViewTouchListener implements OnTouchListener {
        private float initialYPosition;

        @Override
        public boolean onTouch(final View v, final MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    cancelCurrentNudgeDismiss();
                    initialYPosition = event.getRawY();
                    return true;
                }
                case MotionEvent.ACTION_MOVE: {
                    final float deltaY = event.getRawY() - initialYPosition;
                    final float translationY;
                    if (deltaY < 0) {
                        translationY = getPositiveYTranslationForVisibleView() + deltaY;
                    } else {
                        translationY = getPositiveYTranslationForVisibleView() + (deltaY * NUDGE_OVERPULL_FACTOR);
                    }
                    v.setTranslationY(translationY);
                    return true;
                }
                case MotionEvent.ACTION_UP: {
                    final float deltaY = event.getRawY() - initialYPosition;
                    final float dismissAmount = v.getHeight() * NUDGE_DISMISS_THRESHOLD;
                    if (deltaY < 0 && Math.abs(deltaY) >= dismissAmount) {
                        removeTopOfStackAnimated();
                    } else {
                        showTopOfStackAnimated(false);
                        scheduleCurrentNudgeDismiss();
                    }
                    return true;
                }
                default:
                    return false;
            }
        }
    }

    private int getStatusBarHeight() {
        final Resources resources = getResources();
        final int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0)
            return resources.getDimensionPixelSize(resourceId);
        else
            return (int) Math.ceil((Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ?
                    STATUS_BAR_HEIGHT_POST_M_DP : STATUS_BAR_HEIGHT_PRE_M_DP) *
                    resources.getDisplayMetrics().density);
    }

    private abstract class EndAnimationListener implements Animator.AnimatorListener {
        @Override
        public void onAnimationStart(final Animator animation) {
        }

        @Override
        public void onAnimationCancel(final Animator animation) {
        }

        @Override
        public void onAnimationRepeat(final Animator animation) {
        }
    }

    private final class DismissNudgeClickListener implements OnClickListener {
        @Override
        public void onClick(final View view) {
            view.setOnClickListener(null);
            cancelCurrentNudgeDismiss();
            resetTimedCloseAnimationOnTopOfStack();
            removeTopOfStackAnimated();
        }
    }
}
