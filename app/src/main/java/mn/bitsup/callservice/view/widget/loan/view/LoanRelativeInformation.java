package mn.bitsup.callservice.view.widget.loan.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.CitizenInfo;
import mn.bitsup.callservice.client.info.listener.CitizenInfoListener;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.listener.LoanRelListener;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.profile.dto.RelatedContacts;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.dto.TemporaryItems;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;

public class LoanRelativeInformation extends Fragment implements TextWatcher, AdapterView.OnItemSelectedListener {

    private IconSpinner spinnerWho;
    private InputField inputLastName, inputFirstName, inputNumbers, inputLetters, inputPhoneNo;
    private CustomButton buttonContinue;
    private List<TemporaryItems> relItems;
    private String[] letters;
    private int firstPickerPostion = 0;
    private int secondPickerPostion = 0;
    private UpdateContinueButtonTextWatcher updateContinueButtonTextWatcher = new UpdateContinueButtonTextWatcher();
    private LoanContract contract;
    private Context context;
    private String regNum;
    private User user;
    private List<RelatedContacts> relatedContacts = new ArrayList<>();
    private ProgressDialog progressDialog;
    private ForgotPasswordContract forgotPasswordContract;

    public LoanRelativeInformation() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.forgotPasswordContract = new ForgetWidget(context);
        this.relatedContacts = MainApplication.getUser().getRelatedContacts();
        user = MainApplication.getUser();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_relative_information, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputLetters = inflatedView.findViewById(R.id.inputLetters);
        inputNumbers = inflatedView.findViewById(R.id.inputNumbers);
        inputLastName = inflatedView.findViewById(R.id.inputLastName);
        inputFirstName = inflatedView.findViewById(R.id.inputFirstName);
        inputPhoneNo = inflatedView.findViewById(R.id.inputNumber);
        spinnerWho = inflatedView.findViewById(R.id.spinnerWho);
        spinnerWho.setOnItemSelectedListener(this);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initView() {
        if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        progressDialog.show();
        loadRelatedItems();
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        letters = Objects.requireNonNull(getContext()).getResources().getStringArray(R.array.letters);

        inputLetters.setIconViewVisible(false);
        inputLetters.setMaxLength(2);
        inputLetters.setText("АА");
        inputLetters.removeCancelButton();
        inputLetters.setIconViewVisible(false);
        inputLetters.setClickableOnly(v -> showRegisterPicker());

        inputNumbers.hideIcon();
        inputNumbers.setNumericKeyboard();
        inputNumbers.setMaxLength(8);
        inputNumbers.setCancelButton();
        inputNumbers.setIconViewVisible(false);
        inputNumbers.setSingleLine();

        inputFirstName.setMaxLength(50);
        inputFirstName.setSingleLine();
        inputFirstName.setMaxLength(50);
        inputFirstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        inputFirstName.setCancelButton();
        inputLastName.setCancelButton();
        inputLastName.setSingleLine();
        inputLastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        inputPhoneNo.setNumericKeyboard();
        inputPhoneNo.setMaxLength(8);
        inputPhoneNo.setSingleLine();
        inputPhoneNo.setCancelButton();

        inputPhoneNo.addTextChangedListener(updateContinueButtonTextWatcher);
        inputNumbers.addTextChangedListener(updateContinueButtonTextWatcher);
        inputFirstName.addTextChangedListener(updateContinueButtonTextWatcher);
        inputLastName.addTextChangedListener(updateContinueButtonTextWatcher);
        inputLetters.addTextChangedListener(updateContinueButtonTextWatcher);
        inputFirstName.addTextChangedListener(this);
        inputLastName.addTextChangedListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initEvent() {
        spinnerWho.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                boolean isHave = false;
                if (relatedContacts != null)
                    for (RelatedContacts relatedContact : relatedContacts) {
                        if (position != 0) {
                            if (relatedContact.getGuardtype().toUpperCase().equals(relItems.get(position - 1).getValue().toUpperCase())) {
                                isHave = true;
                                fillValues(relatedContact);
                                break;
                            }
                        }
                    }
                if (!isHave) {
                    fillValues(null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        buttonContinue.setOnClickListener(v -> checkCitizenInfo());
    }

    private void publish() {
        if (!user.getRegNum().equals(regNum)) {
            Loan loan = contract.getLoan();
            loan.setRelatedFirstName(inputFirstName.getValue());
            loan.setRelatedRegNum(regNum);
            loan.setRelatedHandPhone(inputPhoneNo.getValue());
            loan.setRelatedLastName(inputLastName.getValue());
            if (spinnerWho.getSelectedPosition() != 0)
                loan.setRelatedWhoIs(relItems.get(spinnerWho.getSelectedPosition() - 1).getValue());

            EventHelper.publishBack(getContext(), new LoanAccountSelectorWidgetView(null, "related"));
        } else {
            CustomAlertDialog.showAlertDialog(getContext(), getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title), "Өөрийн мэдээллийг ойр дотны хүн дээр оруулах боломжгүй.");
        }
    }

    private boolean isValid() {
        return !inputLastName.getValue().equals("")
                && !inputFirstName.getValue().equals("")
                && !inputNumbers.getValue().equals("")
                && inputPhoneNo.getValue().length() == 8
                && !inputPhoneNo.getValue().equals("")
                && inputNumbers.getValue().length() == 8
                && spinnerWho.getSelectedPosition() != 0
                && spinnerWho.getSelectedItem() != null;
    }

    private void showRegisterPicker() {
        final AlertDialog.Builder d = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.view_number_picker, null);
        d.setTitle(getContext().getResources().getString(R.string.onboard_register_title));
        d.setMessage("");
        d.setView(dialogView);

        NumberPicker numberPickerFirst = dialogView.findViewById(R.id.number_picker_first);
        NumberPicker numberPickerSecond = dialogView.findViewById(R.id.number_picker_second);

        numberPickerFirst.setMinValue(0);
        numberPickerFirst.setMaxValue(letters.length - 1);
        numberPickerFirst.setDisplayedValues(letters);
        numberPickerFirst.setValue(firstPickerPostion);

        numberPickerSecond.setMinValue(0);
        numberPickerSecond.setMaxValue(letters.length - 1);
        numberPickerSecond.setDisplayedValues(letters);
        numberPickerSecond.setValue(secondPickerPostion);

        d.setPositiveButton(R.string.shared_button_choice,
                (dialogInterface, i) -> setLetters(numberPickerFirst.getValue(), numberPickerSecond.getValue())
        );
        d.setNegativeButton(R.string.shared_button_back, (dialogInterface, i) -> {
        });

        AlertDialog alertDialog = d.create();
        alertDialog.show();
    }

    private void setLetters(int first, int second) {
        firstPickerPostion = first;
        secondPickerPostion = second;
        inputLetters.setText(letters[first] + letters[second]);
    }

    private void loadRelatedItems() {
        contract.getRelated(new LoanRelListener() {
            @Override
            public void onSuccess(List<TemporaryItems> data) {
                relItems = data;
                try {
                    if (data != null) {
                        List<String> list = new ArrayList<>();
                        list.add("Сонгох");
                        for (int i = 0; i < relItems.size(); i++) {
                            list.add(relItems.get(i).getLocaletext());
                        }
                        if (list.size() != 0) progressDialog.dismiss();
                        spinnerWho.setItems(list);
                    } else {
                        NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), getContext());
                    }
                } catch (Exception e) {
                    Log.e("LoanRelative", "onSuccess: " + e);
                }
            }

            @Override
            public void onError(Response var1) {
                relItems = new ArrayList<>();
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }

    private void fillValues(RelatedContacts relatedContacts) {
        if (relatedContacts != null) {
            regNum = relatedContacts.getRegnum();
            inputNumbers.setText(regNum.substring(2));
            inputLetters.setText(regNum.substring(0, 2).toUpperCase());
            inputPhoneNo.setText(relatedContacts.getHandphone());
            inputLastName.setText(relatedContacts.getLastname());
            inputFirstName.setText(relatedContacts.getFirstname());
        } else {
            inputNumbers.setText("");
            inputLetters.setText("АА");
            inputPhoneNo.setText("");
            inputLastName.setText("");
            inputFirstName.setText("");
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (inputLastName.hasFocus() && !isCyrillic(inputLastName.getValue())) {
            inputLastName.setError("Зөвхөн крил үсэг оруулж болно");
        } else inputLastName.setError("");

        if (inputFirstName.hasFocus() && !isCyrillic(inputFirstName.getValue())) {
            inputFirstName.setError("Зөвхөн крил үсэг оруулж болно");
        } else inputFirstName.setError("");
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
        KeyboardUtils.hideKeyboard(getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    private class UpdateContinueButtonTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
            }
        }
    }

    public boolean isCyrillic(final String name) {
        for (int i = 0; i < name.length(); i++) {
            Pattern pattern;
            Matcher matcher;
            final String PASSWORD_PATTERN = "^(?=.*[-АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯѲӨҮабвгдеёжзийклмнопрстуфхцчшщъыьэюяөѳү]).{1,}$";
            pattern = Pattern.compile(PASSWORD_PATTERN);
            matcher = pattern.matcher(name.charAt(i) + "");
            if (!matcher.matches()) return false;
        }
        return true;
    }

    private void checkCitizenInfo() {
        regNum = inputLetters.getValue() + inputNumbers.getValue();
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        Map<String, String> param = new HashMap<>();
        param.put("regNum", regNum);
        param.put("firstName", inputFirstName.getValue());
        param.put("lastName", inputLastName.getValue());
        contract.getCheckCitizenInfo(new CitizenInfoListener() {
            @Override
            public void onSuccess(CitizenInfo data) {
                if (data.getResultMessage().equals("true")) {
                    publish();
                } else {
                    checkBlockIncrement();
                }
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(context);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, param);
    }

    private void checkBlockIncrement() {
        forgotPasswordContract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                if (blockIncrement != null) {
                    new AnimationDialog(context, context.getResources().getString(R.string.shared_alert_generic_error_title),
                            "Таны оруулсан ойр дотны хүний мэдээлэл буруу байна. Танд " + (blockIncrement.getData().getBlockLimit() - blockIncrement.getData().getFailedCount()) + " удаагийн оролдлого үлдлээ.",
                            context.getResources().getString(
                                    R.string.shared_button_back), "").show();
                }
                Log.e("MBank", "block hillee");
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(getContext());
            }
        }, "4", "8");
    }
}