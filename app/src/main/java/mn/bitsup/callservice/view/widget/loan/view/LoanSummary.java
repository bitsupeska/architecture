package mn.bitsup.callservice.view.widget.loan.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.client.loan.dto.LoanDueScheduleData;
import mn.bitsup.callservice.client.loan.listener.LoanDueScheduleListener;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.loan.view.dto.Charge;

public class LoanSummary extends Fragment {


    private Context context;
    private LoanContract contract;
    private DataCell cellLoanRepaymentSchedule;
    private LoanAccountData accountData;
    private DataCell cellLoanRepaymentAmount;
    private DataCell cellLoanRepaymentDuration;
    private DataCell cellLoanRepaymentInterest;
    private DataCell cellLoanTransactionFee;
    private DataCell cellLoanCalculatedInterest;
    private DataCell cellLoanPenaltyFee;
    private DataCell cellLoanStatus;
    private DataCell cellLoanAccountOwnerName;
    private DataCell cellLoanBankName;
    private DataCell cellBankAccountNumber;
    private ProgressDialog progressDialog;


    public LoanSummary(LoanAccountData accountData) {
        this.accountData = accountData;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_summary, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        cellLoanRepaymentSchedule = inflatedView.findViewById(R.id.cellLoanRepaymentSchedule);
        cellLoanRepaymentAmount = inflatedView.findViewById(R.id.cellLoanRepaymentAmount);
        cellLoanRepaymentDuration = inflatedView.findViewById(R.id.cellLoanRepaymentDuration);
        cellLoanRepaymentInterest = inflatedView.findViewById(R.id.cellLoanRepaymentInterest);
        cellLoanTransactionFee = inflatedView.findViewById(R.id.cellLoanTransactionFee);
        cellLoanCalculatedInterest = inflatedView.findViewById(R.id.cellLoanCalculatedInterest);
        cellLoanPenaltyFee = inflatedView.findViewById(R.id.cellLoanPenaltyFee);
        cellLoanStatus = inflatedView.findViewById(R.id.cellLoanStatus);
        cellLoanAccountOwnerName = inflatedView.findViewById(R.id.cellLoanAccountOwnerName);
        cellLoanBankName = inflatedView.findViewById(R.id.cellLoanBankName);
        cellBankAccountNumber = inflatedView.findViewById(R.id.cellBankAccountNumber);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initViews() {
        if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        cellLoanRepaymentSchedule.setOnClickListener(v -> getAmortSchedule());

        if (accountData.getThisMonthPayment() != null && accountData.getNextMonthPayment() != null) {
            if(Double.parseDouble(accountData.getThisMonthPayment()) != 0){
                cellLoanRepaymentSchedule.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Double.parseDouble(accountData.getThisMonthPayment())));
            }else{
                cellLoanRepaymentSchedule.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Double.parseDouble(accountData.getNextMonthPayment())));
            }
        }

        if (accountData.getLoanAmt() != null) {
            cellLoanRepaymentAmount.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Float.parseFloat(accountData.getLoanAmt())));
        }
        cellLoanRepaymentDuration.setValueText(accountData.getLoanPeriod() + " сар");
        cellLoanRepaymentInterest.setValueText(Double.valueOf(accountData.getIntRate()) / 12 + "%");
        if (accountData.getFeeAmount() != null) {
            cellLoanTransactionFee.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Float.parseFloat(accountData.getFeeAmount())));
            double totalCharge = 0;
            if (accountData.getCharges() != null) {
                for (Charge charge : accountData.getCharges()) {
                    totalCharge += Double.parseDouble(charge.getAmt());
                }
            }

            if (totalCharge == 0) cellLoanTransactionFee.setValueText("₮ " + 0);
            else
                cellLoanTransactionFee.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(totalCharge));

        }

        if (accountData.getIntAmount() != null)
            cellLoanCalculatedInterest.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Double.parseDouble(accountData.getIntAmount())));

        if (accountData.getFineAmount() != null)
            cellLoanPenaltyFee.setValueText("₮ " + CurrencyConverter.getFormattedCurrencyString(Float.parseFloat(accountData.getFineAmount())));

        cellLoanStatus.setValueText(accountData.getLoanStatus());
        cellLoanAccountOwnerName.setValueText(accountData.getOtherBankAccountName());
        cellLoanBankName.setValueText(accountData.getBankName());
        cellBankAccountNumber.setValueText(accountData.getOtherBankAccountNo());
    }

    private void getAmortSchedule() {
        progressDialog.show();
        contract.getLoanFindAmortSchedule(new LoanDueScheduleListener() {
            @Override
            public void onSuccess(List<LoanDueScheduleData> data) {
                progressDialog.dismiss();
                LoanBottomSheetRepaymentPlan bottomSheetRepaymentPlan = LoanBottomSheetRepaymentPlan.newInstance(data, "LoanSummary");
                bottomSheetRepaymentPlan.show(getActivity().getSupportFragmentManager(), "bottom_sheet");
            }

            @Override
            public void onError(Response var2) {
                progressDialog.dismiss();
                CustomAlertDialog.showServerAlertDialog(context);
            }
        }, accountData.getAccountNo());
    }


}
