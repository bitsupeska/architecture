package mn.bitsup.callservice.view.custom.datepicker;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Erdenedalai R&D B.V on 03/04/2018.
 * Provide utility functions on date operations
 */
public class DateUtils {

    private DateUtils() {
        throw new UnsupportedOperationException("Do not initialize DateUtils class");
    }

    /**
     *
     * Method to check if the given deposit issued today
     *
     * @param dateOfDeposit deposit date
     * @return true if deposit date is today, false otherwise
     */
    public static boolean isToday(Date dateOfDeposit) {
        Calendar currentCalendar = Calendar.getInstance();
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(dateOfDeposit);
        return inCurrentWeek(dateOfDeposit) &&
                (targetCalendar.get(Calendar.DAY_OF_WEEK) == currentCalendar.get(Calendar.DAY_OF_WEEK));
    }

    /**
     * Method to check if the deposit took place in the current week
     *
     * @param dateOfDeposit deposit date
     * @return true if date is in this week, false otherwise
     */
    public static boolean inCurrentWeek(Date dateOfDeposit){
        Calendar currentCalendar = Calendar.getInstance();
        int currentWeek = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int currentYear = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(dateOfDeposit);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return (currentWeek == targetWeek) && (currentYear == targetYear);
    }

    public static boolean isSameDay(Calendar target, Calendar current) {
        return (target.get(Calendar.YEAR) == current.get(Calendar.YEAR) &&
                target.get(Calendar.MONTH) == current.get(Calendar.MONTH) &&
                target.get(Calendar.DAY_OF_YEAR) == current.get(Calendar.DAY_OF_YEAR));
    }

    /**
     * Method checks if dates of two different deposits are in the same year
     *
     * @param target target calendar (usually deposit calendar)
     * @param current current date calendar
     * @return true if deposit date is in this year, false otherwise
     */
    public static boolean inSameYear(Calendar target, Calendar current) {
        return target.get(Calendar.YEAR) == current.get(Calendar.YEAR);
    }

    /**
     * Method checks if dates of two different deposits are in the same month
     *
     * @param target target calendar (usually deposit calendar)
     * @param current current date calendar
     * @return true if deposit date is in this month, false otherwise
     */
    public static boolean inSameMonth(Calendar target, Calendar current) {
        return target.get(Calendar.MONTH) == current.get(Calendar.MONTH);
    }
}
