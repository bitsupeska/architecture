package mn.bitsup.callservice.view.widget.miniapp.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.miniapp.dto.MiniApp;
import mn.bitsup.callservice.client.miniapp.listener.MiniAppListener;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.ErrorView;
import mn.bitsup.callservice.view.custom.ErrorViewType;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.miniapp.core.MiniAppWidget;
import mn.bitsup.callservice.view.widget.miniapp.core.contract.MiniAppContract;
import mn.bitsup.callservice.view.widget.miniapp.dto.AppInstallDialog;
import mn.bitsup.callservice.view.widget.miniapp.selector.AllAppListAdapter;

public class AllAppsView extends Fragment {

    private AllAppListAdapter adapter;
    private ListView gridAppList;
    private FloatingActionButton buttonAdd;
    private MiniAppContract contract;
    private WidgetListView widgetListView;
    private List<MiniApp> appList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private ErrorView.ErrorType errorType = ErrorView.ErrorType.EMPTY;
    private final ErrorViewType ERROR_VIEW_TYPE = ErrorViewType.MINIAPP;
    private List<StorageComponent> storageComponents = new ArrayList<>();
    private HashSet<String> images = new HashSet<>();
    private Context context;

    public AllAppsView() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new MiniAppWidget(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mini_app_all, container, false);
        inflatedView(view);
        initViews();
        return view;
    }

    private void inflatedView(View inflatedView) {
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        gridAppList = inflatedView.findViewById(R.id.gridAppList);
        buttonAdd = inflatedView.findViewById(R.id.buttonAdd);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initViews() {
        buttonAdd.setOnClickListener(v -> openQRScanner());
        getAppList();
    }

    private void openQRScanner() {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
            startActivityForResult(intent, 0);

        } catch (Exception e) {
            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
            startActivity(marketIntent);
        }
    }

    private void getAppList() {
        contract.getAllAppList(new MiniAppListener() {

            @Override
            public void onSuccess(List<MiniApp> apps) {
                if (apps.size() < 1) {
                    gridAppList.setVisibility(View.GONE);
                    widgetListView.showErrorView(errorType, ERROR_VIEW_TYPE);
                } else {
                    gridAppList.setVisibility(View.VISIBLE);
                    widgetListView.setVisibility(View.GONE);
                    appList.clear();
                    appList.addAll(apps);
                    ListAdapter listAdapter = new ListAdapter();
                    gridAppList.setAdapter(listAdapter);
                }
            }

            @Override
            public void onError(Response response) {
                showErrorNotification("Уучлаарай", "Сервертэй холбогдоход алдаа гарлаа");
            }
        }, contract.getUser().getUid());
    }

    public void showErrorNotification(@NonNull final String title, @NonNull final String subtitle) {
        NudgeControllerUtil.showErrorNotification(title, subtitle, getContext());
    }

    public void showSuccessNotification(@NonNull final String title, @NonNull final String subtitle) {
        NudgeControllerUtil.showSuccessNotification(title, subtitle, getContext());
    }


    private class ListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return appList.size();
        }

        @Override
        public Object getItem(int position) {
            return appList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint("ViewHolder")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.mini_app_list_item, parent, false);
            ImageView imageViewAppIcon = convertView.findViewById(R.id.imageViewAppIcon);
            TextView textViewAppName = convertView.findViewById(R.id.textViewAppName);
            if (appList != null) convertView.setOnClickListener(v -> openInstallDialog(appList.get(position).getRating(),
                    appList.get(position).getName(), appList.get(position).getDescription(), appList.get(position).getId()));

            Thread thread = new Thread(() -> {
                try {
                    URL url = new URL(appList.get(position).getIconpath());
                    Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    Drawable image = new BitmapDrawable(context.getResources(), bitmap);
                    imageViewAppIcon.setBackground(image);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();

            //imageViewAppIcon.setBackground(getResources().getDrawable(R.drawable.card));
            textViewAppName.setText(appList.get(position).getName());
            return convertView;
        }
    }

    private void openInstallDialog(String rating, String name, String info, String id){
        AppInstallDialog dialog = new AppInstallDialog(getActivity(), rating, name, info, id);
        dialog.show();

        CustomButton button = dialog.findViewById(R.id.buttonContinue);
        button.setOnClickListener(v -> {
            dialog.dismiss();
            installApp(id, name);
            progressDialog.show();
        });
    }

    private void installApp(String id, String name){
        HashMap<String, String> param = new HashMap<>();
        param.put("uid", contract.getUser().getUid());
        param.put("appId", id);
        contract.installApp(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse var1) {
                showSuccessNotification("Амжилттай ", name + " амжилттай нэмэгдлээ.");
                progressDialog.dismiss();
            }

            @Override
            public void onError(Response var2) {
                showSuccessNotification("Амжилтгүй ", name + " суулгахад алдаа гарлаа. Дахин оролдоно уу.");
                progressDialog.dismiss();
            }
        }, param);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("uno", "onActivityResult: " + "Uno" );
        if (requestCode == 0) {

//            if (resultCode == RESULT_OK) {
//                String contents = data.getStringExtra("SCAN_RESULT");
//            }
//            if(resultCode == RESULT_CANCELED){
//                //handle cancel
//            }
        }
    }




}
