package mn.bitsup.callservice.view.widget.card.selector;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import java.util.List;

/**
 * Created by Erdenedalai R&D B.V. on 06/02/2019.
 */
public class CardDiffCallback extends DiffUtil.Callback {

    @NonNull
    private List<CardAdapterItem> oldItems;
    @NonNull
    private List<CardAdapterItem> newItems;

    CardDiffCallback(@NonNull List<CardAdapterItem> oldItems,
                     @NonNull List<CardAdapterItem> newItems) {
        this.oldItems = oldItems;
        this.newItems = newItems;
    }

    @Override
    public int getOldListSize() {
        return oldItems.size();
    }

    @Override
    public int getNewListSize() {
        return newItems.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        @NonNull CardAdapterItem oldItem = oldItems.get(oldItemPosition);
        @NonNull CardAdapterItem newItem = newItems.get(newItemPosition);
        return oldItem.getCard().getId().equals(newItem.getCard().getId());
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        @NonNull CardAdapterItem oldItem = oldItems.get(oldItemPosition);
        @NonNull CardAdapterItem newItem = newItems.get(newItemPosition);
        return oldItem.getCard().getId().equals(newItem.getCard().getId())
                && oldItem.getMaskedNumberWithDots().equals(newItem.getMaskedNumberWithDots())
                && oldItem.getHolderName().equals(newItem.getHolderName())
                && oldItem.getFormattedExpiryDate().equals(newItem.getFormattedExpiryDate())
                && oldItem.isLocked() == newItem.isLocked()
                && oldItem.isInactive() == newItem.isInactive()
                && oldItem.isCancelled() == newItem.isCancelled()
                && oldItem.isInProgress() == newItem.isInProgress();
    }
}
