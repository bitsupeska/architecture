package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.widget.loan.view.LoanFingerView;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.ProfilePayload;

public class ProfilePhoneYearWidgetView extends Fragment implements ProfileView<ProfileContract>, AdapterView.OnItemSelectedListener {

    private Context context;
    private ProfileContract contract;
    private List<String> years = new ArrayList<>();
    private IconSpinner spinnerYear;
    private CustomButton buttonContinue;
    private ProfilePayload payload;

    private StatusListener verifiedListener;

    public ProfilePhoneYearWidgetView() {

    }

    public ProfilePhoneYearWidgetView(StatusListener verifiedListener) {
        this.verifiedListener = verifiedListener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        this.payload = EventHelper.getEventPayload("getPhoneData", ProfilePayload.class);
        Activity activity = (Activity) context;
        try{
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        catch(NullPointerException e){
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_information_used_year_edit, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        spinnerYear = inflatedView.findViewById(R.id.spinnerYear);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
    }

    private void initViews() {
        years.add("Сонгох");
        years.add("0-1 жил");
        years.add("1-2 жил");
        years.add("2-3 жил");
        years.add("3-c дээш");
        spinnerYear.setItems(years);
        spinnerYear.setOnItemSelectedListener(this);
        buttonContinue.setOnClickListener(v -> setNumber());
    }

    private void setNumber() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> param = new HashMap<>();
        param.put("cifId", contract.getUser().getCif());
        param.put("mobileDuration", String.valueOf(spinnerYear.getSelectedPosition()));
        param.put("phoneNumber", payload.getPhoneNumber());
        param.put("phoneType", "Mobile");

        contract.setNumber(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                if (statusResponse.getStatus().equals("SUCCESS")) {
                    //int duration = (Integer.valueOf(spinnerYear.getSelectedPosition())) * 360;
                    User user = contract.getUser();
                    user.setMobileDuration(spinnerYear.getSelectedPosition() + "");
                    user.setPhoneNo(payload.getPhoneNumber());
                    MainApplication.setUser(user);
                    updateDataFinish("success", context.getResources().getString(R.string.setup_completed_title), context.getResources().getString(R.string.profile_information_edit_phone_number_success));

                } else {
                    updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), statusResponse.getMessage());
                }
            }

            @Override
            public void onError(Response response) {
                updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), context.getResources().getString(R.string.shared_alert_generic_error_server));
            }
        }, param);
    }

    private void updateDataFinish(String result, String resultTitle, String resultMessage) {
        if (payload.getPublishType() != null && payload.getPublishType().equals("Loan")) {
            Log.e(result, "updateDataFinish: " + resultMessage);
            EventHelper.publishStackPop(context, new LoanFingerView());
            EventHelper.publishStackPop(context, new LoanFingerView());
            EventHelper.publishStackPop(context, new LoanFingerView());
            verifiedListener.onSuccess(new StatusResponse());
        } else {
            ProfileInfoWidgetView.cellPhoneNumber.setValueText(payload.getPhoneNumber());
            Activity activity = ((Activity) Objects.requireNonNull(getContext()));
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", result);
            returnIntent.putExtra("resultTitle", resultTitle);
            returnIntent.putExtra("resultMessage", resultMessage);
            activity.setResult(Activity.RESULT_FIRST_USER, returnIntent);
            activity.finish();
            ((Activity) context).finish();
        }
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (spinnerYear.getSelectedPosition() != 0)
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
        else buttonContinue.setState(ButtonState.DISABLED, getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}
