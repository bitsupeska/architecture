package mn.bitsup.callservice.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.AnyRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.view.util.StyledAttributesHelper;
import java.util.List;

/**
 * Created by Erdenedalai R&D B.V. on 29/08/2018.
 *
 * A material themed spinner that shows an icon on its left.
 * It aligns vertically with {@link mn.bitsup.callservice.view.custom.inputfield.InputField}.
 */
public class IconSpinner extends ConstraintLayout {
    private TextView titleTextView;
    private ImageView iconImageView;
    private Spinner spinner;
    private boolean isFirstItemAHint;
    private ArrayAdapter<String> adapter;

    public IconSpinner(final Context context) {
        this(context, null);
    }

    public IconSpinner(final Context context, @Nullable final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconSpinner(final Context context, @Nullable final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater.from(context).inflate(R.layout.icon_spinner, this);
        titleTextView = findViewById(R.id.titleTextView);
        iconImageView = findViewById(R.id.iconImageView);
        spinner = findViewById(R.id.spinner);

        spinner.post(() -> spinner.setDropDownWidth(spinner.getWidth()));

        if (attrs != null) {
            TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.IconSpinner, 0, 0);
            titleTextView.setText(StyledAttributesHelper.getStyleableAttributeString(styledAttributes, R.styleable.IconSpinner_iconSpinnerTitle, ""));
            @AnyRes int iconResource = styledAttributes.getResourceId(R.styleable.IconSpinner_iconSpinnerIconRes, -1);
            if (iconResource == -1) {
                iconImageView.setVisibility(GONE);
            } else {
                iconImageView.setVisibility(VISIBLE);
                iconImageView.setImageResource(iconResource);
            }
        }
    }

    public String getSelectedItem() {
        return (String) spinner.getSelectedItem();
    }

    public void setSelection(int position) {
        spinner.setSelection(position);
    }

    public int getSelectedPosition() {
        return spinner.getSelectedItemPosition();
    }

    public void setItems(@NonNull final List<String> items) {
        adapter = new ArrayAdapterWithHint<>(getContext(), R.layout.icon_spinner_collapsed_item, items);
        adapter.setDropDownViewResource(R.layout.icon_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    /**
     * Sets the first item in the adapter as a non-selectable hint.
     * @param enable true if the first item in the adapter should be considered a non-selectable hint
     *               false otherwise
     */
    public void setFirstItemAsHint(boolean enable) {
        isFirstItemAHint = enable;
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * Sets the resource string for the label shown above the spinner.
     * @param resourceId the string resource id for the label shown above the spinner
     */
    public void setHintLabel(@StringRes int resourceId) {
        titleTextView.setText(resourceId);
    }

    public void setHintLabel(String label) {
        titleTextView.setText(label);
    }


    public void setOnItemSelectedListener(@NonNull final AdapterView.OnItemSelectedListener listener) {
        spinner.setOnItemSelectedListener(listener);
    }

    public void setIconViewVisible(boolean visible) {
        if (visible) {
            iconImageView.setVisibility(VISIBLE);
            int size = getResources().getDimensionPixelSize(R.dimen.margin_19dp);
            int sizeZero = getResources().getDimensionPixelSize(R.dimen.margin_3dp);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) spinner.getLayoutParams();
            params.setMarginStart(size);
            params.setMarginEnd(sizeZero);
            spinner.setLayoutParams(params);
        } else {
            iconImageView.setVisibility(GONE);
            int size = getResources().getDimensionPixelSize(R.dimen.padding_4dp);
            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) spinner.getLayoutParams();
            params.setMarginStart(size);
            params.setMarginEnd(size);
            params.setMargins(size, size, size, 0);
            spinner.setLayoutParams(params);
        }
    }


    public void setIconColor(int color){
        iconImageView.setColorFilter(getContext().getResources().getColor(color));
    }

    /**
     * Custom ArrayAdapter implementation that can consider the first item in the adapter as a hint.
     * If the first item is considered a hint, it is not shown in the dropdown menu
     * and it is greyed out when it is shown in the collapsed spinner view.
     * @param <T>
     */
    class ArrayAdapterWithHint<T> extends ArrayAdapter<T> {
        ArrayAdapterWithHint(@NonNull Context context, int resource, @NonNull List<T> objects) {
            super(context, resource, objects);
        }

        @Override
        public boolean isEnabled(int position) {
            return position != 0 || !isFirstItemAHint;
        }

        @Override
        public @NonNull
        View getView(int position, @Nullable View convertView,
                                     @NonNull ViewGroup parent) {
            View view = super.getView(position, convertView, parent);

            if (position == 0 && isFirstItemAHint) {
                TextView tv = view.findViewById(android.R.id.text1);
                tv.setTextColor(getContext().getResources().getColor(R.color.grey));
            }

            return view;
        }

        @Override
        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
            View view;

            if (position == 0 && isFirstItemAHint) {
                TextView textView = new TextView(getContext());
                textView.setHeight(0);
                textView.setVisibility(GONE);
                view = textView;
            } else {
                view = super.getDropDownView(position, null, parent);
            }

            return view;
        }
    }
}
