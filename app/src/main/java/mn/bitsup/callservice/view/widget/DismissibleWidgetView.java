package mn.bitsup.callservice.view.widget;

import android.content.Context;
import androidx.fragment.app.Fragment;

public abstract class DismissibleWidgetView extends Fragment {

    public DismissibleWidgetView(Context context) {
    }

    protected void dismiss() {
        onDismiss();
    }

    protected void setDismissible(boolean dismissible) {
    }

    protected void onDismiss() {}
}

