package mn.bitsup.callservice.view.widget.loan.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.dialog.AnimationDialogListener;
import mn.bitsup.callservice.view.dialog.LoanSuccessDialog;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;

import static mn.bitsup.callservice.view.widget.loan.view.LoanOfferView.loanOffer;

public class LoanRemainingOffer extends Fragment {
    private CustomButton buttonContinue;
    private TextView textFinalLimit;
    private LoanContract contract;
    private Context context;
    private TextView textTitle;
    private CustomAnimationDialogListener animationDialogListener = new CustomAnimationDialogListener();
    private StorageComponent component;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.component = new StorageComponent();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_remaining_offer, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textFinalLimit = inflatedView.findViewById(R.id.textFinalLimit);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        textTitle = inflatedView.findViewById(R.id.textTitle);
    }

    private void initViews() {
        textTitle.setText("Таны үлдэж буй зээлийн эрхийн дүн");
        if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        try {
            String formattedRemainingLimit = CurrencyConverter.getFormattedCurrencyString((int) loanOffer.getData().getRemainingLimit());
            textFinalLimit.setText("₮ " + formattedRemainingLimit);
        }catch (Exception ex){
            CustomAlertDialog.showAlertDialog(context,context.getResources().getString(R.string.shared_alert_generic_alert_error_title),"Үлдсэн дүнг харуулах үед алдаа гарлаа");
        }
        buttonContinue.setOnClickListener(v -> publish());
    }

    private void publish() {
        showSuccessView();
    }

    private void showSuccessView() {
        component.setItem("loanStatus", "success");
        LoanSuccessDialog dialog = new LoanSuccessDialog(context, animationDialogListener);
        dialog.show();
    }


    public class CustomAnimationDialogListener implements AnimationDialogListener {
        @Override
        public void onDismiss() {
            component.setItem("loanStatus", "success");
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        }
    }
}
