package mn.bitsup.callservice.view.widget.forgotpassword.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.AfterTextChangedListener;
import mn.bitsup.callservice.util.EmojiFilter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.ValidationInputField;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.custom.inputfield.InputFieldPassword;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;

import static mn.bitsup.callservice.util.KeyboardUtils.hideKeyboard;

public class ForgetResetPassWidgetView extends Fragment {
    private static String TAG = ForgetResetPassWidgetView.class.getName();

    private CustomButton buttonContinue;
    private DataCell inputUserName;
    private InputFieldPassword inputFirstPassword;
    private InputFieldPassword inputSecondPassword;
    private User user;
    private FirstTextWatcher firstTextWatcher = new FirstTextWatcher();
    private SecondTextWatcher secondTextWatcher = new SecondTextWatcher();
    private ScrollView scrollView;
    private Boolean validFirst = false;

    private ForgotPasswordContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = (Activity) getContext();
        assert activity != null;
        try{
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        catch(NullPointerException e){
            Log.e("activity.getWindow()", "exception: " + e);
        }
        this.user = EventHelper.getEventPayload("getUser", User.class);
        contract = new ForgetWidget(Objects.requireNonNull(getContext()));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.reset_password, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputUserName = inflatedView.findViewById(R.id.inputUserName);
        inputFirstPassword = inflatedView.findViewById(R.id.inputFirstPassword);
        inputSecondPassword = inflatedView.findViewById(R.id.inputSecondPassword);
        scrollView = inflatedView.findViewById(R.id.scrollView);
    }

    private void initView() {
        this.inputUserName.setValueText(this.user.getUid());

        inputFirstPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                scrollView.smoothScrollTo(0, inputFirstPassword.getTop());
            }
        });

        inputSecondPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                scrollView.smoothScrollTo(0, inputSecondPassword.getTop());
            }
        });

        ProgressDialog progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");

        inputFirstPassword.setIconViewVisible(false);
        inputFirstPassword.addTextChangedListener(firstTextWatcher);
        inputFirstPassword.setSingleLine();
        inputFirstPassword.setPasswordType();
        inputFirstPassword.setMaxLength(16);
        inputFirstPassword.setFilters(EmojiFilter.getFilter());

        inputSecondPassword.setIconViewVisible(false);
        inputSecondPassword.addTextChangedListener(secondTextWatcher);
        inputSecondPassword.setSingleLine();
        inputSecondPassword.setMaxLength(16);
        inputSecondPassword.setPasswordType();
        inputSecondPassword.setFilters(EmojiFilter.getFilter());

        buttonContinue.setLoadingText("");
        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        buttonContinue.setOnClickListener(v -> resetPassword());
    }

    private void resetPassword() {
        buttonContinue.setState(ButtonState.LOADING,getActivity());
        contract.resetPassword(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse var1) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                if (var1.getCode().equals("200")) {
                    Activity activity = ((Activity) Objects.requireNonNull(getContext()));
                    hideKeyboard((Activity) getContext());
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", getContext().getString(R.string.reset_password_success_text));
                    activity.setResult(Activity.RESULT_OK, returnIntent);
                    activity.finish();
                } else if (var1.getCode().equals("305")) {
                    NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title)
                            , var1.getMessage(), getContext());

                } else
                    NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title)
                            , getContext().getString(R.string.shared_alert_generic_error_server), getContext());
            }

            @Override
            public void onError(Response var2) {
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                NudgeControllerUtil.showErrorNotification(getContext().getResources().getString(R.string.shared_alert_generic_alert_error_title)
                        , getContext().getString(R.string.shared_alert_generic_error_server), getContext());
            }
        }, inputFirstPassword.getText().toString(), user.getUid());
    }


    private void isValidPassword(InputFieldPassword inputField) {
        String password = inputField.getValue();
        if (!password.equals("")) {
            if (ValidationInputField.hasDigits(password) && ValidationInputField.hasLowerCase(password) && ValidationInputField.hasUpperCase(password)
                    && ValidationInputField.hasSpecial(password) && ValidationInputField.isCorrectLength(password)) {
                inputField.setError(null);
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
                validFirst = true;
                if (!inputFirstPassword.getValue().equals("") && !inputSecondPassword.getValue().equals("")) {
                    if (!inputFirstPassword.getValue().equals(inputSecondPassword.getValue())) {
                        buttonContinue.setState(ButtonState.DISABLED, getActivity());
                        inputSecondPassword.setError(getContext().getResources()
                            .getString(R.string.onboard_valid_confirm));
                    } else {
                        buttonContinue.setState(ButtonState.ENABLED, getActivity());
                        inputSecondPassword.setError(null);
                    }
                }
            } else {
                if (!inputFirstPassword.getValue().isEmpty()) {
                    validFirst = false;
                    buttonContinue.setState(ButtonState.DISABLED,getActivity());
                    inputField.setError(getContext().getResources().getString(R.string.onboard_valid_password));
                }
            }
            String temp_pass = password.toLowerCase();
            if (temp_pass.contains(inputUserName.getValueText().toLowerCase())) {
                validFirst = false;
                buttonContinue.setState(ButtonState.DISABLED,getActivity());
                inputField.setError(getContext().getResources().getString(R.string.onboard_contains_username));
            }

        }
    }


    private void isValidPasswordSecond(InputFieldPassword inputField) {
        String password = inputField.getValue();
        if (!password.equals("")) {
            if (ValidationInputField.hasDigits(password) && ValidationInputField.hasLowerCase(password) && ValidationInputField.hasUpperCase(password)
                    && ValidationInputField.hasSpecial(password) && ValidationInputField.isCorrectLength(password)) {
                inputField.setError(null);
                buttonContinue.setState(ButtonState.ENABLED,getActivity());
            } else {
                if (!inputSecondPassword.getValue().isEmpty()) {
                    buttonContinue.setState(ButtonState.DISABLED,getActivity());
                    inputField.setError(getContext().getResources().getString(R.string.onboard_valid_password));
                }
            }
        }

        if (validFirst) {
            if (!inputFirstPassword.getValue().equals("") && !inputSecondPassword.getValue().equals("")) {
                if (!inputFirstPassword.getValue().equals(inputSecondPassword.getValue())) {
                    buttonContinue.setState(ButtonState.DISABLED,getActivity());
                    inputSecondPassword.setError(getContext().getResources().getString(R.string.onboard_valid_confirm));
                } else {
                    buttonContinue.setState(ButtonState.ENABLED,getActivity());
                    inputSecondPassword.setError(null);
                }
            }
        } else {
            buttonContinue.setState(ButtonState.DISABLED,getActivity());
        }

        String temp_pass = password.toLowerCase();
        if (temp_pass.contains(inputUserName.getValueText().toLowerCase())) {
            buttonContinue.setState(ButtonState.DISABLED,getActivity());
            inputField.setError(getContext().getResources().getString(R.string.onboard_contains_username));
        }

    }

    private class FirstTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                isValidPassword(inputFirstPassword);
            }
        }
    }

    private class SecondTextWatcher extends AfterTextChangedListener {
        @Override
        public void afterTextChanged(final Editable s) {
            if (!buttonContinue.isLoading()) {
                isValidPasswordSecond(inputSecondPassword);
            }
        }
    }
}
