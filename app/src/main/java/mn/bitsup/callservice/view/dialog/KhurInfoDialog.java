package mn.bitsup.callservice.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import mn.bitsup.callservice.R;

public class KhurInfoDialog extends Dialog {

    private TextView textInfo;
    private ImageView imageClose;

    public KhurInfoDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        setContentView(R.layout.dialog_khur_info);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

        inflatedView();
        initView();
    }

    private void inflatedView(){
        textInfo = findViewById(R.id.textInfo);
        imageClose = findViewById(R.id.imageClose);
        imageClose.setOnClickListener(v->dismiss());
    }

    private void initView() {

    }
}
