package mn.bitsup.callservice.view.widget.notification.list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.common.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.notification.dto.Notification;
import mn.bitsup.callservice.client.notification.listener.NotificationsListener;
import mn.bitsup.callservice.view.controller.NudgeControllerUtil;
import mn.bitsup.callservice.view.custom.ErrorView;
import mn.bitsup.callservice.view.custom.ErrorViewType;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.widget.notification.activity.NotificationDetailActivity;
import mn.bitsup.callservice.view.widget.notification.core.NotificationWidget;
import mn.bitsup.callservice.view.widget.notification.core.contract.NotificationContract;
import mn.bitsup.callservice.view.widget.notification.view.NotificationView;

import static mn.bitsup.callservice.constant.AppConstants.PAGE_PREFERENCE_TITLE;

public class NotificationsWidgetView extends Fragment implements
        NotificationView<NotificationContract> {
    private static final ErrorViewType ERROR_VIEW_TYPE = ErrorViewType.NOTIFICATION;

    private SwipeRefreshLayout swipeRefreshLayout;
    public WidgetListView widgetListView;
    private NotificationsListAdapter adapter;
    private NotificationsListenerImpl notificationsListener = new NotificationsListenerImpl();
    private NotificationContract notificationContract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.notificationContract = new NotificationWidget(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.notifications_layout, container, false);
        inflatedView(inflatedView);
        initList();
        return inflatedView;
    }


    private void inflatedView(View inflatedView) {
        swipeRefreshLayout = inflatedView.findViewById(R.id.swipeRefreshLayout);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
    }

    private void initList() {
        adapter = new NotificationsListAdapter(new OnNotificationItemClickListener());
        widgetListView.initialize(new LinearLayoutManager(getContext()),
                adapter,
                swipeRefreshLayout,
                new ShouldLoadNextPageListenerImpl());

        getNotifications();
    }

    private void getNotifications() {
        notificationContract.getNotifications(widgetListView.getCurrentPage(), 20, notificationsListener);
    }

    private class ShouldLoadNextPageListenerImpl implements WidgetListView.ShouldLoadNextPageListener {
        @Override
        public void shouldLoadNextPage() {
            getNotifications();
        }
    }

    private class NotificationsListenerImpl implements NotificationsListener {
        @Override
        public void onSuccess(List<Notification> list) {
            Log.e("Notifications", "success: " + list.size());
            if (CollectionUtils.isEmpty(list) || list.size() < 20) {
                widgetListView.setEndOfListReached(true);
            }

            if (CollectionUtils.isEmpty(list) && widgetListView.getCurrentPage() == 0) {
                widgetListView.showErrorView(ErrorView.ErrorType.EMPTY, ERROR_VIEW_TYPE);
                return;
            }
            widgetListView.showList();
            if (widgetListView.getCurrentPage() == 0) {
                adapter.updateWith(list);
            } else {
                adapter.appendWith(list);
            }
        }

        @Override
        public void onError(Response response) {
            Log.e("Notifications", "error: " + response);
            widgetListView.showErrorView(response, ERROR_VIEW_TYPE);
        }
    }

    private class OnNotificationItemClickListener implements NotificationsListAdapter.OnItemClickListener {
        @Override
        public void onItemClicked(Notification item) {
            Intent intent = new Intent(getContext(), NotificationDetailActivity.class);
            intent.putExtra(PAGE_PREFERENCE_TITLE, "Дэлгэрэнгүй");
            intent.putExtra("id", item.getId());
            NotificationsWidgetView.this.startActivityForResult(intent, 1);
        }

        @Override
        public void onItemLongClicked(final Notification item) {
            showConfirmDeleteNotificationDialog(item);
        }
    }

    private void showConfirmDeleteNotificationDialog(final Notification notification) {
        Map<String, String> params = new HashMap<>();
        params.put("id", notification.getId());
        new AlertDialog.Builder(getContext())
                .setPositiveButton("Устгах", (dialog, which) -> notificationContract.notificationArchive(params, new StatusListener() {
                    @Override
                    public void onSuccess(StatusResponse var1) {
                        if (var1.getCode().equals("200")) {
                            NudgeControllerUtil.showSuccessNotification("Амжилттай", "Мэдэгдэл устлаа", getContext());
                            getNotifications();
                        } else {
                            NudgeControllerUtil.showErrorNotification("Уучлаарай", var1.getMessage(), getContext());
                        }
                    }
                    @Override
                    public void onError(Response var2) {
                        NudgeControllerUtil.showErrorNotification("Уучлаарай", var2.getErrorMessage(), getContext());
                    }
                }))
                .setNegativeButton(R.string.shared_alert_button_cancel, null)
                .setTitle("Итгэлтэй байна уу")
                .setMessage("Мэдэгдэл устгасан тохиолдолд буцаах боломжгүй")
                .show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = intent.getStringExtra("result");
                NudgeControllerUtil.showSuccessNotification(this.getResources().getString(R.string.setup_completed_title), result, getContext());
                getNotifications();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
                getNotifications();

            }
        }
    }
}
