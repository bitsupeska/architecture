package mn.bitsup.callservice.view.widget.loan.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.loan.dto.CreateInvoiceBankItem;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.client.loan.dto.LoanCreateInvoice;
import mn.bitsup.callservice.client.loan.listener.LoanCreateInvoiceListener;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.widget.loan.adapter.BankListAdapter;
import mn.bitsup.callservice.view.widget.loan.view.dto.BankItemPayload;

public class QpayView extends BaseRepaymentView {

    public QpayView(LoanAccountData loanAccountData) {
        super(loanAccountData);
    }

    @Override
    public void loadBankData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("profileSettings", "");
        param.put("qpay", "1");
        param.put("transfer", "");
        loanContract.getBanks(new BankListener() {
            @Override
            public void onSuccess(List<BankItem> var1) {
                bankItems = var1;
                recyclerItems.setLayoutManager(
                        new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                OverScrollDecoratorHelper.setUpOverScroll(recyclerItems,
                        OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);
                BankListAdapter adapter;
                adapter = new BankListAdapter(getContext(), bankItems,
                        new BankListAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClicked(BankItemPayload item) {
                                if (amount == 0) {
                                    CustomAlertDialog.showAlertDialog(getContext(),
                                            getContext().getResources()
                                                    .getString(R.string.shared_alert_button_warning),
                                            "Та төлөлт хийх дүнгээ оруулна уу");
                                } else {
                                    loanCreateInvoice(item.getBankItem());
                                }
                            }
                        });
                recyclerItems.setAdapter(adapter);
            }

            @Override
            public void onError(Response var1) {

            }
        }, param);
    }

    private void loanCreateInvoice(BankItem bankItem) {
        progressDialog.show();
        Map<String, String> params = new HashMap<>();
        params.put("description", "Зээлийн эргэн төлбөр");
        params.put("amount", String.valueOf(amount));
        loanContract.createLoanInvoice(params, new LoanCreateInvoiceListener() {
            @Override
            public void onSuccess(LoanCreateInvoice var1) {
                progressDialog.hide();

                for (CreateInvoiceBankItem createInvoiceBankItem : var1.getqPay_deeplink()) {
                    if (createInvoiceBankItem.getName().equals(bankItem.getQpayname())) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse(createInvoiceBankItem.getLink())));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://" + bankItem.getAndroidlink())));
                        }
                    }
                }
            }

            @Override
            public void onError(Response response) {
                progressDialog.hide();
                Log.e("LoanCreateInvoice", "onSuccess: " + response);
            }
        });
    }
}
