package mn.bitsup.callservice.view.custom.datepicker;

/**
 * Created by Erdenedalai R&D B.V on 19/06/2018.
 * DTO represent date items in the calender to select specific date with the item style.
 */

public class DatePickerItem {

    private String date;
    private int dateIndex;
    private int dateColor;
    private boolean filled;

    public DatePickerItem(String date, int dateColor, boolean filled) {
        this.date = date;
        this.dateColor = dateColor;
        this.filled = filled;
    }

    public int getDateIndex() {
        return dateIndex;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDateIndex(int dateIndex) {
        this.dateIndex = dateIndex;
    }

    public int getDateColor() {
        return dateColor;
    }

    public void setDateColor(int dateColor) {
        this.dateColor = dateColor;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    protected DatePickerItem getClone() {
        return new DatePickerItem(this.date, this.dateColor, this.filled);
    }
}
