package mn.bitsup.callservice.view.widget.loan.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.dto.BankItem;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.IconSpinner;
import mn.bitsup.callservice.view.custom.inputfield.InputField;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public class LoanAccountAdd extends Fragment implements TextWatcher, AdapterView.OnItemSelectedListener {
    private Context context;
    private ProfileContract profileContract;
    private LoanContract contract;
    private List<BankItem> bankItem;
    private IconSpinner spinnerBankName;
    private InputField inputOwnerName;
    private InputField inputAccountNumber;
    private CustomButton buttonContinue;
    private LinearLayout container;
    private TextView textTitle;
    private User user;
    private Loan loan;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.profileContract = new ProfileWidget(context);
        this.contract = new LoanWidget(context);
        this.user = MainApplication.getUser();
        this.loan = contract.getLoan();
        Activity activity = (Activity) context;
        try{
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        }
        catch(NullPointerException e){
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_connect_account_add, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        inputOwnerName = inflatedView.findViewById(R.id.inputOwnerName);
        container = inflatedView.findViewById(R.id.container);
        inputAccountNumber = inflatedView.findViewById(R.id.inputAccountNumber);
        spinnerBankName = inflatedView.findViewById(R.id.spinnerBankName);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        container.setBackgroundColor(context.getResources().getColor(R.color.background));
    }

    private void initViews() {
        if (LoanActivity.progressToolbar != null) LoanActivity.addProgress();
        textTitle.setVisibility(View.VISIBLE);
        inputOwnerName.setInputType(InputType.TYPE_CLASS_TEXT);
        inputOwnerName.setText(user.getFirstName()+" "+ user.getLastName());
        inputOwnerName.setEnabled(false);
        inputAccountNumber.setInputType(InputType.TYPE_CLASS_TEXT);
        inputAccountNumber.setNumericKeyboard();
        inputAccountNumber.setSingleLine();
        buttonContinue.setOnClickListener(v -> addAccount());
        buttonContinue.setState(ButtonState.DISABLED,getActivity());
        spinnerBankName.setOnItemSelectedListener(this);
        getBankData();

        InputFilter[] filter = new InputFilter[1];
        InputFilter[] filters = new InputFilter[1];
        filter[0] = new InputFilter.LengthFilter(25);
        filters[0] = new InputFilter.LengthFilter(16);

        inputOwnerName.setFilters(filter);
        inputAccountNumber.setFilters(filters);

        inputOwnerName.addTextChangedListener(this);
        inputAccountNumber.addTextChangedListener(this);
    }

    private void publish(Account account) {
        KeyboardUtils.hideKeyboard(getActivity());
        loan.setMainAccount("" + account.getId());
        contract.setLoan(loan);

        EventHelper.publishStackPop(getContext(), this);
        EventHelper.publishBack(getContext(), new LoanAccountSelectorWidgetView(account, "add"));
    }

    private boolean isValid(){
        return (!spinnerBankName.getSelectedItem().isEmpty() && inputAccountNumber.getValue().length() >= 8);
    }

    private void addAccount() {
        HashMap<String, String> param = new HashMap<>();
        param.put("account", inputAccountNumber.getValue());
        param.put("bank", bankItem.get(spinnerBankName.getSelectedPosition()).getId().toString());
        param.put("cif", user.getCif());

        profileContract.addAccounts(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse var1) {
                if (var1.getCode().equals("200")) {
                    Account account = new Account();
                    account.setCif(user.getCif());
                    account.setBank_id(bankItem.get(spinnerBankName.getSelectedPosition()).getId());
                    account.setBank_name(bankItem.get(spinnerBankName.getSelectedPosition()).getBankname());
                    account.setAccount_num(inputAccountNumber.getValue());
                    publish(account);
                } else {
                    CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> {
                        ((Activity) context).finish();
                    });
                }

            }

            @Override
            public void onError(Response statusResponse) {
                CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), getContext().getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> {
                    ((Activity) context).finish();
                });
            }
        }, param);
    }

    private void getBankData() {
        HashMap<String, String> param = new HashMap<>();
        param.put("profileSettings", "");
        param.put("qpay", "");
        param.put("transfer", "");

        profileContract.getBanks(new BankListener() {
            @Override
            public void onSuccess(List<BankItem> data) {
                bankItem = data;
                List<String> list = new ArrayList<String>();
                for (int i = 0; i < bankItem.size(); i++) {
                    list.add(bankItem.get(i).getBankname());
                }
                spinnerBankName.setItems(list);
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(context);
                Log.e("uno", "onError: " + "couldn't get bank data");
            }
        }, param);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        buttonContinue.setState(isValid() ? ButtonState.ENABLED : ButtonState.DISABLED, getActivity());
    }
}
