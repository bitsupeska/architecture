package mn.bitsup.callservice.view.widget.loan.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.widget.loan.view.LoanRepaymentView;
import mn.bitsup.callservice.view.widget.loan.view.LoanSummary;

public class AccountListAdapter extends BaseAdapter {

    private Context context;
    private List<LoanAccountData> accounts;
    private long mLastClickTime = 0;

    public AccountListAdapter(Context context, List<LoanAccountData> accounts) {
        this.context = context;
        this.accounts = accounts;
    }

    @Override
    public int getCount() {
        return accounts.size();
    }

    @Override
    public Object getItem(int position) {
        return accounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        convertView = layoutInflater.inflate(R.layout.loan_data_item, parent, false);
        TextView textAmount = convertView.findViewById(R.id.textAmount);
        TextView textDuration = convertView.findViewById(R.id.textDuration);
        RelativeLayout itemLoan = convertView.findViewById(R.id.itemLoan);
        RelativeLayout buttonRepayment = convertView.findViewById(R.id.buttonRepayment);
        double amount = 0;
        if (accounts.get(position).getLoanAmt() != null) {
//            amount = Float.parseFloat(accounts.get(position).getLoanAmt());
            if (Float.parseFloat(accounts.get(position).getPrincipleAmt()) != 0) {
                amount = -1 * Double.parseDouble(accounts.get(position).getPrincipleAmt());
            }
        }
        textAmount.setText("₮ " + CurrencyConverter.getFormattedCurrencyString(amount));


        if (accounts.get(position).getNextPaymentDay() != null)
            textDuration.setText(accounts.get(position).getNextPaymentDay() + " хоног");

        itemLoan.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            EventHelper.publishActivity(context, new LoanSummary(accounts.get(position)), "Дэлгэрэнгүй");
        });

        buttonRepayment.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();

            EventHelper.publishActivity(context, new LoanRepaymentView(accounts.get(position)), "Төлөх");
        });
        return convertView;
    }
}
