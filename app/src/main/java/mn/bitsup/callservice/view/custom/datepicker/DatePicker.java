package mn.bitsup.callservice.view.custom.datepicker;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.util.DateDisplayFormatUtil;

/**
 * Created by Erdenedalai R&D B.V on 20/06/2018.
 * Custom view that represent a schedule date picker
 */
public class DatePicker extends ConstraintLayout implements CustomCalendarView.ItemClickListener {

    @NonNull
    private DatePickerViewData datePickerData = () -> null;
    private CustomCalendarView customCalendarView;
    private TextView todaysDate;
    private LinearLayout calendarContainer;
    private ViewGroup legendItemsContainer;
    private CalendarCallbackInterface calendarCallBackListener;

    public DatePicker(Context context) {
        super(context);
        initializeViews();
    }

    public DatePicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeViews();
    }

    public DatePicker(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initializeViews();
    }

    private void initializeViews() {
        inflate(getContext(), R.layout.view_date_picker, this);
        calendarContainer = findViewById(R.id.calendarViewContainer);
        legendItemsContainer = findViewById(R.id.legendItemsContainer);
        todaysDate = findViewById(R.id.calendarDate);
        findViewById(R.id.cancelBtn).setOnClickListener(view -> {
            if (calendarCallBackListener != null) {
                calendarCallBackListener.onCancelButtonClicked();
            }
        });
        setupHeaderView();
        setupCalendarView();
        setupLegend();
    }

    private void setupLegend() {
        for (DatePickerLegendItem legendItems : datePickerData.getLegendItems()) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.calendar_legend_item,
                    legendItemsContainer, false);
            TextView legendItemText = view.findViewById(R.id.legendItemText);
            legendItemText.setText(legendItems.getText());
            ImageView imageView = view.findViewById(R.id.legendItemIcon);
            Drawable background = imageView.getBackground();
            int color = legendItems.getColor();
            if (background instanceof ShapeDrawable) {
                ((ShapeDrawable) background).getPaint().setColor(
                    ContextCompat.getColor(getContext(), color));
            } else if (background instanceof GradientDrawable) {
                ((GradientDrawable) background).setColor(ContextCompat.getColor(getContext(), color));
            } else if (background instanceof ColorDrawable) {
                ((ColorDrawable) background).setColor(ContextCompat.getColor(getContext(), color));
            }
            legendItemsContainer.addView(view);
        }
    }

    private void setupHeaderView() {
        todaysDate.setText(DateDisplayFormatUtil.toLongDateFormat(getContext(), Calendar.getInstance().getTime()));
    }

    private void setupCalendarView() {
        List<DatePickerItem> datePickerItems = datePickerData.getDatePickerItems();
        customCalendarView = new CustomCalendarView(getContext(), datePickerItems,
                datePickerData.getCalendarMinDate(), datePickerData.getPreSelectedDate());
        customCalendarView.setDateSummaryHidden();
        customCalendarView.setItemClickListener(this);
        customCalendarView.getStyler()
                .setDaysHeaderTextStyle(R.style.BBBillPayCalendarDayHeadersTextStyle, true);
        calendarContainer.removeAllViews();
        calendarContainer.addView(customCalendarView);
    }

    public void setDatePickerViewData(@NonNull DatePickerViewData datePickerViewData) {
        datePickerData = datePickerViewData;
        setupCalendarView();
        setupLegend();
    }

    @Override
    public void onItemClick(Date selectedDate) {
        if (calendarCallBackListener != null) {
            calendarCallBackListener.onDateSelected(selectedDate);
        }
    }

    public CustomCalendarView getCustomCalendarView() {
        return customCalendarView;
    }

    public void setCalendarCallBackListener(CalendarCallbackInterface calendarCallBackListener) {
        this.calendarCallBackListener = calendarCallBackListener;
    }

    public interface CalendarCallbackInterface {
        void onDateSelected(Date selectedDate);

        void onCancelButtonClicked();
    }
}
