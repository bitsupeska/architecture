package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.DataCell;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;

import static mn.bitsup.callservice.MainApplication.getUser;
import static mn.bitsup.callservice.view.widget.onboard.view.temporary.TPEmployeeStatusWidgetView.employeeStatusName;

public class TPDetailWidgetView extends Fragment {
    private Context context;
    private OnboardContract contract;
    private Temporary temporary;
    private CustomButton buttonContinue;
    private DataCell cellLastName, cellFirstName, cellRegNum, cellPhoneNumber, cellEmail, cellEmployeeStatus;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
        this.temporary = contract.getTemporary();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.temporary_details, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        cellLastName = inflatedView.findViewById(R.id.cellLastName);
        cellFirstName = inflatedView.findViewById(R.id.cellFirstName);
        cellRegNum = inflatedView.findViewById(R.id.cellRegNum);
        cellPhoneNumber = inflatedView.findViewById(R.id.cellPhoneNumber);
        cellEmail = inflatedView.findViewById(R.id.cellEmail);
        cellEmployeeStatus = inflatedView.findViewById(R.id.cellEmpStat);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        initView();
    }

    private void initView() {
        User user = MainApplication.getUser();
        cellLastName.setValueText(user.getLastName());
        cellFirstName.setValueText(user.getFirstName());
        cellRegNum.setValueText(user.getRegNum());
        cellPhoneNumber.setValueText(user.getPhoneNo());
        cellEmail.setValueText(user.getEmails().get(0).getEmailId());
        cellEmployeeStatus.setValueText(employeeStatusName);
        buttonContinue.setOnClickListener(v -> {
            createCA();
        });
    }

    private void createCA() {
        String uid = ClientsManager.authClient.getUid();
        Temporary payload = contract.getTemporary();

        payload.setCurrCode("MNT");
        payload.setProdCode("CA101");
        payload.setDidSign("1");
        payload.setFreezeCode("T");
        payload.setReasonCode("10");
        payload.setRemarks("Тайлбар");
        payload.setUserName(uid);
        payload.setEducationName("Default school name");

        User user = MainApplication.getUser();

        if (user.getEmails().size() > 0) {
            if (!user.getEmails().get(0).getEmailId().equals("")) {
                payload.setEmail(user.getEmails().get(0).getEmailId());
                payload.setEmailStatus(user.getEmails().get(0).getEmailStatus());

            } else {
                payload.setEmail("");
                payload.setEmailStatus("0");
            }
        }

        buttonContinue.setState(ButtonState.LOADING, getActivity());
        contract.createCA(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                if (statusResponse.getCode().equals("200")) {
                    User user = getUser();

                    List<Addresses> addresses = new ArrayList();

                    Addresses homeAddress = new Addresses();
                    homeAddress.setTown(payload.getTownHomeName() == null ? "" : payload.getTownHomeName());
                    homeAddress.setTownId(payload.getTownHome() == null ? "" : payload.getTownHome());
                    homeAddress.setLocality(payload.getLocalityHomeName() == null ? "" : payload.getLocalityHomeName());
                    homeAddress.setLocalityId(payload.getLocalityHome() == null ? "" : payload.getLocalityHome());
                    homeAddress.setSection(payload.getSectionHome() == null ? "" : payload.getSectionHome());
                    homeAddress.setDistrict(payload.getDistrictHome() == null ? "" : payload.getDistrictHome());
                    homeAddress.setApartment(payload.getApartmentHome() == null ? "" : payload.getApartmentHome());
                    homeAddress.setDoorNumber(payload.getDoorNumberHome() == null ? "" : payload.getDoorNumberHome());
                    homeAddress.setAddressType("Home");


                    Log.e("TPContractWidgetView", "onSuccess: " + homeAddress);
                    addresses.add(homeAddress);

                    Addresses mailAddress = null;

                    if (user.getAddresses() != null) {
                        for (int i = 0; i < user.getAddresses().size(); i++) {
                            if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                                mailAddress = user.getAddresses().get(i);
                            }
                        }
                    }
                    addresses.add(mailAddress);
                    user.setAddresses(addresses);
                    user.setDidSign("1");
                    MainApplication.setUser(user);

                    new AnimationDialog(getContext(), "Гарын үсэг хүлээгдэж байна,",
                            "Та салбар дээр очиж гарын үсгээ\n" +
                                    "өгснөөр зээл авах боломжтой болно.",
                            "Буцах", "success_anim.json",
                            this::finish
                    ).show();
                } else {
                    CustomAlertDialog.showAlertDialog(context, "Алдаа гарлаа", statusResponse.getMessage());
                }
            }

            private void finish() {
                getActivity().setResult(Activity.RESULT_FIRST_USER);
                getActivity().finish();
            }

            @Override
            public void onError(Response response) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }
}
