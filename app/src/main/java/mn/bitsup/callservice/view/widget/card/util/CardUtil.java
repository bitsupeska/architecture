package mn.bitsup.callservice.view.widget.card.util;


import static mn.bitsup.callservice.view.widget.card.util.formatter.CardNumberFormatter.formatCardNumber;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.ColorRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.card.dto.Card;
import mn.bitsup.callservice.view.widget.card.util.formatter.CardNumberFormat;

public class CardUtil {
    private static final String TAG = CardUtil.class.getSimpleName();

    // implementor-dependent constants

    public static final String STATUS_INACTIVE = "INACTIVE";
    public static final String STATUS_CANCELLED = "Cancelled";
    public static final String STATUS_ACTIVE = "ACTIVE";
    public static final String R_STATUS_NOT_REP = "NotUnderReplacement";
    public static final String R_STATUS_REQ = "ReplacementRequested";

    private static final String BRAND_MASTERCARD = "mastercard";
    private static final String BRAND_VISA = "visa";
    private static final String BRAND_AMERICAN_EXPRESS = "americanexpress";
    private static final String BRAND_DINERS_CLUB = "dinersclub";

    private static final String TYPE_DEBIT = "debit";

    public static final String REASON_LOST = "Lost";
    public static final String REASON_STOLEN = "Stolen";
    public static final String REASON_BROKEN = "Broken";

    private static final int DEFAULT_CVV_LENGTH = 3;
    private static final int AMERICAN_EXPRESS_CVV_LENGTH = 4;

    private static final InputFilter DEFAULT_CVV_FILTER =
            new InputFilter.LengthFilter(DEFAULT_CVV_LENGTH);
    private static final InputFilter AMERICAN_EXPRESS_CVV_FILTER =
            new InputFilter.LengthFilter(AMERICAN_EXPRESS_CVV_LENGTH);

    private CardUtil() {
        throw new UnsupportedOperationException("Cannot initialize util class");
    }

    /**
     * Sets the correct image resource to the brand logo image view, based on the brand and type.
     * Null drawable set instead if brand not recognized
     *
     * @param brand         the brand of the card, for example visa
     * @param type          the type, for example debit or credit
     * @param brandLogoView the image view where the logo will be shown
     */
    public static void setBrandLogo(@NonNull String brand,
                                    @NonNull String type,
                                    @NonNull ImageView brandLogoView) {

        switch (brand) {
            case BRAND_MASTERCARD:
                brandLogoView.setImageResource(R.drawable.visibility);
                break;
            case BRAND_VISA:
                if (TYPE_DEBIT.equalsIgnoreCase(type)) {
                    brandLogoView.setImageResource(R.drawable.visibility);
                } else {
                    brandLogoView.setImageResource(R.drawable.visibility);
                }
                break;
            case BRAND_AMERICAN_EXPRESS:
                brandLogoView.setImageResource(R.drawable.visibility);
                break;
            case BRAND_DINERS_CLUB:
                brandLogoView.setImageResource(R.drawable.visibility);
                break;
            default:
                brandLogoView.setImageDrawable(null);
//                BBLogger.error(TAG, "unknown card brand: " + brand);
                break;
        }
    }

    @NonNull
    public static String getFormattedExpiryDate(@NonNull Card card) {
        String month = card.getExpiryDate().getMonth();
        String year = card.getExpiryDate().getYear();
        String twoDigitYear = year.substring(year.length() - 2);
        return month + "/" + twoDigitYear;
    }

    @NonNull
    public static String getLowerCaseType(@NonNull Card card) {
        return card.getType().toLowerCase();
    }

    @NonNull
    public static String getHolderName(@NonNull Card card) {
        return card.getCardHolder().getName();
    }

    /**
     * The card replacement reason comes as a free string json field from the backend.
     * They need to be translated before they can be shown in the GUI.
     * 3 cases of this field are recognized: lost, stolen and broken. All case sensitive.
     * Any other value defaults to missing when translated by this method.
     *
     * @param context the context that provides string resources
     * @param reason  the reason as received from the backend
     * @return the translation of the reason if lost, stolen or broken. missing otherwise.
     */
    @NonNull
    public static String getTranslatedReplacementReason(@NonNull Context context,
                                                        @NonNull String reason) {
        @StringRes int translatedReasonRes;
        switch (reason) {
            case REASON_LOST:
                translatedReasonRes = R.string.temporary_address_section;
                break;
            case REASON_STOLEN:
                translatedReasonRes = R.string.temporary_address_section;
                break;
            case REASON_BROKEN:
                translatedReasonRes = R.string.temporary_address_section;
                break;
            default:
                translatedReasonRes = R.string.temporary_address_section;
                break;
        }
        return context.getString(translatedReasonRes);
    }

    /**
     * Sets a background color to the card-style background of the passed container.
     * Check .xml for a reference implementation of card-style background.
     *
     * @param context               the context
     * @param backgroundMainLayerId the id of the layer that encompasses the whole background
     * @param cardContainer         the view that has the card-style background
     * @param backgroundColor       the resource id of the color to apply to the background
     */
    public static void setBackground(@NonNull Context context,
                                     @IdRes int backgroundMainLayerId,
                                     @NonNull View cardContainer,
                                     @ColorRes int backgroundColor) {
        LayerDrawable layeredBackground = (LayerDrawable) cardContainer.getBackground();
        GradientDrawable bigBackground = (GradientDrawable)
                layeredBackground.findDrawableByLayerId(backgroundMainLayerId);
        bigBackground.setColor(ContextCompat.getColor(context, backgroundColor));
    }

    @NonNull
    public static String getMaskedNumberWithDots(@NonNull Card card) {
        String formattedCardNumber;
        String cardNumber = card.getMaskedNumber();
        char mask = '●';
        switch (card.getBrand()) {
            case CardUtil.BRAND_MASTERCARD:
                formattedCardNumber = formatCardNumber(cardNumber, formatWithMask(mask, CardNumberFormat.Formats.MASTER_CARD));
                break;
            case CardUtil.BRAND_VISA:
                if (CardUtil.TYPE_DEBIT.equals(card.getType())) {
                    formattedCardNumber = formatCardNumber(cardNumber, formatWithMask(mask, CardNumberFormat.Formats.VISA_DEBIT_CARD));
                } else {
                    formattedCardNumber = formatCardNumber(cardNumber, formatWithMask(mask, CardNumberFormat.Formats.VISA_CARD));
                }
                break;
            case CardUtil.BRAND_AMERICAN_EXPRESS:
                formattedCardNumber = formatCardNumber(cardNumber, formatWithMask(mask, CardNumberFormat.Formats.AMERICAN_EXPRESS_CARD));
                break;
            case CardUtil.BRAND_DINERS_CLUB:
                formattedCardNumber = formatCardNumber(cardNumber, formatWithMask(mask, CardNumberFormat.Formats.DINERS_CLUB_CARD));
                break;
            default:
                formattedCardNumber = cardNumber;
//                BBLogger.error(TAG, "unknown card brand: " + card.getBrand());
                break;
        }
        return formattedCardNumber;
    }

    @NonNull
    private static CardNumberFormat formatWithMask(char mask, @NonNull CardNumberFormat format) {
        format.setMaskChar(mask);
        return format;
    }

    private static boolean isAmericanExpress(@NonNull Card card) {
        return BRAND_AMERICAN_EXPRESS.equals(card.getBrand());
    }

    /**
     * Calculates the cvv length, based on the brand of the card.
     * 4 for American Express, 3 for all others
     *
     * @param card the card
     * @return the card's required cvv length
     */
    public static int getCvvLength(@NonNull Card card) {
        if (isAmericanExpress(card)) {
            return AMERICAN_EXPRESS_CVV_LENGTH;
        } else {
            return DEFAULT_CVV_LENGTH;
        }
    }

    /**
     * Adds a length input filter for the cvv input field, based on the brand of the card.
     *
     * @param cvvInputField the cvv input field
     * @param card          the card
     */
    public static void setCvvInputFilters(@NonNull EditText cvvInputField, @NonNull Card card) {
        InputFilter cvvLengthFilter;
        if (isAmericanExpress(card)) {
            cvvLengthFilter = AMERICAN_EXPRESS_CVV_FILTER;
        } else {
            cvvLengthFilter = DEFAULT_CVV_FILTER;
        }
        // Arrays.asList returns an unmodifiable list, so we wrap it in an ArrayList
        List<InputFilter> filters = new ArrayList<>(Arrays.asList(cvvInputField.getFilters()));
        if (!filters.contains(cvvLengthFilter)) {
            filters.add(cvvLengthFilter);
        }
        cvvInputField.setFilters(filters.toArray(new InputFilter[]{}));
    }
}

