package mn.bitsup.callservice.view.custom;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import mn.bitsup.callservice.R;

public class CustomAlertDialog {

    private CustomAlertDialog() {
    }

    public static void showAlertDialog(Context context, String title, String message) {
        showErrorDialog(context, title, message, (dialog, which) -> dialog.dismiss());
    }
    public static void showAlertDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonClickListener, DialogInterface.OnClickListener negativeButtonDialogListener) {
        showErrorDialog(context, title, message, positiveButtonClickListener,negativeButtonDialogListener);
    }

    public static void showAlertDialog(Context context, String title, String message,DialogInterface.OnClickListener positiveButtonClickListener,DialogInterface.OnCancelListener onCancelListener) {
        showErrorDialog(context, title, message, positiveButtonClickListener,onCancelListener);
    }

    public static void showAlertDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonClickListener) {
        showErrorDialog(context, title, message, positiveButtonClickListener);
    }

    public static void showAlertDialog(Context context, String title, String message,DialogInterface.OnClickListener positiveButtonClickListener,DialogInterface.OnClickListener negativeButtonDialogListener,String positiveButtonText,String negativeButtonText) {
        showErrorDialog(context, title, message, positiveButtonClickListener,negativeButtonDialogListener,positiveButtonText,negativeButtonText);
    }

    public static void showAlertDialog(Context context, int title, int message) {
        showErrorDialog(context, context.getResources().getString(title), context.getResources().getString(message), (dialog, which) -> dialog.dismiss());
    }

    public static void showServerAlertDialog(Context context) {
        showErrorDialog(context,  context.getResources().getString(R.string.shared_alert_generic_error_title), context.getResources().getString(R.string.shared_alert_generic_error_server), (dialog, which) -> dialog.dismiss());
    }

    private static void showErrorDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonClickListener) {
        new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.shared_alert_button_ok, positiveButtonClickListener)
                .show();
    }
    private static void showErrorDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonClickListener,DialogInterface.OnCancelListener cancelButtonClickListener) {
        new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setOnCancelListener(cancelButtonClickListener)
                .setPositiveButton(R.string.shared_alert_button_ok, positiveButtonClickListener)
                .show();
    }

    private static void showErrorDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonClickListener,DialogInterface.OnClickListener negativeButtonClickListener) {
        new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.shared_alert_button_agree, positiveButtonClickListener)
                .setNegativeButton(R.string.shared_button_back, negativeButtonClickListener)
                .show();
    }
    private static void showErrorDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveButtonClickListener, DialogInterface.OnClickListener negativeButtonClickListener, String positiveButtonText, String negativeButtonText) {
        new AlertDialog.Builder(context, R.style.MyAlertDialogStyle)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText, positiveButtonClickListener)
                .setNegativeButton(negativeButtonText, negativeButtonClickListener)
                .show();
    }

}
