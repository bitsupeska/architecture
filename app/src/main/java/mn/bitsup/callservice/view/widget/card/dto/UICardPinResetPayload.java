package mn.bitsup.callservice.view.widget.card.dto;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Objects;
import mn.bitsup.callservice.client.card.dto.Card;

public class UICardPinResetPayload implements Parcelable {

    @NonNull
    protected Card card;
    @Nullable
    protected String token;
    @Nullable
    protected String pin;


    public static final Creator<UICardPinResetPayload> CREATOR =
            new Creator<UICardPinResetPayload>() {
                @Override
                public UICardPinResetPayload createFromParcel(Parcel in) {
                    return new UICardPinResetPayload(in);
                }

                @Override
                public UICardPinResetPayload[] newArray(int size) {
                    return new UICardPinResetPayload[size];
                }
            };

    @ColorRes
    private int backgroundColor;

    public UICardPinResetPayload(@NonNull Card card,
                                 @Nullable String token,
                                 @Nullable String pin,
                                 @ColorRes int backgroundColor) {
        this.card = card;
        this.token = token;
        this.pin = pin;
        this.backgroundColor = backgroundColor;
    }

    @SuppressWarnings("WeakerAccess") // default implementation of Parcelable
    protected UICardPinResetPayload(Parcel parcel) {
        this.card = (Card)Objects.requireNonNull(parcel.readParcelable(Card.class.getClassLoader()));
        this.token = parcel.readString();
        this.pin = parcel.readString();
        backgroundColor = parcel.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.card, flags);
        dest.writeString(this.token);
        dest.writeString(this.pin);
        dest.writeInt(backgroundColor);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @ColorRes
    public int getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        UICardPinResetPayload that = (UICardPinResetPayload) o;
        return backgroundColor == that.backgroundColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), backgroundColor);
    }
}
