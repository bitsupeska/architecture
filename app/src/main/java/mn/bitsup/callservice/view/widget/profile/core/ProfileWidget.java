package mn.bitsup.callservice.view.widget.profile.core;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.info.InfoClient;
import mn.bitsup.callservice.client.info.listener.BankListener;
import mn.bitsup.callservice.client.info.listener.LocationListener;
import mn.bitsup.callservice.client.offline.OfflineClient;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.otp.OTPClient;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.client.profile.ProfileClient;
import mn.bitsup.callservice.client.profile.dto.Questions;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.AccountListener;
import mn.bitsup.callservice.client.profile.listener.QuestionListener;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;
import mn.bitsup.callservice.view.widget.profile.dto.Account;

public class ProfileWidget implements ProfileContract {

    private ProfileClient profileClient;
    private Context context;
    private InfoClient infoClient;
    private OTPClient otpClient;
    private OfflineClient offlineClient;
    public static final String ACCESS_TOKEN = "access_token";

    public ProfileWidget(@NonNull Context context) {
        this.context = context;
        this.offlineClient = ClientsManager.getOfflineClient();
        this.infoClient = ClientsManager.getInfoClient();
        this.otpClient = ClientsManager.getOtpClient();
        this.profileClient = ClientsManager.getProfileClient();
    }

    @Override
    public User getUser() {
        return MainApplication.getUser();
    }

    @Override
    public void setEmail(StatusListener statusListener, HashMap<String, String> param) {
        profileClient.setEmail(statusListener, param);
    }

    @Override
    public void setNumber(StatusListener statusListener, HashMap<String, String> param) {
        profileClient.setNumber(statusListener, param);
    }

    @Override
    public void setAddress(StatusListener statusListener, HashMap<String, String> param) {
        profileClient.setAddress(statusListener, param);
    }

    @Override
    public void matchPassword(StatusListener statusListener, HashMap<String, String> param) {
        profileClient.matchPassword(statusListener, param);
    }

    @Override
    public void setPassword(StatusListener statusListener, HashMap<String, String> param) {
        profileClient.setPassword(statusListener, param);
    }

    @Override
    public void requestOTP(OTPListener otpListener, Map<String, String> params) {
        otpClient.getOTP(params, otpListener);
    }

    @Override
    public void checkOTP(OTPListener otpListener, Map<String, String> params) {
        this.otpClient.checkOTP(params, otpListener);
    }

    @Override
    public void requestEmailOTP(OTPListener otpListener, Map<String, String> params) {
        this.otpClient.getEmailOTP(params, otpListener);
    }

    @Override
    public void checkEmailOTP(OTPListener otpListener, Map<String, String> params) {
        this.otpClient.checkEmailOTP(params, otpListener);
    }

    @Override
    public void loadCityData(LocationListener listener) {
        this.infoClient.getCityData(listener);
    }

    @Override
    public void loadDistrictData(LocationListener listener, String stateCode) {
        this.infoClient.getDistrictData(listener, stateCode);
    }

    @Override
    public void getBanks(BankListener listener, Map<String, String> params) {
        this.infoClient.getBank(listener, params);
    }

    @Override
    public void getAccountLists(AccountListener listener, String params) {
        this.profileClient.getAccountList(new AccountListener() {
            @Override
            public void onSuccess(List<Account> var1) {
                listener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                listener.onError(var1);

            }
        }, params);
    }

    @Override
    public void getAnswerQuestions(QuestionListener listener) {
        this.profileClient.getAnswerQuestion(new QuestionListener() {
            @Override
            public void onSuccess(List<Questions> var1) {
                listener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                listener.onError(var1);

            }
        });
    }

    @Override
    public void addAccounts(StatusListener listener, Map<String, String> params) {
        this.profileClient.addAccount(listener, params);
    }

    @Override
    public void deleteAccounts(StatusListener listener, Map<String, String> params) {
        this.profileClient.deleteAccount(listener, params);
    }

    @Override
    public void checkBlockIncrement(BlockIncrementListener listener, String type) {
        HashMap<String, String> param = new HashMap<>();
        param.put("blockInfo", DeviceAddressUtil.getMACAddress(context,"wlan0"));
        param.put("blockType", "1");
        param.put("blockChannel", "2");
        offlineClient.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                if (blockIncrement.getStatus().equals("SUCCESS"))
                    checkOtpCount(blockIncrement, type);
                else listener.onError(new Response());
            }

            @Override
            public void onError(Response response) {
                listener.onError(response);
            }
        }, param);
    }

    private void checkOtpCount(BlockIncrement blockIncrement, String type) {
        Log.e("checkOtpCount", "getFailed_count(): " + blockIncrement.getData().getFailedCount());
        Log.e("checkOtpCount", "getBlock_limit(): " + blockIncrement.getData().getBlockLimit());
        if (blockIncrement.getData().getFailedCount() >= blockIncrement.getData().getBlockLimit()) {
            setBlock(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    if (statusResponse.getStatus().equals("SUCCESS")) {
                        new AnimationDialog(context, "Уучлаарай,",
                                "Таны өнөөдөр баталгаажуулах 3 удаагийн эрх дууслаа. Та 00:00 цагаас хойш дахин оролдоно уу.",
                                "Дахин оролдох", "anim_rejection_clock.json",
                                () -> ((Activity) context).finish()).show();
                    } else Log.e("MBank", statusResponse.getMessage());

                }

                @Override
                public void onError(Response response) {
                    Log.e("checkOTPCount", "error:" + response);
                }
            }, blockIncrement, type);
        }
    }

    @Override
    public void checkBlock(BlockListener blockListener, HashMap<String, String> param) {
        offlineClient.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                blockListener.onSuccess(var1);
            }

            @Override
            public void onError(Response var1) {
                blockListener.onError(var1);
            }
        }, param);
    }

    @Override
    public void setBlock(StatusListener statusListener, BlockIncrement blockIncrement, String type) {
        HashMap<String, String> param = new HashMap<>();
        String blockType = "";
        String blockChannel = "";
        String blockMsg = "";
        // Mac address = 1 , Username n 3.
        switch (type) {
            case "phone":
                blockType = "3";
                blockChannel = "3";
                blockMsg = "Олон нэг удаагийн нууц үг илгээсэн учир утас солихыг хориглосон. Дараа дахин оролдоно уу.";
                break;
            case "email":
                blockType = "3";
                blockChannel = "4";
                blockMsg = "Олон нэг удаагийн нууц үг илгээсэн учир и-мэйл солихыг хориглосон. Дараа дахин оролдоно уу.";
                break;
        }

        param.put("blockInfo", getUser().getUid().toLowerCase());
        param.put("blockType", blockType);
        param.put("blockChannel", blockChannel);
        param.put("blockMsg", blockMsg);
        offlineClient.setBlock(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                statusListener.onSuccess(statusResponse);
            }

            @Override
            public void onError(Response response) {
                statusListener.onError(response);
            }
        }, param);
    }
}
