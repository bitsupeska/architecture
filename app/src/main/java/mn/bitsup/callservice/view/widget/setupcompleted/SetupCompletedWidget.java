package mn.bitsup.callservice.view.widget.setupcompleted;

import android.app.ProgressDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.RemoteAuthClient;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.profile.ProfileClient;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.profile.listener.UserListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.view.widget.passcode.PasscodeWidgetAuthenticateView;

public class SetupCompletedWidget {
    private ProgressDialog progressDialog;
    private Context context;
    private ProfileClient profileClient;

    public SetupCompletedWidget(@NonNull Context context) {
        this.context = context;
        this.profileClient = ClientsManager.getProfileClient();
        progressDialog = new ProgressDialog(context, R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
    }


    public void getUser(UserListener userListener) {
        profileClient.getUser(new UserListener() {
            @Override
            public void onSuccess(User var1) {
                userListener.onSuccess(var1);
                RemoteAuthClient authClient = ClientsManager.getAuthClient();
                authClient.setUser(var1);
            }

            @Override
            public void onError(Response var1) {
                userListener.onError(var1);
            }
        });
    }

    void confirmed() {
        EventHelper.publish(context, new PasscodeWidgetAuthenticateView());
    }
}
