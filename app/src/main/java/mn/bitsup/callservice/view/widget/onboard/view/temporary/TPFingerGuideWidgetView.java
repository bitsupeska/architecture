package mn.bitsup.callservice.view.widget.onboard.view.temporary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import java.util.HashMap;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.KhurItems;
import mn.bitsup.callservice.client.offline.listener.KhurDataListener;
import mn.bitsup.callservice.client.offline.listener.KhurReturnImageListener;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.client.temporary.dto.Temporary;
import mn.bitsup.callservice.util.DateTimeFormatter;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.PhoneUtils;
import mn.bitsup.callservice.util.TextViewColor;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.dialog.KhurInfoDialog;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;


public class TPFingerGuideWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private int count = 3;
    private Temporary payload;
    private TextView textTitle;
    private TextView textLink;
    private CustomButton buttonContinue;
    private LottieAnimationView animationView;
    private Context context;
    private DataListener dataListener = new DataListener();
    private OnboardContract contract;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
        this.payload = contract.getTemporary();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_finger_guide, container, false);
        inflatedView(inflatedView);
        initView();
        initEvent();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        textLink = inflatedView.findViewById(R.id.textLink);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        animationView = inflatedView.findViewById(R.id.success_animation);
    }

    private void initView() {
        textTitle.setText(context.getResources().getString(R.string.onboard_finger_guide_title));
        textLink.setText(TextViewColor.getUnderlineText(getContext(), context.getResources().getString(R.string.onboard_finger_guide_link), 13, 22));
        PhoneUtils.checkBioPermissions(context);
        setAnimation();
    }

    private void setAnimation() {
        animationView.setAnimation("anim_khur.json");
        animationView.playAnimation();
    }

    private void initEvent() {
        buttonContinue.setOnClickListener(v -> startKhurActivity());
        textLink.setOnClickListener(v -> showInformation());
    }

    private void startKhurActivity() {
        if (count > 0) {
            HashMap<String, String> params = new HashMap<>();
            params.put("regNum", MainApplication.getUser().getRegNum());
            contract.getXyrData(new KhurDataListener() {
                @Override
                public void onSuccess(KhurItems var1) {
                    if (var1.getStatus().equals("SUCCESS")) {
                        createCif(var1);
                    } else {

                    }
                }

                @Override
                public void onError(Response var1) {
                }
            }, params);

        } else {
            contract.setBlockKhur(new StatusListener() {
                @Override
                public void onSuccess(StatusResponse statusResponse) {
                    Log.e("MBank", "username block: " + statusResponse.getMessage());
                }

                @Override
                public void onError(Response response) {
                }
            });

            AnimationDialog dialog = new AnimationDialog(context, "Уучлаарай,",
                    context.getResources().getString(
                            R.string.khur_alert_finger_not_match),
                    context.getResources().getString(R.string.shared_button_back), "error_anim.json",
                    () -> ((Activity) context).finish());
            dialog.show();
        }
    }

    private void showInformation() {
        KhurInfoDialog dialog = new KhurInfoDialog(getContext());
        dialog.show();
    }

    private User getUser() {
        return MainApplication.getUser();
    }

    private void getKhurData(String image) {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        String regNum = getUser().getRegNum();
        HashMap<String, String> params = new HashMap<>();
        params.put("regNum", regNum);
        params.put("image", image);

        contract.getKhurData(new KhurDataListener() {
            @Override
            public void onSuccess(KhurItems khurItems) {
                if (khurItems.getStatus().equals("SUCCESS")) {
                    createCif(khurItems);
                } else {
                    buttonContinue.setState(ButtonState.ENABLED, getActivity());
                    count--;
                    AnimationDialog dialog = new AnimationDialog(context, "", "Хурууны хээ баталгаажуулалт амжилтгүй. "
                            + "Танд " + count + " удаагийн боломж үлдлээ.", context.getResources().getString(
                            R.string.shared_button_retry), "anim_rejection_finger.json");
                    dialog.show();
                }
            }

            @Override
            public void onError(Response response) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showAlertDialog(context, R.string.shared_alert_generic_error_title, R.string.shared_alert_generic_error_server);
            }
        }, params);
    }

    private void createCif(KhurItems khurItems) {

        contract.createCif(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {

                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                if (statusResponse.getStatus().equals("SUCCESS")) {
                    Log.e("createCif", "success. Cif: " + statusResponse.getCif());
                    payload.setCifId(statusResponse.getCif());

                    User user = MainApplication.getUser();
                    user.setCif(statusResponse.getCif());
                    ClientsManager.getRemoteAuthClient().setUser(user);

                    publish(khurItems);
                } else {
                    CustomAlertDialog.showAlertDialog(context, "", statusResponse.getMessage());
                }
            }

            @Override
            public void onError(Response var2) {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                CustomAlertDialog.showServerAlertDialog(context);
            }
        }, khurItems);
    }

    private void publish(KhurItems khurItems) {
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
        payload.setBirthDate(DateTimeFormatter.dateFormat(khurItems.getBirthdateastext()));
        payload.setRegNum(khurItems.getRegNum());
        payload.setFirstName(khurItems.getFirstName());
        payload.setLastName(khurItems.getLastName());
        payload.setFamilyName(khurItems.getSurName());
        payload.setDocExpDate(khurItems.getPassportExpireDate().replace(" 00:00:00", ""));
        payload.setDocIssueDate(khurItems.getPassportIssueDate());
        payload.setDidSign("0");

        payload.setTownHomeName(khurItems.getAimagCityName());
        payload.setLocalityHomeName(khurItems.getSoumDistrictName());
        payload.setTownHome(khurItems.getAimagCityCode());
        payload.setLocalityHome("" + Integer.parseInt(khurItems.getSoumDistrictCode()));
        payload.setSectionHome(khurItems.getBagKhorooName());

        if (!khurItems.getAddressRegionName().equals("")) {
            payload.setDistrictHome(khurItems.getAddressRegionName());
        } else {
            payload.setDistrictHome(khurItems.getAddressStreetName());
        }

        payload.setApartmentHome(khurItems.getAddressApartmentName());
        payload.setDoorNumberHome(khurItems.getAddressDetail());

        if (khurItems.getGender().equals("Эрэгтэй")) {
            payload.setGender("M");
        } else {
            payload.setGender("F");
        }

        LoanActivity.addProgress();
        contract.setTemporary(payload);
        EventHelper.publishBack(context, new TPAddressCheckWidgetView());
    }

    public class DataListener implements KhurReturnImageListener {
        @Override
        public void returnImage(String var1) {
            getKhurData(var1);
        }

        @Override
        public void onErrorHandling() {
            CustomAlertDialog.showAlertDialog(context, "Уучлаарай", "Та хурууны хээгээ дахин уншуулна уу.");
        }
    }
}