package mn.bitsup.callservice.view.widget.loan.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.mikhaellopez.circleview.CircleView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mn.bitsup.callservice.MainApplication;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.dataprovider.storage.StorageComponent;
import mn.bitsup.callservice.client.loan.dto.Loan;
import mn.bitsup.callservice.client.loan.dto.LoanAccount;
import mn.bitsup.callservice.client.loan.dto.LoanAccountData;
import mn.bitsup.callservice.client.loan.dto.LoanIsAllow;
import mn.bitsup.callservice.client.loan.dto.LoanOffer;
import mn.bitsup.callservice.client.loan.listener.LoanAccountListener;
import mn.bitsup.callservice.client.loan.listener.LoanIsAllowedListener;
import mn.bitsup.callservice.client.offline.dto.Block;
import mn.bitsup.callservice.client.offline.dto.BlockData;
import mn.bitsup.callservice.client.offline.listener.BlockListener;
import mn.bitsup.callservice.client.profile.dto.Addresses;
import mn.bitsup.callservice.client.profile.dto.User;
import mn.bitsup.callservice.util.CurrencyConverter;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.util.ResourcesUtil;
import mn.bitsup.callservice.view.activity.MainActivity;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.WidgetListView;
import mn.bitsup.callservice.view.dialog.AnimationDialog;
import mn.bitsup.callservice.view.widget.forgotpassword.core.ForgetWidget;
import mn.bitsup.callservice.view.widget.forgotpassword.core.contract.ForgotPasswordContract;
import mn.bitsup.callservice.view.widget.loan.LoanActivity;
import mn.bitsup.callservice.view.widget.loan.adapter.AccountListAdapter;
import mn.bitsup.callservice.view.widget.loan.core.LoanContract;
import mn.bitsup.callservice.view.widget.loan.core.LoanWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;
import mn.bitsup.callservice.view.widget.profile.views.ProfileBranchAddress;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class LoanOfferView extends Fragment implements
        OnboardView<OnboardContract> {

    private static final String TAG = "LoanOfferView";

    private CustomButton buttonContinue;
    private Context context;
    private LoanContract contract;
    private User user;
    private TextView textFinalLimit, textAlertTitle, textAlertMessage, textTotalAmount, textRepaymentAmount;
    public static LoanOffer loanOffer;
    private LinearLayout viewAlert;
    private ImageView imageAlert;
    private ListView listRepayment;
    private StorageComponent component;
    private CircleView circleView;
    private LinearLayout linearMonthly, linearTotal;
    private TextView textStatus;
    private WidgetListView widgetListView;
    private LinearLayout container;
    private NestedScrollView scrollView;
    private GuideView mGuideView;
    private GuideView.Builder builder;
    private String addressType = "";
    private SwipeRefreshLayout swipeRefreshLayout;
    private SwipeRefreshListener swipeRefreshListener = new SwipeRefreshListener();
    private ForgotPasswordContract forgotPasswordContract;
    private boolean isBlocked = false;
    private String blockMessage;

    public LoanOfferView(LoanOffer var) {
        loanOffer = var;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new LoanWidget(context);
        this.component = new StorageComponent();
        this.user = MainApplication.getUser();
        this.forgotPasswordContract = new ForgetWidget(context);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.loan_offer, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        swipeRefreshLayout = inflatedView.findViewById(R.id.swipeRefreshLayout);
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        textFinalLimit = inflatedView.findViewById(R.id.textFinalLimit);
        viewAlert = inflatedView.findViewById(R.id.viewAlert);
        textAlertTitle = inflatedView.findViewById(R.id.textAlertTitle);
        textAlertMessage = inflatedView.findViewById(R.id.textAlertMessage);
        textTotalAmount = inflatedView.findViewById(R.id.textTotalAmount);
        textRepaymentAmount = inflatedView.findViewById(R.id.textRepaymentAmount);
        imageAlert = inflatedView.findViewById(R.id.imageAlert);
        listRepayment = inflatedView.findViewById(R.id.listRepayment);
        textStatus = inflatedView.findViewById(R.id.textStatus);
        widgetListView = inflatedView.findViewById(R.id.widgetListView);
        container = inflatedView.findViewById(R.id.container);
        imageAlert.setVisibility(View.VISIBLE);
        circleView = inflatedView.findViewById(R.id.circle);
        linearMonthly = inflatedView.findViewById(R.id.linear_monthly);
        linearTotal = inflatedView.findViewById(R.id.linear_total);
        scrollView = inflatedView.findViewById(R.id.scrollView);
    }

    private void initView() {
        //swipeRefreshLayout.setColorSchemeResources(new int[]{R.color.wl_progress});
        swipeRefreshLayout.setOnRefreshListener(this.swipeRefreshListener);
        if (LoanActivity.progressToolbar != null) {
            LoanActivity.addProgress();
        }
        if (user.getDidSign().equals("0")) {
            alertReadyForLoan();
        } else if (user.getDidSign().equals("1")) {
            alertWaitingSignature();
        } else if (Integer.valueOf(user.getDidSign()) > 1) {
            viewAlert.setVisibility(View.GONE);
            getLoanAccount();

        }
        setFinalLimit();
    }

    private void hideProgress() {
        if (loanOffer.getHasPendingLoan()) {
            alertWaitingLoanApproval();
            buttonContinue.setState(ButtonState.DISABLED, getActivity());
        }
        screenEnabled(true);
        widgetListView.setVisibility(View.GONE);
        widgetListView.hideProgress();
        container.setVisibility(View.VISIBLE);
    }

    private void showProgress() {
        screenEnabled(false);
        widgetListView.setVisibility(View.VISIBLE);
        widgetListView.hideProgress();
        container.setVisibility(View.GONE);

    }

    private void publishLocation() {
        EventHelper.publishActivity(context, new ProfileBranchAddress(), "Салбарын хаяг");
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
    }

    private void publishLoanSelector() {
        HashMap<String, String> blockParams = new HashMap<>();
        blockParams.put("value", user.getUid());
        blockParams.put("macAddress", DeviceAddressUtil.getMACAddress(context, "wlan0"));
        blockParams.put("ipAddress", DeviceAddressUtil.getIPAddress(true));

        this.forgotPasswordContract.checkBlock(new BlockListener() {
            @Override
            public void onSuccess(Block var1) {
                if (!var1.getData().isEmpty()) {
                    for (BlockData block :
                            var1.getData()) {
                        if (block.getBlocktype() == 4
                                && block.getBlockchannel() == 8
                                && block.getBlockinfo().equals(user.getUid())) {
                            isBlocked = true;
                            blockMessage = block.getBlockmsg();
                        }
                    }
                } else {
                    isBlocked = false;
                }
                if (!isBlocked) {
                    Intent intent = new Intent(context, LoanActivity.class);
                    intent.putExtra("type", "selector");
                    startActivityForResult(intent, 1);
                } else {
                    new AnimationDialog(getContext(), "Уучлаарай,",
                            blockMessage,
                            "Буцах", "anim_rejection_clock.json").show();
                }
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(context);
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
            }
        }, blockParams);
    }

    private void publishTemporary() {
        Intent intent = new Intent(context, LoanActivity.class);
        intent.putExtra("type", "address");
        startActivityForResult(intent, 1);
    }

    private void setFinalLimit() {
        float finalLimit = loanOffer.getData().getFinal_limit();
        if (finalLimit > 0) {
            String finalLimitText = CurrencyConverter.getFormattedCurrencyString(finalLimit);
            textFinalLimit.setText(finalLimitText);
        }
    }

    private void getLoanAccount() {
        Map<String, String> params = new HashMap<>();
        params.put("cifId", user.getCif());

        contract.getLoanAccount(params, new LoanAccountListener() {
            @Override
            public void onSuccess(LoanAccount response) {
                buttonContinue.setVisibility(View.VISIBLE);
                if (response.getCode() == 200) {
                    List<LoanAccountData> loanAccountData = response.getData();
                    for (int i = 0; i < loanAccountData.size(); i++) {
                        if (loanAccountData.get(i).getPayOffAmt() != null && Double.parseDouble(loanAccountData.get(i).getPayOffAmt()) == 0) {
                            loanAccountData.remove(i);
                        }
                    }
                    hideProgress();
                    if (loanAccountData.size() == 0) {
                        if (!loanOffer.getHasPendingLoan())
                            alertBeginLoan();
                    } else {
                        addressType = "loan";
                        buttonContinue.setOnClickListener(v -> checkAddress());
                        setTotalAmount(loanAccountData);
                        setRepaymentAmount(loanAccountData);
                        setRepaymentAdapter(loanAccountData);

                        if (loanAccountData.size() == 1) {
                            if (component.getItem("guide") == null || !component.getItem("guide").equals("done")) {
                                scrollView.post(() -> {
                                    scrollView.fullScroll(View.FOCUS_DOWN);
                                    component.setItem("guide", "done");
                                    showGuide();
                                });
                            }
                        }
                    }
                } else {
                    buttonContinue.setVisibility(View.VISIBLE);
                    CustomAlertDialog.showAlertDialog(context, "", response.getMessage());
                }
            }

            @Override
            public void onError(Response response) {
                buttonContinue.setVisibility(View.VISIBLE);
                hideProgress();
                viewAlert.setVisibility(View.GONE);
                CustomAlertDialog.showAlertDialog(context, "", "Зээлийн мэдээлэл татах үед алдаа гарлаа");
            }
        });
    }

    private void setRepaymentAdapter(List<LoanAccountData> loans) {
        LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
                ResourcesUtil.dpToPx(115 * loans.size(), context.getResources()));
        listRepayment.setLayoutParams(params);
        AccountListAdapter adapter = new AccountListAdapter(context, loans);
        listRepayment.setAdapter(adapter);
    }


    @SuppressLint("SetTextI18n")
    private void setTotalAmount(List<LoanAccountData> loans) {
        double total = 0;
        for (int i = 0; i < loans.size(); i++) {
            if (loans.get(i).getLoanAmt() != null && !loans.get(i).getLoanAmt().equals("")) {
                float amount = Float.parseFloat(loans.get(i).getLoanAmt());
                total = total + amount;
            }
        }
        textTotalAmount.setText("₮ " + CurrencyConverter.getFormattedCurrencyString(total));
    }

    @SuppressLint("SetTextI18n")
    private void setRepaymentAmount(List<LoanAccountData> loans) {
        double total = 0;
        try {
            for (int i = 0; i < loans.size(); i++) {
                if (loans.get(i).getThisMonthPayment() != null && !loans.get(i).getLoanAmt().equals("")) {
                    double thisMonthPayment = Double.parseDouble(loans.get(i).getThisMonthPayment());
                    if (thisMonthPayment != 0) {
                        total = total + thisMonthPayment;
                    } else {
                        total = total + Double.parseDouble(loans.get(i).getNextMonthPayment());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        textRepaymentAmount.setText("₮ " + CurrencyConverter.getFormattedCurrencyString(total));

    }

    private void alertReadyForLoan() {
        imageAlert.setImageResource(R.drawable.ic_check_circle);
        textAlertTitle.setText("Зээлийн эрх");
        textAlertMessage.setText("Tанд эрх үүссэн байна. Одоо ердөө\n"
                + "хэдхэн алхам хийгээд л зээлээ аваарай.");
        viewAlert.getBackground()
                .setColorFilter(Color.parseColor("#00ca9f"), PorterDuff.Mode.SRC_ATOP);
        buttonContinue.setOnClickListener(v -> publishTemporary());
        hideProgress();
        buttonContinue.setVisibility(View.VISIBLE);
    }

    @SuppressLint("SetTextI18n")
    private void alertWaitingSignature() {

        imageAlert.setImageResource(R.drawable.ic_error_black_32dp);
        textAlertTitle.setText("Гарын үсэг хүлээгдэж байна");
        textAlertMessage.setText("Та салбар дээр очиж гарын үсгээ\n"
                + "өгснөөр зээл авах боломжтой болно.");
        viewAlert.getBackground()
                .setColorFilter(Color.parseColor("#f1be3c"), PorterDuff.Mode.SRC_ATOP);
        buttonContinue.setEnabledText("Салбарын байршил");
        addressType = "signature";
        buttonContinue.setOnClickListener(v -> checkAddress());
        hideProgress();
        buttonContinue.setVisibility(View.VISIBLE);
    }

    private void checkAddress() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        Addresses address = new Addresses();
        if (this.user.getAddresses() != null) {
            for (int i = 0; i < this.user.getAddresses().size(); i++) {
                if (user.getAddresses().get(i).getAddressType().equals("Mailing")) {
                    address = user.getAddresses().get(i);

                }
            }
        }

        if (address.getTownId() != null) {
            Log.e("LoanOffer", "townName: " + address.getLocality());
            if (equalsLocalityId(address.getTownId() + address.getLocalityId())) {
                if (addressType.equals("signature")) {
                    publishLocation();
                } else if (addressType.equals("loan")) {
                    checkIsLoanAllowed();
                }
            } else {
                buttonContinue.setState(ButtonState.ENABLED, getActivity());
                showErrorDialog("Манайх одоогоор зөвхөн Улаанбаатар хотын Баянгол, Баянзүрх, Сонгино хайрхан, Сүхбаатар, Чингэлтэй, Хан-Уул дүүрэг бүртгэлтэй харилцагч нарт үйлчилгээ үзүүлж байгааг анхаарна уу.");
            }

        } else {
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
            showErrorDialog("Манайх одоогоор зөвхөн Улаанбаатар хотын Баянгол, Баянзүрх, Сонгино хайрхан, Сүхбаатар, Чингэлтэй, Хан-Уул дүүрэг бүртгэлтэй харилцагч нарт үйлчилгээ үзүүлж байгааг анхаарна уу.");
        }
    }

    private void checkIsLoanAllowed() {
        HashMap<String, String> params = new HashMap<>();
        params.put("id", "LOAN_ALLOWED");
        params.put("uid", MainApplication.getUser().getUid());
        contract.getLoanAllowed(new LoanIsAllowedListener() {
            @Override
            public void onSuccess(LoanIsAllow var1) {
                if (var1.getStatus().equals("SUCCESS")) {
                    if (var1.isLoanAllowed())
                        publishLoanSelector();
                    else {
                        new AnimationDialog(getContext(), "Уучлаарай,",
                                var1.getMessage(),
                                "Буцах", "").show();
                    }
                } else {
                    CustomAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.shared_alert_generic_alert_error_title), var1.getMessage());
                }
            }

            @Override
            public void onError(Response var1) {
                CustomAlertDialog.showServerAlertDialog(context);
            }
        }, params);
    }

    private void showErrorDialog(String description) {
        component.setItem("loanStatus", "");
        AnimationDialog dialog = new AnimationDialog(context, "Уучлаарай,", description, "Буцах", "anim_rejection_comment.json");
        dialog.show();
    }

    private Boolean equalsLocalityId(String localityId) {
        Log.e("LoanOffer", "townId: " + localityId);
        Boolean correct;
        switch (localityId) {
            case "117":
                correct = true;
                break;
            case "1110":
                correct = true;
                break;
            case "1116":
                correct = true;
                break;
            case "1119":
                correct = true;
                break;
            case "1122":
                correct = true;
                break;
            case "1125":
                correct = true;
                break;
            default:
                correct = false;
                break;

        }
        return correct;
    }

    private void alertBeginLoan() {
        viewAlert.setVisibility(View.VISIBLE);
        imageAlert.setImageResource(R.drawable.ic_check_circle);
        viewAlert.getBackground()
                .setColorFilter(Color.parseColor("#00ca9f"), PorterDuff.Mode.SRC_ATOP);
        textStatus.setVisibility(View.VISIBLE);
        LoanActivity.loan = new Loan();
        addressType = "loan";
        buttonContinue.setOnClickListener(v -> checkAddress());
        textAlertTitle.setText("Зээлийн эрх");
        textAlertMessage.setText("Tанд эрх үүссэн байна. Одоо ердөө\n" + "хэдхэн алхам хийгээд л зээлээ аваарай.");
    }

    private void alertWaitingLoanApproval() {
        buttonContinue.setVisibility(View.VISIBLE);
        imageAlert.setImageResource(R.drawable.ic_error_black_32dp);
        textAlertTitle.setText("Зээл олголт хийгдэж байна");
        textAlertMessage.setText("Та түр хүлээнэ үү");
        viewAlert.getBackground().setColorFilter(Color.parseColor("#f1be3c"), PorterDuff.Mode.SRC_ATOP);
        viewAlert.setVisibility(View.VISIBLE);
    }


    private void showGuide() {

        builder = new GuideView.Builder(getActivity())
                .setContentText("Одоогийн авах боломжтой зээлийн\n эрхийг харах хэсэг.")
                .setGravity(Gravity.center)
                .setDismissType(DismissType.anywhere)
                .setTargetView(circleView)
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        switch (view.getId()) {
                            case R.id.circle:
                                builder.setTargetView(buttonContinue)
                                        .setContentText("Дахин зээл авах бол энд дарахад л хангалттай.")
                                        .build();
                                mGuideView = builder.build();
                                mGuideView.show();
                                break;
                            case R.id.buttonContinue:

                                builder.setTargetView(linearMonthly)
                                        .setContentText("Таны энэ сард төлөх нийт зээлийн дүн.")//эргэн төлөлтийн
                                        .build();
                                mGuideView = builder.build();
                                mGuideView.show();
                                break;
                            case R.id.linear_monthly:

                                builder.setTargetView(linearTotal)
                                        .setContentText("Таны одоогоор авсан нийт\n"
                                                + "зээлийн хэмжээ.")
                                        .build();
                                mGuideView = builder.build();
                                mGuideView.show();
                                break;
                            case R.id.linear_total:

                                break;
                        }

                    }
                });

        mGuideView = builder.build();
        mGuideView.show();
    }

    private class SwipeRefreshListener implements SwipeRefreshLayout.OnRefreshListener {
        private SwipeRefreshListener() {
        }

        public void onRefresh() {
            Log.e(TAG, "onRefresh: refreshed");
            EventHelper.publish(getContext(), new LoanLastOfferView());
        }
    }

    private void screenEnabled(boolean enable) {
        if (enable)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        else
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        for (int i = 0; i < 3; i++)
            MainActivity.bottomNavigation.getMenu().getItem(i).setEnabled(enable); //navigation
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            showProgress();
            EventHelper.publish(getContext(), new LoanLastOfferView());
        }
    }
}