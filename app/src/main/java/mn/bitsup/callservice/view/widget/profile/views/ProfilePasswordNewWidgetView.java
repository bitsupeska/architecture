package mn.bitsup.callservice.view.widget.profile.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;
import java.util.Objects;

import mn.bitsup.callservice.ClientsManager;
import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusListener;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.util.DeviceAddressUtil;
import mn.bitsup.callservice.util.KeyboardUtils;
import mn.bitsup.callservice.util.ValidationInputField;
import mn.bitsup.callservice.view.custom.ButtonState;
import mn.bitsup.callservice.view.custom.CustomButton;
import mn.bitsup.callservice.view.custom.inputfield.InputFieldPassword;
import mn.bitsup.callservice.view.widget.profile.core.ProfileWidget;
import mn.bitsup.callservice.view.widget.profile.core.contract.ProfileContract;

public class ProfilePasswordNewWidgetView extends Fragment implements ProfileView<ProfileContract>, TextWatcher {

    private Context context;
    private ProfileContract contract;
    private InputFieldPassword inputOldPassword, inputFirstPassword, inputSecondPassword;
    private CustomButton buttonContinue;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new ProfileWidget(context);
        Activity activity = (Activity) context;
        try {
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        } catch (NullPointerException e) {
            Log.e("activity.getWindow()", "exception: " + e);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.profile_password_new, container, false);
        inflatedView(inflatedView);
        initViews();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        buttonContinue = inflatedView.findViewById(R.id.buttonContinue);
        inputOldPassword = inflatedView.findViewById(R.id.inputOldPassword);
        inputFirstPassword = inflatedView.findViewById(R.id.inputFirstPassword);
        inputSecondPassword = inflatedView.findViewById(R.id.inputSecondPassword);
    }

    private void initViews() {
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");

        inputOldPassword.setIconViewVisible(false);
        inputOldPassword.addTextChangedListener(this);
        inputOldPassword.setSingleLine();
        inputOldPassword.setMaxLength(30);
        inputOldPassword.setPasswordType();

        inputFirstPassword.setIconViewVisible(false);
        inputFirstPassword.addTextChangedListener(this);
        inputFirstPassword.setSingleLine();
        inputFirstPassword.setPasswordType();
        inputFirstPassword.setMaxLength(30);

        inputSecondPassword.setIconViewVisible(false);
        inputSecondPassword.addTextChangedListener(this);
        inputSecondPassword.setSingleLine();
        inputSecondPassword.setPasswordType();
        inputSecondPassword.setMaxLength(30);

        buttonContinue.setLoadingText("");
        buttonContinue.setState(ButtonState.DISABLED, getActivity());
        buttonContinue.setOnClickListener(v -> setPassword());
    }

    private void setPassword() {
        buttonContinue.setState(ButtonState.LOADING, getActivity());
        HashMap<String, String> param = new HashMap<>();
        param.put("ipAddress", DeviceAddressUtil.getIPAddress(true));
        param.put("macAddress", DeviceAddressUtil.getMACAddress(context, "wlan0"));
        param.put("newPassword", inputFirstPassword.getValue());
        param.put("oldPassword", inputOldPassword.getValue());
        param.put("uid", ClientsManager.getAuthClient().getUid());

        contract.setPassword(new StatusListener() {
            @Override
            public void onSuccess(StatusResponse statusResponse) {
                if (statusResponse.getCode().equals("200")) {
                    KeyboardUtils.hideKeyboard(getContext(), inputSecondPassword);
                    updateDataFinish("success", context.getResources().getString(R.string.setup_completed_title), context.getResources().getString(R.string.profile_information_password_edit_success));
                } else {
                    updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), statusResponse.getMessage());
                }
            }

            @Override
            public void onError(Response var2) {
                updateDataFinish("error", context.getResources().getString(R.string.shared_error_failed_title), context.getResources().getString(R.string.shared_alert_generic_error_server));
            }

        }, param);

    }

    private void isValidPassword(InputFieldPassword inputField) {
        String password = inputField.getValue();
        if (ValidationInputField.hasDigits(password) && ValidationInputField.hasLowerCase(password) && ValidationInputField.hasUpperCase(password) && ValidationInputField.hasSpecial(password) && ValidationInputField.isCorrectLength(password))
            inputField.setError(null);
        else inputField.setError(context.getResources().getString(R.string.onboard_valid_password));
        if (password.toLowerCase().contains(ClientsManager.getAuthClient().getUid().toLowerCase()))
            inputField.setError(context.getResources().getString(R.string.onboard_contains_username));
    }

    private void comparePasswords() {
        if (inputOldPassword.getValue().equals(inputFirstPassword.getValue()))
            inputFirstPassword.setError(context.getResources().getString(R.string.profile_password_new_password_old_password_error));
        if (inputOldPassword.getValue().equals(inputSecondPassword.getValue()))
            inputSecondPassword.setError(context.getResources().getString(R.string.profile_password_new_password_old_password_error));
        if (inputFirstPassword.getValue().length() > 0 && inputSecondPassword.getValue().length() > 0) {
            if (!inputFirstPassword.getValue().equals(inputSecondPassword.getValue())) {
                if (inputFirstPassword.hasFocus())
                    inputFirstPassword.setError(context.getResources().getString(R.string.onboard_valid_confirm));
                if (inputSecondPassword.hasFocus())
                    inputSecondPassword.setError(context.getResources().getString(R.string.onboard_valid_confirm));
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (inputOldPassword.getValue().length() > 0) isValidPassword(inputOldPassword);
        if (inputFirstPassword.getValue().length() > 0) isValidPassword(inputFirstPassword);
        if (inputSecondPassword.getValue().length() > 0) isValidPassword(inputSecondPassword);
        if (inputOldPassword.getValue().length() > 0) comparePasswords();

        if (inputOldPassword.getError().equals("") && inputFirstPassword.getError().equals("") && inputSecondPassword.getError().equals(""))
            buttonContinue.setState(ButtonState.ENABLED, getActivity());
        else buttonContinue.setState(ButtonState.DISABLED, getActivity());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void updateDataFinish(String result, String resultTitle, String resultMessage) {
        buttonContinue.setState(ButtonState.ENABLED, getActivity());
        Log.e("updateDataFinish", "Profile");
        Activity activity = ((Activity) Objects.requireNonNull(getContext()));
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        returnIntent.putExtra("resultTitle", resultTitle);
        returnIntent.putExtra("resultMessage", resultMessage);
        activity.setResult(Activity.RESULT_FIRST_USER, returnIntent);
        activity.finish();
        ((Activity) context).finish();
    }

}
