package mn.bitsup.callservice.view.custom.datepicker;

/**
 * Created by Erdenedalai R&D B.V on 20/06/2018.
 * Date picker dialog legend item representation.
 */

public class DatePickerLegendItem {
    private String text;
    private int color;

    public DatePickerLegendItem(String text, int color) {
        this.text = text;
        this.color = color;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
