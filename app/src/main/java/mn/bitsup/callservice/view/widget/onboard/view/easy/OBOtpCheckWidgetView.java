package mn.bitsup.callservice.view.widget.onboard.view.easy;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.HashMap;

import mn.bitsup.callservice.R;
import mn.bitsup.callservice.client.dataprovider.data.StatusResponse;
import mn.bitsup.callservice.client.dataprovider.response.Response;
import mn.bitsup.callservice.client.offline.dto.BlockIncrement;
import mn.bitsup.callservice.client.offline.listener.BlockIncrementListener;
import mn.bitsup.callservice.client.otp.OTPListener;
import mn.bitsup.callservice.util.EventHelper;
import mn.bitsup.callservice.view.custom.CustomAlertDialog;
import mn.bitsup.callservice.view.custom.OTPView;
import mn.bitsup.callservice.view.custom.OTPViewLisener;
import mn.bitsup.callservice.view.widget.onboard.activity.OnboardActivity;
import mn.bitsup.callservice.view.widget.onboard.core.OnboardWidget;
import mn.bitsup.callservice.view.widget.onboard.core.contract.OnboardContract;
import mn.bitsup.callservice.view.widget.onboard.view.OnboardView;

public class OBOtpCheckWidgetView extends Fragment implements
        OnboardView<OnboardContract> {

    private Context context;
    private String phoneNumber = "";
    private TextView textTitle;
    private OTPView otpView;
    private ProgressDialog progressDialog;
    private OnboardContract contract;
    private int checkCount = 3;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.contract = new OnboardWidget(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.onboard_otp, container, false);
        inflatedView(inflatedView);
        initView();
        return inflatedView;
    }

    private void inflatedView(View inflatedView) {
        textTitle = inflatedView.findViewById(R.id.textTitle);
        otpView = inflatedView.findViewById(R.id.otpView);
        progressDialog = new ProgressDialog(getContext(), R.style.MyAlertDialogStyle);
        progressDialog.setMessage("Уншиж байна");
        progressDialog.setCancelable(false);
    }

    private void initView() {
        phoneNumber = OBPhoneWidgetView.phoneNumber;
        textTitle.setText(phoneNumber + " " + context.getResources().getString(R.string.otp_title));
        otpView.setOTPPhoneNumber(phoneNumber);
        otpView.setLinkOnClickListener(v -> sendRequest());
        OTPFilledListener otpListener = new OTPFilledListener();
        otpView.setFilledListener(otpListener);
    }

    private void sendRequest() {
        checkCount = 3;
        otpView.stopTimer();
        otpView.hideErrorLabel();

        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", phoneNumber);
        progressDialog.show();

        contract.requestOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();

                if (otp.getStatus().equals("SUCCESS")) {
                    otpView.startTimer();
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
            }
        }, additions);
//        else{
//            otpView.hideMessage();
//            new AnimationDialog(context, "", context.getResources().getString(R.string.otp_error_request), "Буцах", "anim_rejection_clock.json").show();
//        }
    }

    private void checkOTP(String code) {
        Log.e("checkOTP", "count: " + checkCount);
        progressDialog.show();
        HashMap<String, String> additions = new HashMap<>();
        additions.put("handPhone", phoneNumber);
        additions.put("otp", code);

        contract.checkOTP(new OTPListener() {
            @Override
            public void onSuccess(StatusResponse otp) {
                progressDialog.dismiss();
                checkCount--;
                if (otp.getStatus().equals("SUCCESS")) {
                    publish();
                } else {
                    if (checkCount == 0) {
                        otpView.stopTimer();
                        otpView.setLabel();
                        otpView.showErrorLabel("3 удаа буруу оруулсан тул та дахин код илгээнэ үү");
                        checkBlockIncrement();
                    } else {

                        otpView.showErrorLabel("Нууц код буруу байна. Танд " + checkCount + " удаагийн оролдлого үлдлээ");
                    }
                }
            }

            @Override
            public void onError(Response errorResponse) {
                progressDialog.dismiss();
                otpView.emptyLabel();
            }
        }, additions);
    }

    private void checkBlockIncrement() {
        contract.checkBlockIncrement(new BlockIncrementListener() {
            @Override
            public void onSuccess(BlockIncrement blockIncrement) {
                Log.e("block", "block hillee");
            }

            @Override
            public void onError(Response response) {
                CustomAlertDialog.showServerAlertDialog(context);
            }
        });
    }

    private void publish() {
        otpView.hideKeyboard();
        otpView.stopTimer();
        OnboardActivity.addProgress();
        EventHelper.publishBack(context, new OBDurationWidgetView());
    }

    public class OTPFilledListener implements OTPViewLisener {
        @Override
        public void onSuccess(String code) {
            if (checkCount != 0) {
                checkOTP(code);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
