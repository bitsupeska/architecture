package mn.bitsup.callservice;

import android.app.Activity;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import mn.bitsup.callservice.client.RemoteAuthClient;
import mn.bitsup.callservice.client.profile.dto.User;

import org.json.JSONException;
import org.json.JSONObject;

public class MainApplication extends MultiDexApplication {

    public static Context context;
    public static Activity activity;
    public static RemoteAuthClient authClient;
    static final String DEFAULT_CONFIGURATION_PATH = "conf/configuration-remote.json";
    static final String serverURL = "serverURL";
    static final String prodURL = "prodURL";
    static final String testURL = "testURL";
    static final String devURL = "devURL";

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        ClientsManager.registerClients(this);
        authClient = ClientsManager.getAuthClient();
    }

    public static String getServerURL() {
        String baseUrl = "";
        try {
            JSONObject obj = new JSONObject(Objects.requireNonNull(loadJSONFromAsset()));
            JSONObject configuration = obj.getJSONObject("configuration");
            baseUrl = configuration.getString(devURL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  baseUrl;
    }

    public static String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = context.getAssets().open(DEFAULT_CONFIGURATION_PATH);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static User getUser() {
        return authClient.getUser();
    }

    public static void setUser(User user) {
        authClient.setUser(user);
    }

}
